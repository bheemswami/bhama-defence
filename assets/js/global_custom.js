
globalFunc.ajaxCall = function(path, post_data, call_type, b_send=null, success=null, error=null, complete=null) {
    var headers={};
    $.ajax({
        url: base_url + "" + path,
        data: post_data,
        type: call_type,
        dataType: "json",
        headers: headers,
        beforeSend: b_send,
        success: success,
        error: error,
        complete: complete
    })
};
globalFunc.before = function() {$('#loader').show()};
globalFunc.error = function(data) {$('#loader').hide();};
globalFunc.complete = function(data) {$('#loader').hide();};


globalFunc.addRecordResponse = function(data) {
    if(data.status){
        reloadList();
         alert('Record successfully added');
    } else {
        alert(data.message);
    }
}
globalFunc.updateRecordResponse = function(data) {
    if(data.status){
        reloadList();
        alert('Record successfully updated');
    } else {
        alert(data.message);
    }
}
function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
function validateMobile(mobile){
    if(mobile.length<10){
        return false
    }
    return true;
}
  