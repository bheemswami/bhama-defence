<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['HOSTAL_ID'] = 1;
$config['PAYMENT_METHODS']=['Cash'=>'Cash','Cheque'=>'Cheque','NetBanking'=>'NetBanking'];
$config['PAYMENT_METHODS_IDS']=[1 => 'Cash', 2 => 'Cheque', 3 => 'NetBanking'];
$config['PAYMENT_METHODS_HINDI']=['Cash'=>'कैश','Cheque'=>'चेक','NetBanking'=>'नेट बैंकिंग'];

$config['INSTALLMENT_LIST']=['1'=>'Installment 1','2'=>'Installment 2','3'=>'Installment 3','4'=>'Installment 4','5'=>'Installment 5'];
