<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends Admin_Controller {

	function __construct () {
		parent::__construct();
		$this->load->model('Admin_m');
        $this->load->model("student_m");
        $this->load->model("classes_m");
        $this->load->model("payment_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('list_lang', $language);
	}

	public function getPlanDetails() {
        $status=201;
		$classes = $this->classes_m ->get_classes($this->input->post("id"));
        if($classes){
           $status=200; 
        }
        echo json_encode(['status'=>$status,'classes'=>$classes,'msg'=>'']);
	}

   
}
