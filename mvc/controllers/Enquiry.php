<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enquiry extends Admin_Controller {

	function __construct () {
		parent::__construct();
		$this->load->model('Admin_m');
        $this->load->model("student_m");
        $this->load->model("payment_m");
		$language = $this->session->userdata('lang');
        $this->lang->load('student', $language);
		$this->lang->load('list_lang', $language);
	}

	public function index() {
        $this->data["students"]=$this->db->select('*')->where_in('addmission_status',[0,1])->order_by('create_date','DESC')->get('student')->result();
        $this->data["subview"] = "new_modules/enquiries/index";
		$this->load->view('_layout_main', $this->data);
	}

	public function add() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/datepicker/datepicker.css',
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css'
            ),
            'js' => array(
                'assets/datepicker/datepicker.js',
                'assets/select2/select2.js'
            )
        );
		if($_POST) {
			$rules = $this->rules();
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE) {
                $this->data['form_validation'] = validation_errors();
            } else {
                $studentID = $this->Admin_m->insert_student();
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                if($_POST['redirect']=='next_form'){
                    redirect(base_url('enquiry/medical_form/'.$studentID));
                }
                redirect(base_url("enquiry/index"));
            }
        }
        $this->data['classes'] = $this->student_m->get_classes();
        $this->data["subview"] = "new_modules/enquiries/add";
        $this->load->view('_layout_main', $this->data);
	}

    public function medical_form() {
        if($_POST) {
            $studentId = $this->input->post('id');
            $invoicID = $this->Admin_m->uniqueInvoiceNO();
            $postDataStudent = [
                'marital_status' => $this->input->post('marital_status'),
                'category' => $this->input->post('category'),
                'short_note' => $this->input->post('short_note'),
                'physical_benchmark'=>json_encode($this->input->post('physical_benchmark')),
                'attention_point'=>$this->attentionPoints(),
                //'addmission_status'=>$this->input->post('form_step'),
            ];
            
            $postDataPayment = [
                'studentID'=>$studentId,
                'paymenttype' => $this->input->post('paymenttype'),
                'paymentbank' => $this->input->post('paymentbank'),
                'chequeno' => $this->input->post('chequeno'),
                'paymentamount' => $this->input->post('paymentamount'),
                'fee_type'=>4,
                'purpose'=>'Medical Form Fees',
                'credit_debit'=>'Debit',
                'payID'=>$this->input->post('payid'),
            ];
            $lastPayId = $this->Admin_m->insertPaymentData($postDataPayment);
            $amt = $_POST['paymentamount'];
            if ($_POST['paymenttype'] == "Cash") {
                $this->db->query("UPDATE `total_balance` set `academic_cash_balance` = `academic_cash_balance` + $amt WHERE `id` = 1");
            } else {
                $this->db->query("UPDATE `total_balance` set `academic_bank_balance` = `academic_bank_balance` + $amt WHERE `id` = 1");
            }
            if($this->input->post('form_step')==0){
                $postDataStudent['addmission_status']=1;
                $postDataStudent['medical_form_payid']=$lastPayId;
            }
            $this->student_m->update_student($postDataStudent, $studentId);
            if($_POST['redirect']=='next_form'){
                redirect(base_url('enquiry/addmission_form/'.$studentId));
            }
            redirect(base_url("enquiry/index"));
        } else {
            $this->data['student'] =  $this->db->select('student.*,p.paymentID,p.paymenttype,p.chequeno,p.paymentbank,p.paymentamount')
                ->where('student.studentID',$this->uri->segment(3))
                ->join('payment p','p.paymentID=student.medical_form_payid','left')
                ->get('student')->result()[0];
            $this->data["subview"] = "new_modules/enquiries/medical_form";
            $this->load->view('_layout_main', $this->data); 
        }
        
    }
    public function addmission_form() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/datepicker/datepicker.css',
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css'
            ),
            'js' => array(
                'assets/datepicker/datepicker.js',
                'assets/select2/select2.js'
            )
        );
        if($_POST){

            $docs=[];
            $installments = $this->input->post('amount_in_installment');
            $planData=$this->Admin_m->getPlans($this->input->post('course_id'));
            $studentId = $this->input->post('id');

            //insert documents
            if($_FILES){
                $files = $this->Admin_m->uploadMultiFiles($_FILES);
                if(count($_POST['doc_type'])>0){
                    foreach ($_POST['doc_type'] as $key => $value) {
                        $docs[]=[
                            'student_id'=>$studentId,
                            'doc_cat_id'=>$value,
                            'doc_other'=>(isset($_POST['other_doc'][$key]) ? $_POST['other_doc'][$key] : ''),
                            'name'=>(isset($files[$key]['name']) ? $files[$key]['name'] : ''),
                            'file_path'=>'uploads/docs/',
                            'type'=>'docs'
                        ];
                    }
                }
                //foreach ($files as $key => $value) {
                    // $docs[]=[
                    //     'student_id'=>$studentId,
                    //     'name'=>$value['name'],
                    //     'file_path'=>'uploads/docs/',
                    //     'type'=>'docs'
                    // ];
                //}
                if(count($docs)>0){
                     $this->db->where('student_id', $studentId)->where('type','docs')->delete('bd_attachments');
                    $this->Admin_m->insertBatch('bd_attachments',$docs);
                }
            }
            
             //if amount pay in installments
            $purpose = 'Admission Form Fees';
            $feeType = 3;
            if(isset($installments)){
                $purpose = 'Installment 1';
                $feeType = 5;
                $postDataStudent['installments'] = 1;
                $postDataStudent['due_date'] = date('Y-m-d',strtotime($this->input->post('due_date')));
            }
            //insert payment details
            $postDataPayment = [
                'studentID'=>$studentId,
                'paymenttype' => $this->input->post('paymenttype'),
                'paymentbank' => $this->input->post('paymentbank'),
                'chequeno' => $this->input->post('chequeno'),
                'paymentamount' => $this->input->post('paymentamount'),
                'fee_type'=>$feeType,
                'purpose'=>$purpose,
                'credit_debit'=>'Debit',
                'payID'=>$this->input->post('payid'),
            ];
            $lastPayId = $this->Admin_m->insertPaymentData($postDataPayment);
            $amt = $_POST['paymentamount'];
            if ($_POST['paymenttype'] == "Cash") {
                $this->db->query("UPDATE `total_balance` set `academic_cash_balance` = `academic_cash_balance` + $amt WHERE `id` = 1");
            } else {
                $this->db->query("UPDATE `total_balance` set `academic_bank_balance` = `academic_bank_balance` + $amt WHERE `id` = 1");
            }
            $postDataStudent['addmission_form_payid']=$lastPayId;
            //form steps 
            if($this->input->post('form_step')==1){ 
                $postDataStudent['addmission_status']=2;
            }
           
            $postDataStudent['classesID'] = $this->input->post('course_id');
            $postDataStudent['course_fee'] = $planData->course_fee;
            $postDataStudent['modify_date'] = date('Y-m-d H:i:s');

            $this->student_m->update_student($postDataStudent, $studentId);
            $this->Admin_m->parentPhptoSignUpload($this->input->post('parentID'));

            redirect(base_url('student'));
        } else {
            $this->data['classes'] = $this->student_m->get_classes();
            $this->data['student'] =  $this->db->select('student.*,p.paymentID,p.paymenttype,p.chequeno,p.paymentbank,p.paymentamount')
                ->where('student.studentID',$this->uri->segment(3))
                ->join('payment p','p.paymentID=student.addmission_form_payid','left')
                ->get('student')->result()[0];
            $this->data["subview"] = "new_modules/enquiries/addmission_form";
            $this->load->view('_layout_main', $this->data);
        }        
    }

    public function delete() {
        $id = htmlentities(escapeString($this->uri->segment(3)));
        if ((int)$id) {
            $this->data['student'] = $this->student_m->get_single_student(array('studentID' => $id));
            if($this->data['student']) {
                $this->student_m->delete_student($id);
                $this->studentextend_m->delete_studentextend_by_studentID($id);
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            }
        }
        redirect(base_url("enquiry/index"));
    }

    function insertPaymentDataBackup($studentId,$feeType,$purpose,$amountType,$formType=null){
        $invoicID = $this->Admin_m->uniqueInvoiceNO();
        $postDataPayment = [
            'studentID'=>$studentId,
            'courseID'=>$this->input->post('course_id'),
            'paymenttype' => $this->input->post('paymenttype'),
            'paymentbank' => $this->input->post('paymentbank'),
            'chequeno' => $this->input->post('chequeno'),
            'paymentamount' => $this->input->post('paymentamount'),            
            "invoiceID" => $invoicID,
            'schoolyearID' => $this->data['siteinfos']->school_year,
            "paymentdate" => date('Y-m-d'),
            "paymentday" => date('d'),
            "paymentmonth" => date('m'),
            "paymentyear" => date('Y'),
            'userID' => $this->session->userdata('loginuserID'),
            'usertypeID' => $this->session->userdata('usertypeID'),
            'uname' => $this->session->userdata('name'),
            'transactionID' => $this->input->post('paymenttype').$invoicID,
            'fee_type'=>$feeType,
            'purpose'=>$purpose,
            'credit_debit'=>$amountType,
        ];
        $paymentIdExist = $this->db->where('paymentId',$this->input->post('payid'))->count_all_results('payment');
        if($this->input->post('payid')>0 && $paymentIdExist>0){
            $this->db->where('paymentId',$this->input->post('payid'))->update('payment',$postDataPayment);
            return $this->input->post('payid');
        } else {
            return $this->Admin_m->insertData('payment', $postDataPayment);
        }
    }

    function add_comment(){
        if($_FILES){
            $file = $this->Admin_m->uploadSingleFile($_FILES,'uploads/comment_docs/');
            $filename = $file['file_name'];
        } else {
            $filename ='';
        }
        $postData = [
            'student_id'=>$this->input->post('student_id'),
            'course_id'=>$this->input->post('course_id'),
            'comment' => $this->input->post('comment'),
            'comment_by' => $this->session->userdata('usertypeID'),
            'user_id' => $this->session->userdata('loginuserID'),
            'files' => $filename,
        ];
        
        if($this->input->post('commentId')>0){
            $this->db->where('commentId',$this->input->post('commentId'))->update('bd_comments',$postData);
        } else {
            $this->Admin_m->insertData('bd_comments', $postData);
        }
        redirect(base_url("student/view/".$this->input->post('student_id').'/'.$this->input->post('course_id')));
    }

    function attentionPoints(){
        $data=[
            'hydrocele' => (isset($_POST['attention_point']['hydrocele'])) ? 1 : 0,
            'varicocele' => (isset($_POST['attention_point']['varicocele'])) ? 1 : 0,
            'varicose_vain' => (isset($_POST['attention_point']['varicose_vain'])) ? 1 : 0,
            'piles' => (isset($_POST['attention_point']['piles'])) ? 1 : 0,
            'sweat' => (isset($_POST['attention_point']['sweat'])) ? 1 : 0,
        ];
        return json_encode($data);
    }
     

    protected function rules() {
        $rules = array(
            array(
                'field' => 'name',
                'label' => $this->lang->line("enq_student_name"),
                'rules' => 'trim|required|xss_clean|max_length[255]'
            ),
            array(
                'field' => 'dob',
                'label' => $this->lang->line("student_dob"),
                'rules' => 'trim|required|xss_clean'
            ),
            array(
                'field' => 'classesID',
                'label' => 'Course Name',
                'rules' => 'trim|required|xss_clean'
            ),
            array(
                'field' => 'father_name',
                'label' => $this->lang->line("enq_father_name"),
                'rules' => 'trim|required|xss_clean'
            ),
            array(
                'field' => 'mother_name',
                'label' => $this->lang->line("enq_mother_name"),
                'rules' => 'trim|required|xss_clean'
            ),
            array(
                'field' => 'address',
                'label' => $this->lang->line("enq_student_address"),
                'rules' => 'trim|required|xss_clean'
            ),
            array(
                'field' => 'parent_mob',
                'label' => $this->lang->line("enq_parent_number"),
                'rules' => 'trim|required|xss_clean'
            )
        );
        return $rules;
    }
}

/* End of file student.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/student.php */
