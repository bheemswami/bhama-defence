<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expense extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("Admin_m");
		$this->load->model("expense_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('expense', $language);
	}

	public function index() {
		$this->data['headerassets'] = ['js' => ['assets/excelexportjs/xlsx.full.min.js', 'assets/excelexportjs/FileSaver.min.js']];
		//dd(['usertypeID' => $this->session->userdata('usertypeID'),'userID' => $this->session->userdata('loginuserID')]);
		// $this->data['expenses'] = $this->expense_m->get_expense();
		$this->data['expenses'] = $this->db->select('expense.*, v.name as vendor_name')->from('expense')->join('vendor v', 'v.vendorID = expense.vendor_Id', 'left')->get()->result();
		$this->data["subview"] = "expense/index";
		$this->load->view('_layout_main', $this->data);
	}

	protected function rules() {
		$rules = array(
				array(
					'field' => 'expense',
					'label' => $this->lang->line("expense_expense"),
					'rules' => 'trim|required|xss_clean|max_length[128]'
				),
				array(
					'field' => 'date',
					'label' => $this->lang->line("expense_date"),
					'rules' => 'trim|required|max_length[10]|xss_clean|callback_date_valid'
				),
				array(
					'field' => 'amount',
					'label' => $this->lang->line("expense_amount"),
					'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_valid_number'
				),
				array(
					'field' => 'payment_method',
					'label' => $this->lang->line("payment_method"),
					'rules' => 'required'
				),
				array(
					'field' => 'note',
					'label' => $this->lang->line("expense_note"),
					'rules' => 'trim|max_length[200]|xss_clean|required'
				)
			);
		return $rules;
	}

	protected function editRUles() {
		$rules = array(
				array(
					'field' => 'date',
					'label' => $this->lang->line("expense_date"),
					'rules' => 'trim|required|max_length[10]|xss_clean|callback_date_valid'
				),
				array(
					'field' => 'note',
					'label' => $this->lang->line("expense_note"),
					'rules' => 'trim|max_length[200]|xss_clean|required'
				)
			);
		return $rules;
	}

	public function add() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/datepicker/datepicker.css',
			),
			'js' => array(
				'assets/datepicker/datepicker.js',
			)
		);
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data["subview"] = "expense/add";
				$this->load->view('_layout_main', $this->data);
			} else {
				if($_FILES){
					$file = $this->Admin_m->uploadSingleFile($_FILES,'uploads/exp_inc/');
					$filename = $file['file_name'];
				} else {
					$filename ='';
				}
				$isWithdraw = $this->input->post("is_withdraw") ? 1 : 0;
				$array = array(
					"type" => $this->input->post("type"),
					"exp_inc_type" => $this->input->post("exp_inc_type"),
					"exp_inc_for" => $this->input->post("exp_inc_for"),
					"expense" => $this->input->post("expense"),
					"payment_status" => $this->input->post("payment_status"),
					"is_withdraw" => $isWithdraw,
					"amount" => $this->input->post("amount"),
					"note" => $this->input->post("note"),
					"payment_method" => $this->input->post("payment_method"),
					"cheque_no" => $this->input->post("cheque_no"),
					'vendor_Id' => $this->input->post('vendor_Id'),
					"create_date" => date("Y-m-d"),
					"date" => date("Y-m-d", strtotime($this->input->post("date"))),
					"expenseday" => date("d", strtotime($this->input->post("date"))),
					"expensemonth" => date("m", strtotime($this->input->post("date"))),
					"expenseyear" => date("Y", strtotime($this->input->post("date"))),
					'usertypeID' => $this->session->userdata('usertypeID'),
					'uname' => $this->session->userdata('name'),
					'userID' => $this->session->userdata('loginuserID'),
					'schoolyearID' => $this->data['siteinfos']->school_year,
					'files' => $filename,
				);
				$this->expense_m->insert_expense($array);
				if($this->input->post("is_withdraw")){
					$array['type'] = 'income';
					$array['payment_method'] = 1;
					$this->expense_m->insert_expense($array);
					$amount = $this->input->post("amount");
					if($this->input->post("exp_inc_for")==1){
						$this->db->query("UPDATE `total_balance` SET `hostel_cash_balance`=`hostel_cash_balance`+$amount, `hostel_bank_balance`=`hostel_bank_balance`-$amount WHERE `id`=1");
					} else {
						$this->db->query("UPDATE `total_balance` SET `academic_cash_balance`=`academic_cash_balance`+$amount, `academic_bank_balance`=`academic_bank_balance`-$amount WHERE `id`=1");
					}					
				} else {
					$amount = $this->input->post("amount");
					if ($this->input->post('payment_status') == '1') {

						if ($this->input->post('type') == 'income' || $this->input->post('type') == 'receipt') {
							if($this->input->post("exp_inc_for") == 1){
								if ($this->input->post('payment_method') == "1") {
									$this->db->query("UPDATE `total_balance` set `hostel_cash_balance` = `hostel_cash_balance` + $amount WHERE `id` = 1");
								} else {
									$this->db->query("UPDATE `total_balance` set `hostel_bank_balance` = `hostel_bank_balance` + $amount WHERE `id` = 1");
								}
							} else {
								if ($this->input->post('payment_method') == "1") {
									$this->db->query("UPDATE `total_balance` set `academic_cash_balance` = `academic_cash_balance` + $amount WHERE `id` = 1");
								} else {
									$this->db->query("UPDATE `total_balance` set `academic_bank_balance` = `academic_bank_balance` + $amount WHERE `id` = 1");
								}
							}
						}
						
						if ($this->input->post('type') == 'expense' || $this->input->post('type') == 'payment') {
							if($this->input->post("exp_inc_for") == 1){
								if ($this->input->post('payment_method') == "1") {
									$this->db->query("UPDATE `total_balance` set `hostel_cash_balance` = `hostel_cash_balance` - $amount WHERE `id` = 1");
								} else {
									$this->db->query("UPDATE `total_balance` set `hostel_bank_balance` = `hostel_bank_balance` - $amount WHERE `id` = 1");
								}
							} else {
								if ($this->input->post('payment_method') == "1") {
									$this->db->query("UPDATE `total_balance` set `academic_cash_balance` = `academic_cash_balance` - $amount WHERE `id` = 1");
								} else {
									$this->db->query("UPDATE `total_balance` set `academic_bank_balance` = `academic_bank_balance` - $amount WHERE `id` = 1");
								}
							}
						}
					}
				}
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("expense/index"));
			}
		} else {
			$this->data['vendors'] = $this->db->query('SELECT * FROM vendor ORDER BY name desc')->result();	
			$this->data["subview"] = "expense/add";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/datepicker/datepicker.css',
			),
			'js' => array(
				'assets/datepicker/datepicker.js',
			)
		);

		$id = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$id) {
			$this->data['expense'] = $this->expense_m->get_expense($id);
			// dd($this->data['expense']);
			$this->data['vendors'] = $this->db->query('SELECT * FROM vendor ORDER BY name desc')->result();	
			if($this->data['expense']) {
				if($_POST) {
					$rules = $this->editRules();
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) {
						$this->data["subview"] = "expense/edit";
						$this->load->view('_layout_main', $this->data);
					} else {
						if($_FILES){
							$file = $this->Admin_m->uploadSingleFile($_FILES);
							$filename = $file['file_name'];
						} else {
							$filename ='';
						}

						$isWithdraw = 0;
						$amount = $this->input->post("amount");
						$prevAmount = $this->input->post("prev_amount");

						if($this->input->post("prev_is_withdraw")==1){
							if($this->input->post("prev_exp_inc_for")==1){
								$this->db->query("UPDATE `total_balance` SET `hostel_cash_balance`=`hostel_cash_balance`-$prevAmount, `hostel_bank_balance`=`hostel_bank_balance`+$prevAmount WHERE `id`=1");
							} else {
								$this->db->query("UPDATE `total_balance` SET `academic_cash_balance`=`academic_cash_balance`-$prevAmount, `academic_bank_balance`=`academic_bank_balance`+$prevAmount WHERE `id`=1");
							}	
						}
						if($this->input->post("is_withdraw")){
							$isWithdraw = 1;
							if($this->input->post("exp_inc_for")==1){
								$this->db->query("UPDATE `total_balance` SET `hostel_cash_balance`=`hostel_cash_balance`+$amount, `hostel_bank_balance`=`hostel_bank_balance`-$amount WHERE `id`=1");
							} else {
								$this->db->query("UPDATE `total_balance` SET `academic_cash_balance`=`academic_cash_balance`+$amount, `academic_bank_balance`=`academic_bank_balance`-$amount WHERE `id`=1");
							}					
						}  

						$array = array(
							"exp_inc_type" => $this->input->post("exp_inc_type"),
							"expense" => $this->input->post("expense"),
							"date" => date("Y-m-d", strtotime($this->input->post("date"))),
							"note" => $this->input->post("note"),

							// "payment_status" => $this->input->post("payment_status"),
							// "is_withdraw" => $isWithdraw,
							// "amount" => $this->input->post("amount"),
							// "payment_method" => $this->input->post("payment_method"),
							// "cheque_no" => $this->input->post("cheque_no"),
							// "create_date" => date("Y-m-d"),
							// "expenseday" => date("d", strtotime($this->input->post("date"))),
							// "expensemonth" => date("m", strtotime($this->input->post("date"))),
							// "expenseyear" => date("Y", strtotime($this->input->post("date"))),
							// 'usertypeID' => $this->session->userdata('usertypeID'),
							// 'uname' => $this->session->userdata('name'),
							// 'userID' => $this->session->userdata('loginuserID'),
							// 'vendor_Id' => $this->input->post('vendor_Id'),
						);
						if($filename!=''){
							$array['files']=$filename;
						}
						$this->expense_m->update_expense($array, $id);
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("expense/index"));
					}
				} else {
					$this->data["subview"] = "expense/edit";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
		
	}

	public function delete() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$id) {
			$this->expense_m->delete_expense($id);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("expense/index"));
		} else {
			redirect(base_url("expense/index"));
		}
	}

	public function print_voucher(){
		$id = htmlentities(escapeString($this->uri->segment(3)));
		$type = htmlentities(escapeString($this->uri->segment(4)));
		if((int)$id) {
            $this->data['expense'] = $this->expense_m->get_expense($id);
            if($this->data['expense']) {
                $this->load->view("expense/print_voucher", $this->data);
            } else {
                redirect(base_url("expense/index"));
            }
        } else {
            redirect(base_url("expense/index/"));
        }
    
	}

	public function general_entry(){
		$this->data['headerassets'] = [
			'css' => ['assets/datepicker/datepicker.css'],
			'js'  => ['assets/datepicker/datepicker.js']
		];
		$userTypeId = $this->session->userdata('usertypeID');

		$this->data['vendor'] = $this->input->post('vendor');
		$this->data['from_date'] = $this->input->post('from_date');
		$this->data['to_date'] = $this->input->post('to_date');
		$this->data['payment_status'] = $this->input->post('payment_status');
		$this->data['payment_method'] = $this->input->post('payment_method');
		$this->data['exp_inc_for'] = $this->input->post('exp_inc_for');


		$this->data['vendors_list'] = $this->db->query("SELECT * FROM vendor")->result_array();
		
		//start get other expense data
		$this->db->select('expense.*, v.name as vendor_name')->from('expense')->where('type','other')->join('vendor v', 'v.vendorID = expense.vendor_Id', 'left');
		if($userTypeId==5){
			$this->db->where("exp_inc_for", 0 );
		} else if($userTypeId==9){
			$this->db->where("exp_inc_for", 1 );
		}
		if($_POST){
			if ($this->data['from_date']!='') {
				$this->db->where("date >=", date('Y-m-d', strtotime($this->data['from_date'])) );
			}
			if ($this->data['to_date']!='') {
				$this->db->where("date >=", date('Y-m-d', strtotime($this->data['to_date'])) );
			}
			if ($this->data['payment_status']!='') {
				$this->db->where("payment_status", $this->data['payment_status'] );
			}
			if ($this->data['payment_method']>0) {
				$this->db->where("payment_method", $this->data['payment_method'] );
			}
			if ($this->data['vendor']>0) {
				$this->db->where("vendor_id", $this->data['vendor'] );
			}
			if ($this->data['exp_inc_for']!='') {
				$this->db->where("exp_inc_for", $this->data['exp_inc_for'] );
			}
		} 
		$this->data['expenses'] = $this->db->order_by('date','DESC')->get()->result();
		//end get other expense data

		$this->data["subview"] = "ledger/general_list";
		$this->load->view('_layout_main', $this->data);
    
	}

	function date_valid($date) {
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("-", $date);
	        $dd = $arr[0];
	        $mm = $arr[1];
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    }
	}

	function valid_number() {
		if($this->input->post('amount') && $this->input->post('amount') < 0) {
			$this->form_validation->set_message("valid_number", "%s is invalid number");
			return FALSE;
		}
		return TRUE;
	}
}

/* End of file expense.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/expense.php */
