<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ledger extends Admin_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('expense_m');
		$this->load->model('make_payment_m');
		$this->load->model('payment_m');
		$this->load->model('schoolyear_m');
		$language = $this->session->userdata('lang');
		//$this->lang->load('expense', $language);
		$this->lang->load('accountledgerreport', $language);
	}

	public function academic() {
		$date = [];
		$student = $salary = $payment_method = $vendor = $userType = $payment_status = $payment_type = null;
		$this->data['headerassets'] = [
			'css' => ['assets/datepicker/datepicker.css'],
			'js'  => ['assets/datepicker/datepicker.js']
		];
		$this->data['students'] = $this->db->query("SELECT * FROM student")->result_array();
		$this->data['userTypes'] = $this->db->query("SELECT * FROM usertype WHERE usertypeID not in  (1, 4, 8, 9)")->result_array();
		$this->data['teachers'] = $this->db->query("SELECT * FROM teacher")->result_array();
		$this->data['librarians'] = $this->db->query("SELECT * FROM user WHERE usertypeID = 6")->result_array();
		$this->data['accountants'] = $this->db->query("SELECT * FROM user WHERE usertypeID = 5")->result_array();
		$this->data['receptionists'] = $this->db->query("SELECT * FROM user WHERE usertypeID = 7")->result_array();
		$this->data['vendors'] = $this->db->query("SELECT * FROM vendor")->result_array();
		$this->data['balances']['cash'] = getBalances()[0]['academic_cash_balance'];
		$this->data['balances']['bank'] = getBalances()[0]['academic_bank_balance'];
		$income_expenses = $this->db->query("SELECT (SELECT SUM(amount) FROM expense WHERE type = 'expense' AND exp_inc_for = 0) as expense, (SELECT SUM(amount) FROM expense WHERE type = 'income' AND exp_inc_for = 0) as income")->result_array()[0];
		$expenses = $income_expenses['expense'];
		$income = $income_expenses['income'];
		$salaries = $this->db->query("SELECT SUM((payment_amount + additional_pay + interim_relief + other_allowance + house_rent + advance) - (provident_fund + insurance_fund + other_deduction)) as amount from make_payment WHERE userTypeId in (1, 2, 3, 4, 5, 6, 7, 8)")->result_array()[0]['amount'];
		$fees = $this->db->query("SELECT SUM(paymentamount) as amount FROM payment WHERE fee_type in (1, 3, 4, 5, 7)")->result_array()[0]['amount'];
		$totalExpenses = (int)$expenses + (int)$salaries;
		$totalIncome = (int)$fees + (int)$income;
		// $this->data['availableBalance'] = ((int)$startingBalance + (int)$totalIncome) - (int)$totalExpenses;
		// dd($this->data['availableBalance']);

		if($_POST) {
			if ($_POST['payment_status'] != "") {$payment_status = $this->data['payment_status'] = $_POST['payment_status'];}
			if ($_POST['payment_type'] != "") {$payment_type = $this->data['payment_type'] = $_POST['payment_type'];}
			if ($_POST['payment_method']) {$payment_method = $this->data['payment_method_resend'] = $_POST['payment_method'];}
			if ($_POST['teacher']) {$salary = $this->data['teacherr'] = $_POST['teacher']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['accountant']) {$salary = $this->data['accountantt'] = $_POST['accountant']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['librarian']) {$salary = $this->data['librariann'] = $_POST['librarian']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['receptionist']) {$salary = $this->data['receptionistt'] = $_POST['receptionist']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['student']) {$student = $this->data['studentt'] = $_POST['student']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['vendor']) {$vendor = $this->data['vendorr'] = $_POST['vendor']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['from_date'] && $_POST['to_date']) {
				$date = ['from_date' => $_POST['from_date'], 'to_date' => $_POST['to_date']];
				$this->data['from_date'] = $date['from_date'];
				$this->data['to_date'] = $date['to_date'];
			}
		}
		$this->data['panel_title'] = "Academic Ledger Report";
		$this->data['ledgers'] = $this->getLedgerData('academic', $student, $salary, $payment_method, $date, $vendor, $userType, $payment_status, $payment_type);
		$this->data["subview"] = "ledger/list";
		$this->load->view('_layout_main', $this->data);
	}

	public function hostel() {
		$date = [];
		$student = $salary = $payment_method = $vendor = $userType = $payment_status = $payment_type = null;
		$user = $teacher = $student = $warden = $accountant = $librarian = $receptionist = $from_date = $to_date = $payment_method = '';
		// $this->data['payment_status'] = 1;
		$this->data['headerassets'] = [
			'css' => ['assets/datepicker/datepicker.css'],
			'js'  => ['assets/datepicker/datepicker.js']
		];
		$this->data['students'] = $this->db->query("SELECT * FROM student WHERE hostel = 1")->result_array();
		$this->data['wardens'] = $this->db->query("SELECT * FROM user WHERE usertypeID = 9")->result_array();
		$this->data['userTypes'] = $this->db->query("SELECT * FROM usertype WHERE usertypeID in (3, 9)")->result_array();
		$this->data['vendors'] = $this->db->query("SELECT * FROM vendor")->result_array();
		$this->data['balances']['cash'] = getBalances()[0]['hostel_cash_balance'];
		$this->data['balances']['bank'] = getBalances()[0]['hostel_bank_balance'];
		$income_expenses = $this->db->query("SELECT (SELECT SUM(amount) FROM expense WHERE type = 'expense' AND exp_inc_for = 1) as expense, (SELECT SUM(amount) FROM expense WHERE type = 'income' AND exp_inc_for = 1) as income")->result_array()[0];
		$expenses = $income_expenses['expense'];
		$income = $income_expenses['income'];
		$salaries = $this->db->query("SELECT SUM((payment_amount + additional_pay + interim_relief + other_allowance + house_rent + advance) - (provident_fund + insurance_fund + other_deduction)) as amount from make_payment WHERE userTypeId in (9)")->result_array()[0]['amount'];
		$fees = $this->db->query("SELECT SUM(paymentamount) as amount FROM payment WHERE fee_type in (2, 6, 8)")->result_array()[0]['amount'];
		$totalExpenses = (int)$expenses + (int)$salaries;
		$totalIncome = (int)$fees + (int)$income;
		// $this->data['availableBalance'] = ((int)$startingBalance + (int)$totalIncome) - (int)$totalExpenses;
		if ($_POST) {
			if ($_POST['payment_status'] != "") {$payment_status = $this->data['payment_status'] = $_POST['payment_status'];}
			if ($_POST['payment_type'] != "") {$payment_type = $this->data['payment_type'] = $_POST['payment_type'];}
			if ($_POST['payment_method']) $payment_method = $this->data['payment_method_resend'] = $_POST['payment_method'];
			if ($_POST['warden']) {$salary = $this->data['wardenn'] = $_POST['warden']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['student']) {$student = $this->data['studentt'] = $_POST['student']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['vendor']) {$vendor = $this->data['vendorr'] = $_POST['vendor']; $this->data['user'] = $userType = $_POST['user'];}
			if ($_POST['from_date'] && $_POST['to_date']) {
				$date = ['from_date' => $_POST['from_date'], 'to_date' => $_POST['to_date']];
				$this->data['from_date'] = $date['from_date'];
				$this->data['to_date'] = $date['to_date'];
			}
		}
		$this->data['ledgers'] = $this->getLedgerData('hostel', $student, $salary, $payment_method, $date, $vendor, $userType, $payment_status, $payment_type);
		$this->data["subview"] = "ledger/list";
		$this->load->view('_layout_main', $this->data);
	}
	


	public function getLedgerData($type = "academic", $student = null, $salaryw = null, $payment_method = null, $date = null, $vendor = null, $userTypeId = null , $paymentStatus = null, $paymentType = null) {
		$expensesWhere = $salariesWhere = $feesWhere = [];
		$from = $to = "";
		if ($student) { 
			if ($student != 'all') { $feesWhere = ['`payment`.`studentID`' => $student]; } }
		if ($salaryw) { 
			if ($salaryw != 'all') { 
				$salariesWhere = ['userID' => $salaryw]; 
			} else {
				$salariesWhere = ['usertypeID' => $userTypeId];
			}
		}
		if ($date) {
			$from = date('Y-m-d', strtotime($date['from_date']));
			$to = date('Y-m-d', strtotime($date['to_date']));
		}
		if ($paymentStatus != null) {
			$expensesWhere['payment_status'] = $paymentStatus;
		}
		if ($paymentType != null) {
			$expensesWhere['type'] = $paymentType;
		}
		$pymt_mthd = [0, 1, 2, 3];
		$fee_pymt_mthd = ["Cash", "Cheque", "NetBanking"];
		if ($payment_method) {
			if ($payment_method == 1) {
				$pymt_mthd = [1];
				$fee_pymt_mthd = ["Cash"];
			} 
			if ($payment_method == 2) {
				$pymt_mthd = [2, 3];
				$fee_pymt_mthd = ["Cheque", "NetBanking"];
			}
			// $expensesWhere['payment_method'] = $payment_method;
			// $salariesWhere['payment_method'] = $payment_method;
			// if ($payment_method == 1) $feesWhere['paymenttype'] = "Cash";
			// if ($payment_method == 2) $feesWhere['paymenttype'] = "Cheque";
			// if ($payment_method == 3) $feesWhere['paymenttype'] = "NetBanking";

		}
		if ($vendor)  { 
			if ($vendor != 'all') { 
				$expensesWhere['vendor_Id'] = $vendor; 
			} else { 
				$expensesWhere['vendor_Id !='] = 'null'; 
			} 
		}
		$expensesWhere['exp_inc_for'] = 0;
		$userTypeId = [1, 2, 3, 4, 5, 6, 7, 8];
		$feeType = [1, 3, 4, 5, 7];
		if ($type == 'hostel') {
			$expensesWhere['exp_inc_for'] = 1;
			$feeType = [2, 6, 8];
			$userTypeId = [9];
		}
		$this->db->select('expense.*, v.name as vendor_name')->from('expense')->join('vendor v', 'v.vendorID = expense.vendor_Id', 'left')->where($expensesWhere)->where_in('payment_method', $pymt_mthd)->where("expense.type !=", "other");
		if ($from != "" && $to != "") $this->db->where("expense.date >=", "$from")->where("expense.date <=", "$to");
		$expenses = $this->db->get()->result();
		$this->db->select('*')->where_in('userTypeId', $userTypeId)->where($salariesWhere)->where_in('payment_method', $pymt_mthd);
		if ($from != "" && $to != "") $this->db->where("make_payment.make_payment_date >=", "$from")->where("make_payment.make_payment_date <=", "$to");
		$salaries = $this->db->get('make_payment')->result();
		$this->db->select('payment.*, s.name as student_name')->where($feesWhere)->where_in('fee_type', $feeType)->where_in('paymenttype', $fee_pymt_mthd)->join('student s', 's.studentID = payment.studentID', 'left');
		if ($from != "" && $to != "") $this->db->where("payment.paymentdate >=", "$from")->where("payment.paymentdate <=", "$to");
		$fees = $this->db->get('payment')->result();
		$array = [];
		if ($paymentStatus == null && $paymentType == null) {
			if ($salaryw != null || ($student == null && $vendor == null)) {
				foreach($salaries as $salary) {
					$debit = totalSalaryAmount($salary)['netSal'];
					$particular = "Salary - ".$salary->userName;
					$credit = "";
					$array[] = [
						"date" => $salary->make_payment_date,
						"particular" => $particular,
						"debit" => $debit,
						"credit" => $credit,
						"payment_method" => $this->config->item('PAYMENT_METHODS_IDS')[$salary->payment_method],
						"cheque_no" => $salary->cheque_no,
						"type" => "expense",
						"status" => ""
					];
				}
			}
			if ($student != null || ($salaryw == null && $vendor == null)){
				foreach($fees as $fee) {
					$debit = "";
					$credit = $fee->paymentamount;
					$particular = $fee->purpose;
					$array[] = [
						"date" => $fee->paymentdate,
						"particular" => $particular . ' - ' . $fee->student_name,
						"debit" => $debit,
						"credit" => $credit,
						"payment_method" => $fee->paymenttype,
						"cheque_no" => $fee->chequeno,
						// "type" => "fees",
						"type" => "income",
						// "status" => ""
						"status" => "1"
					];
				}
			}
		}
		if ($salaryw == null && $student == null) {
			foreach($expenses as $expense) {
				$debit = "";
				$credit = "";
				$particular = "";
				if ($expense->type == 'expense' || $expense->type == 'payment'){ 
					$debit = $expense->amount; 
					$particular = $expense->note . ($expense->vendor_Id ? ' - ' : ' ') . $expense->vendor_name;
				}
				else if ($expense->type == 'income' || $expense->type == 'receipt') {
					$credit = $expense->amount;
					$particular = $expense->note . ($expense->vendor_Id ? ' - ' : ' ') . $expense->vendor_name;
				}
				$array[] = [
					"date" => $expense->date,
					"particular" => $particular,
					"debit" => $debit,
					"credit" => $credit,
					"payment_method" => ($expense->type == "expense" && $expense->is_withdraw) ? "Bank" : $this->config->item('PAYMENT_METHODS_IDS')[$expense->payment_method],
					"cheque_no" => $expense->cheque_no,
					"type" => $expense->type,
					"status" => $expense->payment_status
				];
			}
		}
		usort($array, 'date_compare');
		// dd($array);
		return $array;
	}  
	  
}
