<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Make_payment extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("make_payment_m");
        $this->load->model("manage_salary_m");
        $this->load->model("teacher_m");
        $this->load->model("parents_m");
        $this->load->model("user_m");
        $this->load->model("systemadmin_m");
        $this->load->model("salary_template_m");
        $this->load->model("salaryoption_m");
        $this->load->model("hourly_template_m");


        $language = $this->session->userdata('lang');
        $this->lang->load('make_payment', $language);
    }


    protected function rules() {
        $rules = array(
            array(
                'field' => 'month',
                'label' => $this->lang->line("make_payment_month"),
                'rules' => 'trim|required|xss_clean|max_length[7]|callback_month_valid'
            ),
            array(
                'field' => 'payment_amount',
                'label' => $this->lang->line("make_payment_payment_amount"),
                'rules' => 'trim|required|xss_clean|max_length[11]|numeric'
            ),
            array(
                'field' => 'payment_method',
                'label' => $this->lang->line("make_payment_payment_method"),
                'rules' => 'trim|required|numeric|xss_clean|max_length[11]|callback_unique_payment_method'
            ),
            array(
                'field' => 'comments',
                'label' => $this->lang->line("make_payment_comments"),
                'rules' => 'trim|xss_clean|max_length[128]'
            ),
            array(
                'field' => 'account_no',
                'label' => $this->lang->line("account_no"),
                'rules' => 'numeric|max_length[128]'
            )
        );
        return $rules;
    }

    public function index() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css'
            ),
            'js' => array(
                'assets/select2/select2.js'
            )
        );

        $this->data['roles'] = $this->usertype_m->get_usertype();
        $setrole = htmlentities(escapeString($this->uri->segment(3)));
        
        if(!isset($setrole)) {
            $setrole = 0;
            $this->data['setrole'] = $setrole;
        } else {
            $this->data['setrole'] = $setrole;
        }
        //dd($setrole);
        if($setrole == 1) {
            $this->data['users'] = $this->systemadmin_m->get_systemadmin();
            //$this->data['managesalary'] = pluck($this->manage_salary_m->get_order_by_manage_salary(array('usertypeID' => 1)), 'userID');
         } elseif($setrole == 2) {
            $this->data['users'] = $this->teacher_m->get_teacher();
            //$this->data['managesalary'] = pluck($this->manage_salary_m->get_order_by_manage_salary(array('usertypeID' => 2)), 'userID');
        } elseif($setrole == 4) {
            $this->data['users'] = $this->parents_m->get_parents();
           //$this->data['managesalary'] = pluck($this->manage_salary_m->get_order_by_manage_salary(array('usertypeID' => 4)), 'userID');
        } else {
            $this->data['users'] = $this->user_m->get_order_by_user(array('usertypeID' => $setrole));
            //$this->data['managesalary'] = pluck($this->manage_salary_m->get_order_by_manage_salary(array('usertypeID' => $setrole)), 'userID');
        }
        //dd($this->data['users']);
        $this->data["subview"] = "make_payment/index";
        $this->load->view('_layout_main', $this->data);
    }

    public function add() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/datepicker/datepicker.css',
            ),
            'js' => array(
                'assets/datepicker/datepicker.js'
            )
        );

        $error = FALSE;
        $this->data['grosssalary'] = 0;
        $this->data['totaldeduction'] = 0;
        $this->data['netsalary'] = 0;

        if(permissionChecker('make_payment')) {
            $userID = htmlentities(escapeString($this->uri->segment(3)));
            $usertypeID = htmlentities(escapeString($this->uri->segment(4)));

            if((int)$userID && (int) $usertypeID) {
                if($usertypeID == 1) {
                    $user = $this->systemadmin_m->get_single_systemadmin(array('usertypeID' => $usertypeID, 'systemadminID' => $userID));
                    $this->data['usertype'] = $this->usertype_m->get_usertype($user->usertypeID);
                } elseif($usertypeID == 2) {
                    $user = $this->teacher_m->get_single_teacher(array('usertypeID' => $usertypeID, 'teacherID' => $userID));
                } else {
                    $user = $this->user_m->get_single_user(array('usertypeID' => $usertypeID, 'userID' => $userID));
                }
                $this->data['make_payments'] = $this->make_payment_m->get_order_by_make_payment(array('usertypeID' => $usertypeID, 'userID' => $userID));
                if($user) {
                    $this->data['usertype'] = $this->usertype_m->get_usertype($user->usertypeID);
                    $this->data['user'] = $user;
                    if($error == FALSE) {
                        if($_POST) {
                            $rules = $this->rules();
                            $this->form_validation->set_rules($rules);
                            if ($this->form_validation->run() == FALSE) {
                                $this->data['form_validation'] = validation_errors();
                                $this->data["subview"] = "make_payment/add";
                                $this->load->view('_layout_main', $this->data);
                            } else {
                               
                               
                                $array = array(
                                    "month" => $this->input->post("month"),
                                    'payment_amount' => $this->input->post('payment_amount'),
                                    "payment_method" => $this->input->post("payment_method"),
                                    "comments" => $this->input->post("comments"),
                                    'usertypeID' => $usertypeID,
                                    'userID' => $userID,
                                    'userName' => $user->name,
                                    'create_date'       => date("Y-m-d h:i:s"),
                                    'modify_date'       => date("Y-m-d h:i:s"),
                                    'create_userID'     => $this->session->userdata('loginuserID'),
                                    'create_username'   => $this->session->userdata('username'),
                                    'create_usertype'   => $this->session->userdata('usertype'),
                                    "treasury_num"      => $this->input->post('treasury_num'),
                                    "net_salary"        => $this->input->post('net_salary'),
                                    'make_payment_date' => date("Y-m-d",strtotime($this->input->post('make_payment_date'))),
                                    'additional_pay' => ($this->input->post('additional_pay')>0) ? $this->input->post('additional_pay') : 0,
                                    'interim_relief' => ($this->input->post('interim_relief')>0) ? $this->input->post('interim_relief') : 0,
                                    'other_allowance' => ($this->input->post('other_allowance')>0) ? $this->input->post('other_allowance') : 0,
                                    'house_rent' => ($this->input->post('house_rent')>0) ? $this->input->post('house_rent') : 0,
                                    'advance' => ($this->input->post('advance')>0) ? $this->input->post('advance') : 0,
                                    'provident_fund' => ($this->input->post('provident_fund')>0) ? $this->input->post('provident_fund') : 0,
                                    'insurance_fund' => ($this->input->post('insurance_fund')>0) ? $this->input->post('insurance_fund') : 0,
                                    'other_deduction' => ($this->input->post('other_deduction')>0) ? $this->input->post('other_deduction') : 0,
                                    'account_no' => $this->input->post('account_no'),
                                    'policy_num' => $this->input->post('policy_num'),
                                );
                                
                                $amt = $_POST['payment_amount'];
                                if ($array['usertypeID'] == 9) {
                                    if ($array["payment_method"] == 1) {
                                        $this->db->query("UPDATE `total_balance` set `hostel_cash_balance` = `hostel_cash_balance` - $amt WHERE `id` = 1");
                                    } else {
                                        $this->db->query("UPDATE `total_balance` set `hostel_bank_balance` = `hostel_bank_balance` - $amt WHERE `id` = 1");
                                    }
                                } else {
                                    if ($array["payment_method"] == 1) {
                                        $this->db->query("UPDATE `total_balance` set `academic_cash_balance` = `academic_cash_balance` - $amt WHERE `id` = 1");
                                    } else {
                                        $this->db->query("UPDATE `total_balance` set `academic_bank_balance` = `academic_bank_balance` - $amt WHERE `id` = 1");
                                    }
                                }
                                $this->make_payment_m->insert_make_payment($array);
                                $lastID = $this->db->insert_id();
                                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                                redirect(base_url("make_payment/add/$userID/$usertypeID"));
                            }
                        } else {
                            $this->data["subview"] = "make_payment/add";
                            $this->load->view('_layout_main', $this->data);
                        }
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                   
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            redirect(base_url('exceptionpage/index'));
        }
    }

    public function view() {
        if(permissionChecker('make_payment')) {
            $id = htmlentities(escapeString($this->uri->segment(3)));
            if((int)$id) {
                $this->data['paymentMethod'] = array(
                    '1' => $this->lang->line('make_payment_payment_cash'),
                    '2' => $this->lang->line('make_payment_payment_cheque'),
                );

                $this->data['make_payment'] = $this->make_payment_m->get_single_make_payment(array('make_paymentID' => $id));
                if($this->data['make_payment']) {
                    $userID = $this->data['make_payment']->userID;
                    $usertypeID = $this->data['make_payment']->usertypeID;

                    if((int)$userID && (int) $usertypeID) {
            
                        $this->data['usertypeID'] = $usertypeID;
                        $this->data['userID'] = $userID;

                        if($usertypeID == 1) {
                            $user = $this->systemadmin_m->get_single_systemadmin(array('usertypeID' => $usertypeID, 'systemadminID' => $userID));
                        } elseif($usertypeID == 2) {
                            $user = $this->teacher_m->get_single_teacher(array('usertypeID' => $usertypeID, 'teacherID' => $userID));
                        } else {
                            $user = $this->user_m->get_single_user(array('usertypeID' => $usertypeID, 'userID' => $userID));
                        }

                        if($user) {
                            $this->data['usertype'] = $this->usertype_m->get_usertype($user->usertypeID);
                            $this->data['user'] = $user;
                            $this->data["subview"] = "make_payment/view";
                            $this->load->view('_layout_main', $this->data);
                        } else {
                            $this->data["subview"] = "error";
                            $this->load->view('_layout_main', $this->data);
                        }
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            redirect(base_url('exceptionpage/index'));
        }
    }

    public function delete() {
        if(permissionChecker('make_payment')) {
            $id = htmlentities(escapeString($this->uri->segment(3)));
            if((int)$id) {
                $this->data['make_payment'] = $this->make_payment_m->get_single_make_payment(array('make_paymentID' => $id));
                if($this->data['make_payment']) {
                    $this->make_payment_m->delete_make_payment($id);
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    redirect(base_url("make_payment/add/".$this->data['make_payment']->userID.'/'.$this->data['make_payment']->usertypeID));
                } else {
                    redirect(base_url("make_payment/index"));
                }
            } else {
                redirect(base_url("make_payment/index"));
            }
        } else {
            redirect(base_url('exceptionpage/index'));
        }
    }

    public function role_list() {
        $role = $this->input->post('id');
        if((int)$role) {
            $string = base_url("make_payment/index/$role");
            echo $string;
        } else {
            echo base_url("make_payment/index");
        }
    }

    public function unique_payment_method() {
        if($this->input->post('payment_method') == 0) {
            $this->form_validation->set_message('unique_payment_method', 'The %s field is required.');
            return FALSE;
        }
        return TRUE;
    }

    public function month_valid($date) {
        if($date) {
            if(strlen($date) <7) {
                $this->form_validation->set_message("month_valid", "%s is not valid mm-yyyy");
                return FALSE;
            } else {
                $arr = explode("-", $date);
                $mm = $arr[0];
                $yyyy = $arr[1];
                if(checkdate($mm, 11, $yyyy)) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message("month_valid", "%s is not valid mm-yyyy");
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    public function print_pay_distribution() {
        $id = htmlentities(escapeString($this->uri->segment(3)));
        $this->data['paymentMethod'] = array(
            '1' => $this->lang->line('make_payment_payment_cash'),
            '2' => $this->lang->line('make_payment_payment_cheque'),
        );
        if((int)$id) {
            $this->data['make_payment'] = $this->make_payment_m->get_single_make_payment(array('make_paymentID' => $id));
            $this->data['designation'] = $this->db->select()->where('usertypeID',$this->data['make_payment']->usertypeID)->get('usertype')->row();
            $this->data['salary'] = totalSalaryAmount($this->data['make_payment']);
            if($this->data['make_payment']) {
                $this->load->view("make_payment/pay_distribution", $this->data);
            } else {
                redirect(base_url("make_payment/index"));
            }
        } else {
            redirect(base_url("make_payment/index/"));
        }
    }
}
