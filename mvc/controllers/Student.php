<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student extends Admin_Controller {

	function __construct () {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parents_m");
		$this->load->model("section_m");
		$this->load->model("classes_m");
		$this->load->model("setting_m");
		$this->load->model("idmanager_m");
		$this->load->model('studentrelation_m');
		$this->load->model('studentgroup_m');
		$this->load->model('studentextend_m');
		$this->load->model('subject_m');
		$this->load->model('Admin_m');
		$this->load->model('hmember_m');
		$this->load->model('hostel_m');
		$this->load->model('category_m');

		$language = $this->session->userdata('lang');
		$this->lang->load('student', $language);
		$this->lang->load('list_lang', $language);
	}

	
	public function index() {
		$this->data['headerassets'] = array(
            'css' => array(
            	'assets/select2/css/select2.css',
				'assets/select2/css/select2-bootstrap.css',
                'assets/datepicker/datepicker.css',
            ),
            'js' => array(
            	'assets/select2/select2.js',
                'assets/datepicker/datepicker.js',
            )
        );
		$usertypeID = $this->session->userdata('usertypeID');
		$this->data['studentgroups'] = pluck($this->studentgroup_m->get_studentgroup(), 'group', 'studentgroupID');

		$this->data['optionalSubjects'] = pluck($this->subject_m->get_order_by_subject(array('type' => 0)), 'subject', 'subjectID');
		$this->data['categorys'] = $this->category_m->get_order_by_category(array("hostelID" => 1));

		if($usertypeID == 3) {
			if(permissionChecker('student_view')) {
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				$username = $this->session->userdata("username");
				$singleStudent = $this->student_m->get_single_student(array("username" => $username, 'schoolyearID' => $schoolyearID));
				if($singleStudent) {
					$this->data['students'] = $this->student_m->get_order_by_student(array('classesID' => $singleStudent->classesID, 'schoolyearID' => $schoolyearID));
					if($this->data['students']) {
						$sections = $this->section_m->get_order_by_section(array("classesID" => $singleStudent->classesID));
						$this->data['sections'] = $sections;
						foreach ($sections as $key => $section) {
							$this->data['allsection'][$section->sectionID] = $this->student_m->get_order_by_student(array('classesID' => $singleStudent->classesID, "sectionID" => $section->sectionID, 'schoolyearID' => $schoolyearID));
						}
					} else {
						$this->data['students'] = NULL;
					}
					$this->data["subview"] = "student/index_parents";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				$username = $this->session->userdata("username");
				$this->data['student'] = $this->student_m->get_single_student(array('username' => $username, 'schoolyearID' => $schoolyearID));
				if($this->data['student']) {
					$this->data["class"] = $this->student_m->get_class($this->data['student']->classesID);
					if($this->data["student"] && $this->data["class"]) {
						$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
						$this->data['set'] = $this->data['student']->classesID;
						if ($this->data["student"]->parentID) {
							$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
						}
						$this->data["subview"] = "student/view";
						$this->load->view('_layout_main', $this->data);
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			}
		} elseif($usertypeID == 4) {
			$schoolyearID = $this->session->userdata('defaultschoolyearID');
			$username = $this->session->userdata("username");
			$parents = $this->parents_m->get_single_parents(array('username' => $username));
			if(count($parents)) {
				$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parents->parentsID, 'schoolyearID' => $schoolyearID));
				$this->data["subview"] = "student/index_parents";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$schoolyearID = $this->session->userdata('defaultschoolyearID');
			$id = htmlentities(escapeString($this->uri->segment(3)));
			$this->data['set'] = $id;
			$this->data['classes'] = $this->student_m->get_classes();
			if($id==''){
				$this->data['students'] = $this->student_m->get_order_by_student(array('addmission_status'=>2,'schoolyearID' => $schoolyearID));
			} else {
				$this->data['students'] = $this->student_m->get_order_by_student(array('addmission_status'=>2,'classesID' => $id, 'schoolyearID' => $schoolyearID));
			}
			
			if($this->data['students']) {
				$sections = $this->section_m->get_order_by_section(array("classesID" => $id));
				$this->data['sections'] = $sections;
				foreach ($sections as $key => $section) {
					$this->data['allsection'][$section->sectionID] = $this->student_m->get_order_by_student(array('classesID' => $id, "sectionID" => $section->sectionID, 'schoolyearID' => $schoolyearID));
				}
			} else {
				$this->data['students'] = NULL;
			}
			$this->data["subview"] = "student/index";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function view() {
		 $this->data['headerassets'] = array(
            'css' => array(
                'assets/datepicker/datepicker.css',
            ),
            'js' => array(
                'assets/datepicker/datepicker.js',
            )
		);
		$usertypeID = $this->session->userdata('usertypeID');
		$this->data['studentgroups'] = pluck($this->studentgroup_m->get_studentgroup(), 'group', 'studentgroupID');
		
		$this->data['optionalSubjects'] = pluck($this->subject_m->get_order_by_subject(array('type' => 0)), 'subject', 'subjectID');
		
		$id = htmlentities(escapeString($this->uri->segment(3)));
		$this->data["edu_details"] = $this->student_m->get_edu_details($id);
		$this->data['physical_benchmark'] = json_decode($this->db->query("SELECT `physical_benchmark` FROM `student` WHERE `studentID` = $id")->result()[0]->physical_benchmark);

		// dd($this->data['physical_benchmark']);
		if($usertypeID == 3) {
			if(permissionChecker('student_view')) {
				$id = htmlentities(escapeString($this->uri->segment(3)));
				$url = htmlentities(escapeString($this->uri->segment(4)));
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				if((int)$id && (int)$url) {
					$this->data['set'] = $url;
					$username = $this->session->userdata("username");
					$originalStudent = $this->student_m->get_single_student(array("username" => $username));
					if($originalStudent) {
						$this->data['student'] = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));
						if($this->data['student']) {
							if($originalStudent->classesID == $this->data['student']->classesID) {
								$this->data["class"] = $this->student_m->get_class($url);
								if($this->data["student"] && $this->data["class"]) {
									$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
									$this->data['set'] = $url;
									if ($this->data["student"]->parentID) {
										$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
									}
									$this->data["subview"] = "student/view";
									$this->load->view('_layout_main', $this->data);
								} else {
									$this->data["subview"] = "error";
									$this->load->view('_layout_main', $this->data);
								}
							} else {
								$this->data["subview"] = "error";
								$this->load->view('_layout_main', $this->data);
							}
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				$username = $this->session->userdata("username");
				$this->data['student'] = $this->student_m->get_single_student(array('username' => $username, 'schoolyearID' => $schoolyearID));
				if($this->data['student']) {
					$this->data["class"] = $this->student_m->get_class($this->data['student']->classesID);
					if($this->data["student"] && $this->data["class"]) {
						$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
						$this->data['set'] = $this->data['student']->classesID;
						if ($this->data["student"]->parentID) {
							$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
						}
						$this->data["subview"] = "student/view";
						$this->load->view('_layout_main', $this->data);
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			}
		} elseif($usertypeID == 4) {
			$schoolyearID = $this->session->userdata('defaultschoolyearID');
			$username = $this->session->userdata("username");
			$parents = $this->parents_m->get_single_parents(array('username' => $username));
			if(count($parents)) {
				$id = htmlentities(escapeString($this->uri->segment(3)));
				$url = htmlentities(escapeString($this->uri->segment(4)));
				if((int)$id && (int)$url) {
					$checkstudent = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));
					if(count($checkstudent)) {
						if($checkstudent->parentID == $parents->parentsID) {
							$this->data['set'] = $checkstudent->classesID;
							$this->data['student'] = $checkstudent;
							$this->data["class"] = $this->student_m->get_class($checkstudent->classesID);
							if($this->data["class"]) {
								$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
								$this->data['set'] = $url;
								if ($this->data["student"]->parentID) {
									$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
								}
								$this->data["subview"] = "student/view";
								$this->load->view('_layout_main', $this->data);
							} else {
								$this->data["subview"] = "error";
								$this->load->view('_layout_main', $this->data);
							}
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$schoolyearID = $this->session->userdata('defaultschoolyearID');
			$id = htmlentities(escapeString($this->uri->segment(3)));
			$url = htmlentities(escapeString($this->uri->segment(4)));
			if ((int)$id && (int)$url) {
				$this->data["student"] = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));
				$this->data["class"] = $this->student_m->get_class($url);
				if($this->data["student"] && $this->data["class"]) {
					$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
					$this->data['set'] = $url;
					if ($this->data["student"]->parentID) {
						$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
					}

					//instalment list
					$fee_type  = ($this->data["student"]->installments>0) ? 5 : 3;
					$this->data["payment_inst"] = $this->db->select('*')->where('schoolyearID',$schoolyearID)->where('studentID',$id)->where('fee_type',$fee_type)->get('payment')->result();
					
					//comments list
					$this->data["comments"] = $this->db->select('c.*,sa.name as comment_by_name')->where('student_id',$id)
						->join('systemadmin sa','sa.systemadminID=c.comment_by','left')
						->get('bd_comments c')->result();

					$this->data["subview"] = "student/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		}
	}
	public function add() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/datepicker/datepicker.css',
				'assets/select2/css/select2.css',
				'assets/select2/css/select2-bootstrap.css'
			),
			'js' => array(
				'assets/datepicker/datepicker.js',
				'assets/select2/select2.js'
			)
		);

		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['sections'] = $this->section_m->get_section();
		$this->data['parents'] = $this->parents_m->get_parents();
		$this->data['studentgroups'] = $this->studentgroup_m->get_studentgroup();

		$classesID = $this->input->post("classesID");

		if($classesID != 0) {
			$this->data['sections'] = $this->section_m->get_order_by_section(array("classesID" =>$classesID));
            $this->data['optionalSubjects'] = $this->subject_m->get_order_by_subject(array("classesID" =>$classesID, 'type' => 0));
		} else {
			$this->data['sections'] = "empty";
			$this->data['optionalSubjects'] = 'empty';
		}

		$this->data['sectionID'] = $this->input->post("sectionID");
        $this->data['optionalSubjectID'] = 0;

		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data["subview"] = "student/add";
				$this->load->view('_layout_main', $this->data);
			} else {

				$sectionID = $this->input->post("sectionID");
				if($sectionID == 0) {
					$this->data['sectionID'] = 0;
				} else {
					$this->data['sections'] = $this->section_m->get_allsection($classesID);
					$this->data['sectionID'] = $this->input->post("sectionID");
				}

				if($this->input->post('optionalSubjectID')) {
                    $this->data['optionalSubjectID'] = $this->input->post('optionalSubjectID');
                } else {
                    $this->data['optionalSubjectID'] = 0;
                }

				$array = array();
				$array["name"] = $this->input->post("name");

				$array["sex"] = $this->input->post("sex");
				$array["religion"] = $this->input->post("religion");
				$array["email"] = $this->input->post("email");
				$array["phone"] = $this->input->post("phone");
				$array["address"] = $this->input->post("address");
				$array["classesID"] = $this->input->post("classesID");
				$array["sectionID"] = $this->input->post("sectionID");
				$array["roll"] = $this->input->post("roll");
				$array["bloodgroup"] = $this->input->post("bloodgroup");
				$array["state"] = $this->input->post("state");
				$array["country"] = $this->input->post("country");
				$array["registerNO"] = $this->input->post("registerNO");
				$array["username"] = $this->input->post("username");
				$array['password'] = $this->student_m->hash($this->input->post("password"));
				$array['usertypeID'] = 3;
				$array['parentID'] = $this->input->post('guargianID');
				$array['library'] = 0;
				$array['hostel'] = 0;
				$array['transport'] = 0;
				$array['createschoolyearID'] = $this->data['siteinfos']->school_year;
				$array['schoolyearID'] = $this->data['siteinfos']->school_year;
				$array["create_date"] = date("Y-m-d h:i:s");
				$array["modify_date"] = date("Y-m-d h:i:s");
				$array["create_userID"] = $this->session->userdata('loginuserID');
				$array["create_username"] = $this->session->userdata('username');
				$array["create_usertype"] = $this->session->userdata('usertype');
				$array["active"] = 1;

				if($this->input->post('dob')) {
					$array["dob"] 		= date("Y-m-d", strtotime($this->input->post("dob")));
				}
				$array['photo'] = $this->upload_data['file']['file_name'];
				// For Email
				$this->usercreatemail($this->input->post('email'), $this->input->post('username'), $this->input->post('password'));

				$this->student_m->insert_student($array);
				$studentID = $this->db->insert_id();

				$section = $this->section_m->get_section($this->input->post("sectionID"));
				$classes = $this->classes_m ->get_classes($this->input->post("classesID"));

				if(count($classes)) {
					$setClasses = $classes->classes;
				} else {
					$setClasses = NULL;
				}

				if(count($section)) {
					$setSection = $section->section;
				} else {
					$setSection = NULL;
				}

				$arrayStudentRelation = array(
					'srstudentID' => $studentID,
					'srname' => $this->input->post("name"),
					'srclassesID' => $this->input->post("classesID"),
					'srclasses' => $setClasses,
					'srroll' => $this->input->post("roll"),
					'srregisterNO' => $this->input->post("registerNO"),
					'srsectionID' => $this->input->post("sectionID"),
					'srsection' => $setSection,
					'srstudentgroupID' => $this->input->post('studentGroupID'),
					'sroptionalsubjectID' => $this->input->post('optionalSubjectID'),
					'srschoolyearID' => $this->data['siteinfos']->school_year
				);

                $studentExtendArray = array(
                    'studentID' => $studentID,
                    'studentgroupID' => $this->input->post('studentGroupID'),
                    'optionalsubjectID' => $this->input->post('optionalSubjectID'),
                    'extracurricularactivities' => $this->input->post('extraCurricularActivities'),
                    'remarks' => $this->input->post('remarks')
                );

                $this->studentextend_m->insert_studentextend($studentExtendArray);
				$this->studentrelation_m->insert_studentrelation($arrayStudentRelation);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				 redirect(base_url("student/index"));
			}
		} else {
			$this->data["subview"] = "student/add";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/datepicker/datepicker.css',
				'assets/select2/css/select2.css',
				'assets/select2/css/select2-bootstrap.css'
			),
			'js' => array(
				'assets/datepicker/datepicker.js',
				'assets/select2/select2.js'
			)
		);
		$usertype = $this->session->userdata("usertype");
		$schoolyearID = $this->session->userdata('defaultschoolyearID');
		$studentID = htmlentities(escapeString($this->uri->segment(3)));
		$url = htmlentities(escapeString($this->uri->segment(4)));

		if((int)$studentID && (int)$url) {
			$this->data["edu_details"] = $this->student_m->get_edu_details($studentID);

			$this->data['classes'] = $this->student_m->get_classes();
			$this->data['student'] = $this->student_m->get_single_student(array('studentID' => $studentID, 'schoolyearID' => $schoolyearID));
			$this->data['set'] = $url;
			if($this->data['student']) {
				$this->data['parent'] = $this->parents_m->get_single_parents(['parentsID'=>$this->data['student']->parentID]);
				if($_POST) {
					$rules = $this->rules();
					unset($rules[21]);
					$this->form_validation->set_rules($rules);

					if ($this->form_validation->run() == FALSE) {
						$this->data["subview"] = "student/edit";
						$this->load->view('_layout_main', $this->data);
					} else {
						$array = array();
						$array["name"] = $this->input->post("name");
						$array["sex"] = $this->input->post("sex");
						//$array["religion"] = $this->input->post("religion");
						$array["email"] = $this->input->post("email");
						//$array["phone"] = $this->input->post("phone");
						//$array["address"] = $this->input->post("address");
						$array["classesID"] = $this->input->post("classesID");
						//$array["sectionID"] = $this->input->post("sectionID");
						//$array["roll"] = $this->input->post("roll");
						//$array["bloodgroup"] = $this->input->post("bloodgroup");
						//$array["state"] = $this->input->post("state");
						//$array["country"] = $this->input->post("country");
						$array["registerNO"] = $this->input->post("registerNO");
						//$array["parentID"] = $this->input->post("guargianID");
						//$array["username"] = $this->input->post("username");
						$array["modify_date"] = date("Y-m-d h:i:s");
						//$array['photo'] = $this->upload_data['file']['file_name'];

						if($this->input->post('dob')) {
							$array["dob"] 	= date("Y-m-d", strtotime($this->input->post("dob")));
						} else {
							$array["dob"] = NULL;
						}
						
						if(isset($_FILES['photo']) && $_FILES['photo']['name']!=''){
							unlinkImage($this->data['student']->photo,'uploads/student_photo');
							$file = $this->Admin_m->uploadSingleFile($_FILES,'uploads/student_photo/');
            				$array["photo"] = $file['file_name'];
						}
						if(isset($_FILES['sign']) && $_FILES['sign']['name']!=''){
							unlinkImage($this->data['student']->sign,'uploads/student_sign');
							$file = $this->Admin_m->uploadSingleFile($_FILES,'uploads/student_sign/');
            				$array["sign"] = $file['file_name'];
						}

						$studentReletion = $this->studentrelation_m->get_order_by_studentrelation(array('srstudentID' => $studentID, 'srschoolyearID' => $this->data['siteinfos']->school_year));
						$section = $this->section_m->get_section($this->input->post("sectionID"));
						$classes = $this->classes_m ->get_classes($this->input->post("classesID"));

						if(($classes)) {
							$setClasses = $classes->classes;
						} else {
							$setClasses = NULL;
						}

						if(($section)) {
							$setSection = $section->section;
						} else {
							$setSection = NULL;
						}

						if(!count($studentReletion)) {
							$arrayStudentRelation = array(
								'srstudentID' => $studentID,
								'srname' => $this->input->post("name"),
								'srclassesID' => $this->input->post("classesID"),
								'srclasses' => $setClasses,
								'srroll' => $this->input->post("roll"),
								'srregisterNO' => $this->input->post("registerNO"),
								'srsectionID' => $this->input->post("sectionID"),
								'srsection' => $setSection,
								'srschoolyearID' => $this->data['siteinfos']->school_year
							);
							$this->studentrelation_m->insert_studentrelation($arrayStudentRelation);
						} else {
							$arrayStudentRelation = array(
								'srname' => $this->input->post("name"),
								'srclassesID' => $this->input->post("classesID"),
								'srclasses' => $setClasses,
								'srroll' => $this->input->post("roll"),
								'srregisterNO' => $this->input->post("registerNO"),
								'srsectionID' => $this->input->post("sectionID"),
								'srsection' => $setSection,
							);

							$this->studentrelation_m->update_studentrelation_with_multicondition($arrayStudentRelation, array('srstudentID' => $studentID, 'srschoolyearID' => $this->data['siteinfos']->school_year));
						}

                        $studentExtendArray = array(
                            'studentgroupID' => $this->input->post('studentGroupID'),
                            'optionalsubjectID' => $this->input->post('optionalSubjectID'),
                            'extracurricularactivities' => $this->input->post('extraCurricularActivities'),
                            'remarks' => $this->input->post('remarks')
                        );

                        $this->studentextend_m->update_studentextend_by_studentID($studentExtendArray, $studentID);
						$this->student_m->update_student($array, $studentID);
						if(isset($_POST['edu']['ids'])){
							foreach($_POST['edu']['ids'] as $edu_id){
								$eduArray = [
				        			//'exam_name'=>$this->input->post('edu')['exam_name'][$edu_id],
				        			'year'=>$this->Admin_m->chkValueNull($this->input->post('edu')['year'][$edu_id]),
				        			'subjects'=>$this->input->post('edu')['subject'][$edu_id],
				        			'total_marks'=>$this->Admin_m->chkValueNull($this->input->post('edu')['max_marks'][$edu_id]),
				        			'obtain_marks'=>$this->Admin_m->chkValueNull($this->input->post('edu')['obtain_marks'][$edu_id]),
				        			'division'=>$this->input->post('edu')['division'][$edu_id],
				        			'percentage'=>$this->Admin_m->chkValueNull($this->input->post('edu')['percentage'][$edu_id]),
				        			'borad'=>$this->input->post('edu')['board'][$edu_id],
								];	
								$this->db->where('id',$edu_id)->update('bd_student_edu',$eduArray);		
							}
						}
						$arrayParent=[
							'name'=>$this->input->post('father_name'), 
				            'father_name'=>$this->input->post('father_name'), 
				            'mother_name'=>$this->input->post('mother_name'), 
				            'father_profession'=>$this->input->post('father_business'), 
				            'mother_profession'=>$this->input->post('mother_business'), 
				            'phone'=>$this->input->post('parent_mob'), 
				            'address'=>$this->input->post('address'), 
						];
						$this->parents_m->update_parents($arrayParent, $this->input->post('parent_id'));
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("student/index/$url"));
					}
				} else {
					$this->data["subview"] = "student/edit";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	/*public function editCOPY() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/datepicker/datepicker.css',
				'assets/select2/css/select2.css',
				'assets/select2/css/select2-bootstrap.css'
			),
			'js' => array(
				'assets/datepicker/datepicker.js',
				'assets/select2/select2.js'
			)
		);
		$usertype = $this->session->userdata("usertype");
		$schoolyearID = $this->session->userdata('defaultschoolyearID');
		$studentID = htmlentities(escapeString($this->uri->segment(3)));
		$url = htmlentities(escapeString($this->uri->segment(4)));
		if((int)$studentID && (int)$url) {
			$this->data['classes'] = $this->student_m->get_classes();
			$this->data['student'] = $this->student_m->get_single_student(array('studentID' => $studentID, 'schoolyearID' => $schoolyearID));


			$this->data['parents'] = $this->parents_m->get_parents();
            $this->data['studentgroups'] = $this->studentgroup_m->get_studentgroup();


			if($this->data['student']) {
				$classesID = $this->data['student']->classesID;
				$this->data['sections'] = $this->section_m->get_order_by_section(array('classesID' => $classesID));
                $this->data['optionalSubjects'] = $this->subject_m->get_order_by_subject(array("classesID" =>$classesID, 'type' => 0));

                if($this->input->post('optionalSubjectID')) {
                    $this->data['optionalSubjectID'] = $this->input->post('optionalSubjectID');
                } else {
                    $this->data['optionalSubjectID'] = 0;
                }
			}

			$this->data['set'] = $url;
			if($this->data['student']) {
				if($_POST) {
					$rules = $this->rules();
					unset($rules[21]);
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) {
						$this->data["subview"] = "student/edit";
						$this->load->view('_layout_main', $this->data);
					} else {
						$array = array();
						$array["name"] = $this->input->post("name");
						$array["sex"] = $this->input->post("sex");
						$array["religion"] = $this->input->post("religion");
						$array["email"] = $this->input->post("email");
						$array["phone"] = $this->input->post("phone");
						$array["address"] = $this->input->post("address");
						$array["classesID"] = $this->input->post("classesID");
						$array["sectionID"] = $this->input->post("sectionID");
						$array["roll"] = $this->input->post("roll");
						$array["bloodgroup"] = $this->input->post("bloodgroup");
						$array["state"] = $this->input->post("state");
						$array["country"] = $this->input->post("country");
						$array["registerNO"] = $this->input->post("registerNO");
						$array["parentID"] = $this->input->post("guargianID");
						$array["username"] = $this->input->post("username");
						$array["modify_date"] = date("Y-m-d h:i:s");
						$array['photo'] = $this->upload_data['file']['file_name'];

						if($this->input->post('dob')) {
							$array["dob"] 	= date("Y-m-d", strtotime($this->input->post("dob")));
						} else {
							$array["dob"] = NULL;
						}


						$studentReletion = $this->studentrelation_m->get_order_by_studentrelation(array('srstudentID' => $studentID, 'srschoolyearID' => $this->data['siteinfos']->school_year));
						$section = $this->section_m->get_section($this->input->post("sectionID"));
						$classes = $this->classes_m ->get_classes($this->input->post("classesID"));

						if(count($classes)) {
							$setClasses = $classes->classes;
						} else {
							$setClasses = NULL;
						}

						if(count($section)) {
							$setSection = $section->section;
						} else {
							$setSection = NULL;
						}

						if(!count($studentReletion)) {
							$arrayStudentRelation = array(
								'srstudentID' => $studentID,
								'srname' => $this->input->post("name"),
								'srclassesID' => $this->input->post("classesID"),
								'srclasses' => $setClasses,
								'srroll' => $this->input->post("roll"),
								'srregisterNO' => $this->input->post("registerNO"),
								'srsectionID' => $this->input->post("sectionID"),
								'srsection' => $setSection,
								'srschoolyearID' => $this->data['siteinfos']->school_year
							);
							$this->studentrelation_m->insert_studentrelation($arrayStudentRelation);
						} else {
							$arrayStudentRelation = array(
								'srname' => $this->input->post("name"),
								'srclassesID' => $this->input->post("classesID"),
								'srclasses' => $setClasses,
								'srroll' => $this->input->post("roll"),
								'srregisterNO' => $this->input->post("registerNO"),
								'srsectionID' => $this->input->post("sectionID"),
								'srsection' => $setSection,
							);

							$this->studentrelation_m->update_studentrelation_with_multicondition($arrayStudentRelation, array('srstudentID' => $studentID, 'srschoolyearID' => $this->data['siteinfos']->school_year));
						}

                        $studentExtendArray = array(
                            'studentgroupID' => $this->input->post('studentGroupID'),
                            'optionalsubjectID' => $this->input->post('optionalSubjectID'),
                            'extracurricularactivities' => $this->input->post('extraCurricularActivities'),
                            'remarks' => $this->input->post('remarks')
                        );

                        $this->studentextend_m->update_studentextend_by_studentID($studentExtendArray, $studentID);
						$this->student_m->update_student($array, $studentID);
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("student/index/$url"));
					}
				} else {
					$this->data["subview"] = "student/edit";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}*/

	public function print_preview() {
		$usertypeID = $this->session->userdata('usertypeID');

		$this->data['studentgroups'] = pluck($this->studentgroup_m->get_studentgroup(), 'group', 'studentgroupID');

		$this->data['optionalSubjects'] = pluck($this->subject_m->get_order_by_subject(array('type' => 0)), 'subject', 'subjectID');

		if($usertypeID == 3) {
			if(permissionChecker('student_view')) {
				$id = htmlentities(escapeString($this->uri->segment(3)));
				$url = htmlentities(escapeString($this->uri->segment(4)));
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				if((int)$id && (int)$url) {
					$this->data['set'] = $url;
					$username = $this->session->userdata("username");
					$originalStudent = $this->student_m->get_single_student(array("username" => $username));
					if($originalStudent) {
						$this->data['student'] = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));
						if($this->data['student']) {
							if($originalStudent->classesID == $this->data['student']->classesID) {
								$this->data["class"] = $this->student_m->get_class($url);
								if($this->data["student"] && $this->data["class"]) {
									$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
									$this->data['set'] = $url;
									if ($this->data["student"]->parentID) {
										$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
									} else {
										$this->data["parent"] = array();
									}
									$this->printview($this->data, 'student/print_preview');
								} else {
									$this->data["subview"] = "error";
									$this->load->view('_layout_main', $this->data);
								}
							} else {
								$this->data["subview"] = "error";
								$this->load->view('_layout_main', $this->data);
							}
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				$username = $this->session->userdata("username");
				$this->data['student'] = $this->student_m->get_single_student(array('username' => $username, 'schoolyearID' => $schoolyearID));
				if($this->data['student']) {
					$this->data["class"] = $this->student_m->get_class($this->data['student']->classesID);
					if($this->data["student"] && $this->data["class"]) {
						$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
						$this->data['set'] = $this->data['student']->classesID;
						if ($this->data["student"]->parentID) {
							$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
						}
						$this->printview($this->data, 'student/print_preview');
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			}
		} elseif($usertypeID == 4) {
			if(permissionChecker('student_view')) {
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				$username = $this->session->userdata("username");
				$parents = $this->parents_m->get_single_parents(array('username' => $username));
				if(count($parents)) {
					$id = htmlentities(escapeString($this->uri->segment(3)));
					$url = htmlentities(escapeString($this->uri->segment(4)));
					if((int)$id && (int)$url) {
						$checkstudent = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));
						if(count($checkstudent)) {
							if($checkstudent->parentID == $parents->parentsID) {
								$this->data['set'] = $checkstudent->classesID;
								$this->data['student'] = $checkstudent;
								$this->data["class"] = $this->student_m->get_class($checkstudent->classesID);
								if($this->data["class"]) {
									$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
									$this->data['set'] = $url;
									if ($this->data["student"]->parentID) {
										$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
									} else {
										$this->data["parent"] = array();
									}
									$this->printview($this->data, 'student/print_preview');
								} else {
									$this->data["subview"] = "error";
									$this->load->view('_layout_main', $this->data);
								}
							} else {
								$this->data["subview"] = "error";
								$this->load->view('_layout_main', $this->data);
							}
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "errorpermission";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			if(permissionChecker('student_view')) {
				$schoolyearID = $this->session->userdata('defaultschoolyearID');
				$usertype = $this->session->userdata("usertype");
				$id = htmlentities(escapeString($this->uri->segment(3)));
				$url = htmlentities(escapeString($this->uri->segment(4)));
				if ((int)$id && (int)$url) {
					$this->data["student"] = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));

					$this->data["class"] = $this->student_m->get_class($url);
					if($this->data["student"] && $this->data["class"]) {
						$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
						$this->data['set'] = $url;
						if ($this->data["student"]->parentID) {
							$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
						} else {
							$this->data["parent"] = array();
						}
                       $this->data['panel_title'] = $this->lang->line('panel_title');
						$this->printview($this->data, 'student/print_preview');
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "errorpermission";
				$this->load->view('_layout_main', $this->data);
			}
		}
	}

	public function send_mail() {
		$this->data['studentgroups'] = pluck($this->studentgroup_m->get_studentgroup(), 'group', 'studentgroupID');

		$this->data['optionalSubjects'] = pluck($this->subject_m->get_order_by_subject(array('type' => 0)), 'subject', 'subjectID');

		$usertype = $this->session->userdata("usertype");
		$id = $this->input->post('id');
		$url = $this->input->post('set');
		if ((int)$id && (int)$url) {
			$this->data["student"] = $this->student_m->get_student($id);
			$this->data["class"] = $this->student_m->get_class($url);
			if($this->data["student"] && $this->data["class"]) {
				$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);

				$this->data["parent"] = $this->parents_m->get_parents($this->data["student"]->parentID);
				$email = $this->input->post('to');
				$subject = $this->input->post('subject');
				$message = $this->input->post('message');

				$this->viewsendtomail($this->data, 'student/print_preview', $email, $subject, $message);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}

	}

	public function delete() {
		$schoolyearID = $this->session->userdata('defaultschoolyearID');
		$usertype = $this->session->userdata("usertype");
		$id = htmlentities(escapeString($this->uri->segment(3)));
		$url = htmlentities(escapeString($this->uri->segment(4)));
		if ((int)$id && (int)$url) {
			$this->data['student'] = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));
			if($this->data['student']) {
				if(config_item('demo') == FALSE) {
					if($this->data['student']->photo != 'default.png') {
						if(file_exists(FCPATH.'uploads/images/'.$this->data['student']->photo)) {
							unlink(FCPATH.'uploads/images/'.$this->data['student']->photo);
						}
					}
				}
				$this->student_m->delete_student($id);
				$this->studentextend_m->delete_studentextend_by_studentID($id);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index/$url"));
			} else {
				redirect(base_url("student/index"));
			}
		} else {
			redirect(base_url("student/index/$url"));
		}

	}

	public function unique_roll() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		$schoolyearID = $this->data['siteinfos']->school_year;
		if((int)$id) {
			$student = $this->student_m->get_order_by_student(array("roll" => $this->input->post("roll"), "studentID !=" => $id, "classesID" => $this->input->post('classesID'), 'schoolyearID' => $schoolyearID));
			if(count($student)) {
				$this->form_validation->set_message("unique_roll", "The %s is already exists.");
				return FALSE;
			}
			return TRUE;
		} else {
			$student = $this->student_m->get_order_by_student(array("roll" => $this->input->post("roll"), "classesID" => $this->input->post('classesID'), 'schoolyearID' => $schoolyearID));

			if(count($student)) {
				$this->form_validation->set_message("unique_roll", "The %s is already exists.");
				return FALSE;
			}
			return TRUE;
		}
	}

	public function lol_username() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('student' => 'student', 'parents' => 'parents', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username'), "username !=" => $student_info->username));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parents' => 'parents', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username')));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function date_valid($date) {
		if($date) {
			if(strlen($date) <10) {
				$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		     	return FALSE;
			} else {
		   		$arr = explode("-", $date);
		        $dd = $arr[0];
		        $mm = $arr[1];
		        $yyyy = $arr[2];
		      	if(checkdate($mm, $dd, $yyyy)) {
		      		return TRUE;
		      	} else {
		      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		     		return FALSE;
		      	}
		    }
		}
		return TRUE;
	}

	public function unique_classesID() {
		if($this->input->post('classesID') == 0) {
			$this->form_validation->set_message("unique_classesID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	public function unique_sectionID() {
		if($this->input->post('sectionID') == 0) {
			$this->form_validation->set_message("unique_sectionID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	public function student_list() {
		$classID = $this->input->post('id');
		if((int)$classID) {
			$string = base_url("student/index/$classID");
			echo $string;
		} else {
			redirect(base_url("student/index"));
		}
	}

	public function unique_email() {
		if($this->input->post('email')) {
			$id = htmlentities(escapeString($this->uri->segment(3)));
			if((int)$id) {
				$student_info = $this->student_m->get_single_student(array('studentID' => $id));
				$tables = array('student' => 'student', 'parents' => 'parents', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
				$array = array();
				$i = 0;
				foreach ($tables as $table) {
					$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $student_info->username ));
					if(count($user)) {
						$this->form_validation->set_message("unique_email", "%s already exists");
						$array['permition'][$i] = 'no';
					} else {
						$array['permition'][$i] = 'yes';
					}
					$i++;
				}
				if(in_array('no', $array['permition'])) {
					return FALSE;
				} else {
					return TRUE;
				}
			} else {
				$tables = array('student' => 'student', 'parents' => 'parents', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
				$array = array();
				$i = 0;
				foreach ($tables as $table) {
					$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
					if(count($user)) {
						$this->form_validation->set_message("unique_email", "%s already exists");
						$array['permition'][$i] = 'no';
					} else {
						$array['permition'][$i] = 'yes';
					}
					$i++;
				}

				if(in_array('no', $array['permition'])) {
					return FALSE;
				} else {
					return TRUE;
				}
			}
		}
		return TRUE;
	}

	function sectioncall() {
		$classesID = $this->input->post('id');
		if((int)$classesID) {
			$allsection = $this->section_m->get_order_by_section(array('classesID' => $classesID));
			echo "<option value='0'>", $this->lang->line("student_select_section"),"</option>";
			foreach ($allsection as $value) {
				echo "<option value=\"$value->sectionID\">",$value->section,"</option>";
			}
		}
	}

    function optionalsubjectcall() {
        $classesID = $this->input->post('id');
        if((int)$classesID) {
            $allOptionalSubjects = $this->subject_m->get_order_by_subject(array("classesID" =>$classesID, 'type' => 0));
            echo "<option value='0'>", $this->lang->line("student_select_optionalsubject"),"</option>";
            foreach ($allOptionalSubjects as $value) {
                echo "<option value=\"$value->subjectID\">",$value->subject,"</option>";
            }
        }
    }

	public function unique_capacity() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$id) {
			if($this->input->post('sectionID')) {
				$sectionID = $this->input->post('sectionID');
				$classesID = $this->input->post('classesID');
				$schoolyearID = $this->data['siteinfos']->school_year;
				$section = $this->section_m->get_section($this->input->post('sectionID'));
				$student = $this->student_m->get_order_by_student(array('classesID' => $classesID, 'sectionID' => $sectionID, 'schoolyearID' => $schoolyearID, 'studentID !=' => $id));
				if(count($student) >= $section->capacity) {
					$this->form_validation->set_message("unique_capacity", "The %s capacity is full.");
		     		return FALSE;
				}
				return TRUE;
			} else {
				$this->form_validation->set_message("unique_capacity", "The %s field is required.");
		     	return FALSE;
			}
		} else {
			if($this->input->post('sectionID')) {
				$sectionID = $this->input->post('sectionID');
				$classesID = $this->input->post('classesID');
				$schoolyearID = $this->data['siteinfos']->school_year;
				$section = $this->section_m->get_section($this->input->post('sectionID'));
				$student = $this->student_m->get_order_by_student(array('classesID' => $classesID, 'sectionID' => $sectionID, 'schoolyearID' => $schoolyearID));
				if(count($student) >= $section->capacity) {
					$this->form_validation->set_message("unique_capacity", "The %s capacity is full.");
		     		return FALSE;
				}
				return TRUE;
			} else {
				$this->form_validation->set_message("unique_capacity", "The %s field is required.");
		     	return FALSE;
			}
		}
	}

	public function unique_registerNO() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		$schoolyearID = $this->data['siteinfos']->school_year;
		if((int)$id) {
			$student = $this->student_m->get_single_student(array("registerNO" => $this->input->post("registerNO"), "studentID !=" => $id, "classesID" => $this->input->post('classesID'), 'schoolyearID' => $schoolyearID));
			//if(count($student)) {
			if(($student)) {
				$this->form_validation->set_message("unique_registerNO", "The %s is already exists.");
				return FALSE;
			}
			return TRUE;
		} else {
			$student = $this->student_m->get_single_student(array("registerNO" => $this->input->post("registerNO"), "classesID" => $this->input->post('classesID'), 'schoolyearID' => $schoolyearID));

			if(count($student)) {
				$this->form_validation->set_message("unique_registerNO", "The %s is already exists.");
				return FALSE;
			}
			return TRUE;
		}
	}

	function active() {
		if(permissionChecker('student_edit')) {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'chacked') {
						$this->student_m->update_student(array('active' => 1), $id);
						echo 'Success';
					} elseif($status == 'unchacked') {
						$this->student_m->update_student(array('active' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'name',
				'label' => $this->lang->line("student_name"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'dob',
				'label' => $this->lang->line("student_dob"),
				'rules' => 'trim|max_length[10]|callback_date_valid|xss_clean'
			),
			array(
				'field' => 'sex',
				'label' => $this->lang->line("student_sex"),
				'rules' => 'trim|required|max_length[10]|xss_clean'
			),
			// array(
			// 	'field' => 'bloodgroup',
			// 	'label' => $this->lang->line("student_bloodgroup"),
			// 	'rules' => 'trim|max_length[5]|xss_clean'
			// ),
			// array(
			// 	'field' => 'religion',
			// 	'label' => $this->lang->line("student_religion"),
			// 	'rules' => 'trim|max_length[25]|xss_clean'
			// ),
			// array(
			// 	'field' => 'email',
			// 	'label' => $this->lang->line("student_email"),
			// 	'rules' => 'trim|max_length[40]|valid_email|xss_clean|callback_unique_email'
			// ),
			array(
				'field' => 'phone',
				'label' => $this->lang->line("student_phone"),
				'rules' => 'trim|max_length[25]|min_length[5]|xss_clean'
			),
			array(
				'field' => 'address',
				'label' => $this->lang->line("student_address"),
				'rules' => 'trim|max_length[200]|xss_clean'
			),
			// array(
			// 	'field' => 'state',
			// 	'label' => $this->lang->line("student_state"),
			// 	'rules' => 'trim|max_length[128]|xss_clean'
			// ),
			// array(
			// 	'field' => 'country',
			// 	'label' => $this->lang->line("student_country"),
			// 	'rules' => 'trim|max_length[128]|xss_clean'
			// ),
			// array(
			// 	'field' => 'classesID',
			// 	'label' => $this->lang->line("student_classes"),
			// 	'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_classesID'
			// ),
			// array(
			// 	'field' => 'sectionID',
			// 	'label' => $this->lang->line("student_section"),
			// 	'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_sectionID|callback_unique_capacity'
			// ),
			// array(
			// 	'field' => 'registerNO',
			// 	'label' => $this->lang->line("student_registerNO"),
			// 	'rules' => 'trim|required|max_length[40]|callback_unique_registerNO|xss_clean'
			// ),
			// array(
			// 	'field' => 'roll',
			// 	'label' => $this->lang->line("student_roll"),
			// 	'rules' => 'trim|required|max_length[11]|numeric|callback_unique_roll|xss_clean'
			// ),
			// array(
			// 	'field' => 'guargianID',
			// 	'label' => $this->lang->line("student_guargian"),
			// 	'rules' => 'trim|required|max_length[11]|xss_clean|numeric'
			// ),
			// array(
			// 	'field' => 'photo',
			// 	'label' => $this->lang->line("student_photo"),
			// 	//'rules' => 'trim|max_length[200]|xss_clean|callback_photoupload'
			// ),

            // array(
            //     'field' => 'studentGroupID',
            //     'label' => $this->lang->line("student_studentgroup"),
            //     'rules' => 'trim|max_length[11]|xss_clean|numeric'
            // ),

            // array(
            //     'field' => 'optionalSubjectID',
            //     'label' => $this->lang->line("student_optionalSubject"),
            //     'rules' => 'trim|max_length[11]|xss_clean|numeric'
            // ),

            // array(
            //     'field' => 'extraCurricularActivities',
            //     'label' => $this->lang->line("student_extraCurricularActivities"),
            //     'rules' => 'trim|max_length[128]|xss_clean'
            // ),

            // array(
            //     'field' => 'remarks',
            //     'label' => $this->lang->line("student_remarks"),
            //     'rules' => 'trim|max_length[128]|xss_clean'
            // ),

			// array(
			// 	'field' => 'username',
			// 	'label' => $this->lang->line("student_username"),
			// 	'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'
			// ),
			// array(
			// 	'field' => 'password',
			// 	'label' => $this->lang->line("student_password"),
			// 	'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean'
			// )
		);
		return $rules;
	}

	protected function physical_validation_rules() {
		$rules = array(
			array(
				'field' => 'race_1600m',
				'label' => $this->lang->line("race_1600m"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			// array(
			// 	'field' => 'day',
			// 	'label' => $this->lang->line("day"),
			// 	'rules' => 'required'
			// ),
			// array(
			// 	'field' => 'month',
			// 	'label' => $this->lang->line("month"),
			// 	'rules' => 'required'
			// ),
			// array(
			// 	'field' => 'year',
			// 	'label' => $this->lang->line("year"),
			// 	'rules' => 'required'
			// ),
			array(
				'field' => 'date',
				'label' => $this->lang->line("date"),
				'rules' => 'trim|required|xss_clean'
			),
			array(
				'field' => 'long_jump',
				'label' => $this->lang->line("long_jump"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'high_jump',
				'label' => $this->lang->line("high_jump"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'beam',
				'label' => $this->lang->line("beam"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'push_up',
				'label' => $this->lang->line("push_up"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'up_down',
				'label' => $this->lang->line("up_down"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'zigzag_balance',
				'label' => $this->lang->line("zigzag_balance"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'remarks',
				'label' => $this->lang->line("remarks"),
				'rules' => 'trim|required|xss_clean|max_length[500]'
			)
		);
		return $rules;
	}

	public function get_date_details () {
		echo json_encode($this
							->Admin_m
							->get_record(
								"bd_physical", 
								[
									"studentID" => $_POST['studentID'], 
									"date"      => $_POST['date']
								]
							)
						);
	}

	public function photoupload() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		$student = array();
		if((int)$id) {
			$student = $this->student_m->get_student($id);
		}

		$new_file = "default.png";
		if($_FILES["photo"]['name'] !="") {
			$file_name = $_FILES["photo"]['name'];
			$random = rand(1, 10000000000000000);
	    	$makeRandom = hash('sha512', $random.$this->input->post('username') . config_item("encryption_key"));
			$file_name_rename = $makeRandom;
            $explode = explode('.', $file_name);
            if(count($explode) >= 2) {
	            $new_file = $file_name_rename.'.'.end($explode);
				$config['upload_path'] = "./uploads/images";
				$config['allowed_types'] = "gif|jpg|png";
				$config['file_name'] = $new_file;
				$config['max_size'] = '1024';
				$config['max_width'] = '3000';
				$config['max_height'] = '3000';
				$this->load->library('upload', $config);
				if(!$this->upload->do_upload("photo")) {
					$this->form_validation->set_message("photoupload", $this->upload->display_errors());
	     			return FALSE;
				} else {
					$this->upload_data['file'] =  $this->upload->data();
					return TRUE;
				}
			} else {
				$this->form_validation->set_message("photoupload", "Invalid file");
	     		return FALSE;
			}
		} else {
			if(count($student)) {
				$this->upload_data['file'] = array('file_name' => $student->photo);
				return TRUE;
			} else {
				$this->upload_data['file'] = array('file_name' => $new_file);
			return TRUE;
			}
		}
	}

	/* ************************* Start Custom Functions ************************* */
	public function physical ($id) {
		$this->data['headerassets'] = ['js' => ['assets/highcharts/highcharts.js', 'assets/datepicker/datepicker.js'], 'css' => ['assets/datepicker/datepicker.css']];
		$race_5km_graph_data     = [];
		$race_1600m_graph_data     = [];
		$race_800m_graph_data     = [];
		$race_100m_graph_data     = [];
		$long_jump_graph_data      = [];
		$high_jump_graph_data      = [];
		$beam_graph_data           = [];
		$push_up_graph_data        = [];
		$up_down_graph_data        = [];

		$graph_data = $this
						->Admin_m
						->get_details(
										"bd_physical", 
										[
											"studentID" => $id
										]
									);
	
		foreach ($graph_data as $data) 
		{
			$race_5km_graph_data[$data->date] 		= $data->race_5km;
			$race_1600m_graph_data[$data->date] 	= $data->race_1600m;
			$race_800m_graph_data[$data->date] 		= $data->race_800m;
			$race_100m_graph_data[$data->date] 		= $data->race_100m;
			$long_jump_graph_data[$data->date] 		= $data->long_jump;
			$high_jump_graph_data[$data->date] 		= $data->high_jump;
			$beam_graph_data[$data->date]           = $data->beam;
			$push_up_graph_data[$data->date] 	    = $data->push_up;
			$up_down_graph_data[$data->date] 		= $data->up_down;
		}
		$this->data['race_5km_graph_data']       = $race_5km_graph_data;
		$this->data['race_1600m_graph_data']  	 = $race_1600m_graph_data;
		$this->data['race_800m_graph_data']  	 = $race_800m_graph_data;
		$this->data['race_100m_graph_data']  	 = $race_100m_graph_data;
		$this->data['long_jump_graph_data']   	 = $long_jump_graph_data;
		$this->data['high_jump_graph_data']   	 = $high_jump_graph_data;
		$this->data['beam_graph_data']        	 = $beam_graph_data;
		$this->data['push_up_graph_data']    	 = $push_up_graph_data;
		$this->data['up_down_graph_data']        = $up_down_graph_data;
		$dates_data = $this
						->Admin_m
						->get_details(
										"bd_physical", 
										[
											'studentID' => $id
										], 
										"bd_physical.date"
									);
		foreach ($dates_data as $date) {
			$this->data['dates'][] = date("d-m-Y", strtotime($date->date));
		}

		if ($_POST) {
			$rules = $this->physical_validation_rules();
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE) {
				$this->data['student_id'] = $id;
				$this->data['physical_details'] = $this->student_m->get_physical_details($id);
				$this->data['student_details'] = $this->Admin_m->get_student_details($id, "student.name, parents.father_name");
				$this->data['subview'] = "student/physical";				
				$this->load->view('_layout_main', $this->data);
			} else {
				$_POST['date'] = date("Y-m-d", strtotime($_POST['date']));
				// dd($_POST);
				$this->Admin_m->insert_record("bd_physical", $_POST);
				redirect(base_url("/student/physical/$id"));
			}
		} else {
			$usertypeID = $this->session->userdata('usertypeID');
			//if ($usertypeID == 1 || $userTypeID == 7) {
				$this->data['student_id'] = $id;
				$this->data['physical_details'] = $this->student_m->get_physical_details($id);
				$this->data['student_details'] = $this->Admin_m->get_student_details($id, "student.name, parents.father_name");
				$this->data['subview'] = "student/physical";
				$this->load->view('_layout_main', $this->data);
			// } else {
			// 	redirect(base_url());
			// }

		}
	}

	public function physical_update ($id) {
		$this->data['headerassets'] = ['js' => ['assets/datepicker/datepicker.js'], 'css' => ['assets/datepicker/datepicker.css']];
		if ($_POST) {
			$rules = $this->physical_validation_rules();
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE) {
				$this->data['student_id'] = $id;
				$this->data['physical_details'] = $this->student_m->get_physical_details_by_id($id);
				$this->data['student_details'] = $this->Admin_m->get_student_details($this->data['physical_details']->studentID, "student.name, parents.father_name");
				
				// $this->physical_update($id);
				$this->data['subview'] = "student/physical_update";				
				$this->load->view('_layout_main', $this->data);
			} else {
				$_POST['date'] = date('Y-m-d', strtotime($_POST['date']));
				$this->Admin_m->update_record("bd_physical", $_POST, $_POST['id']);
				$studentID = $_POST['studentID'];
				redirect(base_url("/student/physical/$studentID"));
			}
		} else {
			$usertypeID = $this->session->userdata('usertypeID');
			if ($usertypeID == 1 || $userTypeID == 7) {
				$this->data['student_id'] = $id;
				$this->data['physical_details'] = $this->student_m->get_physical_details_by_id($id);
				$this->data['student_details'] = $this->Admin_m->get_student_details($this->data['physical_details']->studentID, "student.name, parents.father_name");
				$this->data['subview'] = "student/physical_update";
				$this->load->view('_layout_main', $this->data);
			} else {
				redirect(base_url());
			}
		}
	}

	public function delete_physical ($id) {
		if((int)$id) {
			$rec = $this->db->select('*')->where('id',$id)->get('bd_physical')->row();
			if($rec) {
				$this->Admin_m->delete_record('bd_physical',$id);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/physical/".$rec->studentID));
			}
			redirect(base_url("student/"));
		} else {
			redirect(base_url("student/"));
		}
	}

	public function physical_print ($id) {
		if((int)$id) {
			$this->data['physical_details'] = $this->student_m->get_physical_details_by_id($id);
			$this->data['student_details'] = $this->Admin_m->get_student_details($this->data['physical_details']->studentID, "student.name, parents.father_name");
			if($this->data['physical_details'] && $this->data['student_details']) {
				$this->load->view("student/physical_print", $this->data);
			} else {
				redirect(base_url("student/index/"));
			}
		} else {
			redirect(base_url("student/index/"));
		}
	}

	public function add_installment(){
		if($_POST){
	         $postDataPayment = [
                'studentID'=>$this->input->post('student_id'),
                'paymenttype' => 'Cash',//$this->input->post('paymenttype'),
                'paymentbank' => NULL,//$this->input->post('paymentbank'),
                'chequeno' => NULL,//$this->input->post('chequeno'),
                'paymentamount' => $this->input->post('amount'),
                'fee_type'=>5,
                'purpose'=>$this->input->post('installment'),
                'credit_debit'=>'Debit',
                'payID'=>'',
            ];
			$lastId = $this->Admin_m->insertPaymentData($postDataPayment);
			$studentID = $_POST['student_id'];
			$payment_method = $this->db->query("SELECT `paymenttype` FROM `payment` WHERE `studentID` = $studentID ORDER BY `paymentID` asc LIMIT 1")->result()[0]->paymenttype;
			$amt = $_POST['amount'];
            if ($payment_method == "Cash") {
                $this->db->query("UPDATE `total_balance` set `academic_cash_balance` = `academic_cash_balance` + $amt WHERE `id` = 1");
            } else {
                $this->db->query("UPDATE `total_balance` set `academic_bank_balance` = `academic_bank_balance` + $amt WHERE `id` = 1");
            }
	        if($lastId){
	        	if($this->input->post('due_date')!=''){
	        		$postDataStudent['due_date'] = date('Y-m-d',strtotime($this->input->post('due_date')));
	        		$this->student_m->update_student($postDataStudent, $this->input->post('student_id'));
	        	}	        	
	        }
	        $classID = $this->input->post('course_id');
			if((int)$classID) {
				redirect(base_url("student/view/".$this->input->post('student_id').'/'.$classID));
			} else {
				redirect(base_url("student/index"));
			}
       	}
	}
	public function no_dues(){
		$due_data = $studentIDs = [];
		$academic = $this->Admin_m->get_details ('bd_student_stmt', NULL, NULL, ['final_course_fee >', '0'], 'array');
		$hostel   = $this->Admin_m->get_details ('bd_student_stmt', NULL, NULL, ['final_hostel_fee >', '0'], 'array');
		$library  = $this
					->db
					->query("SELECT studentID from lmember where teacherID is NULL AND lID in (SELECT lID FROM issue where return_date IS NULL)")->result_array(); 
		$merged = array_merge($academic, $hostel, $library);
		// dd($merged);
		foreach ($merged as $k => $v) {
			$studentIDs[] = $v['studentID'];
			$due_data[$v['studentID']] = [
											'studentID' => $v['studentID'], 
											'academic'  => 0,
											'hostel'    => 0, 
											'library' 	=> 0
										];		
		}
		foreach ($academic as $a) {
			if (isset($due_data[$a['studentID']])) {
				$due_data[$a['studentID']]['academic'] = 1; 
			}
		}
		foreach ($hostel as $a) {
			if (isset($due_data[$a['studentID']])) {
				$due_data[$a['studentID']]['hostel'] = 1; 
			}
		}
		foreach ($library as $a) {
			if (isset($due_data[$a['studentID']])) {
				$due_data[$a['studentID']]['library'] = 1; 
			}
		}
		$ids = implode(', ', $studentIDs);
		$students = $this->db->query("SELECT * FROM student WHERE studentID in ($ids)")->result_array();
		foreach ($students as $student) {
			$due_data[$student['studentID']]['name'] = $student['name'];
			$due_data[$student['studentID']]['roll'] = $student['roll'];
			$due_data[$student['studentID']]['classesID'] = $student['classesID'];
		}
		$this->data['students'] = $due_data;
		$this->data['subview'] = "student/dues";
		$this->load->view("_layout_main", $this->data);
	}
	public function hostal_accessories() {
		if($_POST) {
			$segments = $_POST['segments'];
			$postDataStudent = [
		        'studentID'=>$this->input->post('student_id'),
		        'hostal_leave_reason'=>$this->input->post('leave_reason'),
		        'hostal_leave_date'=>date('Y-m-d',strtotime($this->input->post('leave_date'))),
		        'hostal_accessories'=>(isset($segments) ? $this->getSegmentJSON() : ''),
		    ];
			$this->student_m->update_student($postDataStudent, $this->input->post('student_id'));
			$classID = $this->input->post('class_id');
			if($this->input->post('redirect')=='hmember'){
				redirect(base_url('hmember/index'));
			}
			redirect(base_url("student/index"));
			// if((int)$classID) {
			// 	redirect(base_url("student/index/$classID"));
			// } else {
			// 	redirect(base_url("student/index"));
			// }
		} else {
			$studentData = $this->db->select('studentID,hostal_accessories,hostal_leave_date,hostal_leave_reason')->where('studentID',$this->input->get('student_id'))->get('student')->result();
			if($studentData){
				$data = $studentData[0];
			}
			echo json_encode(['student_data'=>$data,'count'=>count($studentData)]);
		}
		
	}
	public function student_account(){
		//if(permissionChecker('student_account')) {dd(1);
			$id = htmlentities(escapeString($this->uri->segment(3)));
			$url = htmlentities(escapeString($this->uri->segment(4)));
			$schoolyearID = $this->session->userdata('defaultschoolyearID');
			if((int)$id && (int)$url) {
				$this->data['student'] = $this->student_m->get_single_student(array('studentID' => $id, 'schoolyearID' => $schoolyearID));
				$this->data["payments"] = $this->db->select('*')->where('schoolyearID',$schoolyearID)->where('studentID',$id)->get('payment')->result();
				$this->data["subview"] = "student/account";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		// } else {
		// 	$this->data["subview"] = "error";
		// 	$this->load->view('_layout_main', $this->data);
		// }
	}
	function hostal_fees(){
		if($_POST) {
			//$student = $this->student_m->get_single_student(array('studentID' => $this->input->post('student_id')));
			
			$hostel_main_id = $this->hostel_m->get_hostel($this->config->item('HOSTAL_ID'));
			$category_main_id = $this->category_m->get_single_category(array("hostelID" => $hostel_main_id->hostelID, "categoryID" =>  $this->input->post("categoryID")));
			$memExist = $this->hmember_m->get_single_hmember(['studentID'=>$this->input->post('student_id')]);
			if($memExist){
				$array = array("categoryID" => $this->input->post("categoryID"));
				 $this->hmember_m->update_hmember($array,$memExist->hmemberID);
			} else {
				$array = array(
					"hostelID" => $this->config->item('HOSTAL_ID'),
					"categoryID" => $this->input->post("categoryID"),
					"studentID" => $this->input->post('student_id'),
					"hbalance" => $this->input->post("paymentamount"),//$category_main_id->hbalance,
					"hjoindate" => date("Y-m-d")
				);
				$this->hmember_m->insert_hmember($array);
						
			}
			// pay_by 1=Pay By Installment 0=Full Pay

			$postDataPayment = [
                'studentID'=>$this->input->post('student_id'),
                'paymenttype' => $this->input->post('paymenttype'),
                'paymentbank' => $this->input->post('paymentbank'),
                'chequeno' => $this->input->post('chequeno'),
                //'paymentamount' => $this->input->post('paymentamount'),
                'payID'=>$this->input->post('pay_id'),
                'credit_debit'=>'Debit',
            ];
            if($this->input->post("pay_by")==1){
            	$postDataPayment['fee_type']=6;
            	$postDataPayment['purpose']=$this->input->post('installment');
            	$postDataPayment['paymentamount'] = $this->input->post('installmentamount');

				$studentArr['hostel_installments']=$this->input->post("pay_by");
            } else {
            	$postDataPayment['fee_type']=2;
            	$postDataPayment['purpose']='Hostal Fees';
            	$postDataPayment['paymentamount'] = $this->input->post('paymentamount');
			}
			$amt = $postDataPayment['paymentamount'];
			if ($_POST["paymenttype"] == "Cash") {
				$this->db->query("UPDATE `total_balance` set `hostel_cash_balance` = `hostel_cash_balance` + $amt WHERE `id` = 1");
			} else {
				$this->db->query("UPDATE `total_balance` set `hostel_bank_balance` = `hostel_bank_balance` + $amt WHERE `id` = 1");
			}	
            $lastPayId = $this->Admin_m->insertPaymentData($postDataPayment);

            //update student data
            $studentArr['hostel']=1;
            if($this->input->post("paymentamount")!=null){
            	$studentArr['hostel_fee']=$this->input->post("paymentamount");	
            }            					
			$this->student_m->update_student($studentArr, $this->input->post('student_id'));

			$classID = $this->input->post('class_id');
			
			if($this->input->post('redirect')=='hmember'){
				redirect(base_url('hmember/index'));
			}
			redirect(base_url("student/index"));
			
		} else {
			$data = '';
			$paymentData = $this->db->select('*')->where('fee_type',2)->where('schoolyearID',$this->data['siteinfos']->school_year)->where('studentID',$this->input->get('student_id'))->order_by('paymentID','DESC')->get('payment')->result();
			if($paymentData){
				$data = $paymentData[0];
			}
			$memExist = $this->hmember_m->get_single_hmember(['studentID'=>$this->input->get('student_id')]);
			$cId=0;
			if($memExist){
				$cId = $memExist->categoryID;
			}
			echo json_encode(['payment_data'=>$data,'category_id'=>$cId,'count'=>count($paymentData)]);
		}
	}
	function getSegmentJSON(){
		$segments=[];
		if(count($_POST['segments'])){
			if(count($_POST['segments']['product'])>0){
				for($i=0;$i<count($_POST['segments']['product']);$i++){
					$segments[] = ['segment'=>$_POST['segments']['product'][$i], 'quantity'=>$_POST['segments']['quantity'][$i]];
				}
			}
		}
		return json_encode($segments);
	}
	/* ************************* Start Custom Functions ************************* */
}

/* End of file student.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/student.php */
