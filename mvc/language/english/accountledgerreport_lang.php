<?php
$lang['panel_title'] = "Account Ledger Report";
$lang['accountledgerreport_please_select'] = "Please Select";
$lang['accountledgerreport_all_accademic_year'] = "All Academic Year";
$lang['accountledgerreport_academicyear'] = "Academic Year";
$lang['accountledgerreport_fromdate'] = "From Date";
$lang['accountledgerreport_todate'] = "To Date";
$lang['accountledgerreport_accountledger'] = "Account Ledger";
$lang['accountledgerreport_report_for'] = "Report For";

$lang['accountledgerreport_income']  = "Income";
$lang['accountledgerreport_expense'] = "Expense";
$lang['accountledgerreport_salary']  = "Salary";
$lang['accountledgerreport_fees_collections'] = "Fees Collections";
$lang['accountledgerreport_fines']          = "Fines";
$lang['accountledgerreport_total_balance']  = "Total Balance";

$lang['accountledgerreport_income_des']  = "Income is money that an individual or business receives in exchange for providing a good or service or through investing capital";
$lang['accountledgerreport_expense_des'] = "Every School have several area of expenses, this part shows the total sum of expenses spend over a time range";
$lang['accountledgerreport_salary_des']  = "Total sum of salary given to employees, teacher and other stuffs over the time range";
$lang['accountledgerreport_fees_collections_des'] = "This part of the system shows, sum of total paid amount of student invoices";
$lang['accountledgerreport_fines_des'] = "Total sum of fine taken from students";

$lang['accountledgerreport_total'] = "Total";
$lang['accountledgerreport_grand_total'] = "Grand Total";
$lang['accountledgerreport_submit'] = "Get Report";
$lang['accountledgerreport_hotline'] = "Hotline";
$lang['accountledgerreport_mail'] = "Send Pdf To Mail";

$lang['accountledgerreport_to'] = "To";
$lang['accountledgerreport_subject'] = "Subject";
$lang['accountledgerreport_message'] = "Message";
$lang['accountledgerreport_close'] = "Close";
$lang['accountledgerreport_send'] = "Send";

$lang['accountledgerreport_mail_to'] = "The To field is required";
$lang['accountledgerreport_mail_valid'] = "The To field must contain a valid email address";
$lang['accountledgerreport_mail_subject'] = "The Subject field is required";
$lang['accountledgerreport_permissionmethod'] = "Method not allowed";
$lang['accountledgerreport_permission'] = "Permission not allowed";

$lang['mail_success'] = 'Email send successfully';
$lang['mail_error'] = 'Oops, Email not send';
$lang['academic_ledger'] = "Academic Ledger";
$lang['hostel_ledger'] = "Hostel Ledger";

$lang['label_user'] = "User";
$lang['label_vendor'] = "Vendor";
$lang['label_student'] = "Student";
$lang['label_lect'] = "Lecturer";
$lang['label_accountant'] = "Accountant";
$lang['label_librarian'] = "Librarian";
$lang['label_receptionist'] = "Receptionist";
$lang['label_warden'] = "Warden";
$lang['label_from_date'] = "From Date";
$lang['label_to_date'] = "To Date";
$lang['label_payment_type'] = "Type";
$lang['label_payment_status'] = "Payment Status";
$lang['label_payment_method'] = "Payment Method";

$lang['th_dates'] = "Date";
$lang['th_particulars'] = "Particulars";
$lang['th_debit'] = "Debit";
$lang['th_credit'] = "Credit";
$lang['th_total_income'] = "Total Income";
$lang['th_total_expense'] = "Total Expense";
$lang['th_total_payment'] = "Total Payment";
$lang['th_total_receipt'] = "Total Receipt";
$lang['th_paid_expense'] = "Paid Expense";
$lang['th_unpaid_expense'] = "Unpaid Expense";
$lang['th_paid_income'] = "Paid Income";
$lang['th_unpaid_income'] = "Unpaid Income";
$lang['th_cash_bal'] = "Cash Balance";
$lang['th_bank_bal'] = "Bank Balance";