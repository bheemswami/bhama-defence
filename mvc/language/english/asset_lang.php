<?php

/* List Language  */
$lang['panel_title'] = "Asset";
$lang['add_title'] = "Add an asset";
$lang['slno'] = "#";
$lang['asset_serial'] = "Serial";
$lang['asset_description'] = "Title";
$lang['asset_attachment'] = "Attachment";
$lang['asset_status'] = "Status";
$lang['asset_categoryID'] = "Category";
$lang['asset_locationID'] = "Location";
$lang['asset_condition'] = "Condition";
$lang['asset_select_status'] = "Select Status";
$lang['asset_select_condition'] = "Select Condition";
$lang['asset_select_category'] = "Select Category";
$lang['asset_select_location'] = "Select Location";
$lang['asset_status_checked_out'] = "Out of stock";
$lang['asset_create_date'] = "Create Date";
$lang['asset_status_checked_in'] = "In Storage";
$lang['asset_condition_new'] = "Good";
$lang['asset_condition_used'] = "Bad";
$lang['asset_add'] = "Add";
$lang['action'] = "Action";

$lang['asset_file_browse'] = 'File Browse';
$lang['asset_clear'] = 'Clear';

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['print'] = 'Print';
$lang['pdf_preview'] = 'PDF Preview';
$lang["mail"] = "Send Pdf to Mail";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';

$lang['stock_qty'] = 'Total Quantity';
$lang['used_qty'] = 'Used Quantity';
$lang['remain_qty'] = 'Remaining Quantity';
$lang['add_asset'] = 'Add Asset';
$lang['update_asset'] = 'Update Asset';

$lang['asset_for'] = 'Add For';
$lang['asset_for_th'] = 'For';
$lang['asset_for_academic'] = 'Academic';
$lang['asset_for_hostel'] = 'Hostel';