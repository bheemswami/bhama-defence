<?php

$lang['panel_title'] = "Attendance Report";
$lang['select_val'] = "Select";
$lang['attendancereport_section'] = "Section";
$lang['attendancereport_select_all_section'] = "All Section";
$lang['attendancereport_present'] = "Present";
$lang['attendancereport_late_present_with_excuse'] = "Late Present With Excuse";
$lang['attendancereport_late_present'] = "Late Present";
$lang['attendancereport_absent'] = "Absent";
$lang['attendancereport_report_for'] = "Report For";
$lang['attendancereport_attendance'] = "Attendance";
$lang['attendancereport_photo'] = "Photo";
$lang['attendancereport_name'] = "Name";
$lang['attendancereport_roll'] = "Roll";
$lang['attendancereport_phone'] = "Phone";
$lang['attendancereport_registerNo'] = "Register No";
$lang['attendancereport_email'] = "Email";
$lang['attendancereport_type'] = 'Attendance Type';
$lang['attendancereport_classid'] = 'Course ID';
$lang['attendancereport_sectionid'] = 'Section ID';
$lang['attendancereport_date'] = 'Date';
$lang['attendancereport_subjectid'] = 'Subject ID';
$lang['attendancereport_student_not_found'] = "Don't have any students";
$lang['attendancereport_attendancetype'] = "Attendance Type";
$lang['attendancereport_submit'] = "Get Report";
$lang['attendancereport_please_select'] = "Please Select";
$lang['attendancereport_class'] = "Course";
$lang['attendancereport_subject'] = "Subject";

$lang['attendancereport_print'] = "Print";
$lang['attendancereport_pdf_preview'] = "PDF Preview";
$lang['attendancereport_xml'] = "Xml";
$lang['attendancereport_mail'] = "Send Pdf To Mail";
$lang['attendancereport_hotline'] = "Hotline";

$lang['attendancereport_mail'] = "Send Pdf To Mail";
$lang['attendancereport_to'] = "To";
$lang['attendancereport_subject'] = "Subject";
$lang['attendancereport_message'] = "Message";
$lang['attendancereport_close'] = "Close";
$lang['attendancereport_send'] = "Send";
$lang['attendancereport_slno'] = "#";
$lang['attendancereport_leave'] = "Leave";

$lang['attendancereport_mail_to'] = "The To field is required";
$lang['attendancereport_mail_valid'] = "The To field must contain a valid email address";
$lang['attendancereport_mail_subject'] = "The Subject field is required";
$lang['mail_success'] = 'Email send successfully';
$lang['mail_error'] = 'Oops, Email not send';

$lang['attendancereport_data_not_found'] = "Data not found";
$lang['attendancereport_section_not_found'] = "Section not found";
$lang['attendancereport_class_not_found'] = "Class not found";
$lang['attendancereport_permission'] = "Permission not allowed";
$lang['attendancereport_permissionmethod'] = "Method not allowed";

$lang['attendance_1'] = "1";
$lang['attendance_2'] = "2";
$lang['attendance_3'] = "3";
$lang['attendance_4'] = "4";
$lang['attendance_5'] = "5";
$lang['attendance_6'] = "6";
$lang['attendance_7'] = "7";
$lang['attendance_8'] = "8";
$lang['attendance_9'] = "9";
$lang['attendance_10'] = "10";
$lang['attendance_11'] = "11";
$lang['attendance_12'] = "12";
$lang['attendance_13'] = "13";
$lang['attendance_14'] = "14";
$lang['attendance_15'] = "15";
$lang['attendance_16'] = "16";
$lang['attendance_17'] = "17";
$lang['attendance_18'] = "18";
$lang['attendance_19'] = "19";
$lang['attendance_20'] = "20";
$lang['attendance_21'] = "21";
$lang['attendance_22'] = "22";
$lang['attendance_23'] = "23";
$lang['attendance_24'] = "24";
$lang['attendance_25'] = "25";
$lang['attendance_26'] = "26";
$lang['attendance_27'] = "27";
$lang['attendance_28'] = "28";
$lang['attendance_29'] = "29";
$lang['attendance_30'] = "30";
$lang['attendance_31'] = "31";