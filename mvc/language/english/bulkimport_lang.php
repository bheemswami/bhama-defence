<?php

/* List Language  */
$lang['panel_title'] = "Import";
$lang['add_title'] = "Add a Import";
$lang['slno'] = "#";
$lang['import_title'] = "Title";
$lang['import_import'] = "Import";
$lang['import_date'] = "Date";
$lang['bulkimport_teacher'] = "Add Teacher";
$lang['bulkimport_student'] = "Add Student";
$lang['bulkimport_parent'] = "Add Parent";
$lang['bulkimport_user'] = "Add User";
$lang['bulkimport_book'] = "Add Book";
$lang['bulkimport_submit'] = "Import";

/* Add Language */

$lang['add_class'] = 'Add Import';
$lang['upload_file'] = 'Import file';
$lang['import_file_type_error'] = 'Invalid file';
$lang['import_error'] = 'oops! data not imported, Please try again.';
$lang['import_success'] = 'Data successfully added';
$lang['bulkimport_sample'] = 'Download Sample';

$lang['import_s_campaign_btn'] = "Import School Campaign";
$lang['import_r_work_btn'] = "Import Receptionist Work";

$lang['bulkimport_s_campaign'] = "Add School Campaign";
$lang['bulkimport_r_work'] = "Add Receptionist Work";
$lang['list']='list';
$lang['data_date']='Date';
$lang['sno']='SNo';
$lang['school_name']='School Name';
$lang['school_type']='School Type';
$lang['principal']='Principal';
$lang['owner']='Owner';
$lang['contact_no']='Contact No';
$lang['contact_no_2']='Contact No 2';
$lang['subject']='Subject';
$lang['other']='Other';
$lang['remark']='Remark';
$lang['name']='Name';
$lang['course']='Course';
$lang['action']='Action';
$lang['upload']='Choose File';
$lang['btn_back']='Back';
                         