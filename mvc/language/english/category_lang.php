<?php

/* List Language  */
$lang['panel_title'] = "Rooms";
$lang['add_title'] = "Add a room";
$lang['slno'] = "#";
$lang['category_hname'] = "Hostel Name";
$lang['category_class_type'] = "Room Type/Name";
$lang['category_hbalance'] = "Hostel Fee";
$lang['category_note'] = "Note";

$lang['category_select_hostel'] = "Select Hostel";

$lang['action'] = "Action";
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_category'] = 'Add Room';
$lang['update_category'] = 'Update Room';

$lang['room_num'] = "Room No.";