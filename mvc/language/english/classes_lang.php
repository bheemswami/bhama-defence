<?php

/* List Language  */
$lang['panel_title'] = "Course";
$lang['add_title'] = "Add a course";
$lang['slno'] = "#";
$lang['classes_name'] = "Course";
$lang['classes_numeric'] = "Course Numeric";
$lang['teacher_name'] = "Teacher Name";
$lang['classes_studentmaxID'] = "Student Max ID";
$lang['classes_note'] = "Note";
$lang['action'] = "Action";
$lang['classes_select_teacher'] = "Select Teacher";

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_class'] = 'Add Course';
$lang['update_class'] = 'Update Course';

$lang['course_fee'] = 'Course Fees';