<?php

/* List Language  */
// $lang['panel_title'] = "Expense/Income";
$lang['panel_title'] = "Voucher";
// $lang['add_title'] = "Add an expense/income";
$lang['add_title'] = "Add a Voucher";
$lang['slno'] = "#";
$lang['expense_expense'] = "Name";
$lang['expense_date'] = "Date";
$lang['expense_amount'] = "Amount";
$lang['expense_note'] = "Note";
$lang['expense_uname'] = "User";
$lang['expense_total'] = "Total";
$lang['action'] = "Action";

// $lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_expense'] = 'Add';
$lang['update_expense'] = 'Update';

$lang['expense_type'] = 'Select Type';
$lang['expense_type_th'] = 'Type';
$lang['expense_file'] = 'File';
$lang['file_browse'] = 'File Browse';
$lang['file_clear'] = 'Clear';
$lang['exp_inc_type'] = 'Asset Tag';
$lang['exp_inc_type_th'] = 'Asset Tag';
$lang['exp_inc_permanent'] = 'Permanent';
$lang['exp_inc_non_permanent'] = 'Non Permanent';

$lang['exp_inc_for'] = 'Add For';
$lang['exp_inc_for_th'] = 'For';
$lang['exp_inc_for_academic'] = 'Academic';
$lang['exp_inc_for_hostel'] = 'Hostel';


$lang['exp'] = 'Expense';
$lang['inc'] = 'Income';
$lang['pay'] = 'Payment';
$lang['rec'] = 'Receipt';
$lang['oth'] = 'Other';

$lang['pay_status'] = 'Payment Status';
$lang['pay_status_paid'] = 'Paid';
$lang['pay_status_due'] = 'Due';

$lang['is_withdraw'] = 'Withdraw';

$lang['gen_ledger'] = 'General Entry';

$lang['pdf_hint'] = 'To downlaod PDF with eiher only Expenses or Income information, type "Expense" or "Income" in search field and click on PDF button.';
$lang['print'] = "Print";
$lang['vendor'] = "Vendor";

$lang['select_payment_method'] = 'Select Payment Method';
$lang['payment_cash'] = 'Cash';
$lang['payment_cheque'] = 'Cheque';
$lang['payment_online'] = "Online";
$lang['payment_method'] = 'Payment Method';
$lang['cheque_no'] = "Cheque No.";

$lang['th_total_income'] = "Total Income";
$lang['th_total_expense'] = "Total Expense";
$lang['th_due_paument'] = "Due Payment";
$lang['th_due_receipt'] = "Due receipt";
