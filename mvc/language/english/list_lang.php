<?php
//Enquiries
$lang['enq_panel_title'] = "Add Enquiry";
$lang['enq_student_name'] = "Name";
$lang['enq_student_dob'] = "Date of Birth";
$lang['enq_student_address'] = "Address";
$lang['enq_father_name'] = "Father's Name";
$lang['enq_mother_name'] = "Mother's Name";
$lang['enq_father_business'] = "Father's Occupation";
$lang['enq_mother_business'] = "Mother's Occupation";
$lang['enq_parent_number'] = "Parent's Contact Number";
$lang['enq_educational_details'] = "Educational Qualification";
$lang['enq_physical_benchmark'] = "Physical Benchmark";
$lang['category'] = "Category";
$lang['chest'] = "Chest";
$lang['height'] = "Height";
$lang['weight'] = "Weight";
$lang['knees'] = "Knees";
$lang['eye'] = "Eye";
$lang['left_eye'] = "Left Eye";
$lang['right_eye'] = "Right Eye";
$lang['ear'] = "Ear";
$lang['left_ear'] = "Left Ear";
$lang['right_ear'] = "Right Ear";
$lang['foot'] = "Foot";
$lang['teeth'] = "Teeth";
$lang['nose'] = "Nose";
$lang['bp'] = "BP";
$lang['high_bp'] = "BP";
$lang['low_bp'] = "Low BP";
$lang['pr'] = "PR";
$lang['married'] = "Married";
$lang['unmarried'] = "Unmarried";
$lang['gen'] = "Gen";
$lang['obc'] = "OBC";
$lang['sc'] = "SC";
$lang['st'] = "ST";
$lang['attention_points'] = "Attention Points";
$lang['marital_status'] = "Marital Status";
$lang['date'] = "Date";
$lang['short_note'] = "Note";

$lang['exam_name'] = "Exam Name";
$lang['year'] = "Year";
$lang['subject'] = "Subject";
$lang['obtain_marks'] = "Obtain Marks";
$lang['max_marks'] = "Maximum Marks";
$lang['division'] = "Division";
$lang['percentage'] = "Percentage";
$lang['board'] = "Board/University";
$lang['secondary'] = "Secondary";
$lang['sr_secondary'] = "Senior Secondary";
$lang['ba_bsc_other'] = "B.A,B.Sc./Other";
$lang['ncc_sports'] = "NCC/Sports";

//buttons
$lang['proceed_next_mform'] = "Proceed to Medical Form";
$lang['proceed_next_aform'] = "Proceed to Admission Form";
$lang['btn_only_enq'] = "Only Enquiry";
$lang['btn_add'] = "Submit";
$lang['btn_edit'] = "Edit";
$lang['btn_delete'] = "Delete";
$lang['btn_view'] = "View";

$lang['add_enq'] = "Add an Enquiry";
$lang['medical_form_info'] = "Fill Medical Form";
$lang['admission_form_info'] = "Fill Admission Form";

//colums
$lang['s_no'] = "#";
$lang['reg_no'] = "Reg No.";
$lang['action'] = "Action";

//breadcrumbs
$lang['b_enquiry'] = "Enquiry";

//panel titles
$lang['panel_title1'] = "Enquiry";

//Student list
$lang['hostel_accessories'] = 'Hostel Accessories';
$lang['assign_hostel'] = 'Assign Hostel with Fees';
$lang['student_account'] = 'Student Account';


$lang['installment'] = 'Installments';
$lang['enq_status'] = 'Status';
$lang['enq_course_name'] = 'Course Name';
$lang['select_course'] = 'Select Course';
$lang['select_year'] = 'Select Year';

$lang['medical_form'] = 'Medical Form';
$lang['admission_form'] = 'Admission Form';
$lang['payment_method'] = 'Payment Method';

$lang['pay_installment'] = 'Pay in Installments';
$lang['next_installment_date'] = 'Next Installment Date';
$lang['choose_docs'] = 'Choose Documents';

$lang['placeholder_bank_name'] = 'Bank Name';
$lang['placeholder_cheque_no'] = 'Cheque No.';
$lang['placeholder_amount'] = 'Amount';

$lang['payment_method_arr'] = 'PAYMENT_METHODS';

$lang['acc_acc_details'] = 'Account Details';
$lang['acc_date'] = 'Date';
$lang['acc_desc'] = 'Description';
$lang['acc_amount'] = 'Amount';

$lang['hostel_reason'] = 'Reason';
$lang['hostel_segments'] = 'Segments';
$lang['add_new'] = 'Add New';
$lang['submit_btn'] = 'Submit';



