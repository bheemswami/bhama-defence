<?php

/* List Language  */
$lang['panel_title'] = 'Make Payment';
$lang['add_title'] = 'Add a Make payment';

$lang['make_payment_make_payment'] = 'Make Payment';
$lang['make_payment_role'] = 'Role';
$lang['make_payment_select_role'] = 'Select Role';
$lang['action'] = 'Action';

$lang['slno'] = "#";
$lang['make_payment_photo'] = 'Photo';
$lang['make_payment_name'] = 'Name';
$lang['make_payment_email'] = 'Email';
$lang['make_payment_jod'] = 'Joining Date';
$lang['make_payment_dob'] = 'Date of Birth';

$lang['make_payment_salary_grades'] = "Salary Grades";
$lang['make_payment_basic_salary'] = "Basic Salary";
$lang['make_payment_overtime_rate'] = "Overtime Rate ( Per Hour)";
$lang['make_payment_allowances'] = "Allowances";
$lang['make_payment_deductions'] = "Deductions";

$lang['make_payment_hourly_rate'] = "Hourly Rate";
$lang['make_payment_net_hourly_rate'] = 'Net Hourly Rate';
// $lang['make_payment_net_hourly_rate'] = 'Total';

$lang['make_payment_total_salary_details'] = "Total Salary Details";
$lang['make_payment_gross_salary'] = "Gross Salary";
$lang['make_payment_total_deduction'] = "Total Deduction";
$lang['make_payment_net_salary'] = "Net Salary";
$lang['make_payment_net_salary_hourly'] = "Net Salary (Hourly)";
$lang['make_payment_month'] = "Month";

$lang['make_payment_payment_amount'] = 'Payment Amount';
$lang['make_payment_payment_method'] = 'Payment Method';
$lang['make_payment_comments'] = 'Comments';
$lang['make_payment_total_houres'] = 'Total Houres';


$lang['make_payment_select_payment_method'] = 'Select Payment Method';
$lang['make_payment_payment_cash'] = 'Cash';
$lang['make_payment_payment_cheque'] = 'Cheque';


$lang['make_payment_payment_history'] = 'Payment History';
$lang['make_payment_amount'] = 'Amount';
$lang['make_payment_date'] = 'Payment Date';
$lang['view'] = 'View';
$lang['delete'] = 'Delete';
$lang['print'] = 'Print';

$lang['add_payment'] = 'Add Payment';

$lang['mp_treasury_num'] = 'Treasury Bill No.';
$lang['mp_additional_pay'] = 'Additional Pay';
$lang['mp_interim_relief'] = 'Interim Relief';
$lang['mp_other_allowance'] = 'Other Allowance';
$lang['mp_house_rent'] = 'House Rent';
$lang['mp_advance'] = 'Advance';
$lang['mp_provident_fund'] = 'Provident Fund';
$lang['mp_insurance_fund'] = 'Insurance Fund';
$lang['mp_other_deduction'] = 'Other Deduction';
$lang['mp_policy_num'] = 'Policy No.';
$lang['mp_total_salary'] = 'Total Addition';
$lang['mp_total_deduction'] = 'Total Deduction';
$lang['make_payment_gender'] = 'Gender';
$lang['make_payment_dob'] = 'Date of Birth';
$lang['make_payment_phone'] = 'Phone';
$lang['make_payment_salary'] = 'Salary';
$lang['make_payment_personal_information'] = 'Personal Information';
$lang['account_no'] = "A/ C Number";
$lang['make_payment_payment_online'] = "Online";
$lang['cheque_no'] = "Cheque No.";
