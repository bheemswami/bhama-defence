<?php

/* List Language  */
$lang['panel_title'] = "Lecturer";
$lang['add_title'] = "Add a lecturer";
$lang['slno'] = "#";
$lang['teacher_photo'] = "Photo";
$lang['teacher_name'] = "Name"; 
$lang['teacher_designation'] = 'Designation';
$lang['teacher_email'] = "Email";
$lang['teacher_dob'] = "Date of Birth";
$lang['teacher_sex'] = "Gender";
$lang['teacher_sex_male'] = "Male";
$lang['teacher_sex_female'] = "Female";
$lang['teacher_religion'] = "Religion";
$lang['teacher_phone'] = "Phone";
$lang['teacher_address'] = "Address";
$lang['teacher_jod'] = "Joining Date";
$lang['teacher_username'] = "Username";
$lang['teacher_password'] = "Password";
$lang['teacher_status'] = "Status";

$lang['teacher_file_browse'] = 'File Browse';
$lang['teacher_clear'] = 'Clear';


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['print'] = "Print";
$lang['pdf_preview'] = "PDF Preview";
$lang['idcard'] = "ID Card";
$lang['mail'] = "Send Pdf to Mail";

/* Add Language */
$lang['personal_information'] = "Personal Information";
$lang['add_teacher'] = 'Add Lecturer';
$lang['update_teacher'] = 'Update Lecturer';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';
$lang['resign_reason'] = "Resign Reason";
$lang['resign_date'] = "Resign Date";
$lang['teacher_book'] = "Book";
$lang['teacher_author'] = "Author";
$lang['teacher_serial_no'] = "Serial No.";
$lang['teacher_issue_date'] = "Issue Date";
$lang['teacher_due_date'] = "Due Date";
$lang['teacher_return_date'] = "Return Date";
$lang['book_issue_history'] = "Book Issue History";
$lang['library_books'] = "Library Books";
$lang['teacher_blood_group'] = "Blood Group";
$lang['teacher_salary'] = "Salary";