<?php

$lang['panel_title'] = "आयात";
$lang['add_title'] = "एक आयात जोड़ें";
$lang['slno'] = "#";
$lang['import_title'] = "शीर्षक";
$lang['import_import'] = "आयात";
$lang['import_date'] = "तारीख";
$lang['bulkimport_teacher'] = "शिक्षक जोड़ें";
$lang['bulkimport_student'] = "छात्र जोड़ें";
$lang['bulkimport_parent'] = "अभिभावक जोड़ें";
$lang['bulkimport_user'] = "उपयोगकर्ता जोड़ें";
$lang['bulkimport_book'] = "बुक जोड़ें";
$lang['bulkimport_submit'] = "आयात";
$lang['add_class'] = "आयात जोड़ें";
$lang['upload_file'] = "फ़ाइल आयात करें";
$lang['import_file_type_error'] = "अवैध फाइल";
$lang['import_error'] = "उफ़! डेटा आयात नहीं किया गया! कृपया पुनः प्रयास करें।";
$lang['import_success'] = "डेटा सफलतापूर्वक जोड़ा गया";
$lang['bulkimport_sample'] = "नमूना डाउनलोड करें";

$lang['import_s_campaign_btn'] = "इम्पोर्ट स्कूल अभियान";
$lang['import_r_work_btn'] = "इम्पोर्ट रिसेप्शनिस्ट कार्य";

$lang['bulkimport_s_campaign'] = "स्कूल अभियान जोड़ें";
$lang['bulkimport_r_work'] = "रिसेप्शनिस्ट कार्य जोड़ें";
$lang['list']='सूची';
$lang['data_date']='दिनांक';
$lang['sno']='#';
$lang['school_name']='विद्यालय का नाम';
$lang['school_type']='विद्यालय का प्रकार';
$lang['principal']='प्रधानाचार्य';
$lang['owner']='मालिक';
$lang['contact_no']='संपर्क नंबर';
$lang['contact_no_2']='संपर्क नंबर 2';
$lang['subject']='विषय';
$lang['other']='अन्य';
$lang['remark']='टिप्पणी';
$lang['name']='नाम';
$lang['course']='कोर्स';
$lang['action']='कार्य';
$lang['upload']='फ़ाइल चुनें';
$lang['btn_back']='वापस';
