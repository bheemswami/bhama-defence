<?php

$lang['panel_title'] = "कोर्स";
$lang['add_title'] = "एक कोर्स जोड़ें";
$lang['slno'] = "#";
$lang['classes_name'] = "कोर्स";
$lang['classes_numeric'] = "कोर्स संख्यात्मक";
$lang['teacher_name'] = "अध्यापक का नाम";
$lang['classes_studentmaxID'] = "छात्र मैक्स आईडी";
$lang['classes_note'] = "ध्यान दें";
$lang['action'] = "कार्य";
$lang['classes_select_teacher'] = "शिक्षक का चयन करें";
$lang['view'] = "राय";
$lang['edit'] = "संपादित करें";
$lang['delete'] = "हटाना";
$lang['add_class'] = "कोर्स जोड़ें";
$lang['update_class'] = "अद्यतन कोर्स";
$lang['course_fee'] = 'कोर्स फीस';