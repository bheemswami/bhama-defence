<?php

// $lang['panel_title'] = "व्यय";
$lang['panel_title'] = "वाउचर";
$lang['add_title'] = "एक वाउचर जोड़ें";
$lang['slno'] = "#";
$lang['expense_expense'] = "नाम";
$lang['expense_date'] = "तारीख";
$lang['expense_amount'] = "रकम";
$lang['expense_note'] = "ध्यान दें";
$lang['expense_uname'] = "उपयोगकर्ता";
$lang['expense_total'] = "कुल";
$lang['action'] = "कार्य";
$lang['view'] = "राय";
$lang['edit'] = "संपादित करें";
$lang['delete'] = "हटाना";
$lang['add_expense'] = "वाउचर जोड़ें";
$lang['update_expense'] = "वाउचर का अद्यतन करें";

$lang['expense_type'] = 'Select Type';
$lang['expense_type_th'] = 'Type';
$lang['expense_file'] = 'File';
$lang['file_browse'] = 'File Browse';
$lang['file_clear'] = 'Clear';
$lang['exp_inc_type'] = 'संपत्ति टैग';
$lang['exp_inc_type_th'] = 'संपत्ति टैग';
$lang['exp_inc_permanent'] = 'स्थायी';
$lang['exp_inc_non_permanent'] = 'कोई स्थायी नहीं';

$lang['exp_inc_for'] = 'के लिए जोड़ें';
$lang['exp_inc_for_th'] = 'के लिये';
$lang['exp_inc_for_academic'] = 'शैक्षिक';
$lang['exp_inc_for_hostel'] = 'छात्रावास';

$lang['exp'] = 'व्यय';
$lang['inc'] = 'आय';
$lang['pay'] = 'भुगतान';
$lang['rec'] = 'रसीद';
$lang['oth'] = 'अन्य';

$lang['pay_status'] = 'भुगतान स्थिति';
$lang['pay_status_paid'] = 'भुगतान किया हुआ';
$lang['pay_status_due'] = 'देय';

$lang['is_withdraw'] = 'निकालना';

$lang['pdf_hint'] = 'To downlaod PDF with eiher only Expenses or Income information, type "Expense" or "Income" in search field and click on PDF button.';
$lang['print'] = "छाप";
$lang['vendor'] = "विक्रेता";

$lang['payment_cash'] = 'नकद';
$lang['payment_cheque'] = 'चेक';
$lang['payment_online'] = "ऑनलाइन";
$lang['payment_method'] = 'भुगतान का तरीका';
$lang['select_payment_method'] = 'भुगतान का तरीका चुनें';
$lang['cheque_no'] = "चेक संख्या";

$lang['th_total_income'] = "कुल आय";
$lang['th_total_expense'] = "कुल खर्च";
$lang['th_due_paument'] = "बकाया भुगतान";
$lang['th_due_receipt'] = "बकाया प्राप्ति";