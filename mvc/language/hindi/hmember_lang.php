<?php

$lang['panel_title'] = "छात्रावास का सदस्य";
$lang['panel_title_profile'] = "प्रोफाइल";
$lang['slno'] = "#";
$lang['hmember_photo'] = "तस्वीर";
$lang['hmember_name'] = "नाम";
$lang['hmember_section'] = "अनुभाग";
$lang['hmember_roll'] = "रोल";
$lang['hmember_email'] = "ईमेल";
$lang['hmember_phone'] = "फ़ोन";
$lang['hmember_tfee'] = "छात्रावास शुल्क";
$lang['hmember_classes'] = "कक्षा";
$lang['hmember_select_class'] = "कक्षा चुनें";
$lang['hmember_hname'] = "छात्रावास का नाम";
$lang['hmember_class_type'] = "कक्षा प्रकार";
$lang['hmember_htype'] = "प्रकार";
$lang['hmember_select_class_type'] = "कक्षा प्रकार का चयन करें";
$lang['hmember_select_hostel_name'] = "छात्रावास का चयन करें";
$lang['hmember_select_student'] = "विद्यार्थी का चयन करें";
$lang['hmember_all_students'] = "सभी विद्यार्थी";
$lang['hmember_hname'] = "छात्रावास का नाम";
$lang['hmember_joindate'] = "शामिल होने की तारीख";
$lang['hmember_dob'] = "जन्म की तारीख";
$lang['hmember_sex'] = "लिंग";
$lang['hmember_religion'] = "धर्म";
$lang['hmember_address'] = "पता";
$lang['hmember_hostel_address'] = "छात्रावास का पता";
$lang['hmember_message'] = "आप जोड़ नहीं रहे हैं";
$lang['hmember_registerNO'] = "कोई पंजीकरण करें";
$lang['hmember_bloodgroup'] = "रक्त समूह";
$lang['hmember_state'] = "राज्य";
$lang['hmember_country'] = "देश";
$lang['hmember_studentgroup'] = "समूह";
$lang['hmember_optionalsubject'] = "वैकल्पिक विषय";
$lang['hmember_extracurricularactivities'] = "अतिरिक्त पाठयक्रम गतिविधियों";
$lang['hmember_remarks'] = "टिप्पणियों";
$lang['action'] = "कार्य";
$lang['view'] = "राय";
$lang['edit'] = "संपादित करें";
$lang['hmember'] = "हॉस्टल जोड़ें";
$lang['delete'] = "हटाना";
$lang['pdf_preview'] = "पीडीएफ पूर्वावलोकन";
$lang['print'] = "छाप";
$lang["mail"] = "मेल को पीडीएफ भेजें";
$lang['personal_information'] = "व्यक्तिगत जानकारी";
$lang["add_hmember"] = "सदस्य जोड़ें";
$lang["update_hmember"] = "सदस्य का अद्यतन करें";
$lang['to'] = "सेवा मेरे";
$lang['subject'] = "विषय";
$lang['message'] = "संदेश";
$lang['send'] = "भेजना";
$lang['mail_to'] = "को फ़ील्ड की आवश्यकता है।";
$lang['mail_valid'] = "फ़ील्ड में एक मान्य ईमेल पता होना चाहिए।";
$lang['mail_subject'] = "विषय फ़ील्ड आवश्यक है";
$lang['mail_success'] = "ईमेल सफलतापूर्वक भेजें!";
$lang['mail_error'] = "उफ़! ईमेल न भेजें!";

$lang['installment'] = 'फीस के साथ हॉस्टल असाइन करें';
$lang['SN_No'] = '#';
$lang['amount'] = 'रकम';
$lang['date'] = 'दिनांक';
$lang['description'] = 'विवरण';
$lang['hostel_fee'] = 'छात्रावास की फीस';
$lang['total_installment'] = 'कुल किस्त';
$lang['remaining_fee'] = 'शेष शुल्क';

$lang['letter_add'] = 'जोड़ना';
$lang['letter_update'] = 'अद्यतन करें';
$lang['letter_list'] = 'सूची';
$lang['letter_sno'] = '#';
$lang['letter_remark'] = 'टिप्पणियों';
$lang['letter_subject'] = 'विषय';

$lang['despatch_panel_title'] = 'पत्र प्रेषण';
$lang['despatch_date'] = 'प्रेषण की तारीख';
$lang['despatch_name'] = 'नाम';
$lang['despatch_address'] = 'पता';
$lang['despatch_place'] = 'जगह';
$lang['despatch_file_head'] = 'फ़ाइल शीर्ष और नं.';
$lang['despatch_stamp_received'] = 'स्टाम्प प्राप्त हुआ';
$lang['despatch_stamp_used'] = 'स्टाम्प का उपयोग किया';


$lang['receipt_panle_title'] = 'पत्र प्राप्ति';
$lang['receipt_date'] = 'प्राप्ति की तारीख';
$lang['receipt_letter_date'] = 'पत्र की तारीख';
$lang['receipt_letter_no'] = 'पत्र सं.';
$lang['receipt_from'] = 'जिनसे प्राप्त हुआ';
$lang['receipt_file_head'] = 'फ़ाइल शीर्ष और नं.';
$lang['receipt_disposal'] = 'जगह';
$lang['receipt_reply_date'] = 'उत्तर दिनांक';
$lang['receipt_reply_no'] = 'उत्तर नं.';
$lang['hmember_status'] = 'स्थिति';


$lang['out_date'] = 'दिनांक';
$lang['out_time'] = 'बाहर जाने का समय';
$lang['in_time'] = 'आने का समय ';
$lang['total_time'] = 'कितने घंटे';
$lang['reason'] = 'कारण';

$lang['hostel_fine'] = 'छात्रावास जुर्माना';
$lang['amount'] = 'राशी';
$lang['fine_date'] = 'जुर्माना दिनांक';
$lang['hostel_accessories'] = 'छात्रावास का सामान';