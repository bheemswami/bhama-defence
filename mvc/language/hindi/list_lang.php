<?php
//enquries (Prefix : enq_)
$lang['enq_panel_title'] = "पूछताछ जोड़ें";
$lang['enq_student_name'] = "छात्र का नाम";
$lang['enq_student_dob'] = "जन्म की तारीख";
$lang['enq_student_address'] = "स्थाई पता";
$lang['enq_father_name'] = "पिता का नाम";
$lang['enq_mother_name'] = "माता का नाम";
$lang['enq_father_business'] = "व्यापार";
$lang['enq_mother_business'] = "व्यापार";
$lang['enq_parent_number'] = "अभिभावक का मोबाइल नंबर";
$lang['enq_educational_details'] = "शैक्षणिक योग्यता ";
$lang['category'] = "श्रेणी";

$lang['exam_name'] = "परीक्षा का नाम";
$lang['year'] = "वर्ष";
$lang['subject'] = "विषय";
$lang['obtain_marks'] = "प्राप्तांक";
$lang['max_marks'] = "पूर्णांक";
$lang['division'] = "श्रेणी";
$lang['percentage'] = "प्रतिशत";
$lang['board'] = "बोर्ड / विश्वविद्यालय";
$lang['secondary'] = "सैकण्डरी";
$lang['sr_secondary'] = "सीनियर सैकण्डरी";
$lang['ba_bsc_other'] = "B.A,B.Sc./Other";
$lang['ncc_sports'] = "NCC/खेलकूद";

$lang['chest'] = "Chest";
$lang['height'] = "Height";
$lang['weight'] = "Weight";
$lang['knees'] = "Knees";
$lang['eye'] = "Eye";
$lang['left_eye'] = "Left Eye";
$lang['right_eye'] = "Right Eye";
$lang['ear'] = "Ear";
$lang['left_ear'] = "Left Ear";
$lang['right_ear'] = "Right Ear";
$lang['foot'] = "Foot";
$lang['teeth'] = "Teeth";
$lang['nose'] = "Nose";
$lang['bp'] = "BP";
$lang['high_bp'] = "BP";
$lang['low_bp'] = "Low BP";
$lang['pr'] = "PR";
$lang['married'] = "Married";
$lang['unmarried'] = "Unmarried";
$lang['gen'] = "Gen";
$lang['obc'] = "OBC";
$lang['sc'] = "SC";
$lang['st'] = "ST";
$lang['attention_points'] = "ध्यानार्थ बिंदु ";
$lang['marital_status'] = "वैवाहिक स्थिति";
$lang['date'] = "दिनांक";
$lang['short_note'] = "नोट";


//buttons
$lang['proceed_next_mform'] = "चिकित्सा प्रपत्र के लिए आगे बढ़ें";
$lang['proceed_next_aform'] = "प्रवेश फार्म के लिए आगे बढ़ें";
$lang['btn_only_enq'] = "केवल पूछताछ";
$lang['btn_add'] = "जोड़ें";

$lang['add_enq'] = "पूछताछ जोड़ें";
$lang['medical_form_info'] = "मेडिकल फॉर्म भरें";
$lang['admission_form_info'] = "प्रवेश फॉर्म भरें";

//colums
$lang['s_no'] = "#";
$lang['reg_no'] = "पंजीकरण संख्या";
$lang['action'] = "कार्य";

//breadcrumbs
$lang['b_enquiry'] = "पूछताछ";

//panel titles
$lang['panel_title1'] = "पूछताछ";
$lang['hostel_accessories'] = 'छात्रावास का सामान';
$lang['assign_hostel'] = 'फीस के साथ छात्रावास सौंपें';
$lang['student_account'] = 'छात्र का खाता';
$lang['enq_status'] = 'स्थिति';
$lang['enq_course_name'] = 'पाठ्यक्रम का नाम';
$lang['select_course'] = 'पाठ्यक्रम का चयन करें';
$lang['select_year'] = 'वर्ष का चयन करें';

$lang['medical_form'] = 'चिकित्सा प्रपत्र';
$lang['admission_form'] = 'प्रवेश फॉर्म';
$lang['payment_method'] = 'भुगतान का तरीका';

$lang['pay_installment'] = 'किश्तों में भुगतान करें';
$lang['next_installment_date'] = 'अगली किस्त की तारीख';
$lang['choose_docs'] = 'दस्तावेज़ चुनें';

$lang['placeholder_bank_name'] = 'बैंक का नाम';
$lang['placeholder_cheque_no'] = 'चेक नंबर';
$lang['placeholder_amount'] = 'राशि';

$lang['payment_method_arr'] = 'PAYMENT_METHODS_HINDI';

$lang['acc_acc_details'] = 'खाता विवरण';
$lang['acc_date'] = 'तारीख';
$lang['acc_desc'] = 'विवरण';
$lang['acc_amount'] = 'राशि';

$lang['hostel_reason'] = 'कारण';
$lang['hostel_segments'] = 'सामान';
$lang['add_new'] = 'नया जोड़ें';
$lang['submit_btn'] = 'जोड़ें';