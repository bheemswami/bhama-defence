<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_m extends MY_Model {

	function __construct() {
		parent::__construct();
        $this->load->model('student_m');
        $this->load->model('user_m');
        $this->load->model("lmember_m");
        $this->load->model('studentextend_m');
        $this->load->model('studentrelation_m');
	}
    function parentPhptoSignUpload($parentID){
        $array = ['photo'=>'','sign'=>''];
        if(isset($_FILES['parent_photo'])){
            unset($_FILES['sign']);
            unset($_FILES['photo']);
            $_FILES['photo'] = $_FILES['parent_photo'];
            $_FILES['sign'] = $_FILES['parent_sign'];
        }
        $parentData = $this->db->select('*')->where('parentsID',$parentID)->get('parents')->row();
        if(isset($_FILES['photo']) && $_FILES['photo']['name']!=''){
            unlinkImage($parentData->photo,'uploads/images');
            $file = $this->uploadSingleFile($_FILES,'uploads/images/');
            $array["photo"] = $file['file_name'];
        }
        if(isset($_FILES['sign']) && $_FILES['sign']['name']!=''){
            unlinkImage($parentData->sign,'uploads/parent_sign');
            $file = $this->uploadSingleFile($_FILES,'uploads/parent_sign/');
            $array["sign"] = $file['file_name'];
        }
        if($array["photo"] != '' || $array["sign"] !='' ){
            $this->db->where('parentsID',$parentID)->update('parents',$array);
        }
    }
	function insert_student() {
       
        $regNo = $this->uniqueRegisterNO();
        $rollNo = $regNo;
        // insert parent details 
        $parentArr=[
            'name'=>$this->input->post('father_name'), 
            'father_name'=>$this->input->post('father_name'), 
            'mother_name'=>$this->input->post('mother_name'), 
            'father_profession'=>$this->input->post('father_business'), 
            'mother_profession'=>$this->input->post('mother_business'), 
            'email'=>$this->genEmail($this->input->post('father_name'),$regNo,4), 
            'username'=>$this->genUsername($this->input->post('father_name'),$regNo,4),
            'password' => $this->user_m->hash($this->input->post("password")),
            'phone'=>$this->input->post('parent_mob'), 
            'address'=>$this->input->post('address'), 
            'photo' => '',
            'sign' => '',
            'usertypeID' => 4,
            'create_date' => date("Y-m-d h:i:s"),
            'modify_date' => date("Y-m-d h:i:s"),
            'create_userID' => $this->session->userdata('loginuserID'),
            'create_username' => $this->session->userdata('username'),
            'create_usertype' => $this->session->userdata('usertype'),
            'active' => 1,
        ];

        $parentID = parent::insert_data('parents',$parentArr);
        $this->parentPhptoSignUpload($parentID);
        
        //insert student details
        if(isset($_FILES['photo'])){
            $file = $this->uploadSingleFile($_FILES,'uploads/student_photo/');
            $filename = $file['file_name'];
        } else {
            $filename ='';
        }
        if(isset($_FILES['sign'])){
            $file = $this->uploadSingleFile($_FILES,'uploads/student_sign/');
            $sign = $file['file_name'];
        } else {
            $sign ='';
        }
        
		$array=[
            'roll'=>$rollNo,
            'registerNO'=>$regNo,
        	'name'=>$this->input->post('name'), 
        	'dob'=>date("Y-m-d", strtotime($this->input->post("dob"))),
            'marital_status'=>'', 
        	'classesID'=>$this->input->post('classesID'), 
            'email' => $this->genEmail($this->input->post('name'),$regNo,3),
            'username' => $this->genUsername($this->input->post('name'),$regNo,3),
            'password' => $this->user_m->hash($regNo),
            'usertypeID' => 3,
            'parentID' => $parentID,
            'library' => 0,
            'hostel' => 0,
            'library' => 1,
            'transport' => 0,
            'createschoolyearID' => $this->data['siteinfos']->school_year,
            'schoolyearID' => $this->data['siteinfos']->school_year,
            'create_date' => date("Y-m-d h:i:s"),
            'modify_date' => date("Y-m-d h:i:s"),
            'create_userID' => $this->session->userdata('loginuserID'),
            'create_username' => $this->session->userdata('username'),
            'create_usertype' => $this->session->userdata('usertype'),
            'active' => 0,
            'addmission_status' => 0,
            'photo' => $filename,
            'sign' => $sign,
        ];
        $studentID = parent::insert_data('student',$array);

        $arrayLibrary = array(
            "lID" => $this->genUsername($this->input->post('name'),$regNo,3),
            "studentID" => $studentID,
            "name" => $this->input->post('name'),
            "email" => $this->genEmail($this->input->post('name'),$regNo,3),
            "phone" => $this->input->post('parent_mob'),
            "lbalance" => '0.00',
            "ljoindate" => date("Y-m-d")
        );
        $this->lmember_m->insert_lmember($arrayLibrary);               

        $classes = $this->classes_m ->get_classes($this->input->post("classesID"));
        $arrayStudentRelation = array(
            'srstudentID' => $studentID,
            'srname' => $this->input->post("name"),
            'srclassesID' => $this->input->post("classesID"),
            'srclasses' => count($classes) ? $classes->classes : NULL,
            'srroll' => $rollNo,
            'srregisterNO' => $regNo,
            'srsectionID' => 1,
            'srsection' => NULL,
            'srstudentgroupID' => 1,
            'sroptionalsubjectID' => 0,
            'srschoolyearID' => $this->data['siteinfos']->school_year
        );

        $studentExtendArray = array(
            'studentID' => $studentID,
            'studentgroupID' => 1,
            'optionalsubjectID' => 0,
            'extracurricularactivities' => NULL,
            'remarks' => $this->input->post('remarks')
        );

        $this->studentextend_m->insert_studentextend($studentExtendArray);
        $this->studentrelation_m->insert_studentrelation($arrayStudentRelation);

        //insert education details
        if($studentID){
        	for($i=0;$i<4;$i++){
        		$eduArray[]=[
        			'student_id'=>$studentID,
        			'exam_name'=>$this->input->post('edu')['exam_name'][$i],
        			'year'=>$this->chkValueNull($this->input->post('edu')['year'][$i]),
        			'subjects'=>$this->input->post('edu')['subject'][$i],
        			'total_marks'=>$this->chkValueNull($this->input->post('edu')['max_marks'][$i]),
        			'obtain_marks'=>$this->chkValueNull($this->input->post('edu')['obtain_marks'][$i]),
        			'division'=>$this->input->post('edu')['division'][$i],
        			'percentage'=>$this->chkValueNull($this->input->post('edu')['percentage'][$i]),
        			'borad'=>$this->input->post('edu')['board'][$i],
        		];
	        }
	        parent::insert_batch('bd_student_edu',$eduArray);
        }
       return $studentID;
    }

    function insertBatch($tbl,$data){
        return parent::insert_batch($tbl,$data);
    }
    function insertData($tbl,$data){
        return parent::insert_data($tbl,$data);
    }

    function getStudents($id=null) {
        if($id > 0) {
            return $this->db->select('*')->where('studentID',$id)->get('student')->result()[0];
        } else{
            return $this->db->select('*')->order_by('studentID','DESC')->get('student')->result();
        }
    }

    function getPlans($planId=null) {
        if($planId > 0) {
            return $this->db->select('*')->where('classesID',$planId)->get('classes')->result()[0];
        } else{
            return $this->db->select('*')->get('classes')->result();
        }
    }

/* ********** Start Payment Add/Update Functions ********** */ 
    function insertPaymentData($data){
        $paymentdate = date('Y-m-d');
        $paymentday = date('d');
        $paymentmonth = date('m');
        $paymentyear = date('Y');
        if(isset($data['paymentdate'])){
            $paymentdate = date('Y-m-d',strtotime($data['paymentdate']));
            $paymentday = date('d',strtotime($data['paymentdate']));
            $paymentmonth = date('m',strtotime($data['paymentdate']));
            $paymentyear = date('Y',strtotime($data['paymentdate']));
        }
        $invoicID = $this->uniqueInvoiceNO();
        $postDataPayment = [
            'studentID'=>$data['studentID'],
            'paymenttype' => $data['paymenttype'],
            'paymentbank' => $data['paymentbank'],
            'chequeno' => $data['chequeno'],
            'paymentamount' => $data['paymentamount'],            
            "invoiceID" => $invoicID,
            'schoolyearID' => $this->data['siteinfos']->school_year,
            "paymentdate" => $paymentdate,
            "paymentday" => $paymentday,
            "paymentmonth" => $paymentmonth,
            "paymentyear" => $paymentyear,
            'userID' => $this->session->userdata('loginuserID'),
            'usertypeID' => $this->session->userdata('usertypeID'),
            'uname' => $this->session->userdata('name'),
            'transactionID' => rand(10,99999).$invoicID,
            'fee_type'=>$data['fee_type'],
            'purpose'=>$data['purpose'],
            'credit_debit'=>$data['credit_debit'],
            'remark'=>(isset($data['remark'])) ? $data['remark'] : null,
        ];
        
        $paymentIdExist = $this->db->where('paymentId',$data['payID'])->count_all_results('payment');
        if($data['payID']>0 && $paymentIdExist>0){
            $this->db->where('paymentId',$data['payID'])->update('payment',$postDataPayment);
            return $data['payID'];
        } else {
            return $this->insertData('payment', $postDataPayment);
        }
    }
/* ********** End Payment Add/Update Functions ********** */ 
/* ********** Start Other Functions ********** */
    function chkValueNull($val){
        if($val==''){
            return NULL;
        }
        return $val;
    }
    function uniqueRegisterNO() {
        $regData = $this->db->select('*')->order_by('studentID','DESC')->get('student')->result()[0];
        if($regData){
            $nextRegNo = $regData->registerNO+1;
        } else {
            $nextRegNo ='5001';
        }
        return $nextRegNo;
    }
    function uniqueInvoiceNO() {
        $res = $this->db->select('*')->order_by('paymentID','DESC')->get('payment')->result()[0];
        if($res){
            $nextInvoiceNo = $res->invoiceID+1;
        } else {
            $nextInvoiceNo ='5001';
        }
        return $nextInvoiceNo;
    }
    function genEmail($name,$val,$type){
        
        if($name!=''){
            $exp = explode(' ', $name);
            $name = strtolower($exp[0]);
        }
        return $name.substr($val,-3).$type.'@bhama.in';
    }
    function genUsername($name,$val,$type){
        
        if($name!=''){
            $exp = explode(' ', $name);
            $name = strtolower($exp[0]);
        }
        return $name.substr($val,-3).$type;
    }

    function uploadMultiFiles($files,$prefix=null){
        $filenames=[];
        $config = array(
            'upload_path'   => 'uploads/docs/',
            'allowed_types' => 'gif|png|jpg|jpeg|GIF|PNG|JPG|JPEG',
            'overwrite'     => 1,                       
        );
        $this->load->library('upload');
        $filesArr = $files["doc_file"];
        
        foreach ($filesArr['name'] as $key => $image) {
            if($image!=''){
                if($prefix==null){
                    $prefixName=rand(1,9999);
                }
                $_FILES['doc_file']['name']= $filesArr['name'][$key];
                $_FILES['doc_file']['type']= $filesArr['type'][$key];
                $_FILES['doc_file']['tmp_name']= $filesArr['tmp_name'][$key];
                $_FILES['doc_file']['error']= $filesArr['error'][$key];
                $_FILES['doc_file']['size']= $filesArr['size'][$key];
                $config['file_name'] = $prefixName .'_'. $image;
                $this->upload->initialize($config);
                if ($this->upload->do_upload('doc_file')) {
                    $this->upload->data();
                    $filenames[]=['name'=>$config['file_name']];
                } else {
                    //$this->upload->display_errors();
                }
            }
        }
        return $filenames;
    }
    function uploadSingleFile($file,$upload_path=null,$prefix=null){
        $filenames=[];
       
        $config = array(
            'upload_path'   => $upload_path,
            'allowed_types' => 'gif|png|jpg|jpeg|GIF|PNG|JPG|JPEG',
            'overwrite'     => 1, 
            'max_size'      => 1024, 
            //'max_width'      => 1024, 
            //'max_height'      => 1024, 
        );
        $new_file = "";
        $fileArr = $file["photo"];
        $fieldName = 'photo';
        if($upload_path=='uploads/student_sign/'){
            $fileArr = $file["sign"];
            $fieldName = 'sign';
            echo 12;
        } else {
            echo 34;
        }
        
        if($fileArr['name'] !="") {
            $file_name = $fileArr['name'];
            $random = rand(1, 99999999);
            //$makeRandom = hash('sha512', $random);
            $makeRandom = $random.'_'.$file_name;
            $file_name_rename = $makeRandom;
            $explode = explode('.', $file_name);
            if(count($explode) >= 2) {
                //$new_file = $file_name_rename.'.'.end($explode);
                $new_file = $file_name_rename;
                $config['file_name'] = $new_file;
                $this->load->library('upload');
                $this->upload->initialize($config);
                if(!$this->upload->do_upload($fieldName)) {
                    $file_data = array('file_name'=>'','error'=>$this->form_validation->set_message("photoupload", $this->upload->display_errors()));
                } else {
                    $file_data =  $this->upload->data();
                }
            } else {
                $file_data = array('file_name'=>'','error'=>$this->form_validation->set_message("photoupload", "Invalid file"));
            }
        } else {
            $file_data = array('file_name' => $new_file);
        }

        return $file_data;
    }
/* ********** End Other Functions ********** */ 


    function insert_record($tbl,$array,$batch=false) {
        if($batch){
            $this->db->insert_batch($tbl, $array);
            return true;
        }
        $this->db->insert($tbl, $array);
        $id = $this->db->insert_id();
        return $id;
    }
    function update_record($tbl,$data, $id = NULL) {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update($tbl);
    }
    function get_record($tbl,$array=NULL,$single=false,$with_student=false) {
        $this->db->select()->from($tbl.' bd_l');
        if($array != NULL) {
            $this->db->where($array);
        }
        if($with_student){
            $this->db->join('student s','s.studentID=bd_l.studentID','LEFT');
        }
        $query = $this->db->order_by('id','DESC')->get();
        if($single){
            return $query->row();
        }
        return $query->result();
    }
    function delete_record($tbl,$id,$idField='id'){
        $this->db->where($idField, $id);
        $this->db->delete($tbl);
        return TRUE;
    }
    function get_student_details ($id, $fields) {
        $this->db->select($fields);
        $this->db->from("student");
        $this->db->join("parents", "student.parentID = parents.parentsID", "LEFT");
        $this->db->where("student.studentID", $id);
        return $this->db->get()->result();
    }
    function get_details ($table_name, $where = NULL, $fields = NULL, $w = NULL, $returnType = NULL) {
        if ($table_name) $this->db->from($table_name);
        if ($fields) $this->db->select($fields);
        if ($where) $this->db->where($where);
        if ($w) $this->db->where($w[0], $w[1]);
        if ($returnType == 'array')
            return $this->db->get()->result_array();
        return $this->db->get()->result();
        // echo $this->db->last_query();
    }

   
}
