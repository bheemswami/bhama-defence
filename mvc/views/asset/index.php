<?php $userTypeId = $this->session->userdata('usertypeID');?>

<?php if($userTypeId==1 || $userTypeId==5){?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-fax"></i> <?=$this->lang->line('asset_for_academic')?> <?=$this->lang->line('panel_title')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_asset')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?php if(permissionChecker('asset_add')) { ?>
                    <h5 class="page-header">
                        <a href="<?php echo base_url('asset/add') ?>">
                            <i class="fa fa-plus"></i>
                            <?=$this->lang->line('add_title')?>
                        </a>
                    </h5>
                <?php } ?>
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('asset_serial')?></th>
                                <th><?=$this->lang->line('asset_description')?></th>
                                <th><?=$this->lang->line('asset_categoryID')?></th>
                                <th><?=$this->lang->line('stock_qty')?></th>
                                <th><?=$this->lang->line('used_qty')?></th>
                                <th><?=$this->lang->line('remain_qty')?></th>
                                <th><?=$this->lang->line('asset_condition')?></th>
                                <th><?=$this->lang->line('asset_status')?></th>
                                <?php /*<th><?=$this->lang->line('asset_locationID')?></th>*/ ?>
                                <?php if(permissionChecker('asset_edit') || permissionChecker('asset_delete') || permissionChecker('asset_view')) { ?>
                                    <th><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($assets)) {$i = 1; foreach($assets as $asset) { ?>

                                <?php if($asset->asset_for==0) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_serial')?>">
                                        <?php echo $asset->serial; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_description')?>">
                                        <?php echo $asset->description; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_categoryID')?>">
                                        <?php echo $asset->category; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('stock_qty')?>">
                                        <?php echo $asset->stock_qty; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('used_qty')?>">
                                        <?php echo $asset->used_qty; ?>
                                    </td>
                                     <td data-title="<?=$this->lang->line('remain_qty')?>">
                                        <?php echo ($asset->stock_qty-$asset->used_qty); ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_condition')?>">
                                        <?php 
                                        $arr = [1 => $this->lang->line('asset_condition_new'), 2 => $this->lang->line('asset_condition_used')];
                                        echo ($asset->asset_condition>0) ? $arr[$asset->asset_condition] : 'NA'; 
                                        ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_status')?>">
                                        <?php
                                            if($asset->status==1) {
                                                echo $this->lang->line('asset_status_checked_in');
                                            } else {
                                                echo $this->lang->line('asset_status_checked_out');
                                            }
                                        ?>
                                    </td>
                                    
                                    
                                   <?php /* <td data-title="<?=$this->lang->line('asset_locationID')?>">
                                        <?php echo namesorting($asset->location, 20); ?>
                                    </td> */ ?>
                                    <?php if(permissionChecker('asset_edit') || permissionChecker('asset_delete') || permissionChecker('asset_view')) { ?>
                                        <td data-title="<?=$this->lang->line('action')?>">
                                            <?php echo btn_view('asset/view/'.$asset->assetID, $this->lang->line('view')) ?>
                                            <?php echo btn_edit('asset/edit/'.$asset->assetID, $this->lang->line('edit')) ?>
                                            <?php echo btn_delete('asset/delete/'.$asset->assetID, $this->lang->line('delete')) ?>
                                        </td>
                                    <?php } ?>
                                </tr>

                                <?php $i++; } ?>
                            <?php  }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if($userTypeId==1 || $userTypeId==9){?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-fax"></i> <?=$this->lang->line('asset_for_hostel')?> <?=$this->lang->line('panel_title')?></h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                 <?php if(permissionChecker('asset_add')) { ?>
                    <h5 class="page-header">
                        <a href="<?php echo base_url('asset/add') ?>">
                            <i class="fa fa-plus"></i>
                            <?=$this->lang->line('add_title')?>
                        </a>
                    </h5>
                <?php } ?>
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('asset_serial')?></th>
                                <th><?=$this->lang->line('asset_description')?></th>
                                <th><?=$this->lang->line('asset_categoryID')?></th>
                                <th><?=$this->lang->line('stock_qty')?></th>
                                <th><?=$this->lang->line('used_qty')?></th>
                                <th><?=$this->lang->line('remain_qty')?></th>
                                <th><?=$this->lang->line('asset_condition')?></th>
                                <th><?=$this->lang->line('asset_status')?></th>
                                <?php /*<th><?=$this->lang->line('asset_locationID')?></th>*/ ?>
                                <?php if(permissionChecker('asset_edit') || permissionChecker('asset_delete') || permissionChecker('asset_view')) { ?>
                                    <th><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($assets)) {$i = 1; foreach($assets as $asset) { ?>

                                <?php if($asset->asset_for==1) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_serial')?>">
                                        <?php echo $asset->serial; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_description')?>">
                                        <?php echo $asset->description; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_categoryID')?>">
                                        <?php echo $asset->category; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('stock_qty')?>">
                                        <?php echo $asset->stock_qty; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('used_qty')?>">
                                        <?php echo $asset->used_qty; ?>
                                    </td>
                                     <td data-title="<?=$this->lang->line('remain_qty')?>">
                                        <?php echo ($asset->stock_qty-$asset->used_qty); ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_condition')?>">
                                        <?php 
                                        $arr = [1 => $this->lang->line('asset_condition_new'), 2 => $this->lang->line('asset_condition_used')];
                                        echo ($asset->asset_condition>0) ? $arr[$asset->asset_condition] : 'NA'; 
                                        ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('asset_status')?>">
                                        <?php
                                            if($asset->status==1) {
                                                echo $this->lang->line('asset_status_checked_in');
                                            } else {
                                                echo $this->lang->line('asset_status_checked_out');
                                            }
                                        ?>
                                    </td>
                                    
                                    
                                   <?php /* <td data-title="<?=$this->lang->line('asset_locationID')?>">
                                        <?php echo namesorting($asset->location, 20); ?>
                                    </td> */ ?>
                                    <?php if(permissionChecker('asset_edit') || permissionChecker('asset_delete') || permissionChecker('asset_view')) { ?>
                                        <td data-title="<?=$this->lang->line('action')?>">
                                            <?php echo btn_view('asset/view/'.$asset->assetID, $this->lang->line('view')) ?>
                                            <?php echo btn_edit('asset/edit/'.$asset->assetID, $this->lang->line('edit')) ?>
                                            <?php echo btn_delete('asset/delete/'.$asset->assetID, $this->lang->line('delete')) ?>
                                        </td>
                                    <?php } ?>
                                </tr>

                                <?php $i++; } ?>
                            <?php  }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>