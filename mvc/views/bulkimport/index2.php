
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-upload"></i> <?=$this->lang->line('panel_title')?></h3>


        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_import')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="col-sm-6">
                    <a href="<?=base_url('bulkimport/school_campaign')?>" class="btn btn-success"><?=$this->lang->line('import_s_campaign_btn')?></a>
                </div>
                <div class="col-sm-6">
                    <a href="<?=base_url('bulkimport/receptionist_work')?>" class="btn btn-success"><?=$this->lang->line('import_r_work_btn')?></a>
                </div>
            </div>
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
$('.parentUpload').on('change', function() {
  $('.parent').val($(this).val());
});
$('.userUpload').on('change', function() {
  $('.user').val($(this).val());
});
$('.bookUpload').on('change', function() {
  $('.bookImport').val($(this).val());
});
$('.studentUpload').on('change', function() {
  $('.student').val($(this).val());
});
</script>
