
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-upload"></i> <?=$this->lang->line('panel_title')?></h3>


        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_import')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <form enctype="multipart/form-data" style="" action="<?=base_url('bulkimport/receptionist_work');?>" class="form-horizontal" role="form" method="post">
                    <div class="form-group">
                        <label for="csvSchoolCampain" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("bulkimport_r_work")?>
                            &nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="Download the sample excel file first then see the format and make a copy of that file and add your data with exact format which is used in our csv file then upload the file."></i>
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control bookImport" id="uploadFile" placeholder="Choose File" disabled />
                        </div>

                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload bookUpload" name="csvReceptionistWork" />
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>

                        <div class="col-md-1 rep-mar">
                            <a class="btn btn-info" href="<?=base_url('assets/csv/sample_receptionist_work.xls')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div>

                        <div class="col-md-2 rep-mar text-right">
                            <a class="btn btn-danger" href="<?=base_url('bulkimport/excel_import')?>"><i class="fa fa-arrow-left"></i> <?=$this->lang->line("btn_back")?></a>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('msg')): ?>
                    <div class="callout callout-danger">
                      <h4>These data not inserted</h4>
                      <p><?php echo $this->session->flashdata('msg'); ?></p>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                    <div class="callout callout-danger">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div><!-- row -->
    </div><!-- Body -->
</div>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('list')?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th><?=$this->lang->line('sno')?></th>
                            <th><?=$this->lang->line('data_date')?></th>
                            <th><?=$this->lang->line('name')?></th>
                            <th><?=$this->lang->line('contact_no')?></th>
                            <th><?=$this->lang->line('course')?></th>
                            <th><?=$this->lang->line('other')?></th>
                            <th><?=$this->lang->line('remark')?></th>
                            <th><?=$this->lang->line('action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($records)) {$i = 1; foreach($records as $record) { ?>
                            <tr>
                                <td><?=$i++?></td>
                                <td><?=date('Y-m-d',strtotime($record->data_date))?></td>
                                <td><?=$record->name?></td>
                               <td><?=$record->contact_no?></td>
                               <td><?=$record->course?></td>
                               <td><?=$record->other?></td>
                               <td><?=$record->remark?></td>
                                <td><?=btn_delete_show('bulkimport/delete/2/'.$record->id, $this->lang->line('delete'));?></td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
$('.parentUpload').on('change', function() {
  $('.parent').val($(this).val());
});
$('.userUpload').on('change', function() {
  $('.user').val($(this).val());
});
$('.bookUpload').on('change', function() {
  $('.bookImport').val($(this).val());
});
$('.studentUpload').on('change', function() {
  $('.student').val($(this).val());
});
</script>
