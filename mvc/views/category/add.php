
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-leaf"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("category/index")?>"><?=$this->lang->line('menu_category')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_category')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post">
                     <input type="hidden" class="form-control" name="hname" value="<?=$this->config->item('HOSTAL_ID')?>" >
                    <?php /*
                        if(form_error('hname')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="hname" class="col-sm-2 control-label">
                            <?=$this->lang->line("category_hname")?>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $array = array();
                                $array[0] = $this->lang->line("category_select_hostel");
                                foreach ($hostels as $hostel) {
                                    $array[$hostel->hostelID] = $hostel->name;
                                }
                                echo form_dropdown("hname", $array, set_value("hname"), "id='hname' class='form-control select2'");
                            ?>
                        
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('hname'); ?>
                        </span>
                    </div>
                    */ ?>
                    <?php 
                        if(form_error('class_type')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="class_type" class="col-sm-2 control-label">
                            <?=$this->lang->line("category_class_type")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="class_type" name="class_type" value="<?=set_value('class_type')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('class_type'); ?>
                        </span>
                    </div>
                     <?php 
                        if(form_error('room_num')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="class_type" class="col-sm-2 control-label">
                            <?=$this->lang->line("room_num")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="room_num" name="room_num" value="<?=set_value('room_num')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('room_num'); ?>
                        </span>
                    </div>
                    <?php /*
                        if(form_error('hbalance')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="hbalance" class="col-sm-2 control-label">
                            <?=$this->lang->line("category_hbalance")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="hbalance" name="hbalance" value="<?=set_value('hbalance')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('hbalance'); ?>
                        </span>
                    </div>
                    */ ?>
                    <div class="room_segments">
                        <div class='form-group' >
                            <label for="note" class="col-sm-2 control-label">
                                Facilities
                            </label>
                            <div class="col-sm-6">
                                <div class="col-sm-6" style="padding-left: 0px;"><input type="text" class="form-control" name="segments[product][]" value="" placeholder="Ex: Ac, Fan etc"></div>
                                <div class="col-sm-5" style="padding-left: 0px;"><input type="number" class="form-control" name="segments[quantity][]" value="" placeholder="Quantity"></div>
                                <div class="col-sm-1"><i class="fa fa-plus btn btn-success plus_icon"></i></div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        if(form_error('note')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="note" class="col-sm-2 control-label">
                            <?=$this->lang->line("category_note")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('note')?></textarea>
                        </div>
                         <span class="col-sm-4 control-label">
                            <?php echo form_error('note'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_category")?>" >
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.select2').select2();
    $('.plus_icon').click(function(){
        $('.room_segments').append('<div class="form-group segment_items">\
                        <label for="note" class="col-sm-2 control-label">\
                        </label>\
                        <div class="col-sm-6">\
                            <div class="col-sm-6" style="padding-left: 0px;"><input type="text" class="form-control" name="segments[product][]" value="" ></div>\
                            <div class="col-sm-5" style="padding-left: 0px;"><input type="number" class="form-control" name="segments[quantity][]" value="" ></div>\
                            <div class="col-sm-1"><i class="fa fa-minus btn btn-danger remove_icon"></i></div>\
                        </div>\
                    </div>'
        );
    });
    $(document).on('click','.remove_icon',function(){
        $(this).parents('div').parents('.segment_items').remove();
    });
</script>