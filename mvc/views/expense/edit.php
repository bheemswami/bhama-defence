
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("expense/index")?>"><?=$this->lang->line('menu_expense')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_expense')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="prev_amount" value="<?=$expense->amount?>" />
                    <input type="hidden" name="prev_amount" value="<?=$expense->amount?>" />
                    <input type="hidden" name="prev_is_withdraw" value="<?=$expense->is_withdraw?>" />
                    <?php 
                        if(form_error('expense_type')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="namea" class="col-sm-2 control-label">
                            <?=$this->lang->line("expense_type")?>
                        </label>
                        <div class="col-sm-6">
                            <!-- <?php
                                $array = $array = array(
                                    'expense'=>$this->lang->line('exp'),
                                    'income'=>$this->lang->line('inc'),
                                    'payment'=>$this->lang->line('pay'),
                                    'receipt'=>$this->lang->line('rec'),
                                    'other'=>$this->lang->line('oth'),
                                );
                                echo form_dropdown("type", $array, set_value("type", $expense->type), "id='type' class='form-control'");
                            ?> -->
                            <input type="text"  id="type" value="<?=$expense->type?>" class='form-control' readonly>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('expense_type'); ?>
                        </span>
                    </div>

                    <div class="form-group" id="vendor_div">
                        <label class="col-sm-2 control-label"><?=$this->lang->line('vendor')?></label>
                        <div class="col-sm-6">
                            <!-- <select class="form-control" name="vendor_Id">
                                <option value="">--SELECT--</option>
                                <?php foreach($vendors as $vendor) {?>
                                    <option <?=($expense->vendor_Id == $vendor->vendorID ? "selected" : '')?> value="<?=$vendor->vendorID?>"><?=$vendor->name?></option>
                                <?php } ?>
                            </select> -->
                            <input type="text"  id="" value="<?=$vendor->name?>" class="form-control" readonly>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for="exp_inc_for" class="col-sm-2 control-label">
                            <?=$this->lang->line("exp_inc_for")?>
                        </label>
                        <div class="col-sm-6">
                            <!-- <?php
                                $array = array(0=>$this->lang->line('exp_inc_for_academic'),1=>$this->lang->line('exp_inc_for_hostel'));
                                echo form_dropdown("exp_inc_for", $array, set_value("exp_inc_for", $expense->exp_inc_for), "id='exp_inc_for' class='form-control'");
                            ?> -->
                            <input type="text"  id="exp_inc_for" value="<?=$array[$expense->exp_inc_for]?>" class="form-control" readonly>
                        </div>
                    </div>

                     <div class='form-group'>
                        <label for="namea" class="col-sm-2 control-label">
                            <?=$this->lang->line("exp_inc_type")?>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $array = array(1=>$this->lang->line('exp_inc_permanent'),0=>$this->lang->line('exp_inc_non_permanent'));
                                echo form_dropdown("exp_inc_type", $array, set_value("type", $expense->exp_inc_type), "id='exp_inc_type' class='form-control'");
                            ?>
                        </div>
                    </div>
                    <?php 
                        if(form_error('expense')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="namea" class="col-sm-2 control-label">
                            <?=$this->lang->line("expense_expense")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="namea" name="expense" value="<?=set_value('expense', $expense->expense)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('expense'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-2 control-label">
                            <?=$this->lang->line("expense_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date" name="date" value="<?=set_value('date', date("d-m-Y", strtotime($expense->date)))?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-2 control-label">
                            <?=$this->lang->line("expense_amount")?>
                        </label>
                        <div class="col-sm-6">
                            
                           <input type="text" class="form-control" id="amount"  value="<?=set_value('amount', $expense->amount)?>" readonly>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>
                    <div class="form-group" id="vendor_div">
                    <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('pay_status')?> </label>
                        
                        <div class="col-sm-6">
                            
                        <input type="text" id="payment_status" value="<?=$expense->payment_status==1 ? 'Paid' : 'Due'?>" class="form-control col-sm-10" readonly />
                        </div>
                    </div>
                    


                    <div class="payment_method_elem form-group <?=(form_error('payment_method')) ? 'has-error' : ''?>">
                    <label class="col-sm-2 control-label"><?=$this->lang->line('payment_method')?></label>
                    <div class="col-sm-6">
                        <!-- <?php
                            $paymentArray = array(
                                '0' => $this->lang->line('select_payment_method'),
                                '1' => $this->lang->line('payment_cash'),
                                '2' => $this->lang->line('payment_cheque'),
                                '3' => $this->lang->line('payment_online')
                            );

                            echo form_dropdown("payment_method", $paymentArray, set_value('payment_method', $expense->payment_method),"id='payment_method' class='form-control'");
                            echo $payment_method;
                        ?> -->
                        <input readonly id="payment_method" class="form-control" type="text" value="<?=$paymentArray[$expense->payment_method]?>" />                        
                    </div>
                    <span class="col-sm-4 control-label">
                        <?php echo form_error('payment_method'); ?>
                    </span>
                    </div>

                    <div class='form-group' id="cheque_div">
                        <label class="col-sm-2 control-label"><?=$this->lang->line('cheque_no')?></label>
                        <div class="col-sm-6">
                            <input type="text" name="cheque_no" class="form-control" id="cheque_no" value="<?=set_value('cheque_no', $expense->cheque_no)?>">
                        </div>
                    </div>

                    <!-- <div class="form-group parrent-input withdraw_elem">
                     <label for="is_withdraw" class="col-sm-2 control-label"><?=$this->lang->line('is_withdraw')?> </label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="is_withdraw" id="is_withdraw" class="form-control custom-height-form custom-width-form display-in-line1" type="checkbox"  <?=($expense->is_withdraw==1) ? 'checked' : ''?>/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div> -->

                    <?php 
                        if(form_error('note')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="note" class="col-sm-2 control-label">
                            <?=$this->lang->line("expense_note")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="note" name="note" required><?=set_value('note', $expense->note)?></textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('note'); ?>
                        </span>
                    </div>

                    <?php
                        if(form_error('photo'))
                            echo "<div class='form-group has-error' >";
                        else
                            echo "<div class='form-group' >";
                    ?>
                        <label for="photo" class="col-sm-2 control-label">
                            <?=$this->lang->line("expense_file")?>
                        </label>
                        <div class="col-sm-6">
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="fa fa-remove"></span>
                                        <?=$this->lang->line('file_clear')?>
                                    </button>
                                    <div class="btn btn-success image-preview-input">
                                        <span class="fa fa-repeat"></span>
                                        <span class="image-preview-input-title">
                                        <?=$this->lang->line('file_browse')?></span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="photo"/>
                                    </div>
                                </span>
                            </div>
                        </div>

                        <span class="col-sm-4">
                            <?php echo form_error('photo'); ?>
                        </span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_expense")?>" >
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var selectedServiceType = '<?=$expense->type?>';
    var selectedPaymentMethod = '<?=$expense->payment_method?>';

    $("#date").datepicker();
    $(function() {

    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("<?=$this->lang->line('file_browse')?>");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function (){
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200,
            overflow:'hidden'
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("<?=$this->lang->line('file_browse')?>");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            $('.content').css('padding-bottom', '100px');
        }
        reader.readAsDataURL(file);
    });
});
$(function() {
    if($('#type').val() == 'income') {
        $('#vendor_div').hide();
    }
    if ($('#payment_method').val() == 2) {
        $('#cheque_div').show();
    } else {
        $('#cheque_div').hide();
    }
})
$('#type').change(function() {
    selectedServiceType = $(this).val();
    showWithdrawCheckbox();
    if ($(this).val() == 'expense') {
        $('#vendor_div').show();
    } else {
        //$('#vendor_div').hide();
    }
});

    $('#add_inc_exp').submit (function (event) {
        if ($('#payment_method').val() == 2) {
            if ($('#cheque_no').val() == '') {
                event.preventDefault();
                alert ("Cheque No. Number must be present in case of Cheque payment");
            }
        }
    })

    $("#payment_method").change(function () {
        selectedPaymentMethod = $(this).val();
        showWithdrawCheckbox();
        if ($(this).val() == 2) {
            $("#cheque_div").show();
        } else {
            $("#cheque_div").hide();
        }
    });

    var pStatus = <?=$expense->payment_status?>;
    if(pStatus==1) $('.payment_method_elem').show();
    else $('.payment_method_elem').hide();
    
    $('.display-in-line').click(function(){
        selectedPaymentMethod = '';
        showWithdrawCheckbox();
        $('.payment_method_elem').hide();
        $('#payment_method').val(0);
        if($(this).val()==1){
            $('.payment_method_elem').show();
        }
    });


    $('.withdraw_elem').hide();
    showWithdrawCheckbox();
    function showWithdrawCheckbox(){
        if(selectedServiceType=='expense' && selectedPaymentMethod==1){
            $('.withdraw_elem').show();
        } else {
            $('.withdraw_elem').hide();
            $('#is_withdraw').attr('checked',false);
        }

        if(selectedPaymentMethod!=2){
            $("#cheque_div").hide();
        }
    }
</script>
