
<?php $userTypeId = $this->session->userdata('usertypeID');?>

<?php if($userTypeId==1 || $userTypeId==5){?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i>&nbsp;&nbsp;<?=$this->lang->line('exp_inc_for_academic')?> <?=$this->lang->line('panel_title')?></h3>
       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_expense')?></li>
        </ol>
    </div><!-- /.box-header -->

    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?php if(permissionChecker('expense_add')) { ?>
                <h5 class="page-header">
                    <a href="<?php echo base_url('expense/add') ?>">
                        <i class="fa fa-plus"></i> 
                        <?=$this->lang->line('add_title')?>
                    </a>
                </h5>
                <?php } ?>
               
                <div class="callout callout-danger">
                        <p><b>Note:</b> <?=$this->lang->line('pdf_hint')?></p>
                    </div>
                <div id="hide-table">
                <button class="btn btn-default" onclick="print1(0)">Excel <i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer print1">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('expense_expense')?></th>
                                <th><?=$this->lang->line('expense_date')?></th>
                                <th><?=$this->lang->line('expense_uname')?></th>
                                <th><?=$this->lang->line('expense_amount')?></th>
                                <th><?=$this->lang->line('expense_type_th')?></th>
                                <th><?=$this->lang->line('exp_inc_type_th')?></th>
                                <th><?=$this->lang->line('vendor')?></th>
                                <?php /*<th><?=$this->lang->line('exp_inc_for_th')?></th> */ ?>
                                <th><?=$this->lang->line('expense_note')?></th>
                                <?php if(permissionChecker('expense_edit') || permissionChecker('expense_delete')) { ?>
                                <th><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i = 0;
                            $total_expense = $total_income = $total_payment = $total_receipt = $total_other = $final_amount = 0; 
                            if(count($expenses)) { 
                                foreach($expenses as $k=>$expense) { 
                                     if($expense->exp_inc_for==0){
                                        $i++;
                                        if($expense->type=='income'){
                                            $total_income =$total_income+$expense->amount;
                                        } else if($expense->type=='expense') {
                                            $total_expense =$total_expense+$expense->amount;
                                        } else if($expense->type=='payment') {
                                            $total_payment =$total_payment+$expense->amount;
                                        } else if($expense->type=='receipt') {
                                            $total_receipt =$total_receipt+$expense->amount;
                                        } else if($expense->type=='other') {
                                            $total_other =$total_other+$expense->amount;
                                        }
                                       ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('expense_expense')?>">
                                        <?php echo $expense->expense; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('expense_date')?>">
                                        <?php echo date("d M Y", strtotime($expense->date)); ?>
                                    </td>
                                    
                                    <td data-title="<?=$this->lang->line('expense_uname')?>">
                                        <?php echo $expense->uname; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('expense_amount')?>">
                                        &#8377; <?php echo $expense->amount; ?>
                                    </td>

                                     <td data-title="<?=$this->lang->line('expense_type_th')?>">
                                        <?php echo ucfirst($expense->type); ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('exp_inc_type')?>">
                                        <?php echo ($expense->exp_inc_type==1) ? $this->lang->line('exp_inc_permanent') : $this->lang->line('exp_inc_non_permanent'); ?>
                                    </td>

                                    <td>
                                        <?php echo ($expense->vendor_name) ? $expense->vendor_name : "N/a" ?>
                                    </td>

                                    <?php /* <td data-title="<?=$this->lang->line('exp_inc_for_th')?>">
                                        <?php echo ($expense->exp_inc_for==1) ? $this->lang->line('exp_inc_for_hostel') : $this->lang->line('exp_inc_for_academic'); ?>
                                    </td> */ ?>
                                    
                                    <td data-title="<?=$this->lang->line('expense_note')?>">
                                        <?php echo $expense->note; ?>
                                    </td>

                                    <?php if(permissionChecker('expense_edit') || permissionChecker('expense_delete')) { ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <a href="<?=base_url("expense/print_voucher/$expense->expenseID")?>" class="btn btn-danger btn-xs mrg" target="_blank"><i class="fa fa-print"></i></a>
                                        <?php echo btn_edit('expense/edit/'.$expense->expenseID, $this->lang->line('edit')) ?>
                                        <?php 
                                        if($expense->files!='') echo '<a href="'.base_url('uploads/exp_inc/'.$expense->files).'" class="btn btn-info btn-xs" title="Download File" download><i class="fa fa-download"></i></a>';
                                        ?>
                                        <!-- <?php echo btn_delete('expense/delete/'.$expense->expenseID, $this->lang->line('delete')) ?> -->
                                    </td>
                                    <?php } ?>
                                </tr>
                                <?php }}} ?>
                        </tbody>
                    </table>
                    <table class="table table-bordered subprint1">
                        <tr id="ti" class="">
                            <td class="text-right active"><?=$this->lang->line('th_total_income')?> : </td>
                            <th class="text-left success">&#8377; <?=$total_income?></th>
                        </tr>
                        <tr id="te" class="">
                            <td class="text-right active"><?=$this->lang->line('th_total_expense')?> : </td>
                            <th class="text-left info">&#8377; <?=$total_expense?></th>
                        </tr>
                        <tr>
                            <td class="text-right active"><?=$this->lang->line('th_due_paument')?> : </td>
                            <th class="text-left danger">&#8377; <?=($total_expense-$total_payment)?></th>
                        </tr>
                        <tr>
                            <td class="text-right active"><?=$this->lang->line('th_due_receipt')?> : </td>
                            <th class="text-left warning">&#8377; <?=($total_income-$total_receipt)?></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if($userTypeId==1 || $userTypeId==9){?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i>&nbsp;&nbsp;<?=$this->lang->line('exp_inc_for_hostel')?> <?=$this->lang->line('panel_title')?></h3>
    </div><!-- /.box-header -->

    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <div id="hide-table">
                <button class="btn btn-default" onclick="print1(1)">Excel <i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer print2">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('expense_expense')?></th>
                                <th><?=$this->lang->line('expense_date')?></th>
                                <th><?=$this->lang->line('expense_uname')?></th>
                                <th><?=$this->lang->line('expense_amount')?></th>
                                <th><?=$this->lang->line('expense_type_th')?></th>
                                <th><?=$this->lang->line('exp_inc_type_th')?></th>
                                <th><?=$this->lang->line('vendor')?></th>
                                <?php /*<th><?=$this->lang->line('exp_inc_for_th')?></th> */ ?>
                                <th><?=$this->lang->line('expense_note')?></th>
                                <?php if(permissionChecker('expense_edit') || permissionChecker('expense_delete')) { ?>
                                <th><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i = 0;
                            $total_expense = $total_income = $total_payment = $total_receipt = $total_other = $final_amount = 0; 
                            if(count($expenses)) { 
                                foreach($expenses as $k=>$expense) { 
                                if($expense->exp_inc_for==1){
                                    $i++;
                                    if($expense->type=='income'){
                                        $total_income =$total_income+$expense->amount;
                                    } else if($expense->type=='expense') {
                                        $total_expense =$total_expense+$expense->amount;
                                    } else if($expense->type=='payment') {
                                        $total_payment =$total_payment+$expense->amount;
                                    } else if($expense->type=='receipt') {
                                        $total_receipt =$total_receipt+$expense->amount;
                                    } else if($expense->type=='other') {
                                        $total_other =$total_other+$expense->amount;
                                    }
                                   ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('expense_expense')?>">
                                        <?php echo $expense->expense; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('expense_date')?>">
                                        <?php echo date("d M Y", strtotime($expense->date)); ?>
                                    </td>
                                    
                                    <td data-title="<?=$this->lang->line('expense_uname')?>">
                                        <?php echo $expense->uname; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('expense_amount')?>">
                                        &#8377; <?php echo $expense->amount; ?>
                                    </td>

                                     <td data-title="<?=$this->lang->line('expense_type_th')?>">
                                        <?php echo ucfirst($expense->type); ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('exp_inc_type')?>">
                                        <?php echo ($expense->exp_inc_type==1) ? $this->lang->line('exp_inc_permanent') : $this->lang->line('exp_inc_non_permanent'); ?>
                                    </td>

                                    <?php /* <td data-title="<?=$this->lang->line('exp_inc_for_th')?>">
                                        <?php echo ($expense->exp_inc_for==1) ? $this->lang->line('exp_inc_for_hostel') : $this->lang->line('exp_inc_for_academic'); ?>
                                    </td> */ ?>
                                    <td>
                                        <?php echo ($expense->vendor_name) ? $expense->vendor_name : "N/a" ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('expense_note')?>">
                                        <?php echo $expense->note; ?>
                                    </td>

                                    <?php if(permissionChecker('expense_edit') || permissionChecker('expense_delete')) { ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <a href="<?=base_url("expense/print_voucher/$expense->expenseID")?>?siname=hostel" class="btn btn-danger btn-xs mrg" target="_blank"><i class="fa fa-print"></i></a>
                                        <?php echo btn_edit('expense/edit/'.$expense->expenseID, $this->lang->line('edit')) ?>
                                        <?php 
                                        if($expense->files!='') echo '<a href="'.base_url('uploads/exp_inc/'.$expense->files).'" class="btn btn-info btn-xs" title="Download File" download><i class="fa fa-download"></i></a>';
                                        ?>
                                        <!-- <?php echo btn_delete('expense/delete/'.$expense->expenseID, $this->lang->line('delete')) ?> -->
                                    </td>
                                    <?php } ?>
                                </tr>
                                <?php }}} ?>
                        </tbody>
                    </table>
                    <table class="table table-bordered subprint1">
                        <tr id="ti" class="">
                            <td class="text-right active"><?=$this->lang->line('th_total_income')?> : </td>
                            <th class="text-left success">&#8377; <?=$total_income?></th>
                        </tr>
                        <tr id="te" class="">
                            <td class="text-right active"><?=$this->lang->line('th_total_expense')?> : </td>
                            <th class="text-left info">&#8377; <?=$total_expense?></th>
                        </tr>
                        <tr>
                            <td class="text-right active"><?=$this->lang->line('th_due_paument')?> : </td>
                            <th class="text-left danger">&#8377; <?=($total_expense-$total_payment)?></th>
                        </tr>
                        <tr>
                            <td class="text-right active"><?=$this->lang->line('th_due_receipt')?> : </td>
                            <th class="text-left warning">&#8377; <?=($total_income-$total_receipt)?></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dvjson">
</div>
<?php } ?>

<?php if(!in_array($userTypeId, [1,5,9])){?>
<div class="callout callout-danger">
    <p><b>Permisiion Denied:</b> Please contact to admin to access this page.</p>
</div>
<?php } ?>

<div id="voucher" style="display: none;">
<p id="json_data">Hello</p>
</div>
<script>

    $(function () {
       setTimeout(() => {
        $(".dt-buttons").remove();
       }, 50);
    })
    var expenses = <?=json_encode($expenses)?>;
    function printDiv(divID,_this) {        
        $.each(expenses,function(idx,val){
            if(parseInt(val.expenseID)===parseInt(_this.data('id'))){
                console.log(val,idx);
                $('#json_data').html(val.expense);
            }
        });

        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }

    function print1 (type) {
        var total_expense = total_income = total_payment = total_receipt = total_other = final_amount = 0; 
        var array_of_array = [['#', 
                                '<?=$this->lang->line('slno')?>', 
                                '<?=$this->lang->line('expense_expense')?>', 
                                '<?=$this->lang->line('expense_uname')?>', 
                                '<?=$this->lang->line('expense_amount')?>', 
                                '<?=$this->lang->line('expense_type_th')?>', 
                                '<?=$this->lang->line('exp_inc_type')?>', 
                                '<?=$this->lang->line('expense_note')?>']];
        var data = JSON.parse('<?=json_encode($expenses)?>'); 
        if (type == 0)
        {
            var i = 0;
            data.forEach((e) => {
                if (e.exp_inc_for == 0)
                {
                    if(e.type=='income'){
                        total_income =total_income+parseInt(e.amount);
                    } else if(e.type=='expense') {
                        total_expense =total_expense+parseInt(e.amount);
                    } else if(e.type=='payment') {
                        total_payment =total_payment+parseInt(e.amount);
                    } else if(e.type=='receipt') {
                        total_receipt =total_receipt+parseInt(e.amount);
                    } else if(e.type=='other') {
                        total_other =total_other+parseInt(e.amount);
                    }

                    i++;
                    array_of_array.push([i, e.expense, e.date, e.uname, e.amount, e.type, e.exp_inc_type == 1 ? 'Permanent' : 'Non Permanet', e.note]);
                }
            });
        } 
        else
        {
            var i = 0;
            data.forEach((e) => {
                if (e.exp_inc_for == 1)
                {
                   if(e.type=='income'){
                        total_income =total_income+parseInt(e.amount);
                    } else if(e.type=='expense') {
                        total_expense =total_expense+parseInt(e.amount);
                    } else if(e.type=='payment') {
                        total_payment =total_payment+parseInt(e.amount);
                    } else if(e.type=='receipt') {
                        total_receipt =total_receipt+parseInt(e.amount);
                    } else if(e.type=='other') {
                        total_other =total_other+parseInt(e.amount);
                    }
                    i++;
                    array_of_array.push([i, e.expense, e.date, e.uname, e.amount, e.type, e.exp_inc_type == 1 ? 'Permanent' : 'Non Permanet', e.note]);
                }
            });
        }
        array_of_array.push(
            [], 
            ['', '', '', 'Total Income', total_income], 
            ['', '', '', 'Total Expense', total_expense],
            ['', '', '', 'Due Payment', (total_expense-total_payment)],
            ['', '', '', 'Due receipt', (total_income-total_receipt)],
            );
        var wb = XLSX.utils.book_new();
        wb.props = {
            Title: "Expense/ Income",
            Subject: "Report",
            Author: "Bhama Defence Academy",
            CreatedDate: new Date()
        };
        wb.SheetNames.push("Expense and Income Report");
        var ws = XLSX.utils.aoa_to_sheet(array_of_array);
        wb.Sheets["Expense and Income Report"] = ws;

        var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'ExpenseIncome.xlsx');
    }

    function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }

    function exportTableToExcel(cname, filename = ''){
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementsByClassName([cname]);
        var ts = tableSelect;



        if (cname == 'print1') {
            var add = document.getElementsByClassName(['subprint1']).children("tbody").children();
            console.log(add[0])
        } else {

        }

        var tableHTML = ts.outerHTML.replace(/ /g, '%20');
        
        // Specify file name
        filename = filename?filename+'.xls':'excel_data.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");
        
        document.body.appendChild(downloadLink);
        
        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        } else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        
            // Setting the file name
            downloadLink.download = filename;
            
            //triggering the function
            downloadLink.click();
        }
    }

</script>