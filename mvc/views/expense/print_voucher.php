<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    
    <STYLE TYPE="text/css">
    
     
        table tbody tr td{
            padding: 5px 10px !important;
        }
        font{font-size:14px !important;}
        body{font-family: arial}
    </style>
</HEAD>
<BODY LANG="en-US">
    <!--   <div style=" width: 60%; text-align: right;">
        <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('print_voucher')"><span class="fa fa-print"></span> <?php //$this->lang->line('print')?> </button>
    </div> -->
<div style="margin: auto;width: 640px;height: 100%;background: #fff;padding: 35px;box-shadow: 0px 0px 4px #333;" id="print_voucher">
    <P STYLE="margin-bottom: 0.14in">
    <SPAN >
    
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=5 STYLE="font-size: 20pt"><B><?php if($siteinfos) { echo getSiteName(); } ?></B></FONT></P>
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2><?php if($siteinfos) { echo $siteinfos->address; } ?></FONT></P>
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2><br><br></FONT></P>
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=5 STYLE="font-size: 16pt"><b><?=$expense->type=='expense' ? 'Payment' : 'Receipt';?> Voucher</b></FONT>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>No. : <?=$expense->expenseID?></B></FONT><FONT SIZE=3 style="float:right"><B>Dated : <?=date('d-m-Y',strtotime($expense->date))?></B></FONT></P>
    <br>
    <CENTER>
        <TABLE WIDTH=626 CELLPADDING=7 CELLSPACING=0>
            <COL WIDTH=142>
            <COL WIDTH=143>
            <COL WIDTH=143>
            <COL WIDTH=142>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; border-left:0; border-right:0; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=3><B>Particulars</B></FONT></P>
                </TD>
                
                <TD WIDTH=143 align=right STYLE="border: 1px solid #000001; border-right:0; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=3><B>Amount</B></FONT></P>
                </TD>
                
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 height=300 STYLE=" border-bottom:0; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Account:<br><?=$expense->expense?></FONT></P>
                </TD>
                
                <TD WIDTH=143 align=right STYLE="border: 1px solid #000001; border-right:0; border-top:0;  border-bottom:0; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2><?=$expense->amount?></FONT></P>
                </TD>
                
            </TR>
        
            <TR VALIGN=TOP>
                
                <TD WIDTH=143 STYLE=" padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P style="margin:0"><FONT SIZE=2>Account of:</FONT></P>
                    <P style="margin:0"><FONT SIZE=2 style="margin-left:20px"><?=$expense->note!='' ? $expense->note : '__________________________'?></FONT></P>
                    <P style="margin:0"><FONT SIZE=2>Amount (in words)</FONT></P>
                    <P style="margin:0"><FONT SIZE=2 style="margin-left:20px;text-transform: capitalize;">Rs: <?=convertNum($expense->amount)?> Only</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="vertical-align: bottom;border-left: 1px solid;border-bottom: 1px solid;padding: 0;">
                    <P style="border-top: 1px solid; padding-right:5px;
    padding-top: 6px;" ALIGN=RIGHT><FONT SIZE=3><B><?=$expense->amount?></B></FONT></P>
                </TD>
            </TR>
            
        </TABLE>
    </CENTER>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%; <?=($expense->type=='expense') ? 'text-align:right': ''?>">
        <FONT SIZE=2>
            <?=$expense->type=='income' ? 'Receiver Signature: __________________________ ' : '';?>      
            Authorised Signatory:____________________________
        </FONT>
    </P>
    
</SPAN>
</P>
</div>
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        var divElements = document.getElementById(divID).innerHTML;
        var oldPage = document.body.innerHTML;
        document.body.innerHTML = "<html><head><title></title></head><body>" + divElements + "</body>";
        window.print();
        document.body.innerHTML = oldPage;
    }
   printDiv('print_voucher');
</script>
</BODY>
</HTML>