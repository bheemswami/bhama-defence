
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('despatch_panel_title')?> <?=$this->lang->line('letter_add')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("hmember/index")?>"><?=$this->lang->line('despatch_panel_title')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('despatch_panel_title')?></li>
        </ol>
    </div>
    
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                     
                    <div class="form-group <?=form_error('despatch_date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("despatch_date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="despatch_date" name="despatch_date" value="<?=set_value('despatch_date')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('despatch_date'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('name') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("despatch_name")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('name'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('address') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("despatch_address")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('address'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('place') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("despatch_place")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="place" name="place" value="<?=set_value('place')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('place'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('subject') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("letter_subject")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('subject'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('file_head_no') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("despatch_file_head")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="file_head_no" name="file_head_no" value="<?=set_value('file_head_no')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('file_head_no'); ?></span>
                    </div>  
                    <div class="form-group <?=form_error('remark') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("letter_remark")?></label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="remark" name="remark"><?=set_value('remark')?></textarea>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('remark'); ?></span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("letter_add")?>" >
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('despatch_panel_title')?> <?=$this->lang->line('letter_list')?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th><?=$this->lang->line('letter_sno')?></th>
                            <th><?=$this->lang->line('despatch_date')?></th>
                            <th><?=$this->lang->line('letter_subject')?></th>
                            <th><?=$this->lang->line('despatch_name')?></th>
                            <th><?=$this->lang->line('despatch_address')?></th>
                            <th><?=$this->lang->line('despatch_place')?></th>
                            <th><?=$this->lang->line('despatch_file_head')?></th>
                            <th><?=$this->lang->line('letter_remark')?></th>
                            <th><?=$this->lang->line('action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($letters)) {$i = 1; foreach($letters as $letter) { ?>
                            <tr>
                                <td><?=$i++?></td>
                                <td><?=$letter->despatch_date?></td>
                                <td><?=$letter->subject?></td>
                                <td><?=$letter->despatch_name?></td>
                                <td><?=$letter->despatch_address?></td>
                                <td><?=$letter->place?></td>
                                <td><?=$letter->file_head_no?></td>
                                <td><?=$letter->remark?></td>
                                <td>
                                    <a href="<?=base_url('/hmember/letter_despatch_print/').$letter->id?>" class="btn btn-danger btn-xs mrg" target="_blank"><i class="fa fa-print"></i></a>
                                    <?php 
                                        echo btn_edit_show('hmember/letter_despatch_update/'.$letter->id, $this->lang->line('edit'));
                                        echo btn_delete_show('hmember/letter_despatch_delete/'.$letter->id, $this->lang->line('delete'));
                                    ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#despatch_date").datepicker();
</script>
