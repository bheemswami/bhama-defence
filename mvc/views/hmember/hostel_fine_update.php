
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('hostel_fine')?> <?=$this->lang->line('letter_add')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("hmember/index")?>"><?=$this->lang->line('hostel_fine')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('hostel_fine')?></li>
        </ol>
    </div>
    
    <div class="box-body">
        <div class="row">            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                     <input type="hidden" class="form-control" name="payid" value="0" >
                    <div class="form-group <?=form_error('fine_date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("fine_date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="fine_date" name="fine_date" value="<?=set_value('fine_date',date('d-m-Y',strtotime($records->paymentdate)))?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('fine_date'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('amount') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("amount")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount',$records->paymentamount)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('amount'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('reason') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("reason")?></label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="reason" name="reason"><?=set_value('reason',$records->remark)?></textarea>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('reason'); ?></span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("letter_update")?>" >
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#fine_date").datepicker({
    endDate: '+0d',
});

</script>
