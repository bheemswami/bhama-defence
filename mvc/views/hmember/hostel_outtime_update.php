
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('out_time')?> <?=$this->lang->line('letter_update')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("hmember/index")?>"><?=$this->lang->line('out_time')?></a></li>
            <li class="active"><?=$this->lang->line('letter_update')?> <?=$this->lang->line('out_time')?></li>
        </ol>
    </div>
   
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                     
                    <div class="form-group <?=form_error('out_date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("out_date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="out_date" name="out_date" value="<?=set_value('out_date',date('d-m-Y',strtotime($record->out_date)))?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('out_date'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('out_time') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("out_time")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="out_time" name="out_time" value="<?=set_value('out_time',$record->out_time)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('out_time'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('total_time') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("total_time")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="total_time" name="total_time" value="<?=set_value('total_time',$record->total_time)?>" min="1" max="24">
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('total_time'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('reason') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("reason")?></label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="reason" name="reason"><?=set_value('reason',$record->reason)?></textarea>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('reason'); ?></span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("letter_update")?>" >
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#out_date").datepicker({
    endDate: '+0d',
});
$("#out_time").datetimepicker({
    format: 'hh:mm A'
});
$("#total_time").datetimepicker({
    format: 'hh:mm'
});

</script>
