
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-member"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('panel_title')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <h5 class="page-header">
                    <?php /*if(count($students) > 0 ) { echo btn_add('hmember/add/'.$set, $this->lang->line('hmember')); }*/ ?>
                    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12 pull-right drop-marg">
                        <?php
                            $array = array("0" => $this->lang->line("hmember_select_class"));
                            foreach ($classes as $classa) {
                                $array[$classa->classesID] = $classa->classes;
                            }
                            echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control select2'");
                        ?>
                    </div>
                </h5>

                <?php if(count($students) > 0 ) {?>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("hmember_all_students")?></a></li>
                            <?php /* foreach ($sections as $key => $section) {
                                echo '<li class=""><a data-toggle="tab" href="#tab'.$section->classesID.$section->sectionID .'" aria-expanded="false">'. $this->lang->line("hmember_section")." ".$section->section. " ( ". $section->category." )".'</a></li>';
                            } */?>
                        </ul>

                        <div class="tab-content">
                            <div id="all" class="tab-pane active">
                                <div id="hide-table">
                                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                                <?php /*<th class="col-sm-2"><?=$this->lang->line('hmember_photo')?></th>*/?>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_name')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_roll')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_email')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_status')?></th>
                                                <?php if(permissionChecker('hmember_add') || permissionChecker('hmember_edit') || permissionChecker('hmember_delete') || permissionChecker('hmember_view')) { ?>
                                                    <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($students)) {$i = 1; foreach($students as $student) { 
                                                if($set=='')
                                                    $set = $student->classesID;

                                                ?>
                                                <tr>
                                                    <td data-title="<?=$this->lang->line('slno')?>">
                                                        <?php echo $i; ?>
                                                    </td>
                                                    <?php /*<td data-title="<?=$this->lang->line('hmember_photo')?>">
                                                        <?php $array = array(
                                                                "src" => base_url('uploads/images/'.$student->photo),
                                                                'width' => '35px',
                                                                'height' => '35px',
                                                                'class' => 'img-rounded'
                                                            );
                                                            echo img($array);
                                                        ?>
                                                    </td>*/?>
                                                    <td data-title="<?=$this->lang->line('hmember_name')?>">
                                                        <?php echo $student->name; ?>
                                                    </td>

                                                    <td data-title="<?=$this->lang->line('hmember_roll')?>">
                                                        <?php echo $student->roll; ?>
                                                    </td>

                                                    <td data-title="<?=$this->lang->line('hmember_email')?>">
                                                        <?php echo $student->email; ?>
                                                    </td>

                                                    <td data-title="">
                                                        <?php echo ($student->hostel == 1) ? 'Hostel Assigned' : 'Hostel Not Assigned'; ?>
                                                    </td>
                                        
                                                    <?php if(permissionChecker('hmember_add') || permissionChecker('hmember_edit') || permissionChecker('hmember_delete') || permissionChecker('hmember_view')) { ?>
                                                    <td data-title="<?=$this->lang->line('action')?>">
                                                        <?php
                                                        echo '<a data-id="'.$set.'" data-sid="'.$student->studentID.'" class="btn btn-info btn-xs mrg hostelAccessoriesIcon" data-toggle="modal" data-target="#hostelAccessories" title="'.$this->lang->line('hostel_accessories').'"><i class="fa fa-suitcase"></i></a>';
                                                       // echo '<a data-id="'.$set.'" data-sid="'.$student->studentID.'" class="btn btn-primary btn-xs mrg hostalFeeIcon" data-toggle="modal" data-target="#hostalFeeModel" title="Assign Hostel With Fees"><i class="fa fa-home"></i></a>';
                                                            echo btn_add('hmember/view/'.$student->studentID."/".$student->classesID."/add", $this->lang->line('hmember'));
                                                            if($student->hostel == 0) {
                                                                //echo btn_add('hmember/add/'.$student->studentID."/".$student->classesID, $this->lang->line('hmember'));
                                                            } else {
                                                                echo btn_view('hmember/view/'.$student->studentID."/".$student->classesID, $this->lang->line('view')). " ";
                                                                //echo btn_edit('hmember/edit/'.$student->studentID."/".$student->classesID, $this->lang->line('edit')). " ";
                                                                echo '<a href="'.base_url('/').'hmember/letter_despatch/'.$student->studentID."/".$student->classesID.'" class="btn btn-warning btn-xs mrg" title="Letter Despatch"><i class="fa fa-arrow-right"></i></a>';
                                                                echo '<a href="'.base_url('/').'hmember/letter_receipt/'.$student->studentID."/".$student->classesID.'" class="btn btn-info btn-xs mrg" title="Letter Receipt"><i class="fa fa-arrow-left"></i></a>';
                                                                echo '<a href="'.base_url('/').'hmember/hostel_outtime/'.$student->studentID."/".$student->classesID.'" class="btn btn-success btn-xs mrg" title="Hostel Out Time"><i class="fa fa-calendar"></i></a>';
                                                                echo '<a href="'.base_url('/').'hmember/hostel_fine/'.$student->studentID.'" class="btn btn-warning btn-xs mrg" title="Hostel Fine"><i class="fa fa-inr"></i></a>';

                                                                echo btn_delete('hmember/delete/'.$student->studentID."/".$student->classesID, $this->lang->line('delete'));
                                                            }
                                                        ?>
                                                    </td>
                                                    <?php } ?>
                                                </tr>
                                            <?php $i++; }} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <?php /* foreach ($sections as $key => $section) { ?>
                                    <div id="tab<?=$section->classesID.$section->sectionID?>" class="tab-pane">
                                        <div id="hide-table">
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                                <thead>
                                                    <tr>
                                                        <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                                        <th class="col-sm-1"><?=$this->lang->line('hmember_photo')?></th>
                                                        <th class="col-sm-2"><?=$this->lang->line('hmember_name')?></th>
                                                        <th class="col-sm-2"><?=$this->lang->line('hmember_roll')?></th>
                                                        <th class="col-sm-2"><?=$this->lang->line('hmember_email')?></th>
                                                        <?php if(permissionChecker('hmember_add') || permissionChecker('hmember_edit') || permissionChecker('hmember_delete') || permissionChecker('hmember_view')) { ?>
                                                            <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(count($allsection[$section->sectionID])) { $i = 1; foreach($allsection[$section->sectionID] as $student) { if($section->sectionID === $student->sectionID) { ?>
                                                        <tr>
                                                            <td data-title="<?=$this->lang->line('slno')?>">
                                                                <?php echo $i; ?>
                                                            </td>
                                                            <td data-title="<?=$this->lang->line('hmember_photo')?>">
                                                                <?php $array = array(
                                                                        "src" => base_url('uploads/images/'.$student->photo),
                                                                        'width' => '35px',
                                                                        'height' => '35px',
                                                                        'class' => 'img-rounded'

                                                                    );
                                                                    echo img($array);
                                                                ?>
                                                            </td>
                                                            <td data-title="<?=$this->lang->line('hmember_name')?>">
                                                                <?php echo $student->name; ?>
                                                            </td>

                                                            <td data-title="<?=$this->lang->line('hmember_roll')?>">
                                                                <?php echo $student->roll; ?>
                                                            </td>

                                                            <td data-title="<?=$this->lang->line('hmember_email')?>">
                                                                <?php echo $student->email; ?>
                                                            </td>
                                                
                                                            <?php if(permissionChecker('hmember_add') || permissionChecker('hmember_edit') || permissionChecker('hmember_delete') || permissionChecker('hmember_view')) { ?>
                                                            <td data-title="<?=$this->lang->line('action')?>">
                                                                <?php
                                                                    if($student->hostel == 0) {
                                                                        echo btn_add('hmember/add/'.$student->sStudentID."/".$set, $this->lang->line('hmember'));
                                                                    } else {
                                                                        echo btn_view('hmember/view/'.$student->sStudentID."/".$set, $this->lang->line('view')). " ";
                                                                        echo btn_edit('hmember/edit/'.$student->sStudentID."/".$set, $this->lang->line('edit')). " ";
                                                                        echo btn_delete('hmember/delete/'.$student->sStudentID."/".$set, $this->lang->line('delete'));
                                                                    }
                                                                ?>
                                                            </td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php $i++; }}} ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                            <?php } */?>
                        </div>
                    </div> <!-- nav-tabs-custom -->
                <?php } else { ?>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("hmember_all_students")?></a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="all" class="tab-pane active">
                                <div id="hide-table">
                                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_photo')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_name')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_roll')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('hmember_email')?></th>
                                                <?php if(permissionChecker('hmember_add') || permissionChecker('hmember_edit') || permissionChecker('hmember_delete') || permissionChecker('hmember_view')) { ?>
                                                    <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
                                                <tr>
                                                    <td data-title="<?=$this->lang->line('slno')?>">
                                                        <?php echo $i; ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('hmember_photo')?>">
                                                        <?php $array = array(
                                                                "src" => base_url('uploads/images/'.$student->photo),
                                                                'width' => '35px',
                                                                'height' => '35px',
                                                                'class' => 'img-rounded'

                                                            );
                                                            echo img($array);
                                                        ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('hmember_name')?>">
                                                        <?php echo $student->name; ?>
                                                    </td>

                                                    <td data-title="<?=$this->lang->line('hmember_roll')?>">
                                                        <?php echo $student->roll; ?>
                                                    </td>

                                                    <td data-title="<?=$this->lang->line('hmember_email')?>">
                                                        <?php echo $student->email; ?>
                                                    </td>
                                        
                                                    <?php if(permissionChecker('hmember_add') || permissionChecker('hmember_edit') || permissionChecker('hmember_delete') || permissionChecker('hmember_view')) { ?>
                                                    <td data-title="<?=$this->lang->line('action')?>">
                                                        <?php
                                                            if($student->hostel == 0) {
                                                                echo btn_add('hmember/add/'.$student->sStudentID."/".$set, $this->lang->line('hmember'));
                                                            } else {
                                                                echo btn_view('hmember/view/'.$student->sStudentID."/".$set, $this->lang->line('view')). " ";
                                                                echo btn_edit('hmember/edit/'.$student->sStudentID."/".$set, $this->lang->line('edit')). " ";
                                                                echo btn_delete('hmember/delete/'.$student->sStudentID."/".$set, $this->lang->line('delete'));
                                                            }
                                                        ?>
                                                    </td>
                                                    <?php } ?>
                                                </tr>
                                            <?php $i++; }} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div> <!-- nav-tabs-custom -->
                <?php } ?>

            </div>
        </div>
    </div>
</div>
<div class="modal" id="hostalFeeModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Assign Hostel With Fees<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?=base_url('student/hostal_fees')?>" class="hfees_add_update">
            <input type="hidden" class="form-control" name="redirect" value="hmember">
            <input type="hidden" class="form-control" id="class_id2" name="class_id" value="">
            <input type="hidden" class="form-control" id="student_id2" name="student_id" value="">
            <input type="hidden" class="form-control" id="pay_id2" name="pay_id" value="0">
            <span class="col-sm-12 control-label txt-danger" id="err_msg2" style="padding: 0px 0px 10px 0px;font-size: 15px;color: #fb0a0a;"></span>
            <div class="form-group">
                <label>Room Type</label>     
                <select name="categoryID" id="categoryID" class="form-control" required>
                    <option value="">Select Room</option>
                    <?php
                        foreach ($categorys as $key => $value) {
                            echo '<option value="'.$value->categoryID.'">'.$value->class_type.'</option>';
                        }
                    ?>  
                </select>
            </div>
            <div class="form-group">
                <label>Payment Method</label>     
                <select name="paymenttype" id="payment-method" class="form-control" required>
                    <option value="">Payment Method</option>
                    <?php
                        foreach ($this->config->item('PAYMENT_METHODS') as $key => $value) {
                            echo '<option value="'.$value.'">'.$value.'</option>';
                        }
                    ?>  
                </select>
            </div>
            <div class="form-group hide-elem" id="bank-elem">
                <label>Bank Name</label>    
                <input name="paymentbank" id="paymentbank" class="form-control" type="text" placeholder="Bank Name" value="">
            </div>
            <div class="form-group hide-elem" id="cheque-elem">
                <label>Cheque No.</label>
                <input name="chequeno" id="chequeno" class="form-control" type="text" placeholder="Cheque No." value="">
            </div>
            <div class="form-group hide-elem" id="amount-elem">
                <label>Amount</label>
                <input name="paymentamount" id="paymentamount" class="form-control" type="text" placeholder="Amount" value="" required>
            </div>
            
            <div class="form-group">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- The Hostal Acc Modal -->
<div class="modal" id="hostelAccessories">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hostel Accessories<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
      </div>
      <div class="modal-body">
       <form method="post" action="<?=base_url('student/hostal_accessories')?>" class="hacc_add">
         <input type="hidden" class="form-control" name="redirect" value="hmember">
        <input type="hidden" class="form-control" id="class_id" name="class_id" value="">
        <input type="hidden" class="form-control" id="student_id" name="student_id" value="">
        <span class="col-sm-12 control-label txt-danger" id="err_msg" style="padding: 0px 0px 10px 0px;font-size: 15px;color: #fb0a0a;"></span>
          
          <div class="form-group">
            <label for="leave_reason">Reason</label>
            <input type="text" class="form-control" id="leave_reason" name="leave_reason">
          </div>
          <div class="form-group">
            <label for="date">Date</label>
            <input type="date" class="form-control" id="leave_date" name="leave_date">
          </div>

           <div class="hostal_segments">
                <div class='form-group' >
                    <label for="note" class=""> Segments</label>
                    <button class="btn btn-success btn-xs plus_icon" type="button"> Add New</button>
                    <!-- <i class="fa fa-plus btn btn-success plus_icon"></i> -->
                </div>
            </div>
          <div class="col-sm-12">
              <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </form>
      </div>

      <!-- Modal footer -->
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->

    </div>
  </div>
</div>
<script type="text/javascript">
    $('.select2').select2();
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('hmember/student_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });

     $('.plus_icon').click(function(){
        addRows('','');
    });
    $(document).on('click','.remove_icon',function(){
        $(this).parents('div').parents('.segment_items').remove();
    });
    $(document).on('click','.hostelAccessoriesIcon',function(){
        var sid = $(this).data('sid');
        $('#class_id').val($(this).data('id'));
        $('#student_id').val(sid);
        $.ajax({
            type: 'GET',
            url: "<?=base_url('student/hostal_accessories')?>",
            data: {student_id:sid},
            dataType: "JSON",
            success: function(data) {
                var parseData = JSON.parse(data.student_data.hostal_accessories);
                $('#leave_reason').val(data.student_data.hostal_leave_reason);
                $('#leave_date').val(data.student_data.hostal_leave_date);
                $.each(parseData,function(idx,val){
                    addRows(val.segment,val.quantity);
                })
            }
        });
    });

    function addRows(p,q){
        $('.hostal_segments').append('<div class="form-group segment_items">\
                <label for="note" class="col-sm-2 control-label"></label>\
                <div class="col-sm-12">\
                    <div class="col-sm-6"><input type="text" class="form-control" name="segments[product][]" value="'+p+'" ></div>\
                    <div class="col-sm-5"><input type="number" class="form-control" name="segments[quantity][]" value="'+q+'" ></div>\
                    <div class="col-sm-1"><i class="fa fa-minus btn btn-danger remove_icon"></i></div>\
                </div>\
            </div>'
        );
    }
    
    $('.hacc_add').submit(function(e){
        
    });

    $(document).on('click','.hostalFeeIcon',function(){
        $('#payment-method,#pay_id2,#paymentbank,#chequeno,#paymentamount').val('');
        $('#bank-elem,#cheque-elem,#amount-elem').hide();
        var sid = $(this).data('sid');
        $('#class_id2').val($(this).data('id'));
        $('#student_id2').val(sid);
        $.ajax({
            type: 'GET',
            url: "<?=base_url('student/hostal_fees')?>",
            data: {student_id:sid},
            dataType: "JSON",
            success: function(data) {
                var parseData = data.payment_data;
                if(data.count>0){
                    paymentElemShow(parseData.paymenttype);
                    $('#payment-method').val(parseData.paymenttype);
                    $('#pay_id2').val(parseData.paymentID);
                    $('#paymentbank').val(parseData.paymentbank);
                    $('#chequeno').val(parseData.chequeno);
                    $('#paymentamount').val(parseData.paymentamount);
                }
                $('#categoryID').val(data.category_id);
            }
        });
    });
    $('#payment-method').change(function(){
      $('#bank-elem,#cheque-elem,#amount-elem').hide();
      paymentElemShow($(this).val());
    });
    function paymentElemShow(val){
      if(val=='Cash'){
         $('#amount-elem').show();
      } else if(val=='Cheque'){
         $('#amount-elem').show();
         $('#cheque-elem').show();
         $('#bank-elem').show();
      } else if(val=='NetBanking'){
         $('#amount-elem').show();
         $('#bank-elem').show();
      }
    }

    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
             window.location.href = "<?=base_url('student/student_list')?>";
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('student/student_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>
