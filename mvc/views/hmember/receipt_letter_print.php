<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE> 
     <style>
        table tbody tr td{
            padding: 5px 10px !important;
        }
        font{font-size:14px !important;}
        body{font-family: arial}
    </style>
</HEAD>
<BODY LANG="en-US">
   <!--  <div style=" width: 60%; text-align: right;">
        <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('print_letter')"><span class="fa fa-print"></span> <?php //$this->lang->line('print')?> </button>
    </div> -->
<div style="margin: auto;width: 640px;height: 100%;background: #fff;padding: 35px;box-shadow: 0px 0px 4px #333;" id="print_letter">
<P STYLE="margin-bottom: 0.14in">
    <SPAN >
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><?=getSiteName('h')?></P>
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=5 STYLE="font-size: 20pt"><B>Letter Reciept</B></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>SR. NO:</B></FONT><FONT SIZE=4><B>
    <?=@$letter->id?></B></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>User Name:</B></FONT><FONT SIZE=4><B>
    <?=@$letter->name?></B></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Letter Date:</B></FONT><FONT SIZE=4><B>
    <?=@date('d-m-Y',strtotime($letter->letter_date))?></B></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Letter No:</B></FONT><FONT SIZE=4><B>
    <?=@$letter->letter_no?></B></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <CENTER>
        <TABLE WIDTH=626 CELLPADDING=7 CELLSPACING=0>
            <COL WIDTH=142>
            <COL WIDTH=143>
            <COL WIDTH=143>
            <COL WIDTH=142>
            
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>From Whom Recieved</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$letter->from_received?></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Subject</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$letter->subject?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>File Head & No.</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$letter->file_head_no?></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Disposal</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$letter->disposal?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Reply No.</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$letter->reply_no?></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Reply Date</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@date('d-m-Y',strtotime($letter->reply_date))?></FONT></P>
                </TD>
            </TR>
            
            
            
            
        </TABLE>
    </CENTER>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2>Remarks. <?=@$letter->remark?></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2>Signature: __________________________     Date: <?=@date('d-m-Y',strtotime($letter->receipt_date))?></FONT></P>
</SPAN>
</P>
</div>
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        var divElements = document.getElementById(divID).innerHTML;
        var oldPage = document.body.innerHTML;
        document.body.innerHTML = "<html><head><title></title></head><body>" + divElements + "</body>";
        window.print();
        document.body.innerHTML = oldPage;
    }
   printDiv('print_letter');
</script>
</BODY>
</HTML>