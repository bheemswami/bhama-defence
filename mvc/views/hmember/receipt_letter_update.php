
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('despatch_panel_title')?> <?=$this->lang->line('letter_update')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("hmember/index")?>"><?=$this->lang->line('despatch_panel_title')?></a></li>
            <li class="active"><?=$this->lang->line('letter_update')?> <?=$this->lang->line('despatch_panel_title')?></li>
        </ol>
    </div>
    
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                     
                    <div class="form-group <?=form_error('receipt_date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="receipt_date" name="receipt_date" value="<?=set_value('receipt_date',$letter->receipt_date)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('receipt_date'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('letter_date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_letter_date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="letter_date" name="letter_date" value="<?=set_value('letter_date',$letter->letter_date)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('letter_date'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('letter_no') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_letter_no")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="letter_no" name="letter_no" value="<?=set_value('letter_no',$letter->letter_no)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('letter_no'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('from_received') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_from")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="from_received" name="from_received" value="<?=set_value('from_received',$letter->from_received)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('from_received'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('subject') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("letter_subject")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject',$letter->subject)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('subject'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('file_head_no') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_file_head")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="file_head_no" name="file_head_no" value="<?=set_value('file_head_no',$letter->file_head_no)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('file_head_no'); ?></span>
                    </div>  
                    <div class="form-group <?=form_error('disposal') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_disposal")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="disposal" name="disposal" value="<?=set_value('disposal',$letter->disposal)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('disposal'); ?></span>
                    </div>  
                    <div class="form-group <?=form_error('reply_date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_reply_date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="reply_date" name="reply_date" value="<?=set_value('reply_date',$letter->reply_date)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('reply_date'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('reply_no') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("receipt_reply_no")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="reply_no" name="reply_no" value="<?=set_value('reply_no',$letter->reply_no)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('reply_no'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('remark') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("letter_remark")?></label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="remark" name="remark"><?=set_value('remark',$letter->remark)?></textarea>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('remark'); ?></span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("letter_update")?>" >
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#despatch_date").datepicker();
</script>
