
<?php if(($student)) {     ?>
    <div class="well">
        <div class="row">
            <div class="col-sm-6">
                <?php if($this->uri->segment(5)=='add'){ ?>
                <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                 <?php } ?>
                <?php
                 //echo btn_add_pdf('hmember/print_preview/'.$hmember->studentID."/".$set, $this->lang->line('pdf_preview')) 
                ?>
                <?php if(permissionChecker('hmember_edit')) 
                {    
                    //echo btn_sm_edit('hmember/edit/'.$hmember->studentID."/".$set, $this->lang->line('edit')); 
                } 
                ?>
                <!-- <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button> -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("hmember/index/$set")?>"><?=$this->lang->line('panel_title')?></a></li>
                    <li class="active"><?=$this->lang->line('view')?></li>
                </ol>
            </div>
        </div>
    </div>
    <?php if($student) { ?>

    <div class="">
        <?php if($this->uri->segment(5)==null && $hmember){ ?>
        <div class="panel">
            <div class="profile-view-head"> 
                <h1><?=$student->name?></h1>
                <p><?php //echo $this->lang->line("hmember_classes")." ".$class->classes?></p>

            </div>
            <div class="panel-body profile-view-dis">
                <h1><?=$this->lang->line("personal_information")?></h1>
                <div class="row">
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_registerNO")?> </span>: <?=$student->registerNO?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_roll")?> </span>: <?=$student->roll?></p>
                    </div>
                   
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_hname")?> </span>: <?=$hostel->name?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_htype")?> </span>: <?=$hostel->htype?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_class_type")?> </span>: <?=$category->class_type?></p>
                    </div>
                    <!-- <div class="profile-view-tab">
                        <p><span><?php //$this->lang->line("hmember_tfee")?> </span>: <?=$hmember->hbalance?></p>
                    </div> -->
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_hostel_address")?> </span>: <?=$hostel->address?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_joindate")?> </span>: <?php if($hmember->hjoindate) { echo date("d M Y", strtotime($hmember->hjoindate)); }?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_dob")?> </span>: <?php if($student->dob) { echo date("d M Y", strtotime($student->dob)); }?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_sex")?> </span>: <?=$student->sex?></p>
                    </div>
                   
                  
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_email")?> </span>: <?=$student->email?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("hmember_phone")?> </span>: <?=$student->phone?></p>
                    </div>
                   
                </div>

            </div>
        </div>
        <?php } ?>


        <?php if($this->uri->segment(5)=='add'){ ?>
        <div class="panel-group" id="installments-accordian">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#installments" class="accordion-toggle"><?=$this->lang->line("installment")?></a>
                    </h4>
                </div>
                <div id="installments" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">                        
                                <div class="table-card">
                                    <form method="post" action="<?=base_url('student/hostal_fees')?>" class="hfees_add_update">
                                        <input type="hidden" class="form-control" name="redirect" value="hmember">
                                        <input type="hidden" class="form-control" id="class_id2" name="class_id" value="<?=$student->classesID?>">
                                        <input type="hidden" class="form-control" id="student_id2" name="student_id" value="<?=$student->studentID?>">
                                        <input type="hidden" class="form-control" id="pay_id2" name="pay_id" value="0">
                                        <span class="col-sm-12 control-label txt-danger" id="err_msg2" style="padding: 0px 0px 10px 0px;font-size: 15px;color: #fb0a0a;"></span>
                                        <div class="form-group col-sm-12">
                                            <label class="col-sm-2">Room Type</label>     
                                            <div class="col-sm-6">
                                                <select name="categoryID" id="categoryID" class="form-control" required>
                                                    <option value="">Select Room</option>
                                                    <?php
                                                        foreach ($categorys as $key => $value) {
                                                             if($hmember->categoryID==$value->categoryID) echo '<option value="'.$value->categoryID.'" selected>'.$value->class_type.'</option>';
                                                             else echo '<option value="'.$value->categoryID.'">'.$value->class_type.'</option>';
                                                        }
                                                    ?>  
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12">
                                            <label class="col-sm-2">Payment Method</label>     
                                            <div class="col-sm-6">
                                                <select name="paymenttype" id="payment-method" class="form-control" required>
                                                    <option value="">Payment Method</option>
                                                    <?php
                                                        foreach ($this->config->item('PAYMENT_METHODS') as $key => $value) {
                                                            echo '<option value="'.$value.'">'.$value.'</option>';
                                                        }
                                                    ?>  
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12 hide-elem" id="bank-elem">
                                            <label class="col-sm-2">Bank Name</label>    
                                            <div class="col-sm-6">
                                                <input name="paymentbank" id="paymentbank" class="form-control" type="text" placeholder="Bank Name" value="">
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12 hide-elem" id="cheque-elem">
                                            <label class="col-sm-2">Cheque No.</label>
                                            <div class="col-sm-6">
                                                <input name="chequeno" id="chequeno" class="form-control" type="text" placeholder="Cheque No." value="">
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12 hide-elem" id="amount-elem">
                                            <label class="col-sm-2">Amount</label>
                                            <div class="col-sm-6">
                                                <?php 
                                                if($student->hostel_installments>0){
                                                    $hostelFee = $student->hostel_fee;
                                                    echo '<input id="paymentamount" class="form-control" type="text" placeholder="Amount" value="'.$student->hostel_fee.'" required readonly>';
                                                } else {
                                                    $hostelFee = 0;
                                                    echo '<input name="paymentamount" id="paymentamount" class="form-control" type="text" placeholder="Amount" value="" required>';
                                                } 
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-12 hide-elem" id="payby-elem">
                                            <label class="col-sm-2">Pay By</label>
                                            <?php 
                                                if($student->hostel_installments==0){
                                                    echo '<div class="col-sm-2"><label for="pay_by_full"><input name="pay_by" id="pay_by_full" class="pay_by" type="radio" value="0" required>&nbsp;&nbsp;Full Pay</label></div>';
                                                }
                                            ?>                            
                                            
                                            <div class="col-sm-2">
                                                <label for="pay_by_installment"><input name="pay_by" id="pay_by_installment" class="pay_by" type="radio" value="1" required>&nbsp;&nbsp;Installments</label>
                                            </div>
                                        </div>

                                        <div class="hide-elem" id="installment-elem">
                                            
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-2">Select Installment</label>
                                                <div class="col-sm-6">
                                                    <select class="form-control" name="installment">
                                                        <option value="">Select</option>
                                                        <?php
                                                            foreach ($this->config->item('INSTALLMENT_LIST') as $key => $value) {
                                                                echo '<option value="'.$value.'">'.$value.'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-2">Installment Amount</label>
                                                <div class="col-sm-6">
                                                    <input name="installmentamount" id="installmentamount" class="form-control" type="text" placeholder="Amount" value="">
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group col-sm-8 col-offset-sm-4 text-right">
                                          <button type="submit" class="btn btn-success submit-btn">Submit</button>
                                        </div>
                                    </form>
                                    <div class="printablediv">
                                       <table class="mrgn-top-1 table table-bordered">
                                            <thead>
                                                <tr>
                                                  <th scope="col"><?=$this->lang->line('SN_No')?></th>
                                                  <th scope="col"><?=$this->lang->line('amount')?></th>
                                                  <th scope="col"><?=$this->lang->line('date')?></th>
                                                  <th scope="col"><?=$this->lang->line('description')?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $totalInstalment =0;
                                                if(count($payment_inst)){
                                                foreach ($payment_inst as $key => $value) {
                                                    $totalInstalment =$totalInstalment+($value->paymentamount)
                                                ?>
                                                <tr>
                                                  <th scope="row"><?=$key+1?></th>
                                                  <td>&#8377; <?=$value->paymentamount?></td>
                                                  <td><?=$value->paymentdate?></td>
                                                  <td><?=$value->purpose?></td>
                                                </tr>
                                                <?php }}?>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <th scope="row" colspan="1">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <td class="text-right"><strong><?=$this->lang->line('hostel_fee')?></strong></td>
                                                                <td class="text-right">&#8377; <?=sprintf('%0.2f',$student->hostel_fee)?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right"><strong><?=$this->lang->line('total_installment')?></strong></td>
                                                                <td class="text-right">&#8377; <?=sprintf('%0.2f',$totalInstalment)?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right"><strong><?=$this->lang->line('remaining_fee')?></strong></td>
                                                                <td class="text-right">&#8377; <?=sprintf('%0.2f',($student->hostel_fee)-$totalInstalment)?><strong></td>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php } else { ?>
        <div class="col-lg-12">
        <?php echo "<h2>". $this->lang->line("hmember_message") . "</h2>"; ?>
        </div>
    <?php } ?>
<?php } 
$remainingHostelFees = ($student->hostel_fee-$totalInstalment);

?>
<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('hmember/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('message')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->  

<script>
    var hostelAmount = <?=$remainingHostelFees?>;
    var installmentAmount = 0;
    var payBy = 0;
    $('.pay_by').click(function(){
        payBy = $(this).val();
        $('#installment-elem').hide();
        if(payBy==1){
            $('#installment-elem').show();
        }
    });
    $('#payment-method').change(function(){
      $('#bank-elem,#cheque-elem,#amount-elem,#payby-elem').hide();
      paymentElemShow($(this).val());
    });
    function paymentElemShow(val){
        $('#payby-elem').show();
      if(val=='Cash'){
         $('#amount-elem').show();
      } else if(val=='Cheque'){
         $('#amount-elem').show();
         $('#cheque-elem').show();
         $('#bank-elem').show();
      } else if(val=='NetBanking'){
         $('#amount-elem').show();
         $('#bank-elem').show();
      } else {
        $('#payby-elem').hide();
      }      
    }


    $('#paymentamount').keyup(function(){
        hostelAmount = parseInt($(this).val());
        compaireAmount();
    });
    $('#installmentamount').keyup(function(){
        installmentAmount = parseInt($(this).val());
        compaireAmount();
    });
    $('.submit-btn').submit(function(e){
        
        if(payBy==1){
            //$('#installment-elem').show();
        }
        //alert(1);
    });
    function compaireAmount(){
        $('.submit-btn').attr('disabled',false);
        if(payBy==1){
            if(hostelAmount<installmentAmount){
                $('.submit-btn').attr('disabled',true);
                return true;
            }
        }
        return false;
    }

</script>
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementsByClassName(divID)[0].innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML = 
          "<html><head><title></title></head><body>" + 
          divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }
    function closeWindow() {
        location.reload(); 
    }

    function check_email(email) {
        var status = false;     
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }


    $("#send_pdf").click(function(){
        var to = $('#to').val();
        var subject = $('#subject').val();
        var message = $('#message').val();
        var id = "<?=$student->studentID;?>";
        var set = "<?=$set;?>";
        var error = 0;

        if(to == "" || to == null) {
            error++;
            $("#to_error").html("");
            $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
        } else {
            if(check_email(to) == false) {
                error++
            }
        } 

        if(subject == "" || subject == null) {
            error++;
            $("#subject_error").html("");
            $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
        } else {
            $("#subject_error").html("");
        }

        if(error == 0) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('hmember/send_mail')?>",
                data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message+ "&set=" + set,
                dataType: "html",
                success: function(data) {
                    location.reload();
                }
            });
        }
    });
</script>