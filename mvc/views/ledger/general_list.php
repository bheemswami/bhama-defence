
<?php $userTypeId = $this->session->userdata('usertypeID');?>

<?php if($userTypeId==1 || $userTypeId==5 || $userTypeId == 9){?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i>&nbsp;&nbsp;<?=$this->lang->line('gen_ledger')?></h3>
        <!-- <h3 class="box-title" style="margin-left:60%">Available Balance: <strong>₹ <?=number_format($availableBalance)?></strong></h3> -->
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('gen_ledger')?></li>
        </ol>
    </div><!-- /.box-header -->

    <!-- form start -->
    <div class="box-body">
        <div class="row"> 
            <form method="POST">
                <?php if($userTypeId==1) { ?> 
                    <div class="col-sm-2" id="pstatus_div">
                        <label>General Entry For</label>
                        <select class="form-control" name="exp_inc_for">
                            <option value="">--Select--</option>
                            <option <?=($exp_inc_for == "1") ? "selected" : ""?> value="1">Hostel</option>
                            <option <?=($exp_inc_for == "0") ? "selected" : ""?> value="0">Academic</option>
                        </select>
                    </div>
                <?php } ?>
                <div class="col-sm-2" id="vendor_div">
                    <label>Vendor</label>
                    <select class="form-control" name="vendor" id="vendor">
                        <option value="">--SELECT--</option>
                        <?php foreach($vendors_list as $vendorRow) {?>
                            <option <?=($vendorRow['vendorID'] == $vendor) ? "selected" : ""?> value="<?=$vendorRow['vendorID']?>"><?=$vendorRow['name']?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-2" id="pstatus_div">
                    <label>Payment Status</label>
                    <select class="form-control" name="payment_status" id="p_status">
                        <option value="">--Select--</option>
                        <option <?=($payment_status == "1") ? "selected" : ""?> value="1">Paid</option>
                        <option <?=($payment_status == "0") ? "selected" : ""?> value="0">Due</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label>Payment Method</label>
                    <select class="form-control" name="payment_method" id="payment_method">
                        <option value="">--Select--</option>
                        <option <?=($payment_method == "1") ? "selected" : ""?> value="1">Cash</option>
                        <option <?=($payment_method == "2") ? "selected" : ""?> value="2">Cheque</option>
                        <option <?=($payment_method == "3") ? "selected" : ""?> value="3">Online Payment</option> 
                    </select>
                </div>
                 <div class="col-sm-1">
                    <label>From Date</label>
                    <input class="form-control" type="text" id="date1" name="from_date" value="<?=$from_date?>">
                </div>
                <div class="col-sm-1">
                    <label>To Date</label>
                    <input class="form-control" type="text" id="date2" name="to_date" value="<?=$to_date?>">
                </div>
                <div class="col-sm-1">
                    <label>&nbsp;</label>
                    <input type="submit" value="Filter" class="form-control btn btn-primary">
                </div>
            </form>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Date</th>
                                <th>Vendor</th>
                                <th>Particulars</th>
                                <th>Payment Method</th>
                                <th>Payment Status</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php $totalBalance = $totalPaidBalance = $totalDueBalance = 0; 
                            foreach($expenses as $k=>$row) {  
                                $totalBalance = $totalBalance+$row->amount;
                                if($row->payment_status==1) $totalPaidBalance = $totalPaidBalance+$row->amount;
                                else $totalDueBalance = $totalDueBalance+$row->amount;
                                ?>
                                <tr>
                                    <td><?=$k+1?></td>
                                    <td><?=date('d-M-Y', strtotime($row->date))?></td>
                                    <td><?=$row->vendor_name?></td>
                                    <td><?=$row->note?></td>
                                    <td><?=$this->config->item('PAYMENT_METHODS_IDS')[$row->payment_method]?> <?=($row->cheque_no) ? '(Cheque No: '.$row->cheque_no.')' : ""?></td>
                                    <td><?=($row->payment_status==0) ? '<span class="text-danger"><b> Due </b></span>' : '<span class="text-success"><b> Paid </b></span>'; ?></td>
                                    <td>&#8377; <?=$row->amount?></td>
                                </tr>
                            <?php }
                                
                            ?>
                        </tbody>
                    </table><hr>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer print1">
                                <thead>
                                    <tr>
                                        <th class="active text-right">Total Amount</th>
                                        <th class="text-left success">₹ <?=$totalBalance?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right">Total Paid</th>
                                        <th class="text-left danger">₹ <?=$totalPaidBalance?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right">Total Due</th>
                                        <th class="text-left danger">₹ <?=$totalDueBalance?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- <?php $total = $income - $expense; ?> -->
                    <!-- <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer print1">
                                <thead>
                                    <tr>
                                        <th class="active text-right"><strong>Total</strong></th>
                                        <th class="text-left <?=($total > 0) ? 'success' : (($total < 0) ? 'danger' : '')?>"><strong>₹ <?=number_format($total)?></strong></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div id="dvjson">
</div>

<?php if(!in_array($userTypeId, [1,5,9])){?>
<div class="callout callout-danger">
    <p><b>Permisiion Denied:</b> Please contact to admin to access this page.</p>
</div>
<?php } ?>

<script>
    $(function() {
        setTimeout(function() {
        $('.buttons-copy').hide();
        $('.buttons-excel').hide();
        $('.buttons-csv').hide();
        $('.buttons-pdf').hide();
        $('#example1_filter').hide();
    }, 100)
    });

   

    $('#p_status').change(function(){
        //if ($(this).val() === "0") $("#payment_method").attr('disabled', true);
        //else $("#payment_method").attr('disabled', false);
    }) 

    
    $(function () {  $("#date1, #date2").datepicker(); });

</script>
