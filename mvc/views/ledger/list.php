
<?php $userTypeId = $this->session->userdata('usertypeID');?>

<?php if($userTypeId==1 || $userTypeId==5 || $userTypeId == 9){?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i>&nbsp;&nbsp;<?=$this->lang->line($wardens ? 'hostel_ledger' : 'academic_ledger')?></h3>
        <!-- <h3 class="box-title" style="margin-left:60%">Available Balance: <strong>₹ <?=number_format($availableBalance)?></strong></h3> -->
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('bcrumb')?></li>
        </ol>
    </div><!-- /.box-header -->

    <!-- form start -->
    <div class="box-body">
        <div class="row"> 
            <form method="POST">
                <div class="form-group parrent-input">
                     <!-- <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('pay_status')?> </label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="payment_status" id="payment_status_paid" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="" />
                                 <label for="payment_status_paid" class="control-label pd-btm-5"><?=$this->lang->line('pay_status_paid')?></label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="payment_status" id="payment_status_paid" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="1" <?=($payment_status==1) ? "checked" : ""?>/>
                                 <label for="payment_status_paid" class="control-label pd-btm-5"><?=$this->lang->line('pay_status_paid')?></label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="payment_status" id="payment_status_due" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="0" <?=($payment_status==0) ? "checked" : ""?> />
                                 <label for="payment_status_due" class="control-label pd-btm-5"><?=$this->lang->line('pay_status_due')?></label>
                              </div>
                           </div>
                        </div>
                     </div> -->
                  </div>
                <div class="col-sm-2">
                    <label><?=$this->lang->line('label_user')?></label>
                    <select class="form-control" id="user" name="user">
                        <option value="">--SELECT--</option>
                        <?php foreach($userTypes as $userType) { ?>
                            <option <?=($user == $userType['usertypeID']) ? "selected" : ""?> value="<?=$userType['usertypeID']?>"><?=$userType['usertype']?></option>
                        <?php } ?>
                        <option <?=($user == 10) ? "selected" : ""?> value="10">Vendor</option>
                    </select>
                </div>
                <div class="col-sm-2" id="vendor_div">
                    <label>Vendor<?=$this->lang->line('label_vendor')?></label>
                    <select class="form-control" name="vendor" id="vendor">
                        <option value="">--SELECT--</option>
                        <option value="all">All</option>
                        <?php foreach($vendors as $vendor) {?>
                            <option value="<?=$vendor['vendorID']?>"><?=$vendor['name']?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-2" id="student_div">
                    <label><?=$this->lang->line('label_student')?></label>
                    <select class="form-control" name="student" id="student">
                        <option value="">--SELECT--</option>
                        <option value="all">All</option>
                        <?php foreach($students as $student) { ?>
                            <option value="<?=$student['studentID']?>"><?=$student['name']?></option>
                        <?php  } ?>
                    </select>
                </div>
                <?php if ($teachers) { ?> 
                    <div class="col-sm-2" id="teacher_div">
                        <label><?=$this->lang->line('label_lect')?></label>
                        <select class="form-control" name="teacher" id="teacher">
                            <option value="">--SELECT--</option>
                            <option value="all">All</option>
                            <?php foreach($teachers as $teacher) {?>
                                <option value="<?=$teacher['teacherID']?>"><?=$teacher['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <?php if ($accountants) { ?>

                    <div class="col-sm-2" id="accountant_div">
                        <label><?=$this->lang->line('label_accountant')?></label>
                        <select class="form-control" name="accountant" id="accountant">
                            <option value="">--SELECT--</option>
                            <option value="all">All</option>
                            <?php foreach($accountants as $accountant) {?>
                                <option value="<?=$accountant['userID']?>"><?=$accountant['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <?php if ($librarians) { ?>
                    <div class="col-sm-2" id="librarian_div">
                        <label><?=$this->lang->line('label_librarian')?></label>
                        <select class="form-control" name="librarian" id="librarian">
                            <option value="">--SELECT--</option>
                            <option value="all">All</option>
                            <?php foreach($librarians as $librarian) {?>
                                <option value="<?=$librarian['userID']?>"><?=$librarian['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <?php if ($receptionists) { ?>
                    <div class="col-sm-2" id="recep_div">
                        <label><?=$this->lang->line('label_receptionist')?></label>
                        <select class="form-control" name="receptionist" id="receptionist">
                            <option value="">--SELECT--</option>
                            <option value="all">All</option>
                            <?php foreach($receptionists as $receptionist) {?>
                                <option value="<?=$receptionist['userID']?>"><?=$receptionist['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <?php if ($wardens) { ?>
                    <div class="col-sm-2" id="warden_div">
                        <label><?=$this->lang->line('label_warden')?></label>
                        <select class="form-control" name="warden" id="warden">
                            <option value="">--SELECT--</option>
                            <option value="all">All</option>  
                            <?php foreach($wardens as $warden) {?>
                                <option value="<?=$warden['userID']?>"><?=$warden['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <div class="col-sm-2">
                    <label><?=$this->lang->line('label_from_date')?></label>
                    <input class="form-control" type="text" id="date1" name="from_date" value="<?=$from_date?>">
                </div>
                <div class="col-sm-2">
                    <label><?=$this->lang->line('label_to_date')?></label>
                    <input class="form-control" type="text" id="date2" name="to_date" value="<?=$to_date?>">
                </div>
                <div class="col-sm-2" id="ptype_div">
                    <label><?=$this->lang->line('label_payment_type')?></label>
                    <select class="form-control" name="payment_type" id="type">
                        <option value="">All</option>
                        <option <?=($payment_type == "expense") ? "selected" : ""?> value="expense">Expense</option>
                        <option <?=($payment_type == "income") ? "selected" : ""?> value="income">Income</option>
                        <option <?=($payment_type == "payment") ? "selected" : ""?> value="payment">Payment</option>
                        <option <?=($payment_type == "receipt") ? "selected" : ""?> value="receipt">Receipt</option>
                    </select>
                </div>
                <div class="col-sm-2" id="pstatus_div">
                    <label><?=$this->lang->line('label_payment_status')?></label>
                    <select class="form-control" name="payment_status" id="p_status">
                        <option value="">All</option>
                        <option <?=($payment_status == "1") ? "selected" : ""?> value="1">Paid</option>
                        <option <?=($payment_status == "0") ? "selected" : ""?> value="0">Due</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label><?=$this->lang->line('label_payment_method')?></label>
                    <select class="form-control" name="payment_method" id="payment_method">
                        <option value="">All</option>
                        <option <?=($payment_method_resend == "1") ? "selected" : ""?> value="1">Cash</option>
                        <option <?=($payment_method_resend == "2") ? "selected" : ""?> value="2">Bank</option>
                        <!-- <option <?=($payment_method_resend == "2") ? "selected" : ""?> value="2">Cheque</option>
                        <option <?=($payment_method_resend == "3") ? "selected" : ""?> value="3">Online Payment</option> -->
                    </select>
                </div>
                <div class="col-sm-1">
                    <label>&nbsp;</label>
                    <input type="submit" value="Filter" class="form-control btn btn-primary">
                </div>
            </form>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer print1">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('th_date')?></th>
                                <th><?=$this->lang->line('th_particulars')?></th>
                                <th><?=$this->lang->line('th_debit')?></th>
                                <th><?=$this->lang->line('th_credit')?></th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php $income = $expense = $payment = $receipt = $netBalance = $p_expense = $up_expense = $p_income = $up_income = 0; foreach($ledgers as $ledger) { 
                                if ($ledger['type'] == "income") {
                                    if ($ledger['status'] >= 0) {
                                        if ((int)$ledger['status'] == 1) {
                                            $p_income += $ledger['credit'];
                                        } 
                                        if ((int)$ledger['status'] == 0) {
                                            $up_income += $ledger['credit'];
                                        }
                                    }
                                    $income += $ledger['credit'];
                                }
                                if ($ledger['type'] == "expense") {
                                    if ($ledger['status'] >= 0) {
                                        if ((int)$ledger['status'] == 1) {
                                            $p_expense += $ledger['debit'];
                                        } 
                                        if ((int)$ledger['status'] == 0) {
                                            $up_expense += $ledger['debit'];
                                        }
                                    }
                                    $expense += $ledger['debit'];
                                }
                                if ($ledger['type'] == "payment") $payment += $ledger['debit'];
                                if ($ledger['type'] == "receipt") $receipt += $ledger['credit'];
                                ?>
                                <tr>
                                    <td><?=date('d-M-Y', strtotime($ledger['date']))?></td>
                            <td><?=$ledger['particular']?> <?php if($ledger['payment_method'] ||$ledger['cheque_no'] ){?><?='('.$ledger['payment_method'].')'?> <?=($ledger['cheque_no']) ? '(Cheque No: '.$ledger['cheque_no'].')' : ""?><?php } ?></td>
                                    <td><?=$ledger['debit']?></td>
                                    <td><?=$ledger['credit']?></td>
                                </tr>
                            <?php }
                                $netBalance = ($total_balance + $income) - $expense;
                            ?>
                        </tbody>
                    </table><hr>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer print1">
                                <thead>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_total_income')?></th>
                                        <th class="text-left success">₹ <?=$income?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_total_expense')?></th>
                                        <th class="text-left danger">₹ <?=$expense?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_total_payment')?></th>
                                        <th class="text-left danger">₹ <?=$payment?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_total_receipt')?></th>
                                        <th class="text-left success">₹ <?=$receipt?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_paid_expense')?></th>
                                        <th class="text-left success">₹ <?=$p_expense?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_unpaid_expense')?></th>
                                        <th class="text-left danger">₹ <?=$up_expense - $payment?></th>
                                    </tr>

                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_paid_income')?></th>
                                        <th class="text-left success">₹ <?=$p_income?></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_unpaid_income')?></th>
                                        <th class="text-left danger">₹ <?=$up_income - $receipt?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer print1">
                                <thead>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_cash_bal')?></th>
                                        <th class="text-left <?=$balances['cash'] > 0 ? 'success' : 'danger'?>"><b>₹ <?=$balances['cash']?></b></th>
                                    </tr>
                                    <tr>
                                        <th class="active text-right"><?=$this->lang->line('th_bank_bal')?></th>
                                        <th class="text-left <?=$balances['bank'] > 0 ? 'success' : 'danger'?>"><b>₹ <?=$balances['bank']?><b></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- <?php $total = $income - $expense; ?> -->
                    <!-- <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer print1">
                                <thead>
                                    <tr>
                                        <th class="active text-right"><strong>Total</strong></th>
                                        <th class="text-left <?=($total > 0) ? 'success' : (($total < 0) ? 'danger' : '')?>"><strong>₹ <?=number_format($total)?></strong></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div id="dvjson">
</div>

<?php if(!in_array($userTypeId, [1,5,9])){?>
<div class="callout callout-danger">
    <p><b>Permisiion Denied:</b> Please contact to admin to access this page.</p>
</div>
<?php } ?>

<div id="voucher" style="display: none;">
<p id="json_data">Hello</p>
</div>
<script>
    $(function() {
        setTimeout(function() {
        $('.buttons-copy').hide();
        $('.buttons-excel').hide();
        $('.buttons-csv').hide();
    }, 100)
    });

    $('#user').change(function() {
        hide();
        val = $(this).val();
        if (val == 2) {$('#teacher_div').show(); $('#teacher').attr('required', true).val('all')}
        if (val == 3) {$('#student_div').show(); $('#student').attr('required', true).val('all')}
        if (val == 5) {$('#accountant_div').show(); $('#accountant').attr('required', true).val('all')}
        if (val == 6) {$('#librarian_div').show(); $('#librarian').attr('required', true).val('all')}
        if (val == 7) {$('#recep_div').show(); $('#receptionist').attr('required', true).val('all')}
        if (val == 9) {$('#warden_div').show(); $('#warden').attr('required', true).val('all')}
        if (val == 10) {$('#vendor_div').show(); $('#vendor').attr('required', true).val('all')}
    })

    $('#p_status').change(function(){
        if ($(this).val() === "0") $("#payment_method").attr('disabled', true);
        else $("#payment_method").attr('disabled', false);
    }) 

    function hide() {
        $('#student_div, #teacher_div, #accountant_div, #librarian_div, #recep_div, #warden_div, #vendor_div')
        .hide()
        .children('select')
        .val('').attr('required', false);
    }
    $(function () { hide(); $("#date1, #date2").datepicker(); });

    $(function(){
        $('#p_status').change();
        if ("<?=$user?>" != ""){
            
            let user = "<?=$user?>";
            $('#user').change();
            if (user == 2) $('#teacher').val(<?=$teacherr?>);
            if (user == 3) $('#student').val(<?=$studentt?>);
            if (user == 5) $('#accountant').val(<?=$accountantt?>);
            if (user == 6) $('#librarian').val(<?=$librariann?>);
            if (user == 7) $('#receptionist').val(<?=$receptionistt?>);
            if (user == 9) $('#warden').val(<?=$wardenn?>);
            if (user == 10) $('#vendor').val(<?=$vendorr?>);
        }
    })
</script>
