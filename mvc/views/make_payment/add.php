

<div class="row">
    <div class="col-sm-12">
        <div class="box" style="margin-bottom:40px">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-money"></i> <?=$this->lang->line('panel_title')?> : <?=$user->name?> <span style="font-size: 12px">(<?=$usertype->usertype?>)</span></h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <form role="form" method="post" id="add_payment">
                  <div class="row">  
                    <div class="form-group col-sm-4 <?=form_error('treasury_num') ? 'has-error' : '' ?>">
                        <label for="treasury_num"><?=$this->lang->line('mp_treasury_num')?></label> 
                        <input type="text" name="treasury_num" class="form-control" id="treasury_num" value="<?=set_value('treasury_num')?>">
                        <span class="text-red"><?=form_error('treasury_num')?></span>
                    </div>

                    <div class="form-group col-sm-4 <?=form_error('month') ? 'has-error' : '' ?>">
                        <label for="month"><?=$this->lang->line('make_payment_month')?></label>
                        <input type="type" name="month" class="form-control" id="month" value="<?=set_value('month', date('m-Y'))?>">
                        <span class="text-red"><?=form_error('month')?></span>
                    </div>
                     <div class="form-group col-sm-4 <?=form_error('make_payment_date') ? 'has-error' : '' ?>">
                        <label for="make_payment_date"><?=$this->lang->line('make_payment_date')?></label> 
                        <input type="text" name="make_payment_date" class="form-control" id="make_payment_date" value="<?=set_value('make_payment_date')?>">
                        <span class="text-red"><?=form_error('make_payment_date')?></span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('payment_amount') ? 'has-error' : '' ?>">
                        <label for="payment_amount"><?=$this->lang->line('make_payment_payment_amount')?></label> 
                        <input type="text" name="payment_amount" class="form-control" id="payment_amount" value="<?=set_value('payment_amount')?>">
                        <span class="text-red"><?=form_error('payment_amount')?> </span>
                    </div>

                    <div class="form-group col-sm-4 <?=form_error('additional_pay') ? 'has-error' : '' ?>">
                        <label for="additional_pay"><?=$this->lang->line('mp_additional_pay')?></label> 
                        <input type="text" name="additional_pay" class="form-control" id="additional_pay" value="<?=set_value('additional_pay')?>">
                        <span class="text-red"><?=form_error('additional_pay')?></span>
                    </div>

                    <div class="form-group col-sm-4 <?=form_error('interim_relief') ? 'has-error' : '' ?>">
                        <label for="interim_relief"><?=$this->lang->line('mp_interim_relief')?></label> 
                        <input type="text" name="interim_relief" class="form-control" id="interim_relief" value="<?=set_value('interim_relief')?>">
                        <span class="text-red"><?=form_error('interim_relief')?></span>
                    </div>

                    <div class="form-group col-sm-4 <?=form_error('other_allowance') ? 'has-error' : '' ?>">
                        <label for="other_allowance"><?=$this->lang->line('mp_other_allowance')?></label> 
                        <input type="text" name="other_allowance" class="form-control" id="other_allowance" value="<?=set_value('other_allowance')?>">
                        <span class="text-red"><?=form_error('other_allowance')?></span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('house_rent') ? 'has-error' : '' ?>">
                        <label for="house_rent"><?=$this->lang->line('mp_house_rent')?></label> 
                        <input type="text" name="house_rent" class="form-control" id="house_rent" value="<?=set_value('house_rent')?>">
                        <span class="text-red"><?=form_error('house_rent')?></span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('advance') ? 'has-error' : '' ?>">
                        <label for="advance"><?=$this->lang->line('mp_advance')?></label> 
                        <input type="text" name="advance" class="form-control" id="advance" value="<?=set_value('advance')?>">
                        <span class="text-red"><?=form_error('advance')?></span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('provident_fund') ? 'has-error' : '' ?>">
                        <label for="provident_fund"><?=$this->lang->line('mp_provident_fund')?></label> 
                        <input type="text" name="provident_fund" class="form-control" id="provident_fund" value="<?=set_value('provident_fund')?>">
                        <span class="text-red"><?=form_error('provident_fund')?></span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('insurance_fund') ? 'has-error' : '' ?>">
                        <label for="insurance_fund"><?=$this->lang->line('mp_insurance_fund')?></label> 
                        <input type="text" name="insurance_fund" class="form-control" id="insurance_fund" value="<?=set_value('insurance_fund')?>">
                        <span class="text-red"><?=form_error('insurance_fund')?></span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('other_deduction') ? 'has-error' : '' ?>">
                        <label for="other_deduction"><?=$this->lang->line('mp_other_deduction')?></label> 
                        <input type="text" name="other_deduction" class="form-control" id="other_deduction" value="<?=set_value('other_deduction')?>">
                        <span class="text-red"><?=form_error('other_deduction')?></span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('policy_num') ? 'has-error' : '' ?>">
                        <label for="policy_num"><?=$this->lang->line('mp_policy_num')?></label> 
                        <input type="text" name="policy_num" class="form-control" id="policy_num" value="<?=set_value('policy_num')?>">
                        <span class="text-red"><?=form_error('policy_num')?></span>
                    </div>
                   

                  
                    <div class="form-group col-sm-4 <?=form_error('payment_method') ? 'has-error' : '' ?>">
                        <label for="payment_method"><?=$this->lang->line('make_payment_payment_method')?></label>
                        <?php
                            $paymentArray = array(
                                '0' => $this->lang->line('make_payment_select_payment_method'),
                                '1' => $this->lang->line('make_payment_payment_cash'),
                                '2' => $this->lang->line('make_payment_payment_cheque'),
                                '3' => $this->lang->line('make_payment_payment_online')
                            );

                            echo form_dropdown("payment_method", $paymentArray, set_value("payment_method"), "id='payment_method' class='form-control'");
                        ?>
                        <span class="text-red">
                            <?=form_error('payment_method')?>
                        </span>
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('account_no') ? 'has-error' : '' ?>" id="account_div">
                        <label for="account_no"><?=$this->lang->line('account_no')?></label>
                        <input type="text" name="account_no" class="form-control" id="account_no">
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('cheque_no') ? 'has-error' : '' ?>" id="cheque_div">
                        <label for="cheque_no"><?=$this->lang->line('cheque_no')?></label>
                        <input type="text" name="cheque_no" class="form-control" id="cheque_no">
                    </div>
                    <div class="form-group col-sm-4 <?=form_error('comments') ? 'has-error' : '' ?>">
                        <label for="comments"><?=$this->lang->line('make_payment_comments')?></label>
                        <input type="text" name="comments" class="form-control" id="comments">
                    </div>
                </div>
                  
                    
                <div class="row">
                    <div class=" col-sm-12">
                        <button type="submit" class="btn btn-success"><?=$this->lang->line('add_payment')?></button>
                    </div>
                </div>
                </form>
            </div>
        </div>

    </div>

    <div class="col-sm-12">
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><i class="fa icon-payment"></i> <?=$this->lang->line('make_payment_payment_history')?></h3>
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("make_payment/index/$usertype->usertypeID")?>"><?=$this->lang->line('menu_make_payment')?></a></li>
                    <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_make_payment')?></li>
                </ol>
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('mp_treasury_num')?></th>
                                <th><?=$this->lang->line('make_payment_month')?></th>
                                <th><?=$this->lang->line('make_payment_date')?></th>
                                <th><?=$this->lang->line('make_payment_payment_amount')?></th>
                                <th><?=$this->lang->line('mp_total_salary')?></th>
                                <th><?=$this->lang->line('mp_total_deduction')?></th>
                                <th><?=$this->lang->line('make_payment_net_salary')?></th>
                                <th><?=$this->lang->line('account_no')?></th>
                                <?php if(permissionChecker('make_payment')) { ?>
                                    <th><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($make_payments)) { $i = 1; foreach($make_payments as $make_payment) { 
                                $salary = totalSalaryAmount($make_payment);?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('mp_treasury_num')?>">
                                        <?php echo $make_payment->treasury_num; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('make_payment_month')?>">
                                        <?php echo date("M Y", strtotime('1-'.$make_payment->month)); ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('make_payment_date')?>">
                                        <?php echo date("d M Y", strtotime($make_payment->create_date)); ?>
                                    </td>
                                   
                                    <td data-title="<?=$this->lang->line('make_payment_amount')?>">
                                        <?php echo $make_payment->payment_amount; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('mp_total_salary')?>">
                                        <?php echo $salary['totalAddition']; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('mp_total_deduction')?>">
                                        <?php echo $salary['totalDeduction']; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('make_payment_net_salary')?>">
                                        <?php echo $salary['netSalary']; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('account_no')?>">
                                        <?php echo $make_payment->account_no; ?>
                                    </td>

                                    <?php if(permissionChecker('make_payment')) { ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <a href="<?=base_url("make_payment/print_pay_distribution/$make_payment->make_paymentID")?>" class="btn btn-danger btn-xs mrg" target="_blank"><i class="fa fa-print"></i></a>
                                        <a href="<?=base_url("make_payment/view/$make_payment->make_paymentID")?>" class="btn btn-xs btn-success" data-placement="top" data-toggle="tooltip" data-original-title="<?=$this->lang->line('view')?>"><i class='fa fa-check-square-o'></i></a>

                                        <a href="<?=base_url("make_payment/delete/$make_payment->make_paymentID")?>" class="btn btn-xs btn-danger" data-placement="top" data-toggle="tooltip" data-original-title="<?=$this->lang->line('delete')?>"><i class='fa fa fa-trash-o'></i></a>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    $(function() {
        $("#account_div").hide();
        $("#cheque_div").hide();
        $('#payment_method').change();
    });

    $('#add_payment').submit (function (event) {
        if ($('#payment_method').val() == 3) {
            if ($('#account_no').val() == '') {
                event.preventDefault();
                alert ("A/C Number must be present in case of online payment");
            }
        }
        if ($('#payment_method').val() == 2) {
            if ($('#cheque_no').val() == '') {
                event.preventDefault();
                alert ("Cheque No. Number must be present in case of Cheque payment");
            }
        }
    })

    $("#payment_method").change(function () {
        if ($(this).val() == 3) {
            $("#account_div").show();
        } else {
            $("#account_div").hide();
        }
    })

    $("#payment_method").change(function () {
        if ($(this).val() == 2) {
            $("#cheque_div").show();
        } else {
            $("#cheque_div").hide();
        }
    })

    $("#month").datepicker( {
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months"
    });
    $("#make_payment_date").datepicker();

</script>





