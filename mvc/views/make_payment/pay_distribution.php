<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    
    <STYLE TYPE="text/css">
    

        table tbody tr td{
            padding: 5px 10px !important;
        }
        font{font-size:14px !important;}
        body{font-family: arial}
   
    </STYLE>
</HEAD>
<BODY LANG="en-US">
     <!--  <div style=" width: 60%; text-align: right;">
        <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('print_letter')"><span class="fa fa-print"></span> <?php //$this->lang->line('print')?> </button>
    </div> -->
<div style="margin: auto;width: 640px;height: 100%;background: #fff;padding: 35px;box-shadow: 0px 0px 4px #333;" id="print_slip">
<P STYLE="margin-bottom: 0.14in">
    <SPAN >
    
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=5 STYLE="font-size: 20pt"><B><?php if($siteinfos) { echo namesorting($siteinfos->sname, 100); } ?></B></FONT></P>
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2>Salary
    Slip</FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Employee
    Name:</B></FONT><FONT SIZE=2><B>
    <?=$make_payment->userName?></B></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Designation:</B></FONT><FONT SIZE=4>
    <?=@$designation->usertype?></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Month
    of Payment:</B></FONT><FONT SIZE=4>
    <?=date('M Y', strtotime('1-'.$make_payment->month))?></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Treasury Bill No.:</B></FONT><FONT SIZE=4>
    <?=$make_payment->treasury_num; ?></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <CENTER>
        <TABLE WIDTH=626 CELLPADDING=7 CELLSPACING=0>
            <COL WIDTH=142>
            <COL WIDTH=143>
            <COL WIDTH=143>
            <COL WIDTH=142>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=3><B>Earnings</B></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=3><B>Deductions</B></FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
            </TR>
      
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Payment</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->payment_amount, 2)?></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Insurance Deductions</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->insurance_fund, 2)?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                 <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Advance</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->advance, 2)?></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Provident Fund</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->provident_fund, 2)?></FONT></P>
                </TD>
                
            </TR>
            <TR VALIGN=TOP>
                 <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Additional Pay</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->additional_pay, 2)?></FONT></P>
                </TD>                 
               <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Other Deduction</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->other_deduction, 2)?></FONT></P>
                </TD>
                
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Intern Relief</FONT>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->interim_relief, 2)?></FONT></P>
                </TD>              
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Policy No.</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=$make_payment->policy_num?></FONT></P>
                </TD>
               
            </TR>
            
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>House Rent</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->house_rent, 2)?></FONT></P>
                </TD>
                 <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><BR>
                    </P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
               <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Other Allowance</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=number_format($make_payment->other_allowance, 2)?></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><BR>
                    </P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><BR>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><BR>
                    </P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Total Addition</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=$salary['totalAddition']?></FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Total Deduction</FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=$salary['totalDeduction']?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><BR>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=3><B>NET Salary</B></FONT></P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=3><B><?=$salary['netSalary']?></B></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><BR>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><BR>
                    </P>
                </TD>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><BR>
                    </P>
                </TD>
            </TR>
        </TABLE>
    </CENTER>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2><B><?=$salary['netSalaryInWord']?></B></FONT></P>
   
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2>Signature
    of the Employee: __________________________     Director:
    ____________________</FONT></P>
</SPAN>
</div>
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        var divElements = document.getElementById(divID).innerHTML;
        var oldPage = document.body.innerHTML;
        document.body.innerHTML = "<html><head><title></title></head><body>" + divElements + "</body>";
        window.print();
        document.body.innerHTML = oldPage;
    }
   printDiv('print_slip');
</script>
</BODY>
</HTML>