

<?php if($user) { ?>
    <div class="well">
        <div class="row">

            <div class="col-sm-7">
               
            </div>

            <div class="col-sm-5">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("make_payment/add/$userID/$usertypeID")?>"><?=$this->lang->line('menu_make_payment')?></a></li>
                    <li class="active"><?=$this->lang->line('view')?></li>
                </ol>
            </div>

        </div>
    </div>
<?php } ?>

<?php if($user) {
    $salary = totalSalaryAmount($make_payment);
 ?>
    <div id="printablediv">
        <div class="row">
            <div class="col-sm-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="student-username text-center"><?=$user->name?></h3>
                        <p class="text-muted text-center"><?=$usertype ? $usertype->usertype : ''?></p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item" style="background-color: #FFF">
                                <b><?=$this->lang->line('make_payment_gender')?></b> <a class="pull-right"><?=$user->sex?></a>
                            </li>
                            <li class="list-group-item" style="background-color: #FFF">
                                <b><?=$this->lang->line('make_payment_dob')?></b> <a class="pull-right"><?=($user->dob) ? date('d M Y', strtotime($user->dob)) : '';?></a>
                            </li>
                            <li class="list-group-item" style="background-color: #FFF">
                                <b><?=$this->lang->line('make_payment_phone')?></b> <a class="pull-right"><?=$user->phone?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#salary" data-toggle="tab"><?=$this->lang->line('make_payment_salary')?></a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="active tab-pane" id="salary">
                            <br>
                            <div class="row">
                                <div class="col-sm-6" style="margin-bottom: 10px;">
                                    <div class="info-box">
                                        
                                            <?php if($make_payment->salaryID == 1) { ?>
                                                <p style="margin:0 0 20px">
                                                    <span><?=$this->lang->line("make_payment_salary_grades")?></span>
                                                    <?=$persent_salary_template->salary_grades?>
                                                </p>
                                                <p style="margin:0 0 20px">
                                                    <span><?=$this->lang->line("make_payment_basic_salary")?></span>
                                                    <?=number_format($persent_salary_template->basic_salary, 2)?>
                                                </p>
                                                <p style="margin:0 0 20px">
                                                    <span><?=$this->lang->line("make_payment_overtime_rate")?></span>
                                                    <?=number_format($persent_salary_template->overtime_rate, 2)?>
                                                </p>
                                            <?php } ?>
                                        

                                        <p style="margin:0 0 20px">
                                            <span><?=$this->lang->line("make_payment_month")?></span>
                                            <?=date('M Y', strtotime('1-'.$make_payment->month))?>
                                        </p>

                                        <p style="margin:0 0 20px">
                                            <span><?=$this->lang->line("make_payment_date")?></span>
                                            <?=date('d M Y', strtotime($make_payment->create_date))?>
                                        </p>

                                        <p style="margin:0 0 20px">
                                            <span><?=$this->lang->line("make_payment_payment_method")?></span>
                                            <?=isset($paymentMethod[$make_payment->payment_method]) ? $paymentMethod[$make_payment->payment_method] : ''?>
                                        </p>

                                        <p style="margin:0 0 20px">
                                            <span><?=$this->lang->line("make_payment_comments")?></span>
                                            <?=$make_payment->comments?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                           
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="box" style="border: 1px solid #eee">
                                            <div class="box-header" style="background-color: #fff;border-bottom: 1px solid #eee;color: #000;">
                                                <h3 class="box-title" style="color: #1a2229"><?=$this->lang->line('make_payment_allowances')?></h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-sm-12" id="allowances">
                                                        <div class="info-box">
                                                            <p><span>Payment</span><?=number_format($make_payment->payment_amount, 2)?></p>
                                                            <p><span><?=$this->lang->line('mp_additional_pay')?></span> <?=number_format($make_payment->additional_pay, 2)?></p>                                                           
                                                            <p><span><?=$this->lang->line('mp_interim_relief')?></span> <?=number_format($make_payment->interim_relief, 2)?></p>                                                           
                                                            <p><span><?=$this->lang->line('mp_house_rent')?></span> <?=number_format($make_payment->house_rent, 2)?></p>                                                           
                                                            <p><span><?=$this->lang->line('mp_advance')?></span> <?=number_format($make_payment->advance, 2)?></p>                                                           
                                                            <p><span><?=$this->lang->line('mp_other_allowance')?></span> <?=number_format($make_payment->other_allowance, 2)?></p>                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="box" style="border: 1px solid #eee;">
                                            <div class="box-header" style="background-color: #fff;border-bottom: 1px solid #eee;color: #000;">
                                                <h3 class="box-title" style="color: #1a2229"><?=$this->lang->line('make_payment_deductions')?></h3>
                                            </div><!-- /.box-header -->
                                            <!-- form start -->
                                            
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-sm-12" id="deductions">
                                                        <div class="info-box">
                                                            <p><span><?=$this->lang->line('mp_provident_fund')?></span> <?=number_format($make_payment->provident_fund, 2)?></p>    
                                                            <p><span><?=$this->lang->line('mp_insurance_fund')?></span> <?=number_format($make_payment->insurance_fund, 2)?></p>    
                                                            <p><span><?=form_error('other_deduction')?></span> <?=number_format($make_payment->other_deduction, 2)?></p>    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <div class="box" style="border: 1px solid #eee;">
                                        <div class="box-header" style="background-color: #fff;border-bottom: 1px solid #eee;color: #000;">
                                            <h3 class="box-title" style="color: #1a2229"><?=$this->lang->line('make_payment_total_salary_details')?></h3>
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td class="col-sm-8" style="line-height: 36px"><?=$this->lang->line('mp_total_salary')?></td>
                                                    <td class="col-sm-4" style="line-height: 36px"><?=$salary['totalAddition']?></td>
                                                </tr>

                                                <tr>
                                                    <td class="col-sm-8" style="line-height: 36px"><?=$this->lang->line('mp_total_deduction')?></td>
                                                    <td class="col-sm-4" style="line-height: 36px"><?=$salary['totalDeduction']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-sm-8" style="line-height: 36px"><?=$this->lang->line('make_payment_net_salary')?></td>
                                                    <td class="col-sm-4" style="line-height: 36px"><b><?=$salary['netSalary']?></b></td>
                                                </tr>                                             

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

