<div class="box">
   <div class="box-header">
      <h3 class="box-title"><i class="fa icon-visitorinfo"></i> <?=$this->lang->line('enq_panel_title')?></h3>
      <ol class="breadcrumb">
         <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
         <li class="active"><?=$this->lang->line('menu_visitorinfo')?></li>
      </ol>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="box-body">
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12">
               <form  id="myform" action="<?=base_url('Enquiry/add')?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("enq_course_name")?> </label>
                     <div class="col-sm-6">
                        <select name="classesID" id="classID" class="form-control" required>
                            <option value=""><?=$this->lang->line("select_course")?></option>
                            <?php 
                            foreach($classes as $k=>$c){
                              echo '<option value="'.$c->classesID.'">'.$c->classes.'</option>';
                            }
                            ?>
                        </select>
                     </div>
                  </div>
                  <div class="form-group <?=(form_error('name')) ? 'has-error' : ''?>">
                     <label for="name" class="col-sm-2 control-label"><?=$this->lang->line("enq_student_name")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('name'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('dob')) ? 'has-error' : ''?>">
                     <label for="dob" class="col-sm-2 control-label"><?=$this->lang->line("enq_student_dob")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="dob" name="dob" value="<?=set_value('dob')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('dob'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('father_name')) ? 'has-error' : ''?>">
                     <label for="father_name" class="col-sm-2 control-label"><?=$this->lang->line("enq_father_name")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="father_name" name="father_name" value="<?=set_value('father_name')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('father_name'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('father_business')) ? 'has-error' : ''?>">
                     <label for="father_business" class="col-sm-2 control-label"><?=$this->lang->line("enq_father_business")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="father_business" name="father_business" value="<?=set_value('father_business')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('father_business'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('mother_name')) ? 'has-error' : ''?>">
                     <label for="mother_name" class="col-sm-2 control-label"><?=$this->lang->line("enq_mother_name")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="mother_name" name="mother_name" value="<?=set_value('mother_name')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('mother_name'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('mother_business')) ? 'has-error' : ''?>">
                     <label for="mother_business" class="col-sm-2 control-label"><?=$this->lang->line("enq_mother_business")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="mother_business" name="mother_business" value="<?=set_value('mother_business')?>">
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('mother_business'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('address')) ? 'has-error' : ''?>">
                     <label for="address" class="col-sm-2 control-label"><?=$this->lang->line("enq_student_address")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('address'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('parent_mob')) ? 'has-error' : ''?>">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("enq_parent_number")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="parent_mob" name="parent_mob" value="<?=set_value('parent_mob')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('parent_mob'); ?></span>
                  </div>
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("enq_academic_details")?></label>
                  </div>  
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("enq_educational_details")?></label>
                     <div class="col-sm-10">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th><?=$this->lang->line("exam_name")?></th>
                                 <th><?=$this->lang->line("year")?></th>
                                 <th><?=$this->lang->line("subject")?></th>
                                 <th><?=$this->lang->line("obtain_marks")?></th>
                                 <th><?=$this->lang->line("max_marks")?></th>
                                 <th><?=$this->lang->line("percentage")?></th>
                                 <th><?=$this->lang->line("division")?></th>
                                 <th><?=$this->lang->line("board")?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php for($i=0;$i<4;$i++){ ?>
                              <tr>
                                 <?php
                                    if($i==0)
                                        echo '<th>'.$this->lang->line("secondary").'<input type="hidden" class="form-control" name="edu[exam_name][]" value="secondary"> </th>';
                                    else if($i==1)
                                        echo '<th>'.$this->lang->line("sr_secondary").'<input type="hidden" class="form-control" name="edu[exam_name][]" value="senior secondary"> </th>';
                                    else if($i==2)
                                        echo '<th>'.$this->lang->line("ba_bsc_other").'<input type="hidden" class="form-control" name="edu[exam_name][]" value="B.A,B.Sc./Other"> </th>';
                                    else
                                        echo '<th>'.$this->lang->line("ncc_sports").'<input type="hidden" class="form-control" name="edu[exam_name][]" value="NCC/Sports"> </th>';
                                    ?>
                                 <td> 
                                    <select class="form-control" name="edu[year][]" style="width: 110px;">
                                    <?php
                                       echo '<option value="">'.$this->lang->line("select_year").'</option>';
                                       for($year=1980;$year<=date('Y');$year++){
                                           echo '<option value="'.$year.'">'.$year.'</option>';
                                       }
                                       ?>
                                    </select>
                                 </td>
                                 <td> <input type="text" class="form-control" name="edu[subject][]"></td>
                                 <td> <input type="number" class="form-control obtain_marks" name="edu[obtain_marks][]"></td>
                                 <td> <input type="number" class="form-control max_marks" name="edu[max_marks][]"></td>
                                 <td> <input type="number" class="form-control percentage" name="edu[percentage][]" readonly></td>
                                 <td> <input type="text" class="form-control" name="edu[division][]"></td>
                                 <td> <input type="text" class="form-control" name="edu[board][]"></td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('parent_mob'); ?></span>
                  </div>
                    
                    
                    <div class="form-group <?=(form_error('photo')) ? 'has-error' : ''?>">
                        <label for="photo" class="col-sm-2 control-label"><?=$this->lang->line("student_photo")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="photo" class="form-control"/>
                        </div>
                        <span class="col-sm-4"><?php echo form_error('photo'); ?></span>
                    </div>

                    <div class="form-group <?=(form_error('sign')) ? 'has-error' : ''?>">
                        <label for="sign" class="col-sm-2 control-label"><?=$this->lang->line("student_sign")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="sign" class="form-control"/>
                        </div>
                        <span class="col-sm-4"><?php echo form_error('sign'); ?></span>
                    </div>
                    <hr/>
                     <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label"><?=$this->lang->line("parent_photo")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="parent_photo" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sign" class="col-sm-2 control-label"><?=$this->lang->line("parent_sign")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="parent_sign" class="form-control"/>
                        </div>
                    </div>

                  <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-8">
                        <button class="btn btn-success" type="submit" name="redirect" value="only_enquiry"><?=$this->lang->line("btn_only_enq")?></button>
                        <button class="btn btn-warning" type="submit" name="redirect" value="next_form"><?=$this->lang->line("proceed_next_mform")?></button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <br>
      </div>
   </div>
</div>
<script>
$(document).ready(function(){
  $("#clone-it").click(function(){
    $("#choose-docs").append("<div class='removed'><div class='col-sm-5 col-sm-offset-2 pd-top-15'>\
                              <div class='form-group nomrgin-ltor'>\
                                 <select name='py-height' id='py-height' class='form-control'>\
                                    <option value='0'>Choose Documents</option>\
                                    <option value='1'>10th/12th</option>\
                                    <option value='2'>10th/12th</option>\
                                    <option value='3'>10th/12th</option>\
                                 </select>\
                              </div></div>\
                              <div class='col-sm-3 pd-top-15'><div class='form-group nomrgin-ltor'> <input name='py-height' id='py-height' class='' type='File' value='Amount'></div></div><div class='col-sm-2'><i class='remove-it fa fa-minus-square' aria-hidden='true'></i></div></div>");
  }); 
  $(document).on('click',".remove-it",function(){
    $(this).parents(".removed").remove();
  });
});
</script>
<script>
   $(function() {
     $('#payment-method').change(function(){
         $('#net-banking,#cash,#cheque').hide();
       $('#' + $(this).val()).show();
     });
   });
</script>
<script>
   $('#dob').datepicker({ startView: 2 });
   var maxMarks = 0;
   var obtainMarks = 0;
   var percentage = 0;
   $(document).on('keyup click','.obtain_marks',function(){
       obtainMarks=$(this).val();
       calculatePercentage($(this));
   });
   $(document).on('keyup click','.max_marks',function(){
       maxMarks=$(this).val();
       calculatePercentage($(this));
   });
   function calculatePercentage(_this){
       var p = obtainMarks/maxMarks*100;
       _this.closest('tr').find('.percentage').val(Math.round(p * 100) / 100);
   }
</script>