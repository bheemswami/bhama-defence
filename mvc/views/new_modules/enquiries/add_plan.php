<div class="box">
   <div class="box-header">
      <h3 class="box-title"><i class="fa icon-visitorinfo"></i>Add Plan</h3>
      <ol class="breadcrumb">
         <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
         <li class="active"><?=$this->lang->line('menu_visitorinfo')?></li>
      </ol>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="box-body">
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12">
               <form  id="myform" action="<?=base_url('Enquiry/add')?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
               <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label">Plan Name </label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group nomrgin-ltor">
                                 <input name="py-height" id="py-height" class="form-control" type="text" value="">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label">Amount</label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group nomrgin-ltor">
                                 <input name="py-height" id="py-height" class="form-control" type="text" value="">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label">Status</label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group nomrgin-ltor">
                                 <select name="py-height" id="py-height" class="form-control">
                                    <option value="active">Active</option>
                                    <option value="inactive">Inactive</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-8">
                        <input class="btn btn-success" type="submit" value="Add Plan">
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <br>
      </div>
   </div>
</div>
<!-- <script>
$(document).ready(function(){
  $("#clone-it").click(function(){
    $("#cloned-elem").clone().appendTo("#choose-docs");
  });
});
</script> -->
<script>
$(document).ready(function(){
  $("#clone-it").click(function(){
    $("#choose-docs").append("<div class='removed'><div class='col-sm-5 col-sm-offset-2 pd-top-15'>\
                              <div class='form-group nomrgin-ltor'>\
                                 <select name='py-height' id='py-height' class='form-control'>\
                                    <option value='0'>Choose Documents</option>\
                                    <option value='1'>10th/12th</option>\
                                    <option value='2'>10th/12th</option>\
                                    <option value='3'>10th/12th</option>\
                                 </select>\
                              </div></div>\
                              <div class='col-sm-3 pd-top-15'><div class='form-group nomrgin-ltor'> <input name='py-height' id='py-height' class='' type='File' value='Amount'></div></div><div class='col-sm-2'><i class='remove-it fa fa-minus-square' aria-hidden='true'></i></div></div>");
  }); 
  $(document).on('click',".remove-it",function(){
    $(this).parents(".removed").remove();
  });
});
</script>
<script>
   $(function() {
     $('#payment-method').change(function(){
         $('#net-banking,#cash,#cheque').hide();
       $('#' + $(this).val()).show();
     });
   });
</script>
<script>
   //$('#dob').datepicker({ startView: 2 });
   var maxMarks = 0;
   var obtainMarks = 0;
   var percentage = 0;
   $(document).on('keyup click','.obtain_marks',function(){
       obtainMarks=$(this).val();
       calculatePercentage($(this));
   });
   $(document).on('keyup click','.max_marks',function(){
       maxMarks=$(this).val();
       calculatePercentage($(this));
   });
   function calculatePercentage(_this){
       var p = obtainMarks/maxMarks*100;
       _this.closest('tr').find('.percentage').val(Math.round(p * 100) / 100);
   }
</script>