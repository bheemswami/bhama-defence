<div class="box">
   <div class="box-header">
      <h3 class="box-title"><i class="fa icon-visitorinfo"></i> <?=$this->lang->line('admission_form')?></h3>
      <ol class="breadcrumb">
         <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
         <li class="active"><?=$this->lang->line('admission_form')?></li>
      </ol>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="box-body">
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12">
               <form  id="myform" action="<?=base_url('Enquiry/addmission_form')?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?=$this->uri->segment(3)?>"/>
               <input type="hidden" name="form_step" value="<?=$student->addmission_status?>"/>
               <input type="hidden" name="payid" value="<?=$student->addmission_form_payid?>"/>
               <input type="hidden" name="parentID" value="<?=$student->parentID?>"/>
               <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("enq_course_name")?></label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group nomrgin-ltor">
                                    <select name="course_id" id="course_list" class="form-control">
                                      <option><?=$this->lang->line("select_course")?></option>
                                      <?php 
                                      foreach($classes as $k=>$c){
                                        if($student->classesID==$c->classesID)
                                          echo '<option value="'.$c->classesID.'" selected>'.$c->classes.'</option>';
                                        else
                                          echo '<option value="'.$c->classesID.'">'.$c->classes.'</option>';
                                      }
                                      ?>
                                   </select>
                                 </div>
                           </div>
                        </div>
                     </div>
                  </div>                 
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('payment_method')?> </label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group nomrgin-ltor">
                                  <select name="paymenttype" id="payment-method" class="form-control" required> 
                                    <option value=""><?=$this->lang->line('payment_method')?></option>
                                    <?php
                                        foreach ($this->config->item($this->lang->line('payment_method_arr')) as $key => $value) {
                                            $selected='';
                                            if($student->paymenttype=='' && $key=='Cash'){
                                              $selected='selected';
                                            } else if($student->paymenttype==$key){
                                              $selected='selected';
                                            } 
                                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                        }
                                    ?>                                   
                                  </select>
                              </div>
                           </div>
                         </div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"></label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-4 hide-elem" id="bank-elem">
                              <div class="form-group nomrgin-ltor">                                                                    
                                 <input name="paymentbank" id="paymentbank" class="form-control" type="text" placeholder="<?=$this->lang->line("placeholder_bank_name")?>" value="<?=$student->paymentbank?>">
                              </div>
                           </div>
                           <div class="col-sm-4 hide-elem" id="cheque-elem">
                              <div class="form-group nomrgin-ltor">                                                                    
                                 <input name="chequeno" id="chequeno" class="form-control" type="text" placeholder="<?=$this->lang->line("placeholder_cheque_no")?>" value="<?=$student->chequeno?>">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="form-group <?=(form_error('name')) ? 'has-error' : ''?>">
                     <label for="name" class="col-sm-2 control-label">&nbsp;</label>
                     <div class="col-sm-6">
                        <input name="amount_in_installment" id="amount_in_installment" class="form-control custom-height-form custom-width-form display-in-line" type="checkbox" style="width: auto !important;" />
                        <label for="name" class="control-label pd-btm-5"><?=$this->lang->line("pay_installment")?></label>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('name'); ?></span>
                  </div>

                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"></label>
                     <div class="col-sm-10">
                        <div class="row">
                          <div class="col-sm-4" id="amount-elem">
                              <div class="form-group nomrgin-ltor">                                                                    
                                 <input readonly name="paymentamount" id="paymentamount" class="form-control" type="text" placeholder="<?=$this->lang->line("placeholder_amount")?>" value="<?=$student->paymentamount?>" required>
                              </div>
                           </div>
                        </div>
                      </div>
                  </div>

                  

                  <div class="form-group hide-elem" id="duedate">
                     <label for="name" class="col-sm-2 control-label"><?=$this->lang->line("next_installment_date")?></label>
                     <div class="col-sm-5">
                        <input name="due_date" class="form-control" type="text" id="due_date"/>
                     </div>
                     <!-- <span class="col-sm-4 control-label"><?php echo form_error('name'); ?></span> -->
                  </div>

                  <div class="form-group <?=(form_error('photo')) ? 'has-error' : ''?>">
                        <label for="photo" class="col-sm-2 control-label">Parent <?=$this->lang->line("student_photo")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="photo" class="form-control"/>
                        </div>
                        <span class="col-sm-4"><?php echo form_error('photo'); ?></span>
                    </div>

                    <div class="form-group <?=(form_error('sign')) ? 'has-error' : ''?>">
                        <label for="sign" class="col-sm-2 control-label">Parent <?=$this->lang->line("student_sign")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="sign" class="form-control"/>
                        </div>
                        <span class="col-sm-4"><?php echo form_error('sign'); ?></span>
                    </div>

                  <div class="form-group" id="choose-docs">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("choose_docs")?></label>
                     <div class="col-sm-10">
                        <div class="row" id="cloned-elem">
                           <div class="col-sm-6">
                              <div class="form-group nomrgin-ltor">
                                 <select name="doc_type[]" class="form-control doc_category">
                                    <option value=''><?=$this->lang->line("choose_docs")?></option>
                                    <option value='1'>10th</option>
                                    <option value='2'>12th</option>
                                    <option value='99999'>Other</option>
                                 </select>
                                 <input type="text" class="form-control other_doc hidden" name="other_doc[]">
                              </div>
                           </div>
                          <div class="col-sm-4">
                            <input type="file" class="form-control" name="doc_file[]">
                          </div>
                          <div class="col-sm-2">
                            <i class="fa fa-plus-square custom-plus" aria-hidden="true" id="clone-it" ></i>
                          </div>
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-8">
                        <input class="btn btn-success" type="submit" value="<?=$this->lang->line("btn_add")?>">
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <br>
      </div>
   </div>
</div>


<script>
  $('#due_date').datepicker({ startView: 2 });
   $(document).on('change','.doc_category',function(e) {
      var id = $(this).val();
      if(parseInt(id)==99999){
        $(this).siblings('.other_doc').removeClass('hidden');
      } else {
        $(this).siblings('.other_doc').addClass('hidden');
      }
    });
$(document).ready(function(){
  
});
$("#clone-it").click(function(){
   addNewDocRow();
}); 
$(document).on('click',".remove-it",function(){
  $(this).parents(".removed").remove();
});

  function addNewDocRow(){
     var str='';
     var docOptions="<option value=''><?=$this->lang->line("choose_docs")?></option><option value='1'>10th</option><option value='2'>12th</option><option value='99999'>Other</option>";
     str='<div class="removed"> <label for="parent_mob" class="col-sm-2 control-label pd-top-15"></label> <div class="col-sm-10 pd-top-15"> <div class="row mrgn-top-2" id="cloned-elem"> <div class="col-sm-6"> <div class="form-group nomrgin-ltor"> <select name="doc_type[]" class="form-control doc_category">'+docOptions+'</select> <input type="text" class="form-control other_doc hidden" name="other_doc[]"></div></div><div class="col-sm-4">  <input type="file" class="form-control" name="doc_file[]"> </div><div class="col-sm-2"> <i class="custom-minus fa fa-minus-square remove-it" aria-hidden="true"></i> </div></div></div></div>';
  $("#choose-docs").append(str);
  }
</script>
<script>
    $(function() {
      var selectedPayMethod = '<?=$student->paymenttype?>';
      var courseId = '<?=$student->classesID?>';
      paymentElemShow(selectedPayMethod);
      getCourse(courseId);
   });
   $('#payment-method').change(function(){
      $('#bank-elem,#cheque-elem,#amount-elem').hide();
      paymentElemShow($(this).val());
   });
   function paymentElemShow(val){
      if(val=='Cash'){
         $('#amount-elem').show();
      } else if(val=='Cheque'){
         $('#amount-elem').show();
         $('#cheque-elem').show();
         $('#bank-elem').show();
      } else if(val=='NetBanking'){
         $('#amount-elem').show();
         $('#bank-elem').show();
      }
   }
   $('#course_list').change(function(e) {
      var id = $(this).val();
      getCourse(id);
    });
     function getCourse(id){
        if(id>0){
          globalFunc.ajaxCall('ajax/getPlanDetails', {id:id}, 'POST', globalFunc.before, globalFunc.getCourseDetailsSuccess, globalFunc.error, globalFunc.complete);
        } else {
          
        }
     }
   globalFunc.getCourseDetailsSuccess = function(data) {
      if(data.status){
          $('#paymentamount').val(data.classes.course_fee);
      } else {
          alert(data.message);
      }
    }

    $('#amount_in_installment').click(function(e) {
     if ($(this).prop('checked')){ 
        $('#paymentamount').attr('readonly',false);
        $('#duedate').show();
     } else {
        $('#paymentamount').attr('readonly',true);
        $('#duedate').hide();
     }
     
    });

   
</script>
