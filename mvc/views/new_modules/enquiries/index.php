
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-student"></i> <?=$this->lang->line('panel_title1')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('b_enquiry')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <h5 class="page-header">
                    <?php
                        //$usertype = $this->session->userdata("usertype");
                        //if(permissionChecker('student_add')) {
                    ?>
                        <a href="<?php echo base_url('Enquiry/add') ?>">
                            <i class="fa fa-plus"></i>
                            <?=$this->lang->line('add_enq')?>
                        </a>
                    <?php //} ?>
                   
                </h5>

                <?php if(count($students) > 0 ) { ?>
                    <div class="nav-tabs-custom">
                        
                        <div class="tab-content">
                            <div id="all" class="tab-pane active">
                                <div id="hide-table">
                                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1"><?=$this->lang->line('s_no')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('reg_no')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('enq_student_name')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('enq_status')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('date')?></th>
                                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>

                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $i = 1; foreach($students as $stu) { ?>
                                                <tr>
                                                    
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $stu->registerNO; ?></td>
                                                    <td><?php echo $stu->name; ?></td>
                                                    <td><?php echo (in_array($stu->addmission_status,[1]))? 'Submitted Medical Form only' : 'Submitted Enquiry Only';?></td>
                                                    <td><?php echo $stu->create_date; ?></td>
                                                    
                                                    <?php //if(permissionChecker('student_edit') || permissionChecker('student_delete') || permissionChecker('student_view')) { ?>
                                                    <td data-title="<?=$this->lang->line('action')?>">
                                                        <?php
                                                            echo btn_view('student/view/'.$stu->studentID.'/'.$stu->classesID, $this->lang->line('view'));
                                                            if(in_array($stu->addmission_status,[0,1,2])){
                                                                 echo btn_medical_form('enquiry/medical_form/'.$stu->studentID, $this->lang->line('medical_form_info'));
                                                            } 
                                                            if($stu->addmission_status==0) {
                                                                 echo btn_delete_show('enquiry/delete/'.$stu->studentID, $this->lang->line('btn_delete'));
                                                            }
                                                            if(in_array($stu->addmission_status,[1,2])) echo btn_addmission_form('enquiry/addmission_form/'.$stu->studentID, $this->lang->line('addmission_form_info'));
                                                            //echo btn_edit_show('enquiry/edit/'.$stu->studentID, $this->lang->line('btn_edit'));
                                                           
                                                        ?>
                                                    </td>
                                                    <?php //} ?>
                                               </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                           
                        </div>
                    </div> <!-- nav-tabs-custom -->
                <?php } ?>

            </div> <!-- col-sm-12 -->

        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

