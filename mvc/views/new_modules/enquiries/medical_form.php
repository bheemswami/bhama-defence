<?php 

?>
<div class="box">
   <div class="box-header">
      <h3 class="box-title"><i class="fa icon-visitorinfo"></i><?=$this->lang->line('medical_form')?></h3>
      <ol class="breadcrumb">
         <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
         <li class="active"><?=$this->lang->line('medical_form')?></li>
      </ol>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="box-body">
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12">
               <form  id="myform" action="<?=base_url('Enquiry/medical_form')?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
               <input type="hidden" name="id" value="<?=$this->uri->segment(3)?>"/>
               <input type="hidden" name="form_step" value="<?=$student->addmission_status?>"/>
               <input type="hidden" name="payid" value="<?=$student->medical_form_payid?>"/>
               <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('payment_method')?></label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group nomrgin-ltor">
                                 <select name="paymenttype" id="payment-method" class="form-control" required>
                                    <option value=""><?=$this->lang->line('payment_method')?></option>
                                    <?php
                                        foreach ($this->config->item($this->lang->line('payment_method_arr')) as $key => $value) {
                                            $selected='';
                                            if($student->paymenttype=='' && $key=='Cash'){
                                              $selected='selected';
                                            } else if($student->paymenttype==$key){
                                              $selected='selected';
                                            } 
                                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                        }
                                    ?>  
                                 </select>
                              </div>
                           </div>
                           </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"></label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-4 hide-elem" id="bank-elem">
                              <div class="form-group nomrgin-ltor">                                                                    
                                 <input name="paymentbank" id="paymentbank" class="form-control" type="text" placeholder="<?=$this->lang->line('placeholder_bank_name')?>" value="<?=$student->paymentbank?>">
                              </div>
                           </div>
                           <div class="col-sm-4 hide-elem" id="cheque-elem">
                              <div class="form-group nomrgin-ltor">                                                                    
                                 <input name="chequeno" id="chequeno" class="form-control" type="text" placeholder="<?=$this->lang->line('placeholder_cheque_no')?>" value="<?=$student->chequeno?>">
                              </div>
                           </div>
                           <div class="col-sm-4 hide-elem" id="amount-elem">
                              <div class="form-group nomrgin-ltor">                                                                    
                                 <input name="paymentamount" id="paymentamount" class="form-control" type="text" placeholder="<?=$this->lang->line('placeholder_amount')?>" value="<?=$student->paymentamount?>" required>
                              </div>
                           </div>
                           </div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('enq_physical_benchmark');?></label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="height" class="control-label pd-btm-5"><?=$this->lang->line('height')?></label>
                                 <input name="physical_benchmark[height]" id="height" class="form-control" type="text" value="<?=physicalBenchmarkVal('height',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="chest" class="control-label pd-btm-5"><?=$this->lang->line('chest')?></label>
                                 <input name="physical_benchmark[chest]" id="chest" class="form-control" type="text" value="<?=physicalBenchmarkVal('chest',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="weight" class="control-label pd-btm-5"><?=$this->lang->line('weight')?></label>
                                 <input name="physical_benchmark[weight]" id="weight" class="form-control" type="text" value="<?=physicalBenchmarkVal('weight',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="knees" class="control-label pd-btm-5"><?=$this->lang->line('knees')?></label>
                                 <input name="physical_benchmark[knees]" id="knees" class="form-control" type="text" value="<?=physicalBenchmarkVal('knees',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="eye" class="control-label pd-btm-5"><?=$this->lang->line('left_eye')?></label>
                                 <input name="physical_benchmark[left_eye]" id="eye" class="form-control" type="text" value="<?=physicalBenchmarkVal('left_eye',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="eye" class="control-label pd-btm-5"><?=$this->lang->line('right_eye')?></label>
                                 <input name="physical_benchmark[right_eye]" id="eye" class="form-control" type="text" value="<?=physicalBenchmarkVal('right_eye',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="ear" class="control-label pd-btm-5"><?=$this->lang->line('left_ear')?></label>
                                 <input name="physical_benchmark[left_ear]" id="ear" class="form-control" type="text" value="<?=physicalBenchmarkVal('left_ear',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="ear" class="control-label pd-btm-5"><?=$this->lang->line('right_ear')?></label>
                                 <input name="physical_benchmark[right_ear]" id="ear" class="form-control" type="text" value="<?=physicalBenchmarkVal('right_ear',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="foot" class="control-label pd-btm-5"><?=$this->lang->line('foot')?></label>
                                 <input name="physical_benchmark[foot]" id="foot" class="form-control" type="text" value="<?=physicalBenchmarkVal('foot',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="teeth" class="control-label pd-btm-5"><?=$this->lang->line('teeth')?></label>
                                 <input name="physical_benchmark[teeth]" id="teeth" class="form-control" type="text" value="<?=physicalBenchmarkVal('teeth',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="nose" class="control-label pd-btm-5"><?=$this->lang->line('nose')?></label>
                                 <input name="physical_benchmark[nose]" id="nose" class="form-control" type="text" value="<?=physicalBenchmarkVal('nose',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="bp" class="control-label pd-btm-5"><?=$this->lang->line('high_bp')?></label>
                                 <input name="physical_benchmark[high_bp]" id="bp" class="form-control" type="text" value="<?=physicalBenchmarkVal('high_bp',$student->physical_benchmark)?>">
                              </div>
                           </div>
                           <!-- <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="bp" class="control-label pd-btm-5"><?=$this->lang->line('low_bp')?></label>
                                 <input name="physical_benchmark[low_bp]" id="bp" class="form-control" type="text" value="<?=physicalBenchmarkVal('low_bp',$student->physical_benchmark)?>">
                              </div>
                           </div> -->
                           <div class="col-sm-4">
                              <div class="form-group nomrgin-ltor">
                                 <label for="pr" class="control-label pd-btm-5"><?=$this->lang->line('pr')?></label>
                                 <input name="physical_benchmark[pr]" id="pr" class="form-control" type="text" value="<?=physicalBenchmarkVal('pr',$student->physical_benchmark)?>">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group parrent-input">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('short_note')?> </label>
                     <div class="col-sm-10 ">
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="form-group nomrgin-ltor">
                                 <textarea name="short_note" class="form-control"><?=$student->short_note?></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group parrent-input">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('attention_points')?> </label>
                     <div class="col-sm-10 ">
                        <div class="row">
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="attention_point[hydrocele]" id="hydrocele" class="form-control custom-height-form custom-width-form display-in-line" type="checkbox" <?=attenstionPoints('hydrocele',$student->attention_point)?> />
                                 <label for="hydrocele" class="control-label pd-btm-5">Hydrocele</label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="attention_point[varicocele]" id="varicocele" class="form-control custom-height-form custom-width-form display-in-line" type="checkbox" <?=attenstionPoints('varicocele',$student->attention_point)?> />
                                 <label for="varicocele" class="control-label pd-btm-5">Varicocele</label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="attention_point[varicose_vain]" id="varicose_vain" class="form-control custom-height-form custom-width-form display-in-line" type="checkbox" <?=attenstionPoints('varicose_vain',$student->attention_point)?> />
                                 <label for="varicose_vain" class="control-label pd-btm-5">Varicose Vain</label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="attention_point[piles]" id="piles" class="form-control custom-height-form custom-width-form display-in-line" type="checkbox" <?=attenstionPoints('piles',$student->attention_point)?> />
                                 <label for="piles" class="control-label pd-btm-5">Piles</label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="attention_point[sweat]" id="sweat" class="form-control custom-height-form custom-width-form display-in-line" type="checkbox" <?=attenstionPoints('sweat',$student->attention_point)?> />
                                 <label for="sweat" class="control-label pd-btm-5">Sweat</label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group parrent-input">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('marital_status')?> </label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="marital_status" id="married" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="Married" <?=isCheckedOrNot('Married',$student->marital_status)?> />
                                 <label for="married" class="control-label pd-btm-5"><?=$this->lang->line('married')?></label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="marital_status" id="unmarried" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="Unmarried" <?=isCheckedOrNot('Unmarried',$student->marital_status)?> />
                                 <label for="unmarried" class="control-label pd-btm-5"><?=$this->lang->line('unmarried')?></label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group parrent-input">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line('category')?> </label>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="category" id="c_gen" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="GEN" <?=isCheckedOrNot('GEN',$student->category)?> />
                                 <label for="c_gen" class="control-label pd-btm-5"><?=$this->lang->line('gen')?></label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="category" id="c_obc" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="OBC" <?=isCheckedOrNot('OBC',$student->category)?> />
                                 <label for="c_obc" class="control-label pd-btm-5"><?=$this->lang->line('obc')?></label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="category" id="c_sc" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="SC" <?=isCheckedOrNot('SC',$student->category)?> />
                                 <label for="c_sc" class="control-label pd-btm-5"><?=$this->lang->line('sc')?></label>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="form-group nomrgin-ltor">
                                 <input name="category" id="c_st" class="form-control custom-height-form custom-width-form display-in-line" type="radio" value="ST" <?=isCheckedOrNot('ST',$student->category)?> />
                                 <label for="c_st" class="control-label pd-btm-5 "><?=$this->lang->line('st')?></label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-8">
                        <button class="btn btn-success" type="submit" name="redirect" value="only_enquiry"><?=$this->lang->line("btn_add")?></button>
                        <button class="btn btn-warning" type="submit" name="redirect" value="next_form"><?=$this->lang->line("proceed_next_aform")?></button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <br>
      </div>
   </div>
</div>
<!-- <script>
$(document).ready(function(){
  $("#clone-it").click(function(){
    $("#cloned-elem").clone().appendTo("#choose-docs");
  });
});
</script> -->

<?php
function physicalBenchmarkVal($filed,$pbData){
   if($pbData!=''){
      $pbData = json_decode($pbData);
      return $pbData->$filed;
   }
   return '';
}
function attenstionPoints($filed,$data){
   if($data!=''){
      $data = json_decode($data);
      if($data->$filed==1){
         return 'checked';
      }

   }
   return '';
}
function isCheckedOrNot($val,$data){
      if($data==$val){
         return 'checked';
      }
   return '';
}
?>
<script>
   $(function() {
      $('#amount-elem').show();
      var selectedPayMethod = '<?=$student->paymenttype?>';
      paymentElemShow(selectedPayMethod);
   });
   $('#payment-method').change(function(){
      $('#bank-elem,#cheque-elem,#amount-elem').hide();
      paymentElemShow($(this).val());
   });
   function paymentElemShow(val){
      if(val=='Cash'){
         $('#amount-elem').show();
      } else if(val=='Cheque'){
         $('#amount-elem').show();
         $('#cheque-elem').show();
         $('#bank-elem').show();
      } else if(val=='NetBanking'){
         $('#amount-elem').show();
         $('#bank-elem').show();
      }
   }
</script>