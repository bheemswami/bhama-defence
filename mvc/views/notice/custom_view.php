
<div class="well">
    <div class="row">
        <div class="col-sm-6">
            <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
        </div>


        <div class="col-sm-6">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>
                <li class="active"><?=$this->lang->line('menu_view')?> Due Fees Students</li>
            </ol>
        </div>

    </div>

</div>


<section class="panel">
    <div class="panel-body bio-graph-info">
        <div id="printablediv" class="box-body">
            <div class="row">
                <div class="col-sm-12">
                     <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">S.No.</th>
                                                <th class="col-sm-2">Name</th>
                                                <th class="col-sm-2">Roll No.</th>
                                                <th class="col-sm-2">Due Academic Fee</th>                                               
                                                <th class="col-sm-2">Due Hostel Fee</th>                                               
                                                <?php if(permissionChecker('student_view')) { ?>
                                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($due_fee_students)>0) {$i = 1; foreach($due_fee_students as $student) { ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $student['name']; ?></td>
                                                    <td><?php echo $student['roll']; ?></td>
                                                    <th><?php echo '&#8377; '.number_format($student['final_course_fee']); ?></th>
                                                    <th><?php echo '&#8377; '.number_format($student['final_hostel_fee']); ?></th>
                                                    <?php if(permissionChecker('student_view')) { ?>
                                                    <td>
                                                        <?php
                                                            echo btn_view('student/view/'.$student['studentID']."/".$student['classesID'], $this->lang->line('view'));
                                                            //$accUrl = base_url('student/student_account/'.$student['studentID']."/".$set);
                                                            //echo '<a href="'.$accUrl.'" class="btn btn-warning btn-xs" title="'.$this->lang->line('student_account').'"><i class="fa fa-inr"></i></a>';
                                                        ?>
                                                    </td>
                                                    <?php } ?>
                                               </tr>
                                            <?php $i++; }} ?>
                                        </tbody>
                                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML = 
          "<html><head><title></title></head><body>" + 
          divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }
</script>

