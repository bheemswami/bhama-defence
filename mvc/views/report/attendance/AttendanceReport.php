<div id="printablediv">
    <div class="box-header bg-gray">
        <h3 class="box-title text-navy"><i class="fa fa-clipboard"></i> <?=$this->lang->line('report_class')?> :  <?=$class->classes?> || <?=$type?> <?=$this->lang->line('report_attendance')?> <?=$this->lang->line('panel_title')?> : ( <?=$date?> )</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <?php
                                $monthArray = array(
                                  "01" => "jan", "02" => "feb", "03" => "mar", "04" => "apr", "05" => "may", "06" => "jun", "07" => "jul", "08" => "aug","09" => "sep", "10" => "oct", "11" => "nov", "12" => "dec");
                            ?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?php if(count($students)) { ?>
                <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th class="">#</th>
                            <!-- <th class="col-sm-2"><?=$this->lang->line('report_photo')?></th> -->
                            <th class=""><?=$this->lang->line('report_name')?></th>
                            
                           <th class="">Attendance</th>
                            <!-- <th class="col-sm-2"><?=$this->lang->line('report_phone')?></th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                $sno = 0;
                                $flag = 0;
                                foreach($students as $student) {
                                    $sno++;
                                    if(isset($attendances[$student->studentID])) {
                                        if($typeSortForm == 'P' && ($attendances[$student->studentID]->$day == 'A' || $attendances[$student->studentID]->$day == NULL)) {
                                            continue;
                                        } elseif($typeSortForm == 'A' && $attendances[$student->studentID]->$day == 'P') {
                                            continue;
                                        }
                                    } elseif($typeSortForm == 'P') {
                                        continue;
                                    }
                                    $flag = 1;
                        ?>
                            <tr>
                                <td data-title="#">
                                    <?php echo $sno; ?>
                                </td>

                                <!-- <td data-title="<?=$this->lang->line('report_photo')?>">
                                    <?php $array = array(
                                            "src" => base_url('uploads/images/'.$student->photo),
                                            'width' => '35px',
                                            'height' => '35px',
                                            'class' => 'img-rounded'

                                        );
                                        echo img($array);
                                    ?>
                                </td> -->
                                <td data-title="<?=$this->lang->line('report_name')?>">
                                    <?php echo $student->name; ?><br/>
                                    (<?=$this->lang->line('report_roll')?> No. : <?php echo $student->roll; ?>)
                                </td>
                               
                                <td data-title="<?=$this->lang->line('report_email')?>">
                                   <!--  <?php echo $student->email; ?> -->
                                    <table class="attendance_table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <?php
                                                            for($i=1; $i<=31; $i++) {
                                                               echo  "<th>".$i."</th>";
                                                            }
                                                        ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $holidayCount = 0;
                                                        $weekendayCount = 0;
                                                        $leavedayCount = 0;
                                                        $presentCount = 0;
                                                        $lateexcuseCount = 0;
                                                        $lateCount = 0;
                                                        $absentCount = 0;

                                                        //$schoolyearstartingdate = $schoolyearsessionobj->startingdate;
                                                        $schoolyearstartingdate = '';
                                                        $schoolyearendingdate = '';
                                                        //$schoolyearendingdate = $schoolyearsessionobj->endingdate;
                                                        $allMonths = get_month_and_year_using_two_date($schoolyearstartingdate, $schoolyearendingdate);
                                                        $holidaysArray = explode('","', $holidays);

                                                        foreach($allMonths as $yearKey => $months) {
                                                            foreach ($months as $month) {
                                                                $monthyear = $month."-".$yearKey;

                                                                if(isset($attendancesArray[$student->studentID]) && $attendancesArray[$student->studentID]->monthyear==$monthyear) {
                                                                    echo "<tr>";
                                                                    echo "<td>".ucwords($monthArray[$selected_month])."</td>";
                                                                    for ($i=1; $i <= 31; $i++) {    
                                                                        $acolumnname = 'a'.$i;
                                                                        $d = sprintf('%02d',$i);

                                                                        $date = $d."-".$month."-".$yearKey;
                                                                        if(in_array($date, $holidaysArray)) {
                                                                            $holidayCount++;
                                                                            echo "<td class='ini-bg-primary'>".'H'."</td>";
                                                                        } elseif (in_array($date, $getWeekendDays)) {
                                                                            $weekendayCount++;
                                                                            echo "<td class='ini-bg-info'>".'W'."</td>";
                                                                        } elseif(in_array($date, $leaveapplications)) {
                                                                            $leavedayCount++;
                                                                            echo "<td class='ini-bg-success'>".'LA'."</td>";
                                                                        } else {
                                                                            $textcolorclass = '';
                                                                            $val = false;
                                                                            if(isset($attendancesArray[$student->studentID]) && $attendancesArray[$student->studentID]->$acolumnname == 'P') {
                                                                                $presentCount++;
                                                                                $textcolorclass = 'ini-bg-success';
                                                                            } elseif(isset($attendancesArray[$student->studentID]) && $attendancesArray[$student->studentID]->$acolumnname == 'LE') {
                                                                                $lateexcuseCount++;
                                                                                $textcolorclass = 'ini-bg-success';
                                                                            } elseif(isset($attendancesArray[$student->studentID]) && $attendancesArray[$student->studentID]->$acolumnname == 'L') {
                                                                                $lateCount++;
                                                                                $textcolorclass = 'ini-bg-success';
                                                                            } elseif(isset($attendancesArray[$student->studentID]) && $attendancesArray[$student->studentID]->$acolumnname == 'A') {
                                                                                $absentCount++;
                                                                                $textcolorclass = 'ini-bg-danger';
                                                                            } elseif((isset($attendancesArray[$student->studentID]) && ($attendancesArray[$student->studentID]->$acolumnname == NULL || $attendancesArray[$student->studentID]->$acolumnname == ''))) {
                                                                                $textcolorclass = 'ini-bg-secondary';
                                                                                $defaultVal = 'N/A';
                                                                                $val = true;
                                                                            }

                                                                            if($val) {
                                                                                echo "<td class='".$textcolorclass."'>".$defaultVal."</td>";
                                                                            } else {
                                                                                echo "<td class='".$textcolorclass."'>".$attendancesArray[$student->studentID]->$acolumnname."</td>";
                                                                            }

                                                                        }
                                                                    }
                                                                    echo "</tr>";
                                                                } else {
                                                                    $monthyear = $month."-".$yearKey;
                                                                    echo "<tr>";
                                                                    echo "<td>".ucwords($monthArray[$selected_month])."</td>";
                                                                    for ($i=1; $i <= 31; $i++) {    
                                                                        $acolumnname = 'a'.$i;
                                                                        $d = sprintf('%02d',$i);

                                                                        $date = $d."-".$month."-".$yearKey;
                                                                        if(in_array($date, $holidaysArray)) {
                                                                            $holidayCount++;
                                                                            echo "<td class='ini-bg-primary'>".'H'."</td>";
                                                                        } elseif (in_array($date, $getWeekendDays)) {
                                                                            $weekendayCount++;
                                                                            echo "<td class='ini-bg-info'>".'W'."</td>";
                                                                        } elseif(in_array($date, $leaveapplications)) {
                                                                            $leavedayCount++;
                                                                            echo "<td class='ini-bg-success'>".'LA'."</td>";
                                                                        } else {
                                                                            $textcolorclass = 'ini-bg-secondary';
                                                                            echo "<td class='".$textcolorclass."'>".'N/A'."</td>";
                                                                        }
                                                                    }
                                                                    echo "</tr>";
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                </td>
                               <!--  <td data-title="<?=$this->lang->line('report_phone')?>">
                                    <?php echo $student->phone; ?>
                                </td> -->
                           </tr>
                        <?php  }
                            if(!$flag) {
                        ?>
                            <tr>
                                <td data-title="#" colspan="6">
                                    <?=$this->lang->line('report_student_not_found')?>
                                </td>
                            </tr>
                        <?php
                            }

                        } else { ?>
                            <div class="callout callout-danger">
                                <p><b class="text-info"><?=$this->lang->line('report_student_not_found')?></b></p>
                            </div>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

        </div><!-- row -->
    </div><!-- Body -->
</div>
