<div class="row">
    <div class="col-sm-12" style="margin:10px 0px">
        <?php
            $pdf_preview_uri = base_url('idcardreport/pdf/'.$usertypeID.'/'.$classesID.'/'.$sectionID.'/'.$userID.'/'.$type.'/'.$background);
            echo btn_printReport('idcardreport', $this->lang->line('idcardreport_print'), 'printablediv');
            //echo btn_pdfPreviewReport('idcardreport',$pdf_preview_uri, $this->lang->line('idcardreport_pdf_preview'));
            //echo btn_sentToMailReport('idcardreport', $this->lang->line('idcardreport_mail'));
        ?>
    </div>
</div>
<div class="box">
    <div class="box-header bg-gray">
        <h3 class="box-title text-navy"><i class="fa fa-clipboard"></i> 
            <?=$this->lang->line('idcardreport_report_for')?> -
            <?=isset($usertypes[$usertypeID]) ? $usertypes[$usertypeID]: ' ';?>
        </h3>
    </div><!-- /.box-header -->
    <div id="printablediv">
        <style>
            .idcardreport {
                font-family: arial;    
                max-width:794px;
                max-height: 1123px;
                margin-left: auto;
                margin-right: auto;
                -webkit-print-color-adjust: exact;
            }
            /*IDcard Front Part Css Code*/
            .idcardreport-frontend{
                margin: 3px;
                float: left;
                border: 1px solid #000;
                padding: 10px;
                width: 257px;
                text-align: center;
                height:290px;
                <?php if($background == 1) { ?>
                background:url("<?=base_url('uploads/default/idcard-border.png')?>")!important;
                background-size: 100% 100% !important;
                <?php } ?>
            }
            
            .idcardreport-frontend h3{
                font-size: 20px;
                color: #1A2229;
            }
            
            .idcardreport-frontend img{
                width: 50px;
                height: 50px;
                border: 1px solid #ddd;
                margin-bottom: 5px;
            }

            .idcardreport-frontend p{
                text-align: left;
                font-size: 12px;
                margin-bottom: 0px;
                color: #1A2229;
            }

            /*ID Card Back Part Css Code*/
            .idcardreport-backend{
                margin: 3px;
                /*float: left;*/
                float: right;
                border: 1px solid #1A2229;
                padding: 10px;
                width: 257px;
                text-align: center;
                height:290px;
                <?php if($background == 1) { ?>
                background:url("<?=base_url('uploads/default/idcard-border.png')?>")!important;
                background-size: 100% 100% !important;
                <?php } ?>
            }

            .idcardreport-backend h3{
                background-color: #1A2229;
                color: #fff;
                font-size: 13px;
                padding: 5px 0px;
                margin:5px;
                margin-top: 13px;
            }

            .idcardreport-backend h4{
                font-size: 11px;
                color: #1A2229;
                font-weight: bold;
                padding: 5px 0px;
            }

            .idcardreport-backend p{
                font-size: 17px;
                color: #1A2229;
                font-weight: 500;
                line-height: 17px;
            }

            .idcardreport-schooladdress {
                color: #1A2229 !important;
                font-weight: 500;
            }

            .idcardreport-bottom {
                text-align: center;
                padding-top: 5px
            }

            .idcardreport-qrcode{
                float: left;
                width: 50%;
            }

            .idcardreport-qrcode img{
                width: 80px;
                height: 80px;
            }

            .idcardreport-session{
                float: right;
                width: 50%;
            }
            
            .idcardreport-session span{
                color: #1A2229;
                font-weight: bold;
                margin-top: 35px;
                overflow: hidden;
                float: left;
            }

            @media print {
                .idcardreport {
                    max-width:794px;
                    max-height: 1123px;
                    margin-left: auto;
                    margin-right: auto;
                    -webkit-print-color-adjust: exact;
                    margin:0px auto;    
                }

                /*ID Card Front Part Css Code*/
                .idcardreport-frontend{
                    margin: 1px;
                    float: left;
                    border: 1px solid #000;
                    padding: 10px;
                    width: 250px;
                }

                h3{
                    color: #1A2229 !important;
                }

                .idcardreport-frontend .profile-view-dis .profile-view-tab {
                    width: 100%;
                    float: left;
                    margin-bottom: 0px;
                    padding: 0 15px;
                    font-size: 14px;
                    margin-top: 5px;
                }

                /*ID Card Back Part Css Code*/
                .idcardreport-backend {
                    margin: 1px;
                    float: right;
                    border: 1px solid #1A2229;
                    padding: 10px;
                    width: 250px;
                }

                .idcardreport-backend h3{
                    background-color: #1A2229 !important;
                    font-size: 12px;
                    color: #fff !important;
                    overflow: hidden;
                    display: block;
                }
            }

            .idcardreport-frontend .profile-view-dis .profile-view-tab {
                width: 100%;
                float: left;
                margin-bottom: 0px;
                padding: 0 15px;
                font-size: 14px;
                margin-top: 5px;
            }


            .main-card{
                padding: 20px;
                margin: 0 auto;
                width: 50%;
                background: url(assets/images/Lined-Paper-bg.jpg);
            }
            .card-banner{
                background: url(assets/images/triangle-bg.jpg);
                min-height: 90px;
                background-size: cover;
                position: relative;
            }
            .user-profile-pic{
                float: left;
            }
            .user-profile-pic img{
                width: 100px;
                position: absolute;
                top: 30%;
                left: 5%;
                border-radius: 50px;
                box-shadow: 0px 0px 1px 5px #fff;
            }
            .owner-logo img{
                width: 200px;
            }
            .owner-logo{
                float: right;
                margin-top: 10px;
            }
            .user-name{
                padding: 3px 0px;
            }
            .user-name h4{
                color: #0099CC;
                font-weight: bold;
            }
            .user-name h5{
                color: #444343;
                font-weight: bold;
            }
            .info-heading{
                color: #0099CC;
                font-size: 12px;
                font-weight: bold;
            }
            .info-desc{
                font-size: 12px;
                font-weight: bold;
            }
            .border-dashed-ltor{
                border-left: 2px dotted #8080807d;
                border-right: 2px dotted #8080807d;
            }
            .border-dashed-left{
                border-left: 2px dotted #8080807d;
            }
            .pd-btm{
                padding-bottom: 10px;
            }

            .main-card-wrapper{
                background-color: #fff;
                width: 40%;
                margin: 0 auto;
                margin-top: 60px;
            }
            .owner-logo-info img{
                width: 200px;
            }
            .owner-logo-info{
                float: left;
                position: absolute;
                border-radius: 20px;
                margin-top: 0px;               
                background-color: #0099CC;
            }
            .owner-info-full{
                float: right;
                background-color: #0099CC;
                width: 100%;
                text-align: right;
            }
            .info-banner{
                position: relative;
            }
            .owner-info-full h4{
                font-size: 21px;
                color: #fff;
                padding-right: 10px;
            }
            .owner-info-full h5{
                font-size: 16px;
                font-weight: bold;
                color: #fff;
                padding-right: 10px;
            }
            .border-effects{
                border-bottom: 2px solid #cccccc;
                border-bottom-left-radius: 16px;
            }
            .card-info h3{
                font-weight: bold;
                padding-left: 100px;
                font-size: 19px;
            }
            .year-info h3{
                font-weight: bold;
                padding-right: 16px;
                font-size: 19px;    
            }
            .id-profile-img img{
                width: 160px;
                border-radius: 20px;
                box-shadow: 0px 0px 7px 1px #80808073;

            }
            .id-profile-img{
                padding-top: 25px;
            }
            .pdtop-15{
                padding-top: 15px;
            }
            .student_information h3{
                font-weight: bold;
                font-size: 20px;                
            }
            .student_description h3{
                font-weight: bold;
                font-size: 20px;
                color: #0583ad;             
            }
            .signature-info img{
                width: 120px;
            }
            .signature-info{
                padding-top: 15px;    
                padding-bottom: 15px;
            }
            .signature-info span{
                color: #0583ad;
                font-weight: bold;
                font-size: 18px;
                padding-left: 20px;
            }
            .id-card-footer{
                background-color: #0099cc;
                padding: 12px;
                text-align: center;     
            }
            .id-card-footer span{
                font-size: 16px;
                font-weight: bold;
                color: #FFF;
            }
            @media print {
                .id-card-footer{
                background-color: #0099cc;
                padding: 12px;
                text-align: center; 
                -webkit-print-color-adjust: exact;    
            }
        }
        </style>

        <div class="box-body" style="margin-bottom: 50px;">
            


        </div><!-- box Body -->
    </div>
</div>


<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('idcardreport/send_pdf_to_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?=$this->lang->line('idcardreport_close')?></span></button>
                <h4 class="modal-title"><?=$this->lang->line('idcardreport_mail')?></h4>
            </div>
            <div class="modal-body">

                <?php
                    if(form_error('to'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("idcardreport_to")?> <span class="text-red">*</span>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php
                    if(form_error('subject'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("idcardreport_subject")?> <span class="text-red">*</span>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php
                    if(form_error('message'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("idcardreport_message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <?php/*<input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("idcardreport_send")?>" />*/?>
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->

<script type="text/javascript">
    
    function check_email(email) {
        var status = false;
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("<?=$this->lang->line('idcardreport_mail_valid')?>").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }


    $('#send_pdf').click(function() {
        var field = {
            'to'         : $('#to').val(), 
            'subject'    : $('#subject').val(), 
            'message'    : $('#message').val(),
            'usertypeID' : '<?=$usertypeID?>',
            'classesID'  : '<?=$classesID?>',
            'sectionID'  : '<?=$sectionID?>',
            'userID'     : '<?=$userID?>',
            'type'       : '<?=$type?>',
            'background' : '<?=$background?>',
        };

        var to = $('#to').val();
        var subject = $('#subject').val();
        var error = 0;

        $("#to_error").html("");
        $("#subject_error").html("");

        if(to == "" || to == null) {
            error++;
            $("#to_error").html("<?=$this->lang->line('idcardreport_mail_to')?>").css("text-align", "left").css("color", 'red');
        } else {
            if(check_email(to) == false) {
                error++
            }
        }

        if(subject == "" || subject == null) {
            error++;
            $("#subject_error").html("<?=$this->lang->line('idcardreport_mail_subject')?>").css("text-align", "left").css("color", 'red');
        } else {
            $("#subject_error").html("");
        }

        if(error == 0) {
            $('#send_pdf').attr('disabled','disabled');
            $.ajax({
                type: 'POST',
                url: "<?=base_url('idcardreport/send_pdf_to_mail')?>",
                data: field,
                dataType: "html",
                success: function(data) {
                    var response = JSON.parse(data);
                    if (response.status == false) {
                        $('#send_pdf').removeAttr('disabled');
                        if( response.to) {
                            $("#to_error").html("<?=$this->lang->line('idcardreport_mail_to')?>").css("text-align", "left").css("color", 'red');
                        } 
                        if( response.subject) {
                            $("#subject_error").html("<?=$this->lang->line('idcardreport_mail_subject')?>").css("text-align", "left").css("color", 'red');
                        }
                        if(response.message) {
                            toastr["error"](response.message)
                            toastr.options = {
                              "closeButton": true,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "500",
                              "hideDuration": "500",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            }
                        }
                    } else {
                        location.reload();
                    }
                }
            });
        }
    });
</script>