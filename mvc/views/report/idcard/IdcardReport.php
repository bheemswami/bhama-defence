<div class="row">
    <div class="col-sm-12" style="margin:10px 0px">
        <?php
            $pdf_preview_uri = base_url('idcardreport/pdf/'.$usertypeID.'/'.$classesID.'/'.$sectionID.'/'.$userID.'/'.$type.'/'.$background);
            echo btn_printReport('idcardreport', $this->lang->line('idcardreport_print'), 'printablediv');
            //echo btn_pdfPreviewReport('idcardreport',$pdf_preview_uri, $this->lang->line('idcardreport_pdf_preview'));
            //echo btn_sentToMailReport('idcardreport', $this->lang->line('idcardreport_mail'));
        ?>
    </div>
</div>
<div class="box">
    <div class="box-header bg-gray">
        <h3 class="box-title text-navy"><i class="fa fa-clipboard"></i> 
            <?=$this->lang->line('idcardreport_report_for')?> -
            <?=isset($usertypes[$usertypeID]) ? $usertypes[$usertypeID]: ' ';?>
        </h3>
    </div><!-- /.box-header -->
    <div id="printablediv">
        <?php if($usertypeID == 3) { ?>
            <style>
                .cardwrap-old *,.cardwrap-old ::after,.cardwrap-old ::before{box-sizing:border-box}
                .cardwrap-old{width:550px;max-width:100%;margin:0 auto;color:#474747;padding:0 0 10px;border-radius:6px;border:1px solid transparent;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}
                .card-old-head{display:flex;flex-flow:row wrap;justify-content:center;align-items:flex-start;}
                .card-old-head-left{flex:1 20%;padding-right:11px;text-align: center; padding: 5px;box-sizing: border-box;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-ms-box-sizing: border-box;-o-box-sizing: border-box;
                    border-radius: 0 0 10px 10px;background-color:  #0099cb;overflow: hidden;}
                .card-old-head-left img{width: 82px;}
                .card-old-head-right{flex:1 75%;background-color:#09c}
                .cardwrap-old img{max-width:100%}
                .schoolname{font-size:14px;font-weight:700;color:#ffff;padding:15px;text-align:right}
                .schoolname div:nth-child(2){font-size:12px}
                .identityear{color:#000;background-color:#fff;font-size:14px}
                .identityear>div>div{width:50%;float:left;text-align:center;font-weight:700;text-shadow:0 3px 3px #ccc}
                .identityear>div{overflow:hidden;padding:10px 0;border-bottom:2px solid #ccc;border-bottom-left-radius:30px;border-bottom-right-radius:0}
                .card-old-body-left{flex:1 40%;padding:10px;width: 240px;}
                .card-old-body-right{flex:1 60%;display:flex;flex-flow:row wrap;align-items:center;align-content:space-between;justify-content:space-around}
                .card-old-body{display:flex;flex-flow:row wrap;justify-content:center;align-items:flex-start;align-content:center;margin-top:15px;font-size:14px;font-weight:700;letter-spacing:.02em}
                .card-old-body-right p{margin-bottom:0;margin-top:8px}
                .answers{color:#27585f}
                .card-old-body-left img{width:200px;height:220px;border:3px solid #f19be4;border-radius:22px;box-shadow:0 3px 6px #f19be4}
                .card-old-footer-bottom{font-size:16px;text-align:center;background-color:#0162c9;padding:10px;color:#ffff;font-weight:700;letter-spacing:.02em;text-shadow:1px 3px 3px rgba(255,255,255,.7)}
                /*.card-old-footer{margin-top:-40px}*/
                .signature{text-align:right;padding:25px 0px 0px 0;font-size:16px;font-weight:700;color:#00486d}
                .signature img{width:120px;margin:0px auto 0px}
                .signature-block{
                    display: block;
                    width: 100%;
                    margin-bottom: 20px;
                }
                .text-white{color:#fff !important;}
                @media print {
                    .card-old-head-left,.card-old-head-right{
                        -webkit-print-color-adjust: exact;
                        -moz-print-color-adjust: exact;
                        -ms-print-color-adjust: exact;
                        -o-print-color-adjust: exact;
                        print-color-adjust: exact;
                        background-color: #0099cc;
                        color: #fff;
                    }
                    .card-old-footer-bottom {
                        -webkit-print-color-adjust: exact;
                        -moz-print-color-adjust: exact;
                        -ms-print-color-adjust: exact;
                        -o-print-color-adjust: exact;
                        print-color-adjust: exact;
                        background-color: #0162c9;
                        color: #fff;
                    }
                    .text-white{color:#fff;}
                }
            </style>
        <?php } else{  ?>
            <style>
                .card{width:550px;margin:50px auto;color:#474747;padding:0 0 10px;border-radius:6px;border:1px solid transparent;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}
                .card *,.card ::after,.card ::before{box-sizing:border-box}
                .card-head{overflow:hidden;display:flex;flex-flow:row wrap;justify-content:center;align-items:flex-end;background-image:url(triangle-bg.jpg);background-position:top center;background-size:100% 67px;background-repeat:no-repeat}
                .card-head-left{flex:1 30%;padding:0 15px 0}
                .card-head-right{flex:1 70%}
                .card img{max-width:100%;height:auto}
                .card-head-right-left img{height:30px}
                .card-head-right-left{text-align:right;padding:20px 35px 10px}
                .card-head-right-right{display:flex;flex-flow:row wrap;justify-content:center;align-items:flex-end}
                .card-head-right-right-1{flex:1 60%;line-height:1}
                .card-head-right-right-2{flex:1 10%}
                .card-head-right-right-1 h2{margin-bottom:0;color:#005189}
                .card label,.card label a{color:#005189;text-decoration:none;font-weight:700}
                .card-head-right-right-1 h3{margin-top:10px;margin-bottom:10px}
                .card-foot{padding:15px 10px 0;margin-top:25px;display:flex;flex-flow:row wrap;justify-content:center;align-items:flex-end}
                .card-foot-1{flex:1 23%;border-right:2px dotted #005189;margin-right:11px}
                .card-foot-2{flex:1 18%;border-right:2px dotted #005189;margin-right:10px}
                .card-foot-3{flex:1 27%;border-right:2px dotted #005189;margin-right:9px}
                .card-foot-4{flex:1 24%;text-align:center}
                .card label,.card p{margin:0;font-size:14px}
                .card-head-left,.card-head-right-right{-webkit-transform:translateY(-5%);-moz-transform:translateY(-5%);-o-transform:translateY(-5%);transform:translateY(-5%)}
                .card-head-right-right-1 h2{margin-top:20px}
                .card-head-left img{width:100px;height:100px;box-shadow:0 0 4px #ccc;border-radius:50%;position:relative;top:60%;transform:translateY(-50%);-webkit-transform:translateY(-50%);-moz-transform:translateY(-50%);-ms-transform:translateY(-50%);-o-transform:translateY(-50%)}
            </style>
        <?php } ?>
       

        <div class="box-body" style="margin-bottom: 50px;">
            <div class="row">
                <div class="col-sm-12">
                    <?php 
                    $designation = [1=>'Admin',4=>'Parents',5=>'Accountant',6=>'Librarian',7=>'Receptionist',8=>'Moderator',9=>'Warden'];
                   
                    if (count($idcards)) {
                        foreach($idcards as $idcard) { 
                            if($usertypeID == 3){
                                $parent = getParentById($idcard->parentID); 
                                $libraryData = getLibraryId($idcard->srstudentID,'studentID'); 
                            } elseif($usertypeID == 2){
                                $libraryData = getLibraryId($idcard->teacherID,'teacherID');
                            }
                            ?>
                            <?php if($usertypeID == 3) { ?>
                                <div class="cardwrap-old" style="margin-bottom: 50px">
                                    <div class="card-old">
                                        <div class="card-old-head">
                                            <div class="card-old-head-left" style="position: relative;">
                                                <img src="assets/images/top-bg.jpg" style="position: absolute;width:100%;height: 100%;top:0;left:0;z-index: 0;">
                                                <img style="position: relative;z-index: 99;" src="assets/images/logo-bhama.png">
                                            </div>
                                            <div class="card-old-head-right" style="position:relative;">
                                                <div class="schoolname" style="position:relative;">
                                                    <img src="assets/images/top-bg.jpg" style="position: absolute;width:100%;height: 100%;top:0;left:0;z-index: 0;">
                                                    <div style="z-index: 1;position: relative;color:#fff;">
                                                    <div class="text-white" style="font-size: 18px;"><?=$siteinfos->sname?></div>
                                                    <div class="text-white" style="font-size: 15px;">&nbsp;</div>
                                                    </div>
                                                </div>
                                                <div class="identityear">
                                                    <div>
                                                        <div>IDENTITY CARD</div>
                                                         <div><?=$schoolyearobj->schoolyear?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-old-body">
                                            <div class="card-old-body-left">
                                                <!-- <img src="PROFILE-PIC.png"> -->
                                               <?php 
                                                    if($idcard->photo=='') $src = "assets/images/blank-profile-pic.png";
                                                    else $src = base_url('uploads/student_photo/'.$idcard->photo);
                                                    $array = ["src" => $src];
                                                    echo img($array);
                                                ?>
                                            </div>
                                            <div class="card-old-body-right">
                                                <div class="labels" style="text-align: right;font-weight: bold;">
                                                    <p>Student ID :</p>
                                                    <p>Library ID :</p>
                                                    <p>Student Name :</p>
                                                    <p>Father/Guardian :</p>
                                                    <p>Course :</p>
                                                    <p>Emergency Call :</p>
                                                </div>
                                                <div class="answers">
                                                    <p><?=$idcard->srregisterNO?></p>
                                                    <p><?=($libraryData) ? $libraryData->lID : 'NA'?></p>
                                                    <p><?=$idcard->srname?></p>
                                                    <p><?=$parent->father_name?></p>
                                                    <p><?=$idcard->srclasses?></p>
                                                    <p><?=$parent->phone?></p>
                                                </div>
                                                <div class="card-old-footer-body signature-block">
                                                    <div class="signature">
                                                        <!-- <img src="signature.png"> -->
                                                        <?php 
                                                        if($idcard->sign=='') $src = "assets/images/signature.png";
                                                        else $src = base_url('uploads/student_sign/'.$idcard->sign);
                                                        $array = ["src" => $src];
                                                        //echo img($array);
                                                    ?>
                                                        <br/>
                                                        <label>PRINCIPAL</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-old-footer">
                                            
                                            <div class="card-old-footer-bottom" style="position: relative;">
                                                <img src="assets/images/bottom-bg.png" style="position: absolute;width:100%;height: 100%;top:0;left:0;z-index: 0;">
                                                <div class="sloganhere text-white" style="position: relative;z-index: 1;color:#fff;">
                                                    “Give me Your Time, I Will Give You Economical Freedom” By Dr. G. R.Choudhary
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else {?>
                                <div class="cardwrap">
                                    <div class="card" style="position:relative;">
                                        <img src="assets/images/Lined-Paper-bg.jpg" style="position:absolute;width:100%;height:100%;top:0;left:0;z-index: 0;">
                                        <div style="z-index: 1;position:relative;"><div class="card-head" style=" position:relative;">
                                            <div style="position:absolute;width:100%;height:100%;top:0%;height:86px;left:0;z-index: 3;overflow: hidden;">
                                                <img style="width: 100%; height: auto;" src="assets/images/triangle-bg.jpg" ></div>
                                           <div style="z-index: 9;position:relative;display: flex;width:100%;height: 155px;"> <div class="card-head-left">
                                                <!-- <img src="assets/images/PROFILE-PIC.png" style="border:3px solid #fff;"> -->
                                                <?php 
                                                    if($idcard->photo=='') $src = "assets/images/PROFILE-PIC.png";
                                                    else $src = base_url('uploads/images/'.$idcard->photo);
                                                    $array = ["src" => $src,'style'=>''];
                                                    echo img($array);
                                                ?>
                                            </div>
                                            <div class="card-head-right">
                                                <div class="text-white" style="font-size: 20px;text-align: right;margin-right: 30px;margin-top: 12px;">
                                                    <?=$siteinfos->sname?><br/>
                                                    <span class="text-white" style="font-size: 15px;text-align: right;"> Phone No. : <?=$siteinfos->phone?></span>  
                                                </div>
                                               
                                                <div class="card-head-right-right" style="margin-top: 20px;">
                                                    <div class="card-head-right-right-1">
                                                        <h2 style="font-size: 22px;font-weight: bold;"><?=$idcard->name?></h2>
                                                        <h3 style="font-size: 16px;"><?=($usertypeID==2) ? $idcard->designation : $designation[$usertypeID]?></h3>
                                                    </div>
                                                    <div class="card-head-right-right-2">
                                                        <!-- <img src="QR-CODE.png"> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="card-foot">
                                            <div class="card-foot-1">
                                                <label>D.O.B.</label>
                                                <p><?=date('d/m/Y',strtotime($idcard->dob))?></p>
                                                <br/>
                                                <label>Date of Issue</label>
                                                <p><?=date('d/m/Y',strtotime($idcard->jod))?></p>
                                            </div>
                                            <div class="card-foot-2">
                                                <label>Gender</label>
                                                <p><?=$idcard->sex?></p>
                                                <br/>
                                                <label>Emp No.</label>
                                                <p><?=($usertypeID==2) ? $idcard->teacherID : $idcard->userID?></p>
                                            </div>
                                            <div class="card-foot-3">
                                                <?php if($usertypeID==2){?>
                                                <label>Library ID</label>
                                                <p><?=($libraryData) ? $libraryData->lID : 'NA'?></p>
                                            <?php } else { echo '<label>&nbsp;</label><p>&nbsp;</p>'; } ?>
                                                <!-- <label>Library ID</label><p>5656</p> -->
                                                <br/>
                                                 <label>Phone no</label>
                                                <p><?=$idcard->phone?></p>
                                             
                                            </div>
                                            <div class="card-foot-4" style="text-align: left;">
                                                <?php if($usertypeID==2){?>
                                                    <label>Blood Group</label>
                                                    <p><?=$idcard->blood_group?></p><br/>
                                                <?php } ?>
                                                <label>&nbsp;</label>
                                                <p style="margin-left: 25%">Signature</p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                    <?php   } // end foreach 
                        }  else { ?>   
                        <div class="callout callout-danger">
                            <p><b class="text-info"><?=$this->lang->line('idcardreport_data_not_found')?></b></p>
                        </div>
                    <?php } ?>
                </div>
            </div><!-- row -->
        </div><!-- Body -->
    </div>
</div>


<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('idcardreport/send_pdf_to_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?=$this->lang->line('idcardreport_close')?></span></button>
                <h4 class="modal-title"><?=$this->lang->line('idcardreport_mail')?></h4>
            </div>
            <div class="modal-body">

                <?php
                    if(form_error('to'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("idcardreport_to")?> <span class="text-red">*</span>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php
                    if(form_error('subject'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("idcardreport_subject")?> <span class="text-red">*</span>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php
                    if(form_error('message'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("idcardreport_message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <?php/*<input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("idcardreport_send")?>" />*/?>
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->

<script type="text/javascript">
    
    function check_email(email) {
        var status = false;
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("<?=$this->lang->line('idcardreport_mail_valid')?>").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }


    $('#send_pdf').click(function() {
        var field = {
            'to'         : $('#to').val(), 
            'subject'    : $('#subject').val(), 
            'message'    : $('#message').val(),
            'usertypeID' : '<?=$usertypeID?>',
            'classesID'  : '<?=$classesID?>',
            'sectionID'  : '<?=$sectionID?>',
            'userID'     : '<?=$userID?>',
            'type'       : '<?=$type?>',
            'background' : '<?=$background?>',
        };

        var to = $('#to').val();
        var subject = $('#subject').val();
        var error = 0;

        $("#to_error").html("");
        $("#subject_error").html("");

        if(to == "" || to == null) {
            error++;
            $("#to_error").html("<?=$this->lang->line('idcardreport_mail_to')?>").css("text-align", "left").css("color", 'red');
        } else {
            if(check_email(to) == false) {
                error++
            }
        }

        if(subject == "" || subject == null) {
            error++;
            $("#subject_error").html("<?=$this->lang->line('idcardreport_mail_subject')?>").css("text-align", "left").css("color", 'red');
        } else {
            $("#subject_error").html("");
        }

        if(error == 0) {
            $('#send_pdf').attr('disabled','disabled');
            $.ajax({
                type: 'POST',
                url: "<?=base_url('idcardreport/send_pdf_to_mail')?>",
                data: field,
                dataType: "html",
                success: function(data) {
                    var response = JSON.parse(data);
                    if (response.status == false) {
                        $('#send_pdf').removeAttr('disabled');
                        if( response.to) {
                            $("#to_error").html("<?=$this->lang->line('idcardreport_mail_to')?>").css("text-align", "left").css("color", 'red');
                        } 
                        if( response.subject) {
                            $("#subject_error").html("<?=$this->lang->line('idcardreport_mail_subject')?>").css("text-align", "left").css("color", 'red');
                        }
                        if(response.message) {
                            toastr["error"](response.message)
                            toastr.options = {
                              "closeButton": true,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "500",
                              "hideDuration": "500",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            }
                        }
                    } else {
                        location.reload();
                    }
                }
            });
        }
    });
</script>