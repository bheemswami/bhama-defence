<div class="form-box" id="login-box">
    <div class="header"><?=$this->lang->line('signin')?></div>
    <form method="post">

        <!-- style="margin-top:40px;" -->

        <div class="body white-bg">
        <?php
            if($form_validation == "No"){
            } else {
                if(($form_validation)) {
                    echo "<div class=\"alert alert-danger alert-dismissable\">
                        <i class=\"fa fa-ban\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $form_validation
                    </div>";
                }
            }
            if($this->session->flashdata('reset_success')) {
                $message = $this->session->flashdata('reset_success');
                echo "<div class=\"alert alert-success alert-dismissable\">
                    <i class=\"fa fa-ban\"></i>
                    <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                    $message
                </div>";
            }
        ?>
            <div class="form-group parrent effects">
              <label for="username" class="form-cstm-label">Username</label>
              <div class="effects">
                <input class="form-control form-styling effect-7" placeholder="Username" name="username" type="text" autofocus value="<?=set_value('username')?>" autocomplete="false">
              
                    <span class="focus-border">
                      <i></i>
                    </span>
                    </div>
            </div>
            <div class="form-group">
            <label for="username" class="form-cstm-label">Password</label>
            <div class="effects">
                <input class="form-control form-styling effect-8" placeholder="Password" name="password" type="password" autocomplete="off">
                <span class="focus-border">
                      <i></i>
                    </span>
                    </div>
            </div>


            <div class="checkbox custom-padding">
                <label>
                    <input type="checkbox" class="form-control custom-form-control" value="Remember Me" name="remember" style="width: 16px;height: 20px;">
                    <span class="form-cstm-label"> &nbsp; Remember Me</span>
                </label>
                <span class="pull-right">
                    <label class="form-cstm-label">
                        <a href="<?=base_url('reset/index')?>">Forgot Password?</a>
                    </label>
                </span>
            </div>

            <?php if(isset($siteinfos->captcha_status) && $siteinfos->captcha_status == 0) { ?>
                <div class="form-group">
                    <?php echo $recaptcha['widget']; echo $recaptcha['script']; ?>
                </div>
            <?php } ?>
            
            <input type="submit" class="form-styling btn btn-sm btn-success btn-block" value="SIGN IN" />
        </div>
    </form>
</div>
<?php /*
    <div class="col-sm-12 marg" style="margin-top:30px;">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">
               For Login Click Below...
              </a>
            </div>
          </div>
        </nav>
    </div>
    <div class="col-sm-12 marg">
        <center>
            <div class="btn-group" role="group" aria-label="...">
                <button class="btn btn-sm btn-primary" id="admin">Admin</button>
                <!-- <button class="btn btn-sm btn-info" id="teacher">Teacher</button> -->
                <!-- <button class="btn btn-sm btn-warning" id="student">Student</button>
                <button class="btn btn-sm btn-success" id="parent">Parent</button> -->
                <button class="btn btn-sm btn-danger" id="accountant">Accountant</button>
                <button class="btn btn-sm btn-default" id="librarian">Librarian</button>
                <button class="btn btn-sm btn-primary" id="recep">Receptionist</button>
                <button class="btn btn-sm btn-warning" id="warden">Warden</button>
            </div>
        </center>
    </div>
    */ ?>
</div>
<script type="text/javascript" src="https://demo.inilabs.net/school/v4.2/assets/inilabs/jquery.js"></script>
 <script src="<?php echo base_url('assets/js/particles.js'); ?>"></script>
       <script src="<?php echo base_url('assets/js/app.js'); ?>"></script>
       <script src="<?php echo base_url('assets/js/lib/stats.js'); ?>"></script>
       <script type="text/javascript">
            $('#admin').click(function () {
                $("input[name=username]").val('admin');
                $("input[name=password]").val('password');
            });
            $('#teacher').click(function () {
                $("input[name=username]").val('teacher');
                $("input[name=password]").val('password');
            });
            $('#student').click(function () {
                $("input[name=username]").val('student1');
                $("input[name=password]").val('123456');
            });
            $('#parent').click(function () {
                $("input[name=username]").val('parent1');
                $("input[name=password]").val('123456');
            });
            $('#accountant').click(function () {
                $("input[name=username]").val('accountant');
                $("input[name=password]").val('password');
            });
            $('#librarian').click(function () {
                $("input[name=username]").val('librarian');
                $("input[name=password]").val('password');
            });
            $('#recep').click(function () {
                $("input[name=username]").val('receptionist');
                $("input[name=password]").val('password');
            });
            $('#warden').click(function () {
                $("input[name=username]").val('warden');
                $("input[name=password]").val('password');
            });
        </script>
<script>
  var count_particles, stats, update;
  stats = new Stats;
  stats.setMode(0);
  stats.domElement.style.position = 'absolute';
  stats.domElement.style.left = '0px';
  stats.domElement.style.top = '0px';
  document.body.appendChild(stats.domElement);
  count_particles = document.querySelector('.js-count-particles');
  update = function() {
    stats.begin();
    stats.end();
    if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
      count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
    }
    requestAnimationFrame(update);
  };
  requestAnimationFrame(update);
</script>


<script type="text/javascript">
	// JavaScript for label effects only
	$(window).load(function(){
		$(".effects input").val("");
		
		$(".effects input").focusout(function(){
			if($(this).val() != ""){
				$(this).addClass("has-content");
			}else{
				$(this).removeClass("has-content");
			}
		})
	});
</script>

