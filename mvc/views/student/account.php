    <div id="printablediv">
        <section class="panel">
            <div class="profile-view-head">
               <h1><?=$student->name?>`s <?=$this->lang->line('acc_acc_details')?></h1>
            </div>
        </section>
    </div>

    <div class="row">
        <div class="col-xs-12 text-right">
            
            <div class="table-card">
                   <table class="mrgn-top-1 table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col"><?=$this->lang->line('s_no')?></th>
                      <th scope="col"><?=$this->lang->line('acc_date')?></th>
                      <th scope="col"><?=$this->lang->line('acc_desc')?></th>
                      <th scope="col"><?=$this->lang->line('acc_amount')?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $totalDebit =0;
                    $totalCredit =0;
                    if(count($payments)){
                    foreach ($payments as $key => $value) {
                        $totalDebit =$totalDebit+($value->paymentamount)
                    ?>
                    <tr>
                      <th scope="row"><?=$key+1?></th>
                      <td><?=$value->paymentdate?></td>
                      <td>
                        <?php
                        if($value->fee_type==5){
                            echo 'Addmission Fees ( '.$value->purpose.' )';
                        } else {
                            echo $value->purpose;
                        }
                        ?>
                        </td>
                      <td class="text-right">
                        <?=$value->paymentamount?>&nbsp;
                        <?=($value->credit_debit=='Debit') ? 'Dr.' : 'Cr.'?>
                        </td>
                    </tr>
                    <?php }}?>
                    <!-- <tr>
                        <td colspan="3"></td>
                        <th scope="row" colspan="1">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-right"><strong>Course Fee :</strong></td>
                                    <td class="text-right"><?=sprintf('%0.2f',$student->course_fee)?></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Installments :</strong></td>
                                    <td class="text-right"><?=sprintf('%0.2f',$totalDebit)?></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Remaining :</strong></td>
                                    <td class="text-right"><?=sprintf('%0.2f',($student->course_fee)-$totalDebit)?><strong></td>
                                </tr>
                            </table>
                        </th>
                    </tr> -->
                  </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- modal for add info -->

