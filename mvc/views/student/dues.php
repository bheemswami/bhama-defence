<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-student"></i> <?=$this->lang->line('menu_dues')?></h3>


        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_dues')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?php if(($students) > 0 ) { 
                    ?>
                    <div class="nav-tabs-custom">
                        <div class="tab-content">
                            <div id="all" class="tab-pane active">
                                <div id="hide-table">
                                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                                <th><?=$this->lang->line('student_name')?></th>
                                                <th><?=$this->lang->line('student_roll')?></th>
                                                <th><?=$this->lang->line('student_academic_due')?></th>
                                                <th><?=$this->lang->line('student_hostel_due')?></th>
                                                <th><?=$this->lang->line('student_library_due')?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
                                                <tr>
                                                    <td data-title="<?=$this->lang->line('slno')?>">
                                                        <?php echo $i; ?>
                                                    </td>                                                    
                                                    <td data-title="<?=$this->lang->line('student_name')?>">
                                                        <a 
                                                        href="<?=base_url('student/view/').$student['studentID'].'/'.$student['classesID']?>"
                                                        ><?php echo $student['name']; ?></a>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_roll')?>">
                                                        <?php echo $student['roll']; ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_academic_due')?>">
                                                        <?php if ($student['academic']){ ?>
                                                            <span style="color: red"><b>Due</b></span>
                                                        <?php } else {?>
                                                            <span style="color: green"><b>No Dues</b></span>
                                                        <?php } ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_hostel_due')?>">
                                                        <?php if ($student['hostel']){ ?>
                                                            <span style="color: red"><b>Due</b></span>
                                                        <?php } else {?>
                                                            <span style="color: green"><b>No Dues</b></span>
                                                        <?php } ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_library_due')?>">
                                                        <?php if ($student['library']){ ?>
                                                            <span style="color: red"><b>Due</b></span>
                                                        <?php } else {?>
                                                            <span style="color: green"><b>No Dues</b></span>
                                                        <?php } ?>
                                                    </td>
                                                    
                                               </tr>
                                            <?php $i++; }} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div> <!-- nav-tabs-custom -->
                <?php } ?>
            </div> <!-- col-sm-12 -->

        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

