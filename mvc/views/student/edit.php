
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-student"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("student/index/$set")?>"><?=$this->lang->line('menu_student')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('panel_title')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">

                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                   <input type="hidden" name="parent_id" value="<?=$student->parentID?>">

                   <div class="form-group <?=(form_error('classesID')) ? 'has-error' : ''?>">
                        <label for="classesID" class="col-sm-2 control-label">
                            <?php 
                                if($siteinfos->school_type == 'semesterbase') {
                                    echo $this->lang->line("student_department");
                                    $array = array(0 => $this->lang->line("student_select_department"));
                                } else {
                                    $array = array(0 => $this->lang->line("student_select_class"));
                                    echo $this->lang->line("student_classes");
                                } 
                            ?>
                        </label>
                            <div class="col-sm-6">
                                <?php
                                    foreach ($classes as $classa) {
                                        $array[$classa->classesID] = $classa->classes;
                                    }
                                    echo form_dropdown("classesID", $array, set_value("classesID", $student->classesID), "id='classesID' class='form-control select2'");
                                ?>
                            </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <div class="form-group <?=(form_error('name')) ? 'has-error' : ''?>">
                        <label for="name_id" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name_id" name="name" value="<?=set_value('name', $student->name)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('name'); ?></span>
                    </div>

                    <div class="form-group <?=(form_error('registerNO')) ? 'has-error' : ''?>">
                        <label for="registerNO" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_registerNO")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="registerNO" name="registerNO" value="<?=set_value('registerNO', $student->registerNO)?>" readonly>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('registerNO'); ?>
                        </span>
                    </div>
                    <?php /*<div class="form-group <?=(form_error('username')) ? 'has-error' : ''?>">
                        <label for="username" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_username")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="username" name="username" value="<?=set_value('username', $student->username)?>" >
                        </div>
                         <span class="col-sm-4 control-label">
                            <?php echo form_error('username'); ?>
                        </span>
                    </div>*/ ?>
                    <div class="form-group <?=(form_error('email')) ? 'has-error' : ''?>">
                        <label for="email" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_email")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $student->email)?>" readonly >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('email'); ?>
                        </span>
                    </div>
                    <div class="form-group <?=(form_error('dob')) ? 'has-error' : ''?>">
                        <label for="dob" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_dob")?>
                        </label>
                        <div class="col-sm-6">
                            <?php $dob = ''; if($student->dob) { $dob = date("d-m-Y", strtotime($student->dob)); }  ?>
                            <input type="text" class="form-control" id="dob" name="dob" value="<?=set_value('dob', $dob)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('dob'); ?>
                        </span>
                    </div>

                    <div class="form-group <?=(form_error('sex')) ? 'has-error' : ''?>">
                        <label for="sex" class="col-sm-2 control-label"><?=$this->lang->line("student_sex")?></label>
                        <div class="col-sm-6">
                            <?php 
                                echo form_dropdown("sex", array($this->lang->line('student_sex_male') => $this->lang->line('student_sex_male'), $this->lang->line('student_sex_female') => $this->lang->line('student_sex_female')), set_value("sex", $student->sex), "id='sex' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('sex'); ?>
                        </span>

                    </div>
                   
                    <div class="form-group <?=(form_error('father_name')) ? 'has-error' : ''?>">
                     <label for="father_name" class="col-sm-2 control-label"><?=$this->lang->line("enq_father_name")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="father_name" name="father_name" value="<?=setInputVals($parent,$student,'father_name')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('father_name'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('father_business')) ? 'has-error' : ''?>">
                     <label for="father_business" class="col-sm-2 control-label"><?=$this->lang->line("enq_father_business")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="father_business" name="father_business" value="<?=setInputVals($parent,$student,'father_business')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('father_business'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('mother_name')) ? 'has-error' : ''?>">
                     <label for="mother_name" class="col-sm-2 control-label"><?=$this->lang->line("enq_mother_name")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="mother_name" name="mother_name" value="<?=setInputVals($parent,$student,'mother_name')?>" required>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('mother_name'); ?></span>
                  </div>
                  <div class="form-group <?=(form_error('mother_business')) ? 'has-error' : ''?>">
                     <label for="mother_business" class="col-sm-2 control-label"><?=$this->lang->line("enq_mother_business")?></label>
                     <div class="col-sm-6">
                        <input type="text" class="form-control" id="mother_business" name="mother_business" value="<?=setInputVals($parent,$student,'mother_business')?>">
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('mother_business'); ?></span>
                  </div>

                    <div class="form-group <?=(form_error('address')) ? 'has-error' : ''?>">
                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_address")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" value="<?=setInputVals($parent,$student,'address')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('address'); ?>
                        </span>
                    </div>
                    <div class="form-group <?=(form_error('parent_mob')) ? 'has-error' : ''?>">
                         <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("enq_parent_number")?></label>
                         <div class="col-sm-6">
                            <input type="text" class="form-control" id="parent_mob" name="parent_mob" value="<?=setInputVals($parent,$student,'parent_mob')?>" required>
                         </div>
                         <span class="col-sm-4 control-label"><?php echo form_error('parent_mob'); ?></span>
                      </div>

                       <div class="form-group <?=(form_error('photo')) ? 'has-error' : ''?>">
                        <label for="photo" class="col-sm-2 control-label"><?=$this->lang->line("student_photo")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="photo" class="form-control"/>
                        </div>
                        <span class="col-sm-4"><?php echo form_error('photo'); ?></span>
                    </div>

                    <div class="form-group <?=(form_error('sign')) ? 'has-error' : ''?>">
                        <label for="sign" class="col-sm-2 control-label"><?=$this->lang->line("student_sign")?></label>
                        <div class="col-sm-6">
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="sign" class="form-control"/>
                        </div>
                        <span class="col-sm-4"><?php echo form_error('sign'); ?></span>
                    </div>
                    
                     <div class="form-group">
                     <label for="parent_mob" class="col-sm-2 control-label"><?=$this->lang->line("enq_educational_details")?></label>
                     <div class="col-sm-10">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th><?=$this->lang->line("exam_name")?></th>
                                 <th><?=$this->lang->line("year")?></th>
                                 <th><?=$this->lang->line("subject")?></th>
                                 <th><?=$this->lang->line("obtain_marks")?></th>
                                 <th><?=$this->lang->line("max_marks")?></th>
                                 <th><?=$this->lang->line("percentage")?></th>
                                 <th><?=$this->lang->line("division")?></th>
                                 <th><?=$this->lang->line("board")?></th>
                              </tr>
                           </thead>
                           <tbody>
                                <?php if(count($edu_details)>0) { foreach($edu_details as $val){ ?>
                                    <tr>
                                        <td>
                                            <?=ucfirst($val->exam_name)?>
                                            <input type="hidden" class="form-control" name="edu[ids][<?=$val->id?>]" value="<?=$val->id?>">
                                            <input type="hidden" class="form-control" name="edu[exam_name][<?=$val->id?>]" value="<?=$val->exam_name?>">
                                        </td>
                                        <td>
                                            
                                            <select class="form-control" name="edu[year][<?=$val->id?>]" style="width: 110px;">
                                                <?php
                                                   echo '<option value="">'.$this->lang->line("select_year").'</option>';
                                                   for($year=1980;$year<=date('Y');$year++){
                                                        if($val->year == $year){
                                                            echo '<option value="'.$year.'" selected="selected">'.$year.'</option>';
                                                        } else {
                                                            echo '<option value="'.$year.'">'.$year.'</option>';
                                                        }
                                                       
                                                   }
                                                   ?>
                                            </select> 
                                        </td>
                                        <td> <input type="text" class="form-control" name="edu[subject][<?=$val->id?>]" value="<?=$val->subjects?>"></td>
                                        <td> <input type="number" class="form-control obtain_marks" name="edu[obtain_marks][<?=$val->id?>]" value="<?=$val->obtain_marks?>"></td>
                                        <td> <input type="number" class="form-control max_marks" name="edu[max_marks][<?=$val->id?>]" value="<?=$val->total_marks?>"></td>
                                        <td> <input type="number" class="form-control percentage" name="edu[percentage][<?=$val->id?>]" value="<?=$val->percentage?>" readonly></td>
                                        <td> <input type="text" class="form-control" name="edu[division][<?=$val->id?>]" value="<?=$val->division?>"></td>
                                        <td> <input type="text" class="form-control" name="edu[board][<?=$val->id?>]" value="<?=$val->borad?>"></td>
                                    </tr>
                                <?php } }?>
                           </tbody>
                        </table>
                     </div>
                     <span class="col-sm-4 control-label"><?php echo form_error('parent_mob'); ?></span>
                  </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_student")?>" >
                        </div>
                    </div>

                </form>

            </div> <!-- col-sm-8 -->
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<?php 
function setInputVals($parent,$student,$fieldName){
    $filedsArr = [
        'father_name'=>'father_name',
        'father_business'=>'father_profession',
        'mother_name'=>'mother_name',
        'mother_business'=>'mother_profession',
        'address'=>'address',
        'parent_mob'=>'phone',
    ];
    if($student->parentID>0){
        $fValue = $filedsArr[$fieldName];
        return  set_value($fieldName,$parent->$fValue);
    } else {
        return  set_value($fieldName);
    }
}
?>
<script type="text/javascript">
$( ".select2" ).select2();
$('#dob').datepicker({ startView: 2 });

$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#classesID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('student/sectioncall')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#sectionID').html(data);
            }
        });

        $.ajax({
            type: 'POST',
            url: "<?=base_url('student/optionalsubjectcall')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data2) {
                $('#optionalSubjectID').html(data2);
            }
        });
    }
});

$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
           $('.content').css('padding-bottom', '130px');
        }, 
         function () {
           $('.image-preview').popover('hide');
           $('.content').css('padding-bottom', '20px');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("<?=$this->lang->line('student_file_browse')?>"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200,
            overflow:'hidden'
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("<?=$this->lang->line('student_file_browse')?>");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            $('.content').css('padding-bottom', '130px');
        }        
        reader.readAsDataURL(file);
    });  
});


var maxMarks = 0;
   var obtainMarks = 0;
   var percentage = 0;
   $(document).on('keyup click','.obtain_marks',function(){
       obtainMarks=$(this).val();
       calculatePercentage($(this));
   });
   $(document).on('keyup click','.max_marks',function(){
       maxMarks=$(this).val();
       calculatePercentage($(this));
   });
   function calculatePercentage(_this){
       var p = obtainMarks/maxMarks*100;
       _this.closest('tr').find('.percentage').val(Math.round(p * 100) / 100);
   }
</script>
