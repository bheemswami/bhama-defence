
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-student"></i> <?=$this->lang->line('panel_title')?></h3>


        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_student')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <h5 class="page-header">
                    <?php 
                    /*
                    <?php 
                        $usertype = $this->session->userdata("usertype");
                        if(permissionChecker('student_add')) {
                    ?>
                        <a href="<?php echo base_url('student/add') ?>">
                            <i class="fa fa-plus"></i>
                            <?=$this->lang->line('add_title')?>
                        </a>
                    <?php } ?>
                    */?>
                    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12 pull-right drop-marg">
                        <?php
                            $array = array("0" => $this->lang->line("student_select_class"));
                            if(count($classes)) {
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                            }
                            echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control select2'");
                        ?>

                    </div>
                </h5>

                <?php if(($students) > 0 ) { 
                    $stmts = getStudentStmt();
                    ?>
                    <div class="nav-tabs-custom">
                        <div class="tab-content">
                            <div id="all" class="tab-pane active">
                                <div id="hide-table">
                                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                                <th><?=$this->lang->line('student_photo')?></th>
                                                <th><?=$this->lang->line('student_sign')?></th>
                                                <th><?=$this->lang->line('student_name')?></th>
                                                <th><?=$this->lang->line('student_roll')?></th>
                                                <th><?=$this->lang->line('student_email')?></th>
                                                <th><?=$this->lang->line('student_fee_th')?></th>
                                                <th><?=$this->lang->line('student_fee_amount')?></th>
                                                <?php if(permissionChecker('student_edit')) { ?>
                                                <th class="col-sm-1"><?=$this->lang->line('student_status')?></th>
                                                <?php } ?>
                                                <?php if(permissionChecker('student_edit') || permissionChecker('student_delete') || permissionChecker('student_view')) { ?>
                                                <th><?=$this->lang->line('action')?></th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($students)) {$i = 1; foreach($students as $student) { 
                                                   // if($set==''){
                                                        $set = $student->classesID;
                                                    //}
                                                    $feeAmount = studentStmtFilterArr($student->studentID,$stmts);
                                                ?>
                                                <tr>
                                                    <td data-title="<?=$this->lang->line('slno')?>">
                                                        <?php echo $i; ?>
                                                    </td>
                                                    <?php ?>
                                                    <td data-title="<?=$this->lang->line('student_photo')?>">
                                                        <?php $array = array(
                                                                "src" => base_url('uploads/student_photo/'.$student->photo),
                                                                'width' => '35px',
                                                                'height' => '35px',
                                                                'class' => 'img-rounded'

                                                            );
                                                            echo img($array);
                                                        ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_sign')?>">
                                                        <?php $array = array(
                                                                "src" => base_url('uploads/student_sign/'.$student->sign),
                                                                'width' => '35px',
                                                                'height' => '35px',
                                                                'class' => 'img-rounded'

                                                            );
                                                            echo img($array);
                                                        ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_name')?>">
                                                        <?php echo $student->name; ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_roll')?>">
                                                        <?php echo $student->roll; ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_email')?>">
                                                        <?php echo $student->email; ?>
                                                    </td>
                                                     <td data-title="<?=$this->lang->line('student_fee_th')?>">
                                                        <?php echo checkFeeAmount($feeAmount['final_course_fee']); ?>
                                                    </td>
                                                     <th data-title="<?=$this->lang->line('student_fee_amount')?>">
                                                        <?php 
                                                        $fee = number_format($feeAmount['final_course_fee']);
                                                        echo ($fee>0) ? '<span class="text-danger">&#8377; '.$fee : '&#8377; '.$fee; ?>
                                                    </th>

                                                    <?php if(permissionChecker('student_edit')) { ?>
                                                    <td data-title="<?=$this->lang->line('student_status')?>">
                                                        <div class="onoffswitch-small" id="<?=$student->studentID?>">
                                                            <input type="checkbox" id="myonoffswitch<?=$student->studentID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($student->active === '1') echo "checked='checked'"; ?>>
                                                            <label for="myonoffswitch<?=$student->studentID?>" class="onoffswitch-small-label">
                                                                <span class="onoffswitch-small-inner"></span>
                                                                <span class="onoffswitch-small-switch"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <?php } ?>
                                                    <?php if(permissionChecker('student_edit') || permissionChecker('student_delete') || permissionChecker('student_view')) { ?>
                                                    <td data-title="<?=$this->lang->line('action')?>">
                                                        <?php
                                                            $accUrl = base_url('student/student_account/'.$student->studentID."/".$set);
                                                            echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
                                                            echo btn_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit'));
                                                            echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));
                                                            echo '<a data-id="'.$set.'" data-sid="'.$student->studentID.'" class="btn btn-info btn-xs mrg hostelAccessoriesIcon" data-toggle="modal" data-target="#hostelAccessories" title="'.$this->lang->line('hostel_accessories').'"><i class="fa fa-suitcase"></i></a>';
                                                            //if($student->hostel==1) 
                                                                //echo '<a data-id="'.$set.'" data-sid="'.$student->studentID.'" class="btn btn-info btn-xs mrg hostalFeeIcon" data-toggle="modal" data-target="#hostalFeeModel" title="'.$this->lang->line('assign_hostel').'"><i class="fa fa-home"></i></a>';
                                                           echo '<a href="'.$accUrl.'" class="btn btn-warning btn-xs" title="'.$this->lang->line('student_account').'"><i class="fa fa-inr"></i></a>';
                                                            echo btn_physical_show('student/physical/'.$student->studentID, $this->lang->line('physical_test'));
                                                        ?>
                                                    </td>
                                                    <?php } ?>
                                               </tr>
                                            <?php $i++; }} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <?php foreach ($sections as $key => $section) { ?>
                                    <div id="tab<?=$section->classesID.$section->sectionID?>" class="tab-pane">
                                        <div id="hide-table">
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                                <thead>
                                                    <tr>
                                                        <th><?=$this->lang->line('slno')?></th>
                                                        <th><?=$this->lang->line('student_photo')?></th>
                                                        <th><?=$this->lang->line('student_name')?></th>
                                                        <th><?=$this->lang->line('student_roll')?></th>
                                                        <th><?=$this->lang->line('student_email')?></th>
                                                        <?php if(permissionChecker('student_edit') || permissionChecker('student_delete') || permissionChecker('student_view')) { ?>
                                                        <th><?=$this->lang->line('action')?></th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(count($allsection[$section->sectionID])) { $i = 1; foreach($allsection[$section->sectionID] as $student) { if($section->sectionID === $student->sectionID) { ?>
                                                        <tr>
                                                            <td data-title="<?=$this->lang->line('slno')?>">
                                                                <?php echo $i; ?>
                                                            </td>

                                                            <td data-title="<?=$this->lang->line('student_photo')?>">
                                                                <?php $array = array(
                                                                        "src" => base_url('uploads/images/'.$student->photo),
                                                                        'width' => '35px',
                                                                        'height' => '35px',
                                                                        'class' => 'img-rounded'

                                                                    );
                                                                    echo img($array);
                                                                ?>
                                                            </td>
                                                            <td data-title="<?=$this->lang->line('student_name')?>">
                                                                <?php echo $student->name; ?>
                                                            </td>
                                                            <td data-title="<?=$this->lang->line('student_roll')?>">
                                                                <?php echo $student->roll; ?>
                                                            </td>
                                                            <td data-title="<?=$this->lang->line('student_email')?>">
                                                                <?php echo $student->email; ?>
                                                            </td>
                                                            <?php if(permissionChecker('student_edit') || permissionChecker('student_delete') || permissionChecker('student_view')) { ?>
                                                            <td data-title="<?=$this->lang->line('action')?>">
                                                                <?php
                                                                    echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
                                                                    echo btn_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit'));
                                                                    echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));
                                                                    echo btn_physical_show('student/physical/'.$student->studentID, "Physical");
                                                                ?>
                                                            </td>
                                                            <?php } ?>
                                                       </tr>
                                                    <?php $i++; }}} ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                            <?php } ?>
                        </div>
                    </div> <!-- nav-tabs-custom -->
                <?php } else { ?>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("student_all_students")?></a></li>
                        </ul>


                        <div class="tab-content">
                            <div id="all" class="tab-pane active">
                                <div id="hide-table">
                                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th><?=$this->lang->line('slno')?></th>
                                                <th><?=$this->lang->line('student_photo')?></th>
                                                <th><?=$this->lang->line('student_name')?></th>
                                                <th><?=$this->lang->line('student_roll')?></th>
                                                <th><?=$this->lang->line('student_email')?></th>
                                                <?php if(permissionChecker('student_edit') || permissionChecker('student_delete') || permissionChecker('student_view')) { ?>
                                                <th><?=$this->lang->line('action')?></th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(($students)) {$i = 1; foreach($students as $student) { ?>
                                                <tr>
                                                    <td data-title="<?=$this->lang->line('slno')?>">
                                                        <?php echo $i; ?>
                                                    </td>

                                                    <td data-title="<?=$this->lang->line('student_photo')?>">
                                                        <?php $array = array(
                                                                "src" => base_url('uploads/images/'.$student->photo),
                                                                'width' => '35px',
                                                                'height' => '35px',
                                                                'class' => 'img-rounded'

                                                            );
                                                            echo img($array);
                                                        ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_name')?>">
                                                        <?php echo $student->name; ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_roll')?>">
                                                        <?php echo $student->roll; ?>
                                                    </td>
                                                    <td data-title="<?=$this->lang->line('student_email')?>">
                                                        <?php echo $student->email; ?>
                                                    </td>
                                                    <?php if(permissionChecker('student_edit') || permissionChecker('student_delete') || permissionChecker('student_view')) { ?>
                                                    <td data-title="<?=$this->lang->line('action')?>">
                                                        <?php

                                                            echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
                                                            echo btn_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit'));
                                                            echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));
                                                            echo btn_physical_show('student/physical/'.$student->studentID, "Physical");
                                                        ?>
                                                    </td>
                                                    <?php } ?>
                                               </tr>
                                            <?php $i++; }} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div> <!-- nav-tabs-custom -->
                <?php } ?>

            </div> <!-- col-sm-12 -->

        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

<!-- The Hostal Acc Modal -->
<div class="modal" id="hostelAccessories">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?=$this->lang->line('hostel_accessories')?><button type="button" class="close" data-dismiss="modal">&times;</button></h4>
      </div>
      <div class="modal-body">
       <form method="post" action="<?=base_url('student/hostal_accessories')?>" class="hacc_add">
        <input type="hidden" class="form-control" id="classtudentID" name="classtudentID" value="">
        <input type="hidden" class="form-control" id="student_id" name="student_id" value="">
        <span class="col-sm-12 control-label txt-danger" id="err_msg" style="padding: 0px 0px 10px 0px;font-size: 15px;color: #fb0a0a;"></span>
          
          <div class="form-group">
            <label for="leave_reason"><?=$this->lang->line('hostel_reason')?></label>
            <input type="text" class="form-control" id="leave_reason" name="leave_reason">
          </div>
          <div class="form-group">
            <label for="date"><?=$this->lang->line('acc_date')?></label>
            <input type="text" class="form-control" id="leave_date" name="leave_date">
          </div>

           <div class="hostal_segments">
                <div class='form-group' >
                    <label for="note" class=""> <?=$this->lang->line('hostel_segments')?></label>
                    <button class="btn btn-success btn-xs plus_icon" type="button"> <?=$this->lang->line('add_new')?></button>
                    <!-- <i class="fa fa-plus btn btn-success plus_icon"></i> -->
                </div>
            </div>
          <div class="col-sm-12">
              <button type="submit" class="btn btn-default"><?=$this->lang->line('submit_btn')?></button>
          </div>
        </form>
      </div>

      <!-- Modal footer -->
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->

    </div>
  </div>
</div>

<!-- The Hostal Fees Modal -->
<div class="modal" id="hostalFeeModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Assign Hostel With Fees<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?=base_url('student/hostal_fees')?>" class="hfees_add_update">
            <input type="hidden" class="form-control" id="classtudentID2" name="classtudentID" value="">
            <input type="hidden" class="form-control" id="student_id2" name="student_id" value="">
            <input type="hidden" class="form-control" id="pay_id2" name="pay_id" value="0">
            <span class="col-sm-12 control-label txt-danger" id="err_msg2" style="padding: 0px 0px 10px 0px;font-size: 15px;color: #fb0a0a;"></span>
             <div class="form-group">
                <label>Room Type</label>     
                <select name="categoryID" id="categoryID" class="form-control" required>
                    <option value="">Select Room</option>
                    <?php
                        foreach ($categorys as $key => $value) {
                            echo '<option value="'.$value->categoryID.'">'.$value->class_type.'</option>';
                        }
                    ?>  
                </select>
            </div>
            <div class="form-group">
                <label>Payment Method</label>     
                <select name="paymenttype" id="payment-method" class="form-control" required>
                    <option value="">Payment Method</option>
                    <?php
                        foreach ($this->config->item('PAYMENT_METHODS') as $key => $value) {
                            echo '<option value="'.$value.'">'.$value.'</option>';
                        }
                    ?>  
                </select>
            </div>
            <div class="form-group hide-elem" id="bank-elem">
                <label>Bank Name</label>    
                <input name="paymentbank" id="paymentbank" class="form-control" type="text" placeholder="Bank Name" value="">
            </div>
            <div class="form-group hide-elem" id="cheque-elem">
                <label>Cheque No.</label>
                <input name="chequeno" id="chequeno" class="form-control" type="text" placeholder="Cheque No." value="">
            </div>
            <div class="form-group hide-elem" id="amount-elem">
                <label>Amount</label>
                <input name="paymentamount" id="paymentamount" class="form-control" type="text" placeholder="Amount" value="" required>
            </div>
            
            <div class="form-group">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $('#leave_date').datepicker({ startView: 2 });
    $(".select2").select2();

    $('.plus_icon').click(function(){
        addRows('','');
    });
    $(document).on('click','.remove_icon',function(){
        $(this).parents('div').parents('.segment_items').remove();
    });
    $(document).on('click','.hostelAccessoriesIcon',function(){
        var sid = $(this).data('sid');
        $('#classtudentID').val($(this).data('id'));
        $('#student_id').val(sid);
        $.ajax({
            type: 'GET',
            url: "<?=base_url('student/hostal_accessories')?>",
            data: {student_id:sid},
            dataType: "JSON",
            success: function(data) {
                var parseData = JSON.parse(data.student_data.hostal_accessories);
                $('#leave_reason').val(data.student_data.hostal_leave_reason);
                $('#leave_date').val(data.student_data.hostal_leave_date);
                $.each(parseData,function(idx,val){
                    addRows(val.segment,val.quantity);
                })
            }
        });
    });

    function addRows(p,q){
        $('.hostal_segments').append('<div class="form-group segment_items">\
                <label for="note" class="col-sm-2 control-label"></label>\
                <div class="col-sm-12">\
                    <div class="col-sm-6"><input type="text" class="form-control" name="segments[product][]" value="'+p+'" ></div>\
                    <div class="col-sm-5"><input type="number" class="form-control" name="segments[quantity][]" value="'+q+'" ></div>\
                    <div class="col-sm-1"><i class="fa fa-minus btn btn-danger remove_icon"></i></div>\
                </div>\
            </div>'
        );
    }
    
    $('.hacc_add').submit(function(e){
        
    });

    $(document).on('click','.hostalFeeIcon',function(){
        $('#payment-method,#pay_id2,#paymentbank,#chequeno,#paymentamount').val('');
        $('#bank-elem,#cheque-elem,#amount-elem').hide();
        var sid = $(this).data('sid');
        $('#classtudentID2').val($(this).data('id'));
        $('#student_id2').val(sid);
        $.ajax({
            type: 'GET',
            url: "<?=base_url('student/hostal_fees')?>",
            data: {student_id:sid},
            dataType: "JSON",
            success: function(data) {
                var parseData = data.payment_data;
                if(data.count>0){
                    paymentElemShow(parseData.paymenttype);
                    $('#payment-method').val(parseData.paymenttype);
                    $('#pay_id2').val(parseData.paymentID);
                    $('#paymentbank').val(parseData.paymentbank);
                    $('#chequeno').val(parseData.chequeno);
                    $('#paymentamount').val(parseData.paymentamount);
                }
                $('#categoryID').val(data.category_id);
            }
        });
    });
    $('#payment-method').change(function(){
      $('#bank-elem,#cheque-elem,#amount-elem').hide();
      paymentElemShow($(this).val());
    });
    function paymentElemShow(val){
      if(val=='Cash'){
         $('#amount-elem').show();
      } else if(val=='Cheque'){
         $('#amount-elem').show();
         $('#cheque-elem').show();
         $('#bank-elem').show();
      } else if(val=='NetBanking'){
         $('#amount-elem').show();
         $('#bank-elem').show();
      }
    }

    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
             window.location.href = "<?=base_url('student/student_list')?>";
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('student/student_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });


    var status = '';
    var id = 0;
    $('.onoffswitch-small-checkbox').click(function() {
        if($(this).prop('checked')) {
            status = 'chacked';
            id = $(this).parent().attr("id");
        } else {
            status = 'unchacked';
            id = $(this).parent().attr("id");
        }

        if((status != '' || status != null) && (id !='')) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('student/active')?>",
                data: "id=" + id + "&status=" + status,
                dataType: "html",
                success: function(data) {
                    if(data == 'Success') {
                        toastr["success"]("Success")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "500",
                            "hideDuration": "500",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else {
                        toastr["error"]("Error")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "500",
                            "hideDuration": "500",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                }
            });
        }
    });
</script>
