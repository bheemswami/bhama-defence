<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('physical_test')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="#"><?=$this->lang->line('menu_student')?></a></li>
            <li class="active"><?=$this->lang->line('physical_test')?></li>
            <!-- <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('despatch_panel_title')?></li> -->
        </ol>
    </div>
    
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
            
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <input type="hidden" value="<?=$student_id?>" name="studentID">
                    <div class="form-group">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("student_name")?></label>
                        <div class="col-sm-6">
                            <b><?=$student_details[0]->name?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("enq_father_name")?></label>
                        <div class="col-sm-6">
                            <b><?=$student_details[0]->father_name?></b>
                        </div>
                    </div>
                    <!-- <div class="form-group <?=form_error('date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("date")?></label>
                        <div class="col-sm-2">
                            <select id="p_year" class="form-control" name="year">
                                <option value="">-- SELECT --</option>
                                <?php
                                    for ($i = 2019; $i <= date('Y'); $i++)
                                    {
                                        echo "<option value='".$i."'>".$i."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select id="p_month" class="form-control" name="month">
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select id="p_day" class="form-control" name="day">
                            </select>
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('date'); ?></span>
                    </div> -->
                    <div class="form-group <?=form_error('date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date" name="date" value="<?=set_value('date')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('date'); ?></span>
                    </div>


                    <div class="form-group <?=form_error('race_5km') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_5km")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="race_5kmm" name="race_5km" value="<?=set_value('race_5km')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_5km'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('race_1600m') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_1600m")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="race_1600mm" name="race_1600m" value="<?=set_value('race_1600m')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_1600m'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('race_800m') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_800m")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="race_800mm" name="race_800m" value="<?=set_value('race_800m')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_800m'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('race_100m') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_100m")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="race_100mm" name="race_100m" value="<?=set_value('race_100m')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_100m'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('long_jump') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("long_jump")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="long_jumpp" name="long_jump" value="<?=set_value('long_jump')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('long_jump'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('high_jump') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("high_jump")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="high_jumpp" name="high_jump" value="<?=set_value('high_jump')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('high_jump'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('beam') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("beam")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="beamm" name="beam" value="<?=set_value('beam')?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('beam'); ?></span>
                    </div>  
                    <div class="form-group <?=form_error('push_up') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("push_up")?></label>
                        <div class="col-sm-6">
                            <input type="text" style="resize:none;" class="form-control" id="push_upp" name="push_up" value="<?=set_value('push_up')?>">
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('push_up'); ?></span>
                    </div>

                    <div class="form-group <?=form_error('up_down') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("up_down")?></label>
                        <div class="col-sm-6">
                            <input type="text" style="resize:none;" class="form-control" id="up_downn" name="up_down" value="<?=set_value('up_down')?>">
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('up_down'); ?></span>
                    </div>
                    
                    <div class="form-group <?=form_error('zigzag_balance') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("zigzag_balance")?></label>
                        <div class="col-sm-6">
                            <select class="form-control" id="zigzag_balancee" name="zigzag_balance">
                                <option value="">-- SELECT --</option>
                                <option <?php if (set_value('zigzag_balance')=="QUALIFIED"){
                                    echo "selected";
                                }?> value="QUALIFIED">QUALIFIED</option>
                                <option <?php if (set_value('zigzag_balance')=="NOT QUALIFIED"){
                                    echo "selected";
                                }?> value="NOT QUALIFIED">NOT QUALIFIED</option>
                            </select>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('zigzag_balance'); ?></span>
                    </div>

                    <div class="form-group <?=form_error('remarks') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("remarks")?></label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="remarkss" name="remarks"><?=set_value('remarks')?></textarea>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('remarks'); ?></span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("submit")?>" >
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-area-chart"></i> <?=$this->lang->line('physical_test_stats');?></h3>
    </div>
    <div id="stat" class="box-body">
		<style>
			@media print {
				/* body {transform: scale(48%);} */
				@page {size: landscape}
			}
		</style>
		<!-- <button onClick="printDiv()" class="btn btn-danger mr-auto">Print <i class="fa fa-print"></i></button>									 -->
		<div id="race5kmGraph"></div>
		<div id="race16000mGraph"></div>
		<div id="race800mGraph"></div>
		<div id="race100mGraph"></div>
		<div id="longjumpGraph"></div>				
		<div id="highjumpGraph"></div>				
		<div id="beamGraph"></div>				
		<div id="pushupGraph"></div>				
		<div id="updownGraph"></div>		
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('physical_test_list')?> <?=$this->lang->line('letter_list')?></h3>
    </div>
    <div class="box-body">

        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <!-- <th><?=$this->lang->line('student_name')?></th>
                            <th><?=$this->lang->line("enq_father_name")?></th> -->

                            <th><?=$this->lang->line('date')?></th>
                            <th><?=$this->lang->line('race_5km')?></th>
                            <th><?=$this->lang->line('race_1600m')?></th>
                            <th><?=$this->lang->line('race_800m')?></th>
                            <th><?=$this->lang->line('race_100m')?></th>
                            <th><?=$this->lang->line('long_jump')?></th>
                            <th><?=$this->lang->line('high_jump')?></th>
                            <th><?=$this->lang->line('beam')?></th>
                            <th><?=$this->lang->line('push_up')?></th>
                            <th><?=$this->lang->line('up_down')?></th>
                            <th><?=$this->lang->line('zigzag_balance')?></th>
                            <th><?=$this->lang->line('remarks')?></th>
                            <th><?=$this->lang->line('action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($physical_details)) {$i = 1; foreach($physical_details as $physical_detail) { ?>
                            <tr>
                                <td><?=$i++?></td>
                                <!-- <td><?=$physical_detail->sname?></td>
                                <td><?=$physical_detail->father_name?></td> -->

                                <td><?=$physical_detail->date?></td>
                                <td><?=$physical_detail->race_5km?></td>
                                <td><?=$physical_detail->race_1600m?></td>
                                <td><?=$physical_detail->race_800m?></td>
                                <td><?=$physical_detail->race_100m?></td>
                                <td><?=$physical_detail->long_jump?></td>
                                <td><?=$physical_detail->high_jump?></td>
                                <td><?=$physical_detail->beam?></td>
                                <td><?=$physical_detail->push_up?></td>
                                <td><?=$physical_detail->up_down?></td>
                                <td><?=$physical_detail->zigzag_balance?></td>
                                <td><?=$physical_detail->remarks?></td>

                                <td>
                                    <a href="<?=base_url('/student/physical_print/').$physical_detail->id?>" class="btn btn-danger btn-xs mrg" target="_blank"><i class="fa fa-print"></i></a>
                                    <?php 
                                        echo btn_edit_show('student/physical_update/'.$physical_detail->id, $this->lang->line('edit'));
                                        echo btn_delete_show('student/delete_physical/'.$physical_detail->id, $this->lang->line('delete'));
                                    ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    $this->load->view("student/physical_graph_js");
?>
<script type="text/javascript">
    $("#p_year").change(function() {
        $("#p_month").empty();
        $("#p_day").empty();
        if ($(this).val() != "") {
			
            var options = `<option value="">-- SELECT --</option><option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">Jun</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>`;
            $("#p_month").append(options);
        } else {
            $("#p_month").empty();
            $("#p_day").empty();
        }

    });
    $("#p_month").change(function () {
        $("#p_day").empty();
        if ($(this).val() != "") {
			$("#p_day").append('<option value="">-- SELECT --</option>');
            if ($(this).val() == 02) {
                var year = $("#p_year").val();
                var l = year % 4 == 0 ? 29 : 28;
                $("#p_day").append('<option value="10">10</option>');
                $("#p_day").append('<option value="20">20</option>');
                $("#p_day").append(`<option value="${l}">${l}</option>`);
            } else {
                $("#p_day").append('<option value="10">10</option>');
                $("#p_day").append('<option value="20">20</option>');
                $("#p_day").append(`<option value="30">30</option>`);
			}
			$("#p_day").change();
        } else {
            $("#p_day").empty();
        }
	});
	const monthNames = [
		"January", 
		"February", 
		"March", 
		"April", 
		"May", 
		"June",
		"July", 
		"August", 
		"September", 
		"October", 
		"November", 
		"December"
	];
	$("#p_day").change(function () {
		var day = $("#p_year").val() + "-" + $("#p_month").val() + "-" + $("#p_day").val();
		$.ajax({
				url: '<?=base_url("student/get_date_details/")?>',
				data: {studentID: '<?=$student_id?>', date: day},
				type: 'POST', 
				success: function (data) {
					if (JSON.parse(data).length) {
						var alert_date = day.split("-")[2] + "-" + monthNames[day.split("-")[1].split("")[1] - 1] + "-" + day.split("-")[0];
						alert ("Data for date " + alert_date + " already exists");
						$("#p_day").val("");
					}
			} 
		});
	});

	function printDiv() {
        var divElements = document.getElementById("stat").innerHTML;
        var oldPage = document.body.innerHTML;
		document.body.innerHTML = `<html><head><title></title></head><body>` + divElements + `</body>`;
        window.print();
        document.body.innerHTML = oldPage;
	}
	$('#date').datepicker();
</script>
