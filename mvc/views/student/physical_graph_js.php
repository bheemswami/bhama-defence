<script type="application/javascript">
$(function() {
    LoadPhysicalGraph();
    function LoadPhysicalGraph()
    {
        race_1600m_graph();  
        long_jump_graph();    
        high_jump_graph();  
        beam_graph();
        push_up_graph(); 
        up_down_graph(); 
        race_5km_graph();
        race_800m_graph();
        race_100m_graph();
    }

});

    function race_1600m_graph () {
        $('#race16000mGraph').highcharts({
            chart: { type: 'column' },
            // chart: { type: 'areaspline' },
            title: { text: '<?=$this->lang->line("race_1600m")?>' },
            // subtitle: { text: '<?=$this->lang->line("race_1600m")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                visible: false,
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                // pointFormat: '{series.name}: <b>{point.y}</b>'
                formatter:function(){
                  return this.series.name +': <b>'+(100 - this.y)+' Min</b>';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("race_1600m")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($race_1600m_graph_data[date("Y-m-d", strtotime($date))]) && $race_1600m_graph_data[date("Y-m-d", strtotime($date))] > 0) {
                                echo "{y:".(100 - $race_1600m_graph_data[date("Y-m-d", strtotime($date))])."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(87,200,241)'
            }]
        });
    }

    function race_5km_graph () {
        $('#race5kmGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("race_5km")?>' },
            // subtitle: { text: '<?=$this->lang->line("race_5km")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                visible: false,
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                // pointFormat: `{series.name}: <b>{point.y}</b>`
                formatter:function(){
                  return this.series.name +': <b>'+(100 - this.y)+' Min</b>';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("race_5km")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($race_5km_graph_data[date("Y-m-d", strtotime($date))]) && $race_5km_graph_data[date("Y-m-d", strtotime($date))] > 0) {
                                echo "{y:".(100 - $race_5km_graph_data[date("Y-m-d", strtotime($date))])."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(244, 203, 66)'
            }]
        });
    }

    function race_800m_graph () {
        $('#race800mGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("race_800m")?>' },
            // subtitle: { text: '<?=$this->lang->line("race_800m")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                visible: false,
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                // pointFormat: '{series.name}: <b>{point.y}</b>'
                formatter:function(){
                  return this.series.name +': <b>'+(100 - this.y)+' Min</b>';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("race_800m")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($race_800m_graph_data[date("Y-m-d", strtotime($date))]) && $race_800m_graph_data[date("Y-m-d", strtotime($date))] > 0) {
                                echo "{y:".(100 - $race_800m_graph_data[date("Y-m-d", strtotime($date))])."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(211, 185, 171)'
            }]
        });
    }

    function race_100m_graph () {
        $('#race100mGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("race_100m")?>' },
            // subtitle: { text: '<?=$this->lang->line("race_100m")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                visible: false,
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                // pointFormat: '{series.name}: <b>{point.y}</b>'
                formatter:function(){
                  return this.series.name +': <b>'+(100 - this.y)+' Min</b>';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("race_100m")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($race_100m_graph_data[date("Y-m-d", strtotime($date))]) && $race_100m_graph_data[date("Y-m-d", strtotime($date))] > 0) {
                                echo "{y:".(100 - $race_100m_graph_data[date("Y-m-d", strtotime($date))])."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(229, 163, 237)'
            }]
        });
    }

    function long_jump_graph () {
        $('#longjumpGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("long_jump")?>' },
            // subtitle: { text: '<?=$this->lang->line("dashboard_earning_summary_subtitle")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("long_jump")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($long_jump_graph_data[date("Y-m-d", strtotime($date))])) {
                                echo "{y:".$long_jump_graph_data[date("Y-m-d", strtotime($date))]."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(244, 66, 223)'
            }]
        });
    }

    function high_jump_graph () {
        $('#highjumpGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("high_jump")?>' },
            // subtitle: { text: '<?=$this->lang->line("dashboard_earning_summary_subtitle")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("high_jump")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($high_jump_graph_data[date("Y-m-d", strtotime($date))])) {
                                echo "{y:".$high_jump_graph_data[date("Y-m-d", strtotime($date))]."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(39, 135, 72)'
            }]
        });
    }

    function beam_graph () {
        $('#beamGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("beam")?>' },
            // subtitle: { text: '<?=$this->lang->line("dashboard_earning_summary_subtitle")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("beam")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($beam_graph_data[date("Y-m-d", strtotime($date))])) {
                                echo "{y:".$beam_graph_data[date("Y-m-d", strtotime($date))]."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(244, 166, 65)'
            }]
        });
    }

    function push_up_graph () {
        $('#pushupGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("push_up")?>' },
            // subtitle: { text: '<?=$this->lang->line("dashboard_earning_summary_subtitle")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("push_up")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($push_up_graph_data[date("Y-m-d", strtotime($date))])) {
                                echo "{y:".$push_up_graph_data[date("Y-m-d", strtotime($date))]."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(209, 66, 23)'
            }]
        });
    }

    function up_down_graph () {
        $('#updownGraph').highcharts({
            chart: { type: 'column' },
            title: { text: '<?=$this->lang->line("up_down")?>' },
            // subtitle: { text: '<?=$this->lang->line("dashboard_earning_summary_subtitle")?>' },
            xAxis: {
                categories: [
                    <?php
                        echo implode(',', pluck_bind($dates, NULL, "'", "'"));
                    ?>
                ],
                title: {
                    text: '<?=$this->lang->line("physical_day")?>',
                    align: 'low'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?=$this->lang->line("physical_unit")?>',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                LoadDayWiseExpenseOrIncome(e.type, e.monthID, e.monthName, e.dayWiseData);
                            }
                        }
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: -10,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?=$this->lang->line("up_down")?>',
                data: [
                    <?php
                    
                        foreach ($dates as $key => $date) {
                            if(isset($up_down_graph_data[date("Y-m-d", strtotime($date))])) {
                                echo "{y:".$up_down_graph_data[date("Y-m-d", strtotime($date))]."},";
                            } else {
                                 echo "{y:0},";
                            }
                        }
                    ?>
                ],
                color: 'rgb(29, 135, 124)'
            }]
        });
    }


</script>
