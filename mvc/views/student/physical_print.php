<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE> 
    <style>
        table tbody tr td{
            padding: 5px 10px !important;
        }
        font{font-size:14px !important;}
        body{font-family: arial}
    </style>
</HEAD>
<BODY LANG="en-US">
    <!-- <div style=" width: 60%; text-align: right;">
        <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('print_letter')"><span class="fa fa-print"></span> <?php //$this->lang->line('print')?> </button>
    </div> -->
    
<div style="margin: auto;width: 640px;height: 100%;background: #fff;padding: 35px;box-shadow: 0px 0px 4px #333;" id="print_letter">
<P STYLE="margin-bottom: 0.14in">
    <SPAN >
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=5 STYLE="font-size: 20pt"><B><?php if($siteinfos) { echo getSiteName(); } ?></B></FONT></P>
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2><?php if($siteinfos) { echo $siteinfos->address; } ?></FONT></P>
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=5 STYLE="font-size: 20pt"><B>Physical Details</B></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>SR. NO:</B></FONT><FONT SIZE=2><B>
    <?=@$physical_details->id?></B></FONT></P>

    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Student Name:</B></FONT><FONT SIZE=2><B>
    <?=@$student_details[0]->name?></B></FONT></P>

    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Father's Name:</B></FONT><FONT SIZE=2><B>
    <?=@$student_details[0]->father_name?></B></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <CENTER>
        <TABLE WIDTH=626 CELLPADDING=7 CELLSPACING=0>
            <COL WIDTH=142>
            <COL WIDTH=143>
            <COL WIDTH=143>
            <COL WIDTH=142>
            
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Race 5KM</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->race_5km?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Race 1600M</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->race_1600m?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Race 800M</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->race_800m?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Race 100M</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->race_100m?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Long Jump</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->long_jump?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>High Jump</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->high_jump?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Beam</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->beam?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Push Up</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->up_down?></FONT></P>
                </TD>
            </TR>
            <TR VALIGN=TOP>
                <TD WIDTH=142 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P><FONT SIZE=2>Zigzag Balance</FONT></P>
                </TD>
                <TD WIDTH=143 STYLE="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                    <P ALIGN=RIGHT><FONT SIZE=2><?=@$physical_details->zigzag_balance?></FONT></P>
                </TD>
            </TR>
            
            
            
        </TABLE>
    </CENTER>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2>Remarks: <?=@$physical_details->remarks?></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2>Signature: __________________________
</SPAN>
</P>
</div>
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        var divElements = document.getElementById(divID).innerHTML;
        var oldPage = document.body.innerHTML;
        document.body.innerHTML = "<html><head><title></title></head><body>" + divElements + "</body>";
        window.print();
        document.body.innerHTML = oldPage;
    }
   printDiv('print_letter');
</script>
</BODY>
</HTML>