<?php
    $date = explode("-", $physical_details->date);
    $year = $date[0];
    $month = $date[1];
    $day = $date[2];
?>
<div class="box">
<div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?=$this->lang->line('physical_test_update')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="#"><?=$this->lang->line('menu_student')?></a></li>
            <li><a href="#"><?=$this->lang->line('physical_test')?></a></li>
            <li class="active"><?=$this->lang->line('update')?></li>
        </ol>
    </div>
   
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <input type="hidden" value="<?=$physical_details->id?>" name="id">
                    <input type="hidden" value="<?=$physical_details->studentID?>" name="studentID">
                    <div class="form-group">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("student_name")?></label>
                        <div class="col-sm-6">
                            <b><?=$student_details[0]->name?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("enq_father_name")?></label>
                        <div class="col-sm-6">
                            <b><?=$student_details[0]->father_name?></b>
                        </div>
                    </div>

                    <!-- <div class="form-group <?=form_error('date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("date")?></label>
                        <div class="col-sm-2">
                            <select id="p_year" class="form-control" name="year">
                            <option value="">-- SELECT --</option>
                                <?php
                                    for ($i = 2019; $i <= date('Y'); $i++)
                                    { ?>
                                        <option <?php if ($year == $i) {echo "selected";} ?> value="<?=$i?>"><?=$i?></option>
                                    <?php }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select id="p_month" class="form-control" name="month">
                            </select>
                        </div>
                        
                        <div class="col-sm-2">
                            <select class="form-control" name="day" id="p_day">
                            </select>
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('date'); ?></span>
                    </div> -->
                    <div class="form-group <?=form_error('date') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("date")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date" name="date" 
                            value="<?=set_value('date', @date("d-m-Y",strtotime($physical_details->date)))?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('date'); ?></span>
                    </div>
                    <div class="form-group <?=form_error('race_5km') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_5km")?></label>
                        <div class="col-sm-6">
                            <input 
                                type="text" class="form-control" id="race_5km" name="race_5km"
                                value="<?=set_value('race_5km', @$physical_details->race_5km)?>" 
                            >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_5km'); ?></span>
                    </div>

                    <div class="form-group <?=form_error('race_1600m') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_1600m")?></label>
                        <div class="col-sm-6">
                            <input 
                                type="text" class="form-control" id="race_1600m" name="race_1600m"
                                value="<?=set_value('race_1600m', @$physical_details->race_1600m)?>" 
                            >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_1600m'); ?></span>
                    </div>

                    <div class="form-group <?=form_error('race_800m') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_800m")?></label>
                        <div class="col-sm-6">
                            <input 
                                type="text" class="form-control" id="race_800m" name="race_800m"
                                value="<?=set_value('race_800m', @$physical_details->race_800m)?>" 
                            >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_800m'); ?></span>
                    </div>

                    <div class="form-group <?=form_error('race_100m') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("race_100m")?></label>
                        <div class="col-sm-6">
                            <input 
                                type="text" class="form-control" id="race_100m" name="race_100m"
                                value="<?=set_value('race_100m', @$physical_details->race_100m)?>" 
                            >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('race_100m'); ?></span>
                    </div>


                    <div class="form-group <?=form_error('long_jump') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("long_jump")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="long_jump" name="long_jump" value="<?=set_value('long_jump', @$physical_details->long_jump)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('long_jump'); ?></span>
                    </div>


                    <div class="form-group <?=form_error('high_jump') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("high_jump")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="high_jump" name="high_jump" value="<?=set_value('high_jump', @$physical_details->high_jump)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('high_jump'); ?></span>
                    </div>


                    <div class="form-group <?=form_error('beam') ? 'has-error' : '' ?>">
                        <label for="namea" class="col-sm-2 control-label"> <?=$this->lang->line("beam")?></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="beam" name="beam" value="<?=set_value('beam', @$physical_details->beam)?>" >
                        </div>
                        <span class="col-sm-4 control-label"><?php echo form_error('beam'); ?></span>
                    </div> 


                    <div class="form-group <?=form_error('push_up') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("push_up")?></label>
                        <div class="col-sm-6">
                            <input type="text" style="resize:none;" class="form-control" id="push_up" name="push_up" value="<?=set_value('push_up', @$physical_details->push_up)?>">
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('push_up'); ?></span>
                    </div>


                    <div class="form-group <?=form_error('up_down') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("up_down")?></label>
                        <div class="col-sm-6">
                            <input type="text" style="resize:none;" class="form-control" id="up_down" name="up_down" value="<?=set_value('up_down', @$physical_details->up_down)?>">
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('up_down'); ?></span>
                    </div>  


                    <div class="form-group <?=form_error('zigzag_balance') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("zigzag_balance")?></label>
                        <div class="col-sm-6">
                            <select class="form-control" id="zigzag_balance" name="zigzag_balance">
                                <option value="">-- SELECT --</option>
                                <option <?php if (set_value('zigzag_balance', @$physical_details->zigzag_balance)=="QUALIFIED"){
                                    echo "selected";
                                }?> 
                                value="QUALIFIED">QUALIFIED</option>
                                <option <?php if (set_value('zigzag_balance', @$physical_details->zigzag_balance)=="NOT QUALIFIED"){
                                    echo "selected";
                                }?> value="NOT QUALIFIED">NOT QUALIFIED</option>
                            </select>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('zigzag_balance'); ?></span>
                    </div>


                    <div class="form-group <?=form_error('remarks') ? 'has-error' : '' ?>">
                        <label for="note" class="col-sm-2 control-label"> <?=$this->lang->line("remarks")?></label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="remarks" name="remarks"><?=set_value('remarks', @$physical_details->remarks)?></textarea>
                        </div>
                        <span class="col-sm-4 control-label"> <?php echo form_error('remarks'); ?></span>
                    </div>

                    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update")?>" >
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $("#p_year").change();
        $("#p_month > option").each(function () {
            if ($(this).val() == <?=$month?>) {
                $(this).attr("selected", "selected");
            }
        });
        $("#p_month").change();
        $("#p_day > option").each(function () {
            if ($(this).val() == <?=$day?>) {
                $(this).attr("selected", "selected");
            }
        });
    });
    $("#p_year").change(function() {
        $("#p_months").empty();
        $("#p_day").empty();
        if ($(this).val() != "") {
            var options = `<option value="">-- SELECT --</option><option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">Jun</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>`;
            $("#p_month").append(options);
        } else {
            $("#p_month").empty();
            $("#p_day").empty();
        }

    });
    $("#p_month").change(function () {
        $("#p_day").empty();
        if ($(this).val() != "") {
            if ($(this).val() == 02) {
                var year = $("#p_year").val();
                var l = year % 4 == 0 ? 29 : 28;
                $("#p_day").append('<option value="10">10</option>');
                $("#p_day").append('<option value="20">20</option>');
                $("#p_day").append(`<option value="${l}">${l}</option>`);
            } else {
                $("#p_day").append('<option value="10">10</option>');
                $("#p_day").append('<option value="20">20</option>');
                $("#p_day").append(`<option value="30">30</option>`);
            }
        } else {
            $("#p_day").empty();
        }
    });
	$('#date').datepicker();
</script>