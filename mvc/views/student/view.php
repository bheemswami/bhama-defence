   <div class="well">
        <div class="row">
            <div class="col-sm-6">
                <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                <?php
                 //echo btn_add_pdf('student/print_preview/'.$student->studentID."/".$set, $this->lang->line('pdf_preview'))
                ?>
                <!-- <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#idCard"><span class="fa fa-floppy-o"></span> <?=$this->lang->line('idcard')?> </button> -->

                <?php //if(permissionChecker('student_edit')) { echo btn_sm_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit')); } 
                ?>
                <?php
                $userTypeId = $this->session->userdata("usertypeID");
                    if( $userTypeId==3){
                        $accUrl = base_url('student/student_account/'.$student->studentID."/".$student->classesID);
                        echo '<a href="'.$accUrl.'" class="btn btn-warning btn-cs btn-sm-cs" title="'.$this->lang->line('student_account').'">Account</a>';
                    }
                ?>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("student/index")?>"><?=$this->lang->line('menu_student')?></a></li>
                    <li class="active"><?=$this->lang->line('view')?></li>
                </ol>
            </div>
        </div>
    </div> 

    <div id="printablediv">
        <div class="col-sm-12 text-center"><h3><?=$siteinfos->sname?></h3></div>
        <section class="panel">
            <div class="profile-view-head">
               <!--  <a href="#">
                    <?=img(base_url('uploads/images/'.$student->photo))?>
                </a>  -->

                <h1><?=$student->name?></h1>
                <p><?=$this->lang->line("student_classes")." ".$class->classes?></p> 
            </div>
<div class="panel-group" id="personal_information-accordian">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#personal_information-collapse" class="accordion-toggle"><?=$this->lang->line("personal_information")?></a>
        </h4>
      </div>
      <div id="personal_information-collapse" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="panel-body profile-view-dis">
                <h1><?=$this->lang->line("personal_information")?></h1>
                <div class="row">
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_registerNO")?> </span>: <?=$student->registerNO?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_roll")?> </span>: <?=$student->roll?></p>
                    </div>
                    
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_dob")?> </span>: 
                        <?php if($student->dob) { echo date("d M Y", strtotime($student->dob)); } ?></p>
                    </div>
                    <?php /* <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_sex")?> </span>: 
                        <?=$student->sex?></p>
                    </div> */?>
                    <?php /*<div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_bloodgroup")?> </span>: <?php if(isset($allbloodgroup[$student->bloodgroup])) { echo $student->bloodgroup; } ?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_religion")?> </span>: <?=$student->religion?></p>
                    </div>*/?>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_email")?> </span>: <?=$student->email?></p>
                    </div>
                    <?php /*<div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_phone")?> </span>: <?=$student->phone?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_address")?> </span>: <?=$student->address?></p>
                    </div>*/?>
                    <?php /*<div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_state")?> </span>: <?=$student->state?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_country")?> </span>: 
                        <?php if(isset($allcountry[$student->country])) { echo $allcountry[$student->country]; } ?></p>
                    </div>
                    */?>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_username")?> </span>: <?=$student->username?></p>
                    </div>

                    
                </div>

                <!-- <h1><?=$this->lang->line("parents_information")?></h1>
                <?php   if(isset($parent)) { ?> -->

                <!-- <div class="row">
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_guargian_name")?> </span>: <?=valueNullOrNot($parent->name)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_father_name")?> </span>: <?=valueNullOrNot($parent->father_name)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_mother_name")?> </span>: <?=valueNullOrNot($parent->mother_name)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_father_profession")?> </span>: <?=valueNullOrNot($parent->father_profession)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_mother_profession")?> </span>: <?=valueNullOrNot($parent->mother_profession)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_email")?> </span>: <?=$parent->email?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_phone")?> </span>: <?=valueNullOrNot($parent->phone)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_address")?> </span>: <?=valueNullOrNot($parent->address)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_username")?> </span>: <?=$parent->username?></p>
                    </div>
                </div>
                <?php
                    } else {
                        echo "<div class='col-sm-12'><div class='col-sm-12 alert alert-warning'><span class='fa fa-exclamation-triangle'></span> " .$this->lang->line("parent_error"). "</div></div>";
                    }
                ?> -->

            </div>
           <!--  <div class="row">
        <div class="col-xs-12 text-right">
            
            <div class="table-card">
                <?php if($student->installments>0){
                    echo '<button type="button" class="btn btn-success text-right" data-toggle="modal" data-target="#myModal">Add Installments</button>';
                } ?>
                   <table class="mrgn-top-1 table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">SN NO.</th>
                      <th scope="col">Amount</th>
                      <th scope="col">Date</th>
                      <th scope="col">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $totalInstalment =0;
                    if(count($payment_inst)){
                    foreach ($payment_inst as $key => $value) {
                        $totalInstalment =$totalInstalment+($value->paymentamount)
                    ?>
                    <tr>
                      <th scope="row"><?=$key+1?></th>
                      <td><?=$value->paymentamount?></td>
                      <td><?=$value->paymentdate?></td>
                      <td><?=$value->purpose?></td>
                    </tr>
                    <?php }}?>
                    <tr>
                        <td colspan="3"></td>
                        <th scope="row" colspan="1">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-right"><strong>Course Fee :</strong></td>
                                    <td class="text-right"><?=sprintf('%0.2f',$student->course_fee)?></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Installments :</strong></td>
                                    <td class="text-right"><?=sprintf('%0.2f',$totalInstalment)?></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Remaining :</strong></td>
                                    <td class="text-right"><?=sprintf('%0.2f',($student->course_fee)-$totalInstalment)?><strong></td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </div> -->

        </div>
      </div>
    </div>
  </div>
  <div class="panel-group" id="parents-information-accordian">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#parents_information" class="accordion-toggle"><?=$this->lang->line("parents_information")?></a>
        </h4>
      </div>
      <div id="parents_information" class="panel-collapse collapse">
        <div class="panel-body">
            
                <?php   if(isset($parent)) { ?>

                <div class="row">
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_guargian_name")?> </span>: <?=valueNullOrNot($parent->name)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_father_name")?> </span>: <?=valueNullOrNot($parent->father_name)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_mother_name")?> </span>: <?=valueNullOrNot($parent->mother_name)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_father_profession")?> </span>: <?=valueNullOrNot($parent->father_profession)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_mother_profession")?> </span>: <?=valueNullOrNot($parent->mother_profession)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_email")?> </span>: <?=$parent->email?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_phone")?> </span>:
                            <?php
                                if($parent->phone!=''){
                                    $expPhone = explode(',', $parent->phone);
                                    foreach($expPhone as $p){
                                         echo $p.' <icon style="color: #FF9800;font-weight: bold;">|</icon> ';
                                    }
                                } else {
                                    echo '<span>NA</span>';
                                }
                            ?>
                            <!-- <span><?=valueNullOrNot($parent->phone)?></span></p> -->
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_address")?> </span>: <?=valueNullOrNot($parent->address)?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("parent_username")?> </span>: <?=$parent->username?></p>
                    </div>
                </div>
                <?php
                    } else {
                        echo "<div class='col-sm-12'><div class='col-sm-12 alert alert-warning'><span class='fa fa-exclamation-triangle'></span> " .$this->lang->line("parent_error"). "</div></div>";
                    }
                ?>
        </div>
      </div>
    </div>
  </div>
           
  <div class="panel-group" id="edu_details-accordian">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#edu_details" class="accordion-toggle"><?=$this->lang->line("edu_information")?></a>
                </h4>
            </div>
            <div id="edu_details" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 text-right">            
                            <div class="table-card">                               
                                <table class="mrgn-top-1 table table-bordered">
                                  <thead>
                                    <tr>
                                        <th><?=$this->lang->line("exam_name")?></th>
                                        <th><?=$this->lang->line("year")?></th>
                                        <th><?=$this->lang->line("subject")?></th>
                                        <th><?=$this->lang->line("obtain_marks")?></th>
                                        <th><?=$this->lang->line("max_marks")?></th>
                                        <th><?=$this->lang->line("percentage")?></th>
                                        <th><?=$this->lang->line("division")?></th>
                                        <th><?=$this->lang->line("board")?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if(count($edu_details)>0) { foreach($edu_details as $val){ ?>
                                        <tr>
                                            <td><?=ucfirst($val->exam_name)?></td>
                                            <td><?=($val->year!='') ? $val->year : 'NA'?></td>
                                            <td><?=($val->subjects!='') ? $val->subjects : 'NA'?></td>
                                            <td><?=($val->obtain_marks!='') ? $val->obtain_marks : 'NA'?></td>
                                            <td><?=($val->total_marks!='') ? $val->total_marks : 'NA'?></td>
                                            <td><?=($val->percentage!='') ? $val->percentage : 'NA'?></td>
                                            <td><?=($val->division!='') ? $val->division : 'NA'?></td>
                                            <td><?=($val->borad!='') ? $val->borad : 'NA'?></td>
                                        </tr>
                                    <?php } }?>
                                   
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>










    <div class="panel-group" id="personal_information-accordian">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#physical_information-collapse" class="accordion-toggle collapsed">Physical Benchmark</a>
        </h4>
      </div>
      <div id="physical_information-collapse" class="panel-collapse collapse" style="height: 0px;">
        <div class="panel-body">
            <div class="panel-body profile-view-dis">
                <h1>Physical Benchmark</h1>
                <div class="row">
                    <div class="profile-view-tab">
                        <p><span>Height </span>: <?=$physical_benchmark->height ? $physical_benchmark->height : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Chest </span>: <?=$physical_benchmark->chest ? $physical_benchmark->chest : "N/a"?></p>
                    </div>
                    
                    <div class="profile-view-tab">
                        <p><span>Weight </span>: 
                        <?=$physical_benchmark->weight ? $physical_benchmark->weight : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Knees </span>: <?=$physical_benchmark->knees ? $physical_benchmark->knees : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Left Eye </span>: <?=$physical_benchmark->left_eye ? $physical_benchmark->left_eye : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Right Eye </span>: <?=$physical_benchmark->right_eye ? $physical_benchmark->right_eye : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Left Ear </span>: <?=$physical_benchmark->left_ear ? $physical_benchmark->left_ear : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Right Ear </span>: <?=$physical_benchmark->right_ear ? $physical_benchmark->right_ear : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Foot </span>: <?=$physical_benchmark->foot ? $physical_benchmark->foot : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Teeth </span>: <?=$physical_benchmark->teeth ? $physical_benchmark->teeth : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>Nose </span>: <?=$physical_benchmark->nose ? $physical_benchmark->nose : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>High BP </span>: <?=$physical_benchmark->high_bp ? $physical_benchmark->high_bp : "N/a"?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span>PR </span>: <?=$physical_benchmark->pr ? $physical_benchmark->pr : "N/a"?></p>
                    </div>
                    
                </div>

            </div>

        </div>
      </div>
    </div>
  </div>















    <div class="panel-group" id="installments-accordian">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#installments" class="accordion-toggle"><?=$this->lang->line("installment")?></a>
                </h4>
            </div>
            <div id="installments" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 text-right">            
                            <div class="table-card">
                                <?php if($student->installments>0){
                                    echo '<button type="button" class="btn btn-success text-right" data-toggle="modal" data-target="#myModal">Add Installments</button>';
                                } ?>
                                <table class="mrgn-top-1 table table-bordered">
                                  <thead>
                                    <tr>
                                      <th scope="col"><?=$this->lang->line('SN_No')?></th>
                                      <th scope="col"><?=$this->lang->line('amount')?></th>
                                      <th scope="col"><?=$this->lang->line('date')?></th>
                                      <th scope="col"><?=$this->lang->line('description')?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php 
                                    $totalInstalment =0;
                                    if(count($payment_inst)){
                                    foreach ($payment_inst as $key => $value) {
                                        $totalInstalment =$totalInstalment+($value->paymentamount)
                                    ?>
                                    <tr>
                                      <th scope="row"><?=$key+1?></th>
                                      <td><?=$value->paymentamount?></td>
                                      <td><?=$value->paymentdate?></td>
                                      <td><?=$value->purpose?></td>
                                    </tr>
                                    <?php }}?>
                                    <tr>
                                        <td colspan="3"></td>
                                        <th scope="row" colspan="1">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td class="text-right"><strong><?=$this->lang->line('course_fee')?></strong></td>
                                                    <td class="text-right"><?=sprintf('%0.2f',$student->course_fee)?></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right"><strong><?=$this->lang->line('total_installment')?></strong></td>
                                                    <td class="text-right"><?=sprintf('%0.2f',$totalInstalment)?></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right"><strong><?=$this->lang->line('remaining_fee')?></strong></td>
                                                    <td class="text-right"><?=sprintf('%0.2f',($student->course_fee)-$totalInstalment)?><strong></td>
                                                </tr>
                                            </table>
                                        </th>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </section>
    </div>

    <!-- modal for add info -->

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Installments<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
        
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <form method="post" action="<?=base_url('student/add_installment')?>">
        <input type="hidden" class="form-control" id="student_id" name="student_id" value="<?=$student->studentID?>">
        <input type="hidden" class="form-control" id="classesID" name="course_id" value="<?=$student->classesID?>">
        <span class="col-sm-12 control-label txt-danger" id="amount_err" style="padding: 0px 0px 10px 0px;font-size: 15px;color: #fb0a0a;"></span>
          <div class="form-group">
            <label for="amount">Installment</label>
            <select class="form-control" id="installment" name="installment">
                <option value="">Select</option>
                <?php
                    foreach ($this->config->item('INSTALLMENT_LIST') as $key => $value) {
                        echo '<option value="'.$value.'">'.$value.'</option>';
                    }
                ?>
            </select>
          </div>
          <div class="form-group">
            <label for="amount">Enter Amount</label>
            <input type="text" class="form-control" id="installment_amount" name="amount">
          </div>
          <div class="form-group">
            <label for="date">Due Date (Next Instalment)</label>
            <input type="text" class="form-control" id="due_date" name="due_date">
          </div>
          
          <button type="submit" class="btn btn-default" id="add_inst_btn" disabled>Submit</button>
        </form>
      </div>

      <!-- Modal footer -->
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->

    </div>
  </div>
</div>
    <!-- modal for add info -->
        <div class="panel-group" id="comment-section-accordian">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#comment-section-collapse" class="accordion-toggle"><?=$this->lang->line("comment_section")?></a>
        </h4>
      </div>
      <div id="comment-section-collapse" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="row">
        <div class="col-xs-12">
            <div class="comment-section">
                <form method="post" action="<?=base_url('enquiry/add_comment')?>" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="student_id" name="student_id" value="<?=$student->studentID?>">
                    <input type="hidden" class="form-control" id="classesID" name="course_id" value="<?=$student->classesID?>">
                    <div class="row pd-btm-15">
                        <div class="col-xs-12">
                            <textarea rows="4" name="comment" placeholder="Type Your Comment"></textarea>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-xs-6 ">
                        <label for="photo" class="col-sm-2 control-label">
                            <?=$this->lang->line("comment_file")?>
                        </label>
                        <div class="col-sm-10">
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="fa fa-remove"></span>
                                        <?=$this->lang->line('file_clear')?>
                                    </button>
                                    <div class="btn btn-success image-preview-input">
                                        <span class="fa fa-repeat"></span>
                                        <span class="image-preview-input-title">
                                        <?=$this->lang->line('file_browse')?></span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="photo"/>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>

                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-success "><?=$this->lang->line("save_comment")?></button>
                        </div>
                    </div>
                </form>
                    <div class="comment-list-parrent">
                        <?php 
                        if(count($comments)>0){
                            foreach ($comments as $key => $val) {
                                $name = getUserNameById($val->comment_by,$val->user_id);
                        ?>
                            <div class="comment-list">
                                <p><?=$val->comment?></p>
                                <p>
                                    <?php 
                                        if($val->files!='') 
                                            echo '<a href="'.base_url('uploads/comment_docs/'.$val->files).'" class="btn btn-danger btn-xs" title="Download File" download>Attachment</a>';
                                    ?>
                                </p>
                                <ul class="nav navbar-nav">
                                    <li><i class="fa fa-clock-o"></i> <?=date('d-m-Y',strtotime($val->created_at))?></li>
                                    <li><i class="fa fa-pencil"></i> <?=$name?></li>
                                </ul>                        
                            </div>
                        <?php
                               
                            }
                        }
                        ?>
                        
                    </div>
                
            </div>
        </div>
    </div>
        </div>
      </div>
    </div>
  </div>


<!-- <div class="panel-group" id="empty-accordian">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapse1" class="accordion-toggle">Empty Accordian</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">Panel Body</div>
      </div>
    </div>
  </div> -->
    <!-- Modal content start here -->
    <div class="modal fade" id="idCard">
      <div class="modal-dialog">
        <div class="modal-content">
            <div id="idCardPrint">
              <div class="modal-header">
                <?=$this->lang->line('idcard')?>
              </div>
              <div class="modal-body" >
                <table>
                    <tr>
                        <td>
                            <h4 style="margin:0;">
                            <?php
                                if($siteinfos->photo) {
                                    $array = array(
                                        "src" => base_url('uploads/images/'.$siteinfos->photo),
                                        'width' => '25px',
                                        'height' => '25px',
                                        "style" => "margin-bottom:10px;"
                                    );
                                    echo img($array);
                                }

                            ?>

                            </h4>
                        </td>
                        <td style="padding-left:5px;">
                            <h4><?=$siteinfos->sname;?></h4>
                        </td>
                    </tr>
                </table>

                <table class="idcard-Table">
                    <tr>
                        <td>
                            <h4>
                                <?php
                                    echo img(base_url('uploads/images/'.$student->photo));
                                ?>
                            </h4>
                        </td>
                        <td class="row-style">
                            <h3><?php  echo $student->name; ?></h3>
                            <h5><?php  echo $this->lang->line("student_classes")." : ".$class->classes; ?>
                            </h5>
                            <h5><?php  echo $this->lang->line("student_section")." : ".$section->section; ?>
                            </h5>
                            <h5>
                                <?php  echo $this->lang->line("student_roll")." : ".$student->roll; ?>
                            </h5>
                        </td>
                    </tr>
                </table>
              </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" style="margin-bottom:0px;" onclick="javascript:closeWindow()" data-dismiss="modal"><?=$this->lang->line('close')?></button>
            <button type="button" class="btn btn-success" onclick="javascript:printDiv('idCardPrint')"><?=$this->lang->line('print')?></button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal content End here -->

<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('student/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">

                <?php
                    if(form_error('to'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php
                    if(form_error('subject'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php
                    if(form_error('message'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->
<?php 
function valueNullOrNot($value){
    return ($value!='') ? $value : 'NA';
}
?>

<script language="javascript" type="text/javascript">
    var rem_amount = '<?=($student->course_fee)-$totalInstalment?>';
    $(document).on('keyup','#installment_amount',function(){
        var amount = $(this).val();
        if(parseInt(amount)>parseInt(rem_amount)){
            $('#amount_err').html('Installment amount should not be exceeded the remaining amount ('+rem_amount+')');
            $('#add_inst_btn').attr('disabled',true);
            $('#add_inst_btn').removeClass('btn-success').addClass('btn-default');
        } else {
          $('#amount_err').html('');
          $('#add_inst_btn').attr('disabled',false); 
          $('#add_inst_btn').removeClass('btn-default').addClass('btn-success'); 
        }
    });
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
          "<html><head><title></title></head><body>" +
          divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }
    function closeWindow() {
        location.reload();
    }

    function check_email(email) {
        var status = false;
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }

    /*
    $("#send_pdf").click(function(){
        var to = $('#to').val();
        var subject = $('#subject').val();
        var message = $('#message').val();
        var id = "<?=$student->studentID;?>";
        var set = "<?=$set;?>";
        var set = "<?=$set;?>";
        var error = 0;

        if(to == "" || to == null) {
            error++;
            $("#to_error").html("");
            $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
        } else {
            if(check_email(to) == false) {
                error++
            }
        }

        if(subject == "" || subject == null) {
            error++;
            $("#subject_error").html("");
            $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
        } else {
            $("#subject_error").html("");
        }

        if(error == 0) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('student/send_mail')?>",
                data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message+ "&set=" + set,
                dataType: "html",
                success: function(data) {
                    location.reload();
                }
            });
        }
    });
    */
</script>


<script type="text/javascript">

$('#due_date').datepicker({ startView: 2 });
$(document).on('click', '#close-preview', function(){
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
           $('.content').css('padding-bottom', '100px');
        },
         function () {
           $('.image-preview').popover('hide');
           $('.content').css('padding-bottom', '20px');
        }
    );
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("<?=$this->lang->line('file_browse')?>");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function (){
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200,
            overflow:'hidden'
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("<?=$this->lang->line('file_browse')?>");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            $('.content').css('padding-bottom', '100px');
        }
        reader.readAsDataURL(file);
    });
});
var uTypeId = '<?=$userTypeId?>';
if( uTypeId==3){$('.icon-student').parent().parent().hide();}

</script>
