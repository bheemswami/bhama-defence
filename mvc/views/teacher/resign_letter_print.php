<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE> 
    <style>
        table tbody tr td{
            padding: 5px 10px !important;
        }
        font{font-size:14px !important;}
        body{font-family: arial}
    </style>
</HEAD>
<BODY LANG="en-US">
    <!-- <div style=" width: 60%; text-align: right;">
        <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('print_letter')"><span class="fa fa-print"></span> <?php //$this->lang->line('print')?> </button>
    </div> -->
    
<div style="margin: auto;width: 640px;height: 100%;background: #fff;padding: 35px;box-shadow: 0px 0px 4px #333;" id="print_letter">
<P STYLE="margin-bottom: 0.14in">
    <SPAN >
    
    <P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=5 STYLE="font-size: 20pt"><B>Resignation Letter</B></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B>Date:</B></FONT><FONT SIZE=2><B>
    <?=@date('d-m-Y',strtotime($teacher->resign_date))?></B></FONT></P>
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3><B></B></FONT><FONT SIZE=2><B>
    Mr. <?=@$teacher->name?></B></FONT></P>
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    
        <?=@$teacher->resign_reason?><BR/>
        
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
    </P>
    
    
    <P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=2>Signature: __________________________     </FONT></P>
</SPAN>
</P>
</div>
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        var divElements = document.getElementById(divID).innerHTML;
        var oldPage = document.body.innerHTML;
        document.body.innerHTML = "<html><head><title></title></head><body>" + divElements + "</body>";
        window.print();
        document.body.innerHTML = oldPage;
    }
  printDiv('print_letter');
</script>
</BODY>
</HTML>