
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-rss"></i> <?=$this->lang->line('panel_title')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("vendor/index")?>"><?=$this->lang->line('menu_vendor')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_vendor')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post">
                    <?php
                    if(form_error('name'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="name" class="col-sm-2 control-label">
                            <?=$this->lang->line("vendor_name")?>  <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name', $vendor->name)?>" required>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('name'); ?>
                        </span>
                    </div>
                    <?php
                    if(form_error('add_for'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("add_for")?>
                        </label>
                        <div class="col-sm-6">
                            <!-- <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address')?>" > -->
                            <select name="add_for" value="<?=set_value('name', $vendor->add_for)?>" id="add_for" class="form-control" required>
                                <option value="">--SELECT--</option>
                                <option <?=($vendor->add_for == 'Academic' ? 'selected' : '')?> value="Academic">Academic</option>
                                <option <?=($vendor->add_for == 'Hostel' ? 'selected' : '')?> value="Hostel">Hostel</option>
                            </select>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('add_for'); ?>
                        </span>
                    </div>
                    <?php
                    if(form_error('address'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("address")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('name', $vendor->address)?>" required>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('address'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('deal_in'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="phone" class="col-sm-2 control-label">
                            <?=$this->lang->line("deal_in")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="deal_in" name="deal_in" value="<?=set_value('name', $vendor->deal_in)?>" required>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('deal_in'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('other'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="contact_name" class="col-sm-2 control-label">
                            <?=$this->lang->line("other")?>
                        </label>    
                        <div class="col-sm-6">
                            <!-- <input type="text" class="form-control" id="other" name="other" value="<?=set_value('other')?>" > -->
                            <textarea class="form-control" id="other" name="other">
                            <?=set_value('name', $vendor->other)?>
                            </textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('other'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_vendor")?>" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
